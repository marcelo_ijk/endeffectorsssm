﻿using RootMotion.FinalIK;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class viewOrthoSyntheticEndEffectors : MonoBehaviour
{
    public GameObject EndEffectors;
    public GameObject Avatar;
    public Text LeftTibioFemuralAngleInfo;

    // The Cost function is defined based on the inner product representing the different joint angles
    // Tibio-femural angle
    // Carrying angle: long axis of the extended forearm and the long axis of the arm


    List<string> EndEffectorsList = new List<string>() {
        "CenterEyes_EndEffector",
        "LeftFoot_EndEffector",
        "RightFoot_EndEffector",
        "Bip002 Pelvis_EndEffector",
        "LeftHand_EndEffector",
        "RightHand_EndEffector" };

    Material lineMaterial;
    Vector3 startPoint = Vector3.zero;
    Vector3 endPoint = Vector3.one;
    float scale = 0.25f;

    List<Transform> LeftLegTrs = new List<Transform>();

    // Use this for initialization
    void Start()
    {
        CreateLineMaterial();

        Animator anim = Avatar.GetComponentInChildren<Animator>();


        Transform LeftFemur = anim.GetBoneTransform(HumanBodyBones.LeftUpperLeg);
        Transform LeftKnee = anim.GetBoneTransform(HumanBodyBones.LeftLowerLeg);
        Transform LeftFoot = anim.GetBoneTransform(HumanBodyBones.LeftFoot);

        LeftLegTrs.Add(LeftFoot);
        LeftLegTrs.Add(LeftKnee);
        LeftLegTrs.Add(LeftFemur);

        Debug.Log(LeftLegTrs[0].gameObject.name);
        Debug.Log(LeftLegTrs[1].gameObject.name);
        Debug.Log(LeftLegTrs[2].gameObject.name);

        
    }

    void ViewAnatomicalJointAngles()
    {
        // Left Leg

        Vector3 u = (LeftLegTrs[1].position - LeftLegTrs[0].position).normalized;
        Vector3 v = (LeftLegTrs[2].position - LeftLegTrs[1].position).normalized;
        

        startPoint = LeftLegTrs[0].position;

        GL.PushMatrix();
        //GL.MultMatrix(transform.localToWorldMatrix);
        //GL.MultMatrix(transform.worldToLocalMatrix);
        GL.Begin(GL.LINES);

        //endPoint = LeftLegTrs[1].localToWorldMatrix.MultiplyPoint(new Vector3(1f, 0f, 0f));
        endPoint = LeftLegTrs[1].position + u * 2f;
        GL.Color(Color.red);
        GL.Vertex(startPoint);
        GL.Vertex(endPoint);

        startPoint = LeftLegTrs[1].position;
        //endPoint = LeftLegTrs[2].localToWorldMatrix.MultiplyPoint(new Vector3(1f, 0f, 0f));
        endPoint = LeftLegTrs[2].position + v * 2f;
        GL.Color(Color.red);
        GL.Vertex(startPoint);
        GL.Vertex(endPoint);


        GL.End();
        GL.PopMatrix();


        
        float LeftTibioFemuralAngle = Mathf.Acos( Vector3.Dot(u, v)) * Mathf.Rad2Deg;
        //Debug.LogFormat("Left Tibio-Femural Angle: {0} [deg]", LeftTibioFemuralAngle);
        LeftTibioFemuralAngleInfo.text = string.Format("Left Tibio-Femural Angle:\n{0:F2} [deg]", LeftTibioFemuralAngle);

    }



    void CreateLineMaterial()
    {

        // Unity has a built-in shader that is useful for drawing simple colored things
        var shader = Shader.Find("Hidden/Internal-Colored");
        lineMaterial = new Material(shader);
        lineMaterial.hideFlags = HideFlags.HideAndDontSave;
        // Turn on alpha blending
        lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
        lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        // Turn backface culling off
        lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
        // Turn off depth writes
        lineMaterial.SetInt("_ZWrite", 0);
    }

    void OnRenderObject()
    {
        lineMaterial.SetPass(0);

        ViewAnatomicalJointAngles();

        // View end effectors

        List<Transform> eFs = EndEffectors.GetComponentsInChildren<Transform>().ToList();


        foreach (Transform Tr in eFs)
        {
            startPoint = Tr.transform.position;
            GL.PushMatrix();
            //GL.MultMatrix(transform.localToWorldMatrix);
            //GL.MultMatrix(transform.worldToLocalMatrix);
            GL.Begin(GL.LINES);

            endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(1f, 0f, 0f) * scale);
            GL.Color(Color.red);
            GL.Vertex(startPoint);
            GL.Vertex(endPoint);

            endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(0f, 1f, 0f) * scale);
            GL.Color(Color.green);
            GL.Vertex(startPoint);
            GL.Vertex(endPoint);

            endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(0f, 0f, 1f) * scale);
            GL.Color(Color.blue);
            GL.Vertex(startPoint);
            GL.Vertex(endPoint);

            GL.End();
            GL.PopMatrix();
        }

    }
}
