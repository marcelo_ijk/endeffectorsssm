﻿using RootMotion.FinalIK;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

using LightweightMatrixCSharp;

using Random = UnityEngine.Random;

using Artanim;
using UnityEngine.UI;

public class DefineEndEffectors : MonoBehaviour
{

    public GameObject Avatar;

    SSM_ANSUR SSM;
    ParticleSwarmAlgo PSO;
    public Text ShapeParameter;

    
    VRIK ik;
    bool _isCalibrated = false;
    List<GameObject> VirtualEndEffectorsList = new List<GameObject>();
    List<GameObject> PhysicalEndEffectorsList = new List<GameObject>();
    float _lastPressedTime;
    public bool _isOptimizing = false;

    //List<string> SyntheticEndEffectorsIDs = new List<string>() { "CenterEyes", "Bip002 L Foot", "Bip002 R Foot", "Bip002 Pelvis", "Bip002 L Hand", "Bip002 R Hand" };
    List<string> VirtualEndEffectorsIDs = new List<string>() { "CenterEyes", "LeftFoot", "RightFoot", "Bip002 Pelvis", "LeftHand", "RightHand" };
    List<string> PhysicalEndEffectorsIDs = new List<string>() { "Oculus_Yellow", "Foot_L_Yellow", "Foot_R_Yellow", "Pelvis_Yellow", "Hand_L_Yellow", "Hand_R_Yellow" };

    float EvaluateCostFunc()
    {        
        float rmse = 0f;

        //int[] id = new int[5] { 0, 1, 2, 4, 5 };
        int[] id = new int[3] { 0, 4, 5 };
        for (int i = 0; i < id.Length; ++i)
        {
            Transform TrVirtual = Avatar.GetComponentsInChildren<Transform>().ToList().Where(obj => obj.transform.name == VirtualEndEffectorsIDs[id[i]]).SingleOrDefault();
            Transform TrPhysical = GameObject.Find(PhysicalEndEffectorsIDs[id[i]]).transform;                
                
            rmse += (TrVirtual.position - TrPhysical.position).magnitude;
        }
        rmse /= (float)id.Length;

        return rmse;
    }

    // Use this for initialization
    void Start()
    {
        Avatar = GameObject.Find("DummyIK");
        ik = Avatar.GetComponentInChildren<VRIK>();

        _lastPressedTime = Time.realtimeSinceStartup;


        this.gameObject.AddComponent<SSM_ANSUR>();
        SSM = this.gameObject.GetComponent<SSM_ANSUR>();        
        SSM.SetAvatarModel(Avatar);
        SSM.ShapeParameter = ShapeParameter;
        //SSM.StartCoroutineSSM();

        
        PSO = new ParticleSwarmAlgo(25,3);        
        PSO.EvaluateCostFunc = EvaluateCostFunc;
        //PSO.StatisticalShapeModel = SSM.SSM_SetShapeParameter;

        //ParticleSwarmAlgo.StatisticalShapeModel = SSM.SSM_SetShapeParameter;


        //PSO.StatisticalShapeModel = new SSM.testTux();
        //PSO.StatisticalShapeModel<Matrix a, Matrix B> = new SSM_ANSUR().testTux;




        //PSO = this.gameObject.GetComponents<Swarm>();
        //this.gameObject.AddComponent<Swarm>();


    }

    void DefiningPhysicalEndEffectors()
    {
        if (PhysicalEndEffectorsList.Count > 0)
        {
            foreach (var go in PhysicalEndEffectorsList)
            {
                Destroy(go);
            }
            PhysicalEndEffectorsList.Clear();
        }

        List<Transform> Trs = GameObject.Find("ViconDataStreaming").GetComponentsInChildren<Transform>().ToList();

        foreach (var ef in PhysicalEndEffectorsIDs)
        {
            Transform Tr = Trs.Where(obj => obj.transform.name == ef).SingleOrDefault();

            GameObject EndEffector = new GameObject() as GameObject;
            EndEffector.name = ef + "_EndEffector";
            EndEffector.transform.position = Tr.position;
            EndEffector.transform.rotation = Tr.rotation;
            EndEffector.transform.parent = this.transform;

            GameObject Marker = GameObject.CreatePrimitive(PrimitiveType.Cube) as GameObject;
            Marker.name = "PhysicalMarker";
            Marker.transform.parent = EndEffector.transform;
            Marker.transform.localPosition = Vector3.zero;
            Marker.transform.localRotation = Quaternion.identity;
            //Marker.transform.localScale = new Vector3(0.08f, 0.04f, 0.01f);

            if (PhysicalEndEffectorsIDs.IndexOf(ef) == 0)
            {
                Marker.transform.localScale = new Vector3(0.15f, 0.08f, 0.01f);
            }
            else
            {
                Marker.transform.localScale = new Vector3(0.08f, 0.01f, 0.04f);
            }
                       


            Marker.GetComponent<Renderer>().material.color = Color.red;
            PhysicalEndEffectorsList.Add(EndEffector);
        }

        //ik.references.root.localScale = Vector3.one * 1f;
    }


    void ScalingAvatar()
    {

        //float SyntheticEyeHeight = Vector3.Magnitude(SyntheticEndEffectorsList[0].transform.position - (SyntheticEndEffectorsList[1].transform.position + SyntheticEndEffectorsList[2].transform.position) / 2f);
        //float PhysicalEyeHeight = Vector3.Magnitude(PhysicalEndEffectorsList[0].transform.position - (PhysicalEndEffectorsList[1].transform.position + PhysicalEndEffectorsList[2].transform.position) / 2f);

        float SyntheticEyeHeight = VirtualEndEffectorsList[0].transform.position.y;
        float PhysicalEyeHeight = PhysicalEndEffectorsList[0].transform.position.y;


        float sizeF = PhysicalEyeHeight / SyntheticEyeHeight;
        ik.references.root.localScale = Vector3.one * sizeF;

        Debug.LogFormat("Synthetic eye-height: {0} -- Physical eye-height: {1}", SyntheticEyeHeight, PhysicalEyeHeight);




    }

    void RegisterSyntheticToPhysicalEndEffectors()
    {

        // Mapping the virtual to the physical markers
        for (int id = 0; id < VirtualEndEffectorsList.Count(); ++id)
        {
            VirtualEndEffectorsList[id].transform.rotation = Quaternion.Inverse(PhysicalEndEffectorsList[0].transform.rotation) * VirtualEndEffectorsList[id].transform.localRotation;
            VirtualEndEffectorsList[id].transform.parent = PhysicalEndEffectorsList[id].transform;
            VirtualEndEffectorsList[id].transform.localPosition = Vector3.zero;
        }


        // "CenterEyes", "Bip002 L Foot", "Bip002 R Foot", "Bip002 Pelvis", "Bip002 L Hand", "Bip002 R Hand"
        ik.solver.spine.headTarget = VirtualEndEffectorsList[0].transform;

        ik.solver.leftLeg.target = VirtualEndEffectorsList[1].transform;
        ik.solver.leftLeg.rotationWeight = 1;
        ik.solver.leftLeg.positionWeight = 1f;

        ik.solver.rightLeg.target = VirtualEndEffectorsList[2].transform;
        ik.solver.rightLeg.rotationWeight = 1;
        ik.solver.rightLeg.positionWeight = 1f;

        ik.solver.spine.pelvisTarget = VirtualEndEffectorsList[3].transform;

        ik.solver.leftArm.target = VirtualEndEffectorsList[4].transform;
        ik.solver.leftArm.rotationWeight = 1;
        ik.solver.leftArm.positionWeight = 1;

        ik.solver.rightArm.target = VirtualEndEffectorsList[5].transform;
        ik.solver.rightArm.rotationWeight = 1;
        ik.solver.rightArm.positionWeight = 1;

        //float sizeF = PhysicalEndEffectorsList[0].transform.position.y / ik.references.head.position.y;
        //ik.references.root.localScale = Vector3.one * sizeF;


        ik.enabled = true;
        ik.solver.solveSpine = true;
        ik.solver.solveLegs = true;
        ik.solver.solveArms = true;



    }

    void RegisterSyntheticToPhysicalEndEffectors2()
    {
        //ScalingAvatar();

        int NumberOfLandMarks = 6;// EndEffectorsViconTrs.Count;

        IntPtr X = Eigen.Create(NumberOfLandMarks, 3);
        IntPtr Y = Eigen.Create(NumberOfLandMarks, 3);
        IntPtr Cov = Eigen.Create(3, 3);
        IntPtr RT = Eigen.Create(4, 4);
        IntPtr matU = Eigen.Create(3, 3);
        IntPtr matV = Eigen.Create(3, 3);
        IntPtr matSingVals = Eigen.Create(3, 3);


        for (int i = 0; i < NumberOfLandMarks; ++i)
        {
            Eigen.SetValue(X, i, 0, VirtualEndEffectorsList[i].transform.position.x);
            Eigen.SetValue(X, i, 1, VirtualEndEffectorsList[i].transform.position.y);
            Eigen.SetValue(X, i, 2, VirtualEndEffectorsList[i].transform.position.z);

            Eigen.SetValue(Y, i, 0, PhysicalEndEffectorsList[i].transform.position.x);
            Eigen.SetValue(Y, i, 1, PhysicalEndEffectorsList[i].transform.position.y);
            Eigen.SetValue(Y, i, 2, PhysicalEndEffectorsList[i].transform.position.z);

            // Correspondences
            Debug.LogFormat("{0} --- {1}", VirtualEndEffectorsList[i].name, PhysicalEndEffectorsList[i].name);
        }

        Eigen.CovarianceMat(X, Y, Cov, matU, matV, matSingVals, RT);
        Debug.Log("Cov:\n" + PrintEigenLibMat(Cov));
        Debug.Log("MatU:\n" + PrintEigenLibMat(matU));
        Debug.Log("MatV:\n" + PrintEigenLibMat(matV));
        Debug.Log("Singular Values:\n" + PrintEigenLibMat(matSingVals));
        Debug.Log("Rigid registration:\n" + PrintEigenLibMat(RT));


        Matrix4x4 RigidTransf = new Matrix4x4();
        for (int i = 0; i < Eigen.GetRows(RT); ++i)
        {
            for (int j = 0; j < Eigen.GetCols(RT); ++j)
            {
                RigidTransf[i, j] = Eigen.GetValue(RT, i, j);
            }
        }

        Vector3 pos, lscale;
        Quaternion rot;
        Artanim.QuaternionLib.DecomposeMatrix(ref RigidTransf, out pos, out rot, out lscale);


        // Mapping the virtual to the physical markers
        for (int id = 0; id < VirtualEndEffectorsList.Count(); ++id)
        {
            VirtualEndEffectorsList[id].transform.rotation = Quaternion.Inverse(PhysicalEndEffectorsList[0].transform.rotation) * VirtualEndEffectorsList[id].transform.localRotation;
            VirtualEndEffectorsList[id].transform.parent = PhysicalEndEffectorsList[id].transform;
            VirtualEndEffectorsList[id].transform.localPosition = Vector3.zero;
        }



        //VirtualEndEffectorsList[1].transform.localPosition = TargetTr.worldToLocalMatrix.MultiplyPoint(PhysicalEndEffectorsList[1].transform.position);



        /*
        Avatar.transform.rotation = Avatar.transform.rotation * Quaternion.Inverse(PhysicalEndEffectorsList[0].transform.rotation);        
        Vector3 delta = PhysicalEndEffectorsList[0].transform.position - VirtualEndEffectorsList[0].transform.position;
        Avatar.transform.position = new Vector3(5f,-1f,3f);
        */

        //Avatar.transform.position += RigidTransf.MultiplyPoint(Avatar.transform.position - VirtualEndEffectorsList[0].transform.position);
        //Avatar.transform.rotation = Avatar.transform.rotation * rot;

        //Avatar.transform.rotation = Quaternion.Inverse(PhysicalEndEffectorsList[0].transform.rotation) * Avatar.transform.rotation * Quaternion.Inverse(PhysicalEndEffectorsList[0].transform.rotation);
        //Avatar.transform.rotation = Avatar.transform.rotation * Quaternion.Inverse(PhysicalEndEffectorsList[0].transform.rotation);

        /*
        for (int id = 0; id < PhysicalEndEffectorsList.Count; ++id)
        {            
            VirtualEndEffectorsList[id].transform.rotation = VirtualEndEffectorsList[id].transform.rotation * Quaternion.Inverse(PhysicalEndEffectorsList[0].transform.rotation);
            VirtualEndEffectorsList[id].transform.position = PhysicalEndEffectorsList[id].transform.position;

            VirtualEndEffectorsList[id].transform.parent = PhysicalEndEffectorsList[id].transform;
        }
        */


        // "CenterEyes", "Bip002 L Foot", "Bip002 R Foot", "Bip002 Pelvis", "Bip002 L Hand", "Bip002 R Hand"
        ik.solver.spine.headTarget = VirtualEndEffectorsList[0].transform;

        ik.solver.leftLeg.target = VirtualEndEffectorsList[1].transform;
        ik.solver.leftLeg.rotationWeight = 1;
        ik.solver.leftLeg.positionWeight = 1f;

        ik.solver.rightLeg.target = VirtualEndEffectorsList[2].transform;
        ik.solver.rightLeg.rotationWeight = 1;
        ik.solver.rightLeg.positionWeight = 1f;

        ik.solver.spine.pelvisTarget = VirtualEndEffectorsList[3].transform;

        ik.solver.leftArm.target = VirtualEndEffectorsList[4].transform;
        ik.solver.leftArm.rotationWeight = 1;
        ik.solver.leftArm.positionWeight = 1;

        ik.solver.rightArm.target = VirtualEndEffectorsList[5].transform;
        ik.solver.rightArm.rotationWeight = 1;
        ik.solver.rightArm.positionWeight = 1;

        //float sizeF = PhysicalEndEffectorsList[0].transform.position.y / ik.references.head.position.y;
        //ik.references.root.localScale = Vector3.one * sizeF;


        ik.enabled = true;

        ik.solver.solveSpine = true;
        ik.solver.solveLegs = true;
        ik.solver.solveArms = true;


    }

    void CheckingHowClose()
    {
        foreach (var ef in VirtualEndEffectorsIDs)
        {
            Transform TargetTr = Avatar.GetComponentsInChildren<Transform>().ToList().Where(obj => obj.name == ef).SingleOrDefault();
            //TargetTr.gameObject.GetComponentInChildren<Renderer>().enabled = true;

            Debug.LogFormat("{0} dist(Vicon,Syhntetic) = {1:F6}", ef, Vector3.Magnitude(TargetTr.position - PhysicalEndEffectorsList[VirtualEndEffectorsIDs.IndexOf(ef)].transform.position));
        }
    }

    void TuxTest()
    {

        for (int id = 1; id < VirtualEndEffectorsIDs.Count; ++id)
        {

            if (!VirtualEndEffectorsIDs[id].Contains("Pelvis"))
            {

                Transform TargetTr = Avatar.GetComponentsInChildren<Transform>().ToList().Where(obj => obj.name == VirtualEndEffectorsIDs[id]).SingleOrDefault();
                TargetTr.gameObject.GetComponentInChildren<Renderer>().enabled = true;

                Vector3 delta =
                    VirtualEndEffectorsList[id].transform.worldToLocalMatrix.MultiplyPoint(TargetTr.position) -
                VirtualEndEffectorsList[id].transform.worldToLocalMatrix.MultiplyPoint(PhysicalEndEffectorsList[id].transform.position);


                Matrix4x4 saco = VirtualEndEffectorsList[id].transform.localToWorldMatrix;
                saco.m03 = 0f;
                saco.m13 = 0f;
                saco.m23 = 0f;


                //Vector3 delta = PhysicalEndEffectorsList[id].transform.worldToLocalMatrix.MultiplyPoint(TargetTr.position);
                VirtualEndEffectorsList[id].transform.position -= saco.MultiplyPoint3x4(delta);
            }

        }

        


        //***********************

        /*
        Transform TargetTr = Avatar.GetComponentsInChildren<Transform>().ToList().Where(obj => obj.name == "LeftFoot").SingleOrDefault();
        TargetTr.gameObject.GetComponentInChildren<Renderer>().enabled = true;

        Vector3 delta =
            VirtualEndEffectorsList[1].transform.worldToLocalMatrix.MultiplyPoint(TargetTr.position) -
        VirtualEndEffectorsList[1].transform.worldToLocalMatrix.MultiplyPoint(PhysicalEndEffectorsList[1].transform.position);


        Matrix4x4 saco = VirtualEndEffectorsList[1].transform.localToWorldMatrix;
        saco.m03 = 0f;
        saco.m13 = 0f;
        saco.m23 = 0f;


        //Vector3 delta = PhysicalEndEffectorsList[1].transform.worldToLocalMatrix.MultiplyPoint(TargetTr.position);
        VirtualEndEffectorsList[1].transform.position -= saco.MultiplyPoint3x4( delta);
        */

    }

    

    string PrintEigenLibMat(IntPtr mat)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < Eigen.GetRows(mat); ++i)
        {
            for (int j = 0; j < Eigen.GetCols(mat); ++j)
            {
                sb.AppendFormat("{0:F6}\t", Eigen.GetValue(mat, i, j));
            }
            sb.Append(Environment.NewLine);
        }

        return sb.ToString();
    }

    void DefiningSyntheticEndEffectors()
    {
        if (VirtualEndEffectorsList.Count > 0)
        {
            foreach (var go in VirtualEndEffectorsList)
            {
                Destroy(go);
            }
            VirtualEndEffectorsList.Clear();
        }

        List<Transform> Trs = Avatar.GetComponentsInChildren<Transform>().ToList();

        foreach (var ef in VirtualEndEffectorsIDs)
        {
            Transform Tr = Trs.Where(obj => obj.transform.name == ef).SingleOrDefault();

            GameObject EndEffector = new GameObject() as GameObject;
            EndEffector.name = ef + "_EndEffector";
            EndEffector.transform.position = Tr.position;
            EndEffector.transform.rotation = Tr.rotation;
            EndEffector.transform.parent = this.transform;


            GameObject Marker = GameObject.CreatePrimitive(PrimitiveType.Cube) as GameObject;
            Marker.name = "SyntheticMarker";
            Marker.transform.parent = EndEffector.transform;
            Marker.transform.localPosition = Vector3.zero;
            Marker.transform.localRotation = Quaternion.identity;
            if (VirtualEndEffectorsIDs.IndexOf(ef) == 0)
            {
                Marker.transform.localScale = new Vector3(0.08f, 0.01f, 0.15f);
            }
            else if (VirtualEndEffectorsIDs.IndexOf(ef) == 1 || VirtualEndEffectorsIDs.IndexOf(ef) == 2)
            {
                Marker.transform.localScale = new Vector3(0.05f, 0.01f, 0.08f);
            }
            else if (VirtualEndEffectorsIDs.IndexOf(ef) == 4 || VirtualEndEffectorsIDs.IndexOf(ef) == 5)
            {
                Marker.transform.localScale = new Vector3(0.04f, 0.01f, 0.06f);
            }
            else
            {
                Marker.transform.localScale = new Vector3(0.08f, 0.04f, 0.02f);
            }
            Marker.GetComponent<Renderer>().material.color = Color.blue;

            VirtualEndEffectorsList.Add(EndEffector);
        }
        Debug.Log("Synthetic end-effectors list defined");
    }

    // Update is called once per frame
    void Update()
    {
        

        if (_isOptimizing)
        {

            // Statistical Shape Model Fun
            Matrix b = Matrix.ZeroMatrix(54, 1);
            //b[0, 0] = (double)Random.Range(-0.05f, 0.05f);
            //b[1, 0] = (double)Random.Range(-0.05f, 0.05f);
            //b[2, 0] = (double)Random.Range(-0.05f, 0.05f);
            //b[3, 0] = (double)Random.Range(-0.05f, 0.05f);
            //b[4, 0] = (double)Random.Range(-0.05f, 0.05f);

            
            PSO.Optimize();
            for (int dim = 0; dim < PSO.dim; ++dim)
            {
                b[dim, 0] = PSO.GetOptimalSolution()[dim, 0];
            }

            if (PSO._isGBestUpdated)
            {
                SSM.SSM_SetShapeParameter(b);

                // { "Oculus_Yellow", "Foot_L_Yellow", "Foot_R_Yellow", "Pelvis_Yellow", "Hand_L_Yellow", "Hand_R_Yellow" };

                /*
                ik.solver.spine.headTarget = PhysicalEndEffectorsList[0].transform;

                ik.solver.leftLeg.target = PhysicalEndEffectorsList[1].transform;
                ik.solver.rightLeg.target = PhysicalEndEffectorsList[2].transform;

                ik.solver.leftArm.target = PhysicalEndEffectorsList[4].transform;
                ik.solver.rightArm.target = PhysicalEndEffectorsList[5].transform;
                */

                ik.solver.FixTransforms();
                ik.solver.Update();
            }


            /*
            Transform TargetTr = Avatar.GetComponentsInChildren<Transform>().ToList().Where(obj => obj.name == "LeftFoot").SingleOrDefault();

            float _rmse = Vector3.Magnitude(TargetTr.position - PhysicalEndEffectorsList[1].transform.position);
            Debug.LogFormat("RMSE [{0}]: {1:F6}", 0, _rmse);
            */

            //SyntheticEndEffectorsList[1].transform.position += new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f));
            //CheckingHowClose();


            // Updating the IK solvers in a specific order.
            //foreach (IK component in components) component.GetIKSolver().Update();
            //ik.GetIKSolver().Update();


            //Debug.Log("IsValid:" + ik.GetIKSolver().IsValid());
            //ik.solver.FixTransforms()




            //head.Rotate(headRotate);

            //ik.solver.Update();

        }


        if (Input.GetKey(KeyCode.UpArrow))
        {

            if (Time.realtimeSinceStartup > _lastPressedTime + 1f)
            {
                DefiningSyntheticEndEffectors();
                DefiningPhysicalEndEffectors();
                _isCalibrated = true;
                _lastPressedTime = Time.realtimeSinceStartup;
            }
        }


        if (Input.GetKey(KeyCode.DownArrow))
        {
            if (Time.realtimeSinceStartup > _lastPressedTime + 1f)
            {
                _lastPressedTime = Time.realtimeSinceStartup;
                RegisterSyntheticToPhysicalEndEffectors();
            }
        }


        if (Input.GetKey(KeyCode.LeftArrow))
        {
            if (Time.realtimeSinceStartup > _lastPressedTime + 1f)
            {
                _lastPressedTime = Time.realtimeSinceStartup;
                TuxTest();
            }
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            if (Time.realtimeSinceStartup > _lastPressedTime + 1f)
            {
                _lastPressedTime = Time.realtimeSinceStartup;

                _isOptimizing = !_isOptimizing;
                Debug.LogFormat("PSO: {0}", _isOptimizing);

                /*
                Matrix b = Matrix.ZeroMatrix(54, 1);
                PSO.StatisticalShapeModel(b);
                //CheckingHowClose();

                //_isOptimizing = true;
                SSM.StartCoroutineSSM();
                */
            }
        }
    }
}
