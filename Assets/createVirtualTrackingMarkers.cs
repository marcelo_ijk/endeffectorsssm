﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

using Artanim;
using RootMotion.FinalIK;
using RootMotion.Demos;

public class createVirtualTrackingMarkers : MonoBehaviour
{

    public GameObject Avatar;
    float LastKeyPressedTime;

    private List<string> VirtualMarkers = new List<string>() {
        "CenterEyes",
        "Bip002 Pelvis",
        "Bip002 L Foot",
        "Bip002 R Foot",
        "Bip002 L Hand",
        "Bip002 R Hand"};

    // Vicon Markers
    List<string> PhysicalMarkers = new List<string>() {
            "Oculus_Yellow",
            "Pelvis_Yellow",
            "Foot_L_Yellow",
            "Foot_R_Yellow",
            "Hand_L_Yellow",
            "Hand_R_Yellow"};



    private List<GameObject> VirtualMarkersList = new List<GameObject>();
    private List<GameObject> PhysicalMarkersList = new List<GameObject>();


    // Use this for initialization
    void Start()
    {
        LastKeyPressedTime = Time.realtimeSinceStartup;
    }


    void ScalingAvatar()
    {
        // Reseting correspondences
        if (VirtualMarkersList.Count() > 0)
        {
            foreach (var go in VirtualMarkersList)
                Destroy(go);
        }
        VirtualMarkersList.Clear();


        // Virtual Markers        
        List<Transform> Trs = Avatar.GetComponentsInChildren<Transform>().ToList();

        foreach (var bp in VirtualMarkers)
        {
            Transform Tr = Trs.Where(obj => obj.transform.name == bp).SingleOrDefault();
            GameObject VirtualTrackingMarker = Instantiate(Tr.gameObject, Tr.position, Tr.rotation);
            VirtualTrackingMarker.name = Tr.gameObject.name + "VirtualMarker";


            if (VirtualTrackingMarker.GetComponentsInChildren<Transform>().Count() > 1)
            {
                for (int id = 1; id < Tr.GetComponentsInChildren<Transform>().Count(); ++id)
                {
                    Destroy(VirtualTrackingMarker.GetComponentsInChildren<Transform>()[id].gameObject);
                }
            }

            GameObject Marker = GameObject.CreatePrimitive(PrimitiveType.Cube) as GameObject;
            Marker.name = "Marker";
            Marker.transform.parent = VirtualTrackingMarker.transform;
            Marker.transform.localPosition = Vector3.zero;
            Marker.transform.localRotation = Quaternion.identity;
            Marker.transform.localScale = new Vector3(0.08f, 0.04f, 0.01f);
            Marker.GetComponent<Renderer>().material.color = Color.blue;

            VirtualMarkersList.Add(VirtualTrackingMarker);
        }


        // Physical markers        
        foreach (var PhysM in PhysicalMarkers)
        {
            if (GameObject.Find(PhysM).scene.IsValid())
            {
                if (!PhysicalMarkersList.Contains(GameObject.Find(PhysM)))
                {
                    PhysicalMarkersList.Add(GameObject.Find(PhysM));
                }
                else
                {
                    PhysicalMarkersList[PhysicalMarkers.IndexOf(PhysM)].transform.position = GameObject.Find(PhysM).transform.position;
                    PhysicalMarkersList[PhysicalMarkers.IndexOf(PhysM)].transform.rotation = GameObject.Find(PhysM).transform.rotation;

                }
            }
        }


        // Scaling the avatar based on the user's eye-height
        float PhysEyeHeight = Vector3.Magnitude(PhysicalMarkersList[0].transform.position - (PhysicalMarkersList[1].transform.position + PhysicalMarkersList[2].transform.position) / 2f);
        float VirtEyeHeight = Vector3.Magnitude(VirtualMarkersList[0].transform.position - (VirtualMarkersList[1].transform.position + VirtualMarkersList[2].transform.position) / 2f);
        Debug.LogFormat("Virual Eye-Height: {0}  ---  Physical Eye-Height: {1}", VirtEyeHeight, PhysEyeHeight);

        VRIK ik = Avatar.GetComponentInChildren<VRIK>();
        float sizeF = PhysEyeHeight / VirtEyeHeight;
        ik.references.root.localScale = Vector3.one * sizeF;

        PhysEyeHeight = Vector3.Magnitude(PhysicalMarkersList[0].transform.position - (PhysicalMarkersList[1].transform.position + PhysicalMarkersList[2].transform.position) / 2f);
        VirtEyeHeight = Vector3.Magnitude(VirtualMarkersList[0].transform.position - (VirtualMarkersList[1].transform.position + VirtualMarkersList[2].transform.position) / 2f);
        Debug.LogFormat("Virual Eye-Height: {0}  ---  Physical Eye-Height: {1}", VirtEyeHeight, PhysEyeHeight);

    }

    void RegisterVirtualToPhysicalMarkers()
    {
        ScalingAvatar();

        // Reseting correspondences
        if (VirtualMarkersList.Count() > 0)
        {
            foreach (var go in VirtualMarkersList)
                Destroy(go);
        }
        VirtualMarkersList.Clear();


        // Virtual Markers        
        List<Transform> Trs = Avatar.GetComponentsInChildren<Transform>().ToList();

        foreach (var bp in VirtualMarkers)
        {
            Transform Tr = Trs.Where(obj => obj.transform.name == bp).SingleOrDefault();
            GameObject VirtualTrackingMarker = Instantiate(Tr.gameObject, Tr.position, Tr.rotation);
            VirtualTrackingMarker.name = Tr.gameObject.name + "VirtualMarker";


            if (VirtualTrackingMarker.GetComponentsInChildren<Transform>().Count() > 1)
            {
                for (int id = 1; id < Tr.GetComponentsInChildren<Transform>().Count(); ++id)
                {
                    Destroy(VirtualTrackingMarker.GetComponentsInChildren<Transform>()[id].gameObject);
                }
            }

            GameObject Marker = GameObject.CreatePrimitive(PrimitiveType.Cube) as GameObject;
            Marker.name = "Marker";
            Marker.transform.parent = VirtualTrackingMarker.transform;
            Marker.transform.localPosition = Vector3.zero;
            Marker.transform.localRotation = Quaternion.identity;
            Marker.transform.localScale = new Vector3(0.08f, 0.04f, 0.01f);
            Marker.GetComponent<Renderer>().material.color = Color.blue;

            VirtualMarkersList.Add(VirtualTrackingMarker);
        }


        // Physical markers        
        foreach (var PhysM in PhysicalMarkers)
        {
            if (GameObject.Find(PhysM).scene.IsValid())
            {
                if (!PhysicalMarkersList.Contains(GameObject.Find(PhysM)))
                {
                    PhysicalMarkersList.Add(GameObject.Find(PhysM));
                }
                else
                {
                    PhysicalMarkersList[PhysicalMarkers.IndexOf(PhysM)].transform.position = GameObject.Find(PhysM).transform.position;
                    PhysicalMarkersList[PhysicalMarkers.IndexOf(PhysM)].transform.rotation = GameObject.Find(PhysM).transform.rotation;

                }
            }
        }

        // Center eyes, left-and right feet
        int NumberOfLandMarks = 3;// EndEffectorsViconTrs.Count;

        IntPtr X = Eigen.Create(NumberOfLandMarks, 3);
        IntPtr Y = Eigen.Create(NumberOfLandMarks, 3);
        IntPtr Cov = Eigen.Create(3, 3);
        IntPtr RT = Eigen.Create(4, 4);
        IntPtr matU = Eigen.Create(3, 3);
        IntPtr matV = Eigen.Create(3, 3);
        IntPtr matSingVals = Eigen.Create(3, 3);


        for (int i = 0; i < NumberOfLandMarks; ++i)
        {
            Eigen.SetValue(X, i, 0, VirtualMarkersList[i].transform.position.x);
            Eigen.SetValue(X, i, 1, VirtualMarkersList[i].transform.position.y);
            Eigen.SetValue(X, i, 2, VirtualMarkersList[i].transform.position.z);

            Eigen.SetValue(Y, i, 0, PhysicalMarkersList[i].transform.position.x);
            Eigen.SetValue(Y, i, 1, PhysicalMarkersList[i].transform.position.y);
            Eigen.SetValue(Y, i, 2, PhysicalMarkersList[i].transform.position.z);

            // Correspondences
            Debug.LogFormat("{0} --- {1}", VirtualMarkersList[i].name, PhysicalMarkersList[i].name);
        }


        Eigen.CovarianceMat(X, Y, Cov, matU, matV, matSingVals, RT);
        Debug.Log("Cov:\n" + PrintEigenLibMat(Cov));
        Debug.Log("MatU:\n" + PrintEigenLibMat(matU));
        Debug.Log("MatV:\n" + PrintEigenLibMat(matV));
        Debug.Log("Singular Values:\n" + PrintEigenLibMat(matSingVals));
        Debug.Log("Rigid registration:\n" + PrintEigenLibMat(RT));


        Matrix4x4 RigidTransf = new Matrix4x4();
        for (int i = 0; i < Eigen.GetRows(RT); ++i)
        {
            for (int j = 0; j < Eigen.GetCols(RT); ++j)
            {
                RigidTransf[i, j] = Eigen.GetValue(RT, i, j);
            }
        }

        Vector3 pos, lscale;
        Quaternion rot;
        QuaternionLib.DecomposeMatrix(ref RigidTransf, out pos, out rot, out lscale);

        // Mapping the virtual to the physical markers
        for (int id = 0; id < VirtualMarkersList.Count(); ++id)
        {
            VirtualMarkersList[id].transform.parent = PhysicalMarkersList[id].transform;
            VirtualMarkersList[id].transform.position = RigidTransf.MultiplyPoint(VirtualMarkersList[id].transform.position);
            //VirtualMarkersList[id].transform.position = PhysicalMarkersList[id].transform.position;
            VirtualMarkersList[id].transform.rotation = VirtualMarkersList[id].transform.rotation * rot;
        }

           
        /*
        VRIK ik = Avatar.GetComponentInChildren<VRIK>();
        ik.solver.spine.headTarget = VirtualMarkersList[0].transform;
        ik.solver.leftLeg.target = VirtualMarkersList[1].transform;
        ik.solver.rightLeg.target = VirtualMarkersList[2].transform;
        ik.solver.leftArm.target = VirtualMarkersList[3].transform;
        ik.solver.rightArm.target = VirtualMarkersList[4].transform;
        ik.solver.spine.pelvisTarget = VirtualMarkersList[5].transform;
        */

        
        VRIKCalibrationController CalibCtrl = GameObject.Find("VRIK Calibration Controller").GetComponentInChildren<VRIKCalibrationController>();

        /*
        CalibCtrl.settings.headTrackerForward = new Vector3(0f, 1f, 0f);
        CalibCtrl.settings.headTrackerUp = new Vector3(-1f, 0f, 0f);

        CalibCtrl.settings.bodyTrackerForward = new Vector3(0f, 1f, 0f);
        CalibCtrl.settings.bodyTrackerUp = new Vector3(-1f, 0f, 0f);

        CalibCtrl.settings.handTrackerForward = new Vector3(0f, -1f, 0f);
        CalibCtrl.settings.handTrackerUp = new Vector3(1f, 0f, 0f);

        CalibCtrl.settings.footTrackerForward = new Vector3(0f, 1f, 0f);
        CalibCtrl.settings.footTrackerUp = new Vector3(1f, 0f, 0f);
        */


        CalibCtrl.headTracker = VirtualMarkersList[0].transform;
        CalibCtrl.leftFootTracker = VirtualMarkersList[1].transform;
        CalibCtrl.rightFootTracker = VirtualMarkersList[2].transform;
        CalibCtrl.leftHandTracker = VirtualMarkersList[3].transform;
        CalibCtrl.rightHandTracker = VirtualMarkersList[4].transform;
        CalibCtrl.bodyTracker = VirtualMarkersList[5].transform;




    }



    void TestRigidRegistration()
    {
        int NumberOfLandMarks = 4;


        IntPtr X = Eigen.Create(NumberOfLandMarks, 3);
        IntPtr Y = Eigen.Create(NumberOfLandMarks, 3);
        IntPtr Cov = Eigen.Create(3, 3);
        IntPtr RT = Eigen.Create(4, 4);
        IntPtr matU = Eigen.Create(3, 3);
        IntPtr matV = Eigen.Create(3, 3);
        IntPtr matSingVals = Eigen.Create(3, 3);

        if (VirtualMarkersList.Count() > 0)
        {
            foreach (var go in VirtualMarkersList)
                Destroy(go);
        }
        VirtualMarkersList.Clear();

        if (PhysicalMarkersList.Count() > 0)
        {
            foreach (var go in PhysicalMarkersList)
                Destroy(go);
        }
        PhysicalMarkersList.Clear();

        for (int i = 0; i < NumberOfLandMarks; ++i)
        {
            GameObject Virtual = GameObject.CreatePrimitive(PrimitiveType.Cube) as GameObject;
            Virtual.transform.localScale = Vector3.one / 10f;
            //Virtual.transform.localScale = new Vector3(0.08f, 0.04f, 0.01f);
            Virtual.GetComponent<Renderer>().material.color = Color.green;
            //float delta = (i > 0) ? 0 : 2;
            float delta = 2f;

            Virtual.transform.position = new Vector3(UnityEngine.Random.Range(-delta, delta), UnityEngine.Random.Range(-delta, delta), UnityEngine.Random.Range(-delta, delta));
            VirtualMarkersList.Add(Virtual);

            Eigen.SetValue(X, i, 0, Virtual.transform.position.x);
            Eigen.SetValue(X, i, 1, Virtual.transform.position.y);
            Eigen.SetValue(X, i, 2, Virtual.transform.position.z);
        }

        Quaternion R = Quaternion.Euler(45f, 20f, 75f);
        Vector3 T = new Vector3(3f, 1f, -2f);
        Matrix4x4 _RT = Matrix4x4.TRS(T, R, Vector3.one);

        for (int i = 0; i < NumberOfLandMarks; ++i)
        {
            GameObject Physical = GameObject.CreatePrimitive(PrimitiveType.Cube) as GameObject;
            Physical.transform.localScale = Vector3.one / 10f;
            //Physical.transform.localScale = new Vector3(0.08f, 0.04f, 0.01f);
            Physical.GetComponent<Renderer>().material.color = Color.green;

            Physical.transform.position = _RT.MultiplyVector(VirtualMarkersList[i].transform.position);
            Physical.transform.rotation = Physical.transform.localRotation * R;
            Physical.GetComponent<Renderer>().material.color = Color.red;
            PhysicalMarkersList.Add(Physical);

            Eigen.SetValue(Y, i, 0, Physical.transform.position.x);
            Eigen.SetValue(Y, i, 1, Physical.transform.position.y);
            Eigen.SetValue(Y, i, 2, Physical.transform.position.z);
        }




        Eigen.CovarianceMat(X, Y, Cov, matU, matV, matSingVals, RT);
        Debug.Log("Cov:\n" + PrintEigenLibMat(Cov));
        Debug.Log("MatU:\n" + PrintEigenLibMat(matU));
        Debug.Log("MatV:\n" + PrintEigenLibMat(matV));
        Debug.Log("Singular Values:\n" + PrintEigenLibMat(matSingVals));
        Debug.Log("Rigid registration:\n" + PrintEigenLibMat(RT));


        Matrix4x4 RigidTransf = new Matrix4x4();
        for (int i = 0; i < Eigen.GetRows(RT); ++i)
        {
            for (int j = 0; j < Eigen.GetCols(RT); ++j)
            {
                RigidTransf[i, j] = Eigen.GetValue(RT, i, j);
            }
        }

        Vector3 pos, lscale;
        Quaternion rot;
        QuaternionLib.DecomposeMatrix(ref RigidTransf, out pos, out rot, out lscale);

        for (int i = 0; i < NumberOfLandMarks; ++i)
        {
            VirtualMarkersList[i].transform.position = RigidTransf.MultiplyPoint(VirtualMarkersList[i].transform.position);
            VirtualMarkersList[i].transform.rotation = VirtualMarkersList[i].transform.rotation * rot;
        }


        Debug.LogFormat("Applied:\n{0}\nComputed:\n{0}", _RT.ToString(), RigidTransf.ToString());

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            if (Time.realtimeSinceStartup > LastKeyPressedTime + 2f)
            {
                RegisterVirtualToPhysicalMarkers();

                LastKeyPressedTime = Time.realtimeSinceStartup;
            }
        }


        if (Input.GetKey(KeyCode.DownArrow))
        {
            if (Time.realtimeSinceStartup > LastKeyPressedTime + 2f)
            {
                TestRigidRegistration();

                LastKeyPressedTime = Time.realtimeSinceStartup;
            }
        }
    }

    string PrintEigenLibMat(IntPtr mat)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < Eigen.GetRows(mat); ++i)
        {
            for (int j = 0; j < Eigen.GetCols(mat); ++j)
            {
                sb.AppendFormat("{0:F6}\t", Eigen.GetValue(mat, i, j));
            }
            sb.Append(Environment.NewLine);
        }

        return sb.ToString();
    }
}
