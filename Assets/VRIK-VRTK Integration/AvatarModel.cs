﻿using System.Collections;
using UnityEngine;
using RootMotion.FinalIK;

public class AvatarModel : MonoBehaviour {

	[Tooltip("The Transform representing the position and rotation of the 'CenterEyeAnchor'[Oculus] or 'Camera (head)'[SteamVR] relative to this model.")]
	public Transform centerEyeAnchor;

	[Tooltip("The Transform representing the position and rotation of the 'LeftHandAnchor'[Oculus] or 'Controller (left)'[SteamVR] relative to this model.")]
	public Transform leftHandAnchor;

	[Tooltip("The Transform representing the position and rotation of the 'RightHandAnchor'[Oculus] or 'Controller (right)'[SteamVR] relative to this model.")]
	public Transform rightHandAnchor;
 
    public Vector3 headLocalPosition { get; private set; }
    public Vector3 leftHandLocalPosition { get; private set; }
    public Vector3 rightHandLocalPosition { get; private set; }
    public Quaternion headLocalRotation { get; private set; }
    public Quaternion leftHandLocalRotation { get; private set; }
    public Quaternion rightHandLocalRotation { get; private set; }

    private VRIK ik;

    void Awake() {
    	ik = GetComponent<VRIK>();
		Animator animator = GetComponent<Animator>();

		// Find the head and hand bones
		Transform head = animator.GetBoneTransform(HumanBodyBones.Head);
		Transform leftHand = animator.GetBoneTransform(HumanBodyBones.LeftHand);
		Transform rightHand = animator.GetBoneTransform(HumanBodyBones.RightHand);

		// Positions of the bones relative to their anchors
		headLocalPosition = centerEyeAnchor.InverseTransformPoint(head.position);
		leftHandLocalPosition = leftHandAnchor.InverseTransformPoint(leftHand.position);
		rightHandLocalPosition = rightHandAnchor.InverseTransformPoint(rightHand.position);

		// Rotations of the bones relative to their anchors
	 	headLocalRotation = Quaternion.Inverse(centerEyeAnchor.rotation) * head.rotation;
	 	leftHandLocalRotation = Quaternion.Inverse(leftHandAnchor.rotation) * leftHand.rotation;
	 	rightHandLocalRotation = Quaternion.Inverse(rightHandAnchor.rotation) * rightHand.rotation;

	 	// We have calculated all the localPositions/Rotations, so we don't need those gameobjects anymore.
	 	Destroy(centerEyeAnchor.parent.gameObject);
	}

	/// <summary>
	/// Creates the IK targets for VRIK based on the localPositions/Rotations calculated above, parents those IK targets to the proxies.
	/// Proxies (remote) are the Transforms that are synced to their respective VR controllers (local) in a networked game.
	/// </summary>
	public void CreateIKTargets(Transform centerEyeProxy, Transform leftHandProxy, Transform rightHandProxy) {
		// Build and assign VRIK targets
        ik.solver.spine.headTarget = new GameObject("VRIK Head Target").transform;
        ik.solver.spine.headTarget.parent = centerEyeProxy;
        ik.solver.spine.headTarget.localPosition = headLocalPosition;
        ik.solver.spine.headTarget.localRotation = headLocalRotation;
 
        ik.solver.leftArm.target = new GameObject("VRIK Left Hand Target").transform;
        ik.solver.leftArm.target.parent = leftHandProxy;
        ik.solver.leftArm.target.localPosition = leftHandLocalPosition;
        ik.solver.leftArm.target.localRotation = leftHandLocalRotation;
 
        ik.solver.rightArm.target = new GameObject("VRIK Right Hand Target").transform;
        ik.solver.rightArm.target.parent = rightHandProxy;
        ik.solver.rightArm.target.localPosition = rightHandLocalPosition;
        ik.solver.rightArm.target.localRotation = rightHandLocalRotation;
	}
}
