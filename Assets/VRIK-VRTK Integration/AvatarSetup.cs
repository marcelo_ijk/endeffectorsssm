﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AvatarSetup : MonoBehaviour {

	[Tooltip("The avatar model to set up for VRIK.")]
	public AvatarModel model;

	[Tooltip("The Transfrom that is sycned to the 'CenterEyeProxy'[Oculus] or 'Camera (head)'[SteamVR] in a networked game.")]
	public Transform centerEyeProxy;

	[Tooltip("The Transfrom that is sycned to the 'LeftHandProxy'[Oculus] or 'Controller (left)'[SteamVR] in a networked game.")]
	public Transform leftHandProxy;

	[Tooltip("The Transfrom that is sycned to the 'RightHandProxy'[Oculus] or 'Controller (right)'[SteamVR] in a networked game.")]
	public Transform rightHandProxy;

	void Start() {
		model.CreateIKTargets(centerEyeProxy, leftHandProxy, rightHandProxy);
	}
}
