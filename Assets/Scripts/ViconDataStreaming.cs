﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using ViconDataStreamSDK.CSharp;


//using CSML;
using LightweightMatrixCSharp;
using Artanim;

using RootMotion.FinalIK;
using RootMotion.Demos;
using System.Linq;
using System.Text;

public class ViconDataStreaming : MonoBehaviour
{



    public String HostName = "localhost";
    public String Port = "801";

    public double Offset = 0.0;
    public float Height = 0;
    float LastKeyPressedTime = 0f;


    List<string> PlayersList = new List<string>() { "Blue" };
    List<string> ViconBodyPartsList = new List<string>() { "Oculus_", "Pelvis_", "Hand_L_", "Hand_R_", "Foot_L_", "Foot_R_" };
    List<string> AvatarBodyPartsList = new List<string>() { "Bip002 Head", "Bip002 Pelvis", "Bip002 L Hand", "Bip002 R Hand", "Bip002 L Foot", "Bip002 R Foot" };
    List<Transform> AvatarTransfList = new List<Transform>();
    Dictionary<string, Transform> InitialPosesDict = new Dictionary<string, Transform>();
    public GameObject Avatar;
    public GameObject Pilot;
    float AvatarScaleFactor = 1f;
    List<GameObject> EndEffectorsList = new List<GameObject>();

    List<Transform> ListViconTrsBodyParts = new List<Transform>();
    List<Transform> ListViconTrsEndEffectors = new List<Transform>();

    Dictionary<string, Quaternion> InitialPosesCalibration = new Dictionary<string, Quaternion>();
    List<Transform> ViconTransformsList = new List<Transform>();
    List<Transform> QdList = new List<Transform>();


    private Dictionary<string, Transform> rbMapping = new Dictionary<string, Transform>();
    static float ViconToUnityScale = 1e-3f;


    VRIKCalibrationController CalibCtrl;
    VRIKCalibrator.Settings settings;

    private ViconDataStreamSDK.CSharp.Client m_Client;
    //private GameObject m_CameraObject;
    public enum Bool
    {
        False = 0,
        True
    }

    private static class OVRPluginServices
    {
        public const string OvrPluginName = "OVRPlugin";
        [DllImport(OvrPluginName, CallingConvention = CallingConvention.Cdecl)]
        public static extern Bool ovrp_SetTrackingPositionEnabled(Bool value);
    }



    bool SetupViconClient()
    {
        //m_Client = new ViconDataStreamSDK.CSharp.RetimingClient();
        //m_Client.SetAxisMapping(Direction.Forward, Direction.Up, Direction.Right);

        m_Client = new ViconDataStreamSDK.CSharp.Client();
        //m_Client.SetAxisMapping(Direction.Left, Direction.Up, Direction.Forward);
        m_Client.SetAxisMapping(Direction.Forward, Direction.Up, Direction.Right);

        String Host = String.Concat(HostName, ":", Port);
        print("Connecting to " + Host + "...");
        Int32 Attempt = 0;
        while (!m_Client.IsConnected().Connected)
        {
            // Direct connection
            Output_Connect ConnectOutput = m_Client.Connect(Host);
            print("Connect result: " + ConnectOutput.Result);

            Attempt += 1;
            if (Attempt == 3)
                break;
            System.Threading.Thread.Sleep(200);
        }
        m_Client.EnableSegmentData();

        return true;
    }





    // Use this for initialization
    void Start()
    {

        SetupViconClient();


        /*
        Transform[] Trs = Avatar.GetComponentsInChildren<Transform>();

        foreach (var bp in AvatarBodyPartsList)
        {
            foreach (Transform Tr in Trs)
            {
                if (Tr.gameObject.name == bp)
                {
                    QdList.Add(Tr);
                    //Debug.LogFormat("{0} {1}", Tr.gameObject.name, Tr.rotation);
                    break;
                }
            }
        }
        */

        /*

        Avatar.AddComponent<VRIKCalibrationController>();
        CalibCtrl = Avatar.GetComponent<VRIKCalibrationController>();
        

                

        settings = new VRIKCalibrator.Settings();
        CalibCtrl.settings = settings;
        
        CalibCtrl.settings.headTrackerForward = new Vector3(0f, 0f, 1f);
        CalibCtrl.settings.headTrackerUp = new Vector3(0f, 1f, 0f);

        CalibCtrl.settings.bodyTrackerForward = new Vector3(0f, -1f, 0f);
        CalibCtrl.settings.bodyTrackerUp = new Vector3(0f, 0f, -1f);

        CalibCtrl.settings.handTrackerForward = new Vector3(0f, 0f, 1f);
        CalibCtrl.settings.handTrackerUp = new Vector3(0f, 1f, 0f);

        CalibCtrl.settings.footTrackerForward = new Vector3(0f, 0f, 1f);
        CalibCtrl.settings.footTrackerUp = new Vector3(0f, 1f, 0f);

        //CalibCtrl.settings.headOffset = new Vector3(0f, -0.15f, -0.15f);
        //CalibCtrl.settings.handOffset = new Vector3(0f, -0.03f, -0.07f);
        */
    }


    void GetInitialTransform()
    {

        /*
        //List<string> ViconBodyPartsList = new List<string>() { "Oculus_", "Pelvis_", "Hand_L_", "Hand_R_", "Foot_L_", "Foot_R_" };
        //List<string> AvatarBodyPartsList = new List<string>() { "Bip002 Head", "Bip002 Pelvis", "Bip002 L Hand", "Bip002 R Hand", "Bip002 L Foot", "Bip002 R Foot" };

        List<Transform> ViconRBs = new List<Transform>();
        List<Transform> AvatarRBs = new List<Transform>();

        Transform[] TrsAvatar = Avatar.GetComponentsInChildren<Transform>();
        foreach (var rb in AvatarBodyPartsList)
        {
            Transform Tr = TrsAvatar.ToList().Where(obj => obj.name == rb).SingleOrDefault();
            if (Tr != null)
            {
                AvatarRBs.Add(Tr);
                //Debug.Log(Tr.gameObject.name);
            }
            
        }


        foreach (string rb in ViconBodyPartsList)
        {
            Transform Tr = GameObject.Find(rb + "Blue").transform;
            if (Tr != null)
            {
                ViconRBs.Add(Tr);
            }
        }

        for (int id = 0; id < AvatarRBs.Count; ++id)
        {
            AvatarRBs[id].parent = ViconRBs[id];
            AvatarRBs[id].localPosition = Vector3.zero;
        }

        */




        if (InitialPosesCalibration.Count > 0) InitialPosesCalibration.Clear();


        foreach (var player in PlayersList)
        {
            //foreach (var bp in ViconBodyPartsList)
            for (int id = 0; id < ViconBodyPartsList.Count; ++id)
            {
                string rbName = ViconBodyPartsList[id] + player;
                Transform Tr = GameObject.Find(rbName).transform;

                if (Tr != null)
                {
                    Quaternion Lambda = Quaternion.identity;
                    if (rbName.Contains("Foot"))
                    {
                        //Lambda = Quaternion.Inverse(Tr.rotation) * QdList[id].rotation;// * Quaternion.Euler(-90f, 0f, 0f) * Quaternion.Euler(0f, 0f, -90f);
                        Lambda = Quaternion.Inverse(Tr.rotation) * QdList[id].rotation;
                    }
                    else if (rbName.Contains("Pelvis"))
                    {
                        Lambda = Quaternion.Inverse(Tr.rotation) * QdList[id].rotation;// * Quaternion.Euler(0f, 90f, 0f) * Quaternion.Euler(0f, 0f, 180f);
                    }
                    else if (rbName.Contains("Oculus"))
                    {
                        Lambda = Quaternion.Inverse(Tr.rotation) * QdList[id].rotation;// * Quaternion.Euler(0f, 90f, 0f) * Quaternion.Euler(-90f, 0f, 0f);
                    }
                    else if (rbName.Contains("Hand"))
                    {
                        Lambda = Quaternion.Inverse(Tr.rotation) * QdList[id].rotation;// * Quaternion.Euler(0f, -90f, 0f) * Quaternion.Euler(0f, 0f, -180f);
                    }


                    InitialPosesCalibration.Add(rbName, Lambda);

                    Debug.LogFormat("Tr: {0}\n{1}", Tr.gameObject.name, Tr.localToWorldMatrix);
                }
                else
                {
                    id = 0;
                }
            }
        }



        /*
        
        // Vicon Objects        
        foreach (var rb in ViconBodyPartsList)
        {
            Transform Tr = GameObject.Find(rb + "Blue").transform;
            if (Tr != null)
            {
                ViconTransformsList.Add(Tr);
            }

        }

        CalibCtrl.headTracker = ViconTransformsList[0];
        CalibCtrl.bodyTracker = ViconTransformsList[1];
        CalibCtrl.leftHandTracker = ViconTransformsList[2];
        CalibCtrl.rightHandTracker = ViconTransformsList[3];
        CalibCtrl.leftFootTracker = ViconTransformsList[4];
        CalibCtrl.rightFootTracker = ViconTransformsList[5];
                     
                

        VRIKCalibrator.Calibrate(CalibCtrl.ik, CalibCtrl.settings, ViconTransformsList[0], ViconTransformsList[1], ViconTransformsList[2], ViconTransformsList[3], ViconTransformsList[4], ViconTransformsList[5]);
        */
    }


    void DefineEndEffector3()
    {
        List<string> AvatarRBsList = new List<string>() { "Bip002 Head", "CenterEyes", "Bip002 Pelvis", "Bip002 L Hand", "Bip002 R Hand", "Bip002 L Foot", "Bip002 R Foot" };
        List<Transform> AvatarTrs = Avatar.GetComponentsInChildren<Transform>().ToList();

        foreach (var bp in AvatarRBsList)
        {
            Transform Tr = AvatarTrs.Where(obj => obj.transform.name == bp).SingleOrDefault();
            AvatarTransfList.Add(Tr);
        }

    }


    void DefineEndEffectorsTux()
    {
        VRIK ik = Avatar.GetComponentInChildren<VRIK>();

        // EndEffectors Idealization


        List<string> EndEffectorIdeal = new List<string>() {
            "Tracker Mock (CenterEyeAnchor)",
            "Tracker Mock (Left Foot)",
            "Tracker Mock (Right Foot)",
            "Tracker Mock (Body)",
            "Tracker Mock (Left Hand)",
            "Tracker Mock (Right Hand)" };
            

        /*
        List<string> EndEffectorIdeal = new List<string>() {
            "CenterEyes",
            "Bip002 L Foot",
            "Bip002 R Foot",
            "Bip002 Pelvis",
            "Bip002 L Hand",
            "Bip002 R Hand"};
            */
        
        // Vicon Markers
        List<string> ViconBodyPartsList = new List<string>() {
            "Oculus_Yellow",
            "Foot_L_Yellow",
            "Foot_R_Yellow",
            "Pelvis_Yellow",
            "Hand_L_Yellow",
            "Hand_R_Yellow"};

        List<Transform> EndEffectorsViconTrs = new List<Transform>();
        foreach (var rbName in ViconBodyPartsList)
        {
            EndEffectorsViconTrs.Add(GameObject.Find(rbName).transform);
        }


        List<Transform> EndEffectorsTrs = Pilot.GetComponentsInChildren<Transform>().ToList();
        //List<Transform> EndEffectorsTrs = Avatar.GetComponentsInChildren<Transform>().ToList();
        foreach (var ef in EndEffectorIdeal)
        {
            Transform Tr = EndEffectorsTrs.Where(obj => obj.transform.name == ef).SingleOrDefault();

            GameObject EndEffector = Instantiate(Tr.gameObject, Tr.position, Tr.rotation);
            EndEffector.name = Tr.gameObject.name;
            EndEffector.GetComponentsInChildren<Transform>()[1].gameObject.GetComponent<Renderer>().material.color = Color.blue;
            //Debug.LogFormat("{0}", Tr.gameObject.name);
            EndEffectorsList.Add(EndEffector);
        }


        // Center eyes, left-and right feet
        int NumberOfLandMarks = 3;// EndEffectorsViconTrs.Count;

        IntPtr X = Eigen.Create(NumberOfLandMarks, 3);
        IntPtr Y = Eigen.Create(NumberOfLandMarks, 3);
        IntPtr Cov = Eigen.Create(3, 3);
        IntPtr RT = Eigen.Create(4, 4);
        IntPtr matU = Eigen.Create(3, 3);
        IntPtr matV = Eigen.Create(3, 3);
        IntPtr matSingVals = Eigen.Create(3, 3);


        for (int i = 0; i < NumberOfLandMarks; ++i)
        {
            Eigen.SetValue(X, i, 0, EndEffectorsList[i].transform.position.x);
            Eigen.SetValue(X, i, 1, EndEffectorsList[i].transform.position.y);
            Eigen.SetValue(X, i, 2, EndEffectorsList[i].transform.position.z);

            Eigen.SetValue(Y, i, 0, EndEffectorsViconTrs[i].transform.position.x);
            Eigen.SetValue(Y, i, 1, EndEffectorsViconTrs[i].transform.position.y);
            Eigen.SetValue(Y, i, 2, EndEffectorsViconTrs[i].transform.position.z);

            // Correspondences
            Debug.LogFormat("{0} - {1}", EndEffectorsList[i].gameObject.name, EndEffectorsViconTrs[i].gameObject.name);
        }


        Eigen.CovarianceMat(X, Y, Cov, matU, matV, matSingVals, RT);
        Debug.Log("Cov:\n" + PrintEigenLibMat(Cov));
        Debug.Log("MatU:\n" + PrintEigenLibMat(matU));
        Debug.Log("MatV:\n" + PrintEigenLibMat(matV));
        Debug.Log("Singular Values:\n" + PrintEigenLibMat(matSingVals));
        Debug.Log("Rigid registration:\n" + PrintEigenLibMat(RT));


        Matrix4x4 RigidTransf = new Matrix4x4();
        for (int i = 0; i < Eigen.GetRows(RT); ++i)
        {
            for (int j = 0; j < Eigen.GetCols(RT); ++j)
            {
                RigidTransf[i, j] = Eigen.GetValue(RT, i, j);
            }
        }

        Vector3 pos, lscale;
        Quaternion rot;
        QuaternionLib.DecomposeMatrix(ref RigidTransf, out pos, out rot, out lscale);
        //Debug.LogFormat("Pos: {0:F6}, {1:F6}, {2:F6}", pos.x, pos.y, pos.z );


        Vector3 AvatarPos = Avatar.GetComponentsInChildren<Transform>()[1].position;
        //Avatar.GetComponentsInChildren<Transform>()[1].position = RigidTransf.MultiplyPoint(AvatarPos);
        Avatar.transform.root.rotation = Avatar.transform.rotation * rot;


        for (int i = 0; i < EndEffectorsList.Count; ++i)
        {            

            if (EndEffectorsList[i].name.Contains("CenterEyeAnchor"))
            {
                EndEffectorsList[i].transform.parent = GameObject.Find(ViconBodyPartsList[0]).transform;
            }
            else if (EndEffectorsList[i].name.Contains("Left Foot"))
            {
                EndEffectorsList[i].transform.parent = GameObject.Find(ViconBodyPartsList[1]).transform;
            }
            else if (EndEffectorsList[i].name.Contains("Right Foot"))
            {
                EndEffectorsList[i].transform.parent = GameObject.Find(ViconBodyPartsList[2]).transform;
            }
            else if (EndEffectorsList[i].name.Contains("Body"))
            {
                EndEffectorsList[i].transform.parent = GameObject.Find(ViconBodyPartsList[3]).transform;
            }
            else if (EndEffectorsList[i].name.Contains("Left Hand"))
            {
                EndEffectorsList[i].transform.parent = GameObject.Find(ViconBodyPartsList[4]).transform;
            }
            else if (EndEffectorsList[i].name.Contains("Right Hand"))
            {
                EndEffectorsList[i].transform.parent = GameObject.Find(ViconBodyPartsList[5]).transform;
            }


            //EndEffectorsList[i].transform.position = RigidTransf.MultiplyPoint(EndEffectorsList[i].transform.position);
            EndEffectorsList[i].transform.position = EndEffectorsViconTrs[i].position;
            EndEffectorsList[i].transform.rotation = EndEffectorsList[i].transform.rotation * rot;
        }


        VRIKCalibrationController _ctrl = GameObject.Find("VRIK Calibration Controller").GetComponentInChildren<VRIKCalibrationController>();
        
        _ctrl.headTracker = EndEffectorsList[0].transform;
        _ctrl.leftFootTracker = EndEffectorsList[1].transform;
        _ctrl.rightFootTracker = EndEffectorsList[2].transform;
        _ctrl.bodyTracker = EndEffectorsList[3].transform;
        _ctrl.leftHandTracker = EndEffectorsList[4].transform;
        _ctrl.rightHandTracker = EndEffectorsList[5].transform;
        

        /*

        _ctrl.settings.headTrackerForward = new Vector3(0f, 1f, 0f);
        _ctrl.settings.headTrackerUp = new Vector3(0f, 1f, 0f);

        _ctrl.settings.footTrackerForward = new Vector3(0f, 0f, -1f);
        _ctrl.settings.footTrackerUp = new Vector3(0f, 1f, 0f);

        _ctrl.settings.bodyTrackerForward = new Vector3(0f, 0f, -1f);
        _ctrl.settings.bodyTrackerUp = new Vector3(0f, 1f, 0f);

        _ctrl.settings.handTrackerForward = new Vector3(0f, 0f, -1f);
        _ctrl.settings.handTrackerUp = new Vector3(0f, 1f, 0f);

        */

        _ctrl.Calibrate();


        //ik.solver.leftArm.rotationWeight = 1f;
        ik.solver.solveSpine = true;
        ik.solver.solveLegs = true;
        ik.solver.solveArms = true;













        /*
        List<Transform> AvatarTrs = Avatar.GetComponentsInChildren<Transform>().ToList();



        for (int id = 0; id < AvatarBodyPartsList.Count(); ++id)
        {
            string bpAvatar = AvatarBodyPartsList[id];
            string bpVicon = ViconBodyPartsList[id] + "Blue";

            Transform Tr = AvatarTrs.Where(obj => obj.transform.name == bpAvatar).SingleOrDefault();

            GameObject EndEffector = new GameObject() as GameObject;
            EndEffector.name = bpAvatar + "_EndEffector";

            GameObject marker = GameObject.CreatePrimitive(PrimitiveType.Cube) as GameObject;
            marker.name = "Marker";
            marker.transform.parent = EndEffector.transform;
            marker.transform.localScale = new Vector3(0.08f, 0.04f, 0.02f);
            marker.transform.localPosition = Vector3.zero;
            marker.transform.localRotation = Quaternion.identity;

            if (bpAvatar.Contains("Head"))
            {
                EndEffector.transform.parent = rbMapping["Oculus_Blue"].transform;
                EndEffector.transform.localPosition = Vector3.zero;
                EndEffector.transform.localRotation = Tr.localRotation;
            }
            else if (bpAvatar.Contains("Pelvis"))
            {
                EndEffector.transform.parent = rbMapping["Pelvis_Blue"].transform;
                EndEffector.transform.localPosition = Vector3.zero;
                EndEffector.transform.localRotation = Tr.localRotation;
            }
            else if (bpAvatar.Contains("L Hand"))
            {
                EndEffector.transform.parent = rbMapping["Hand_L_Blue"].transform;
                EndEffector.transform.localPosition = Vector3.zero;
                EndEffector.transform.localRotation = Tr.localRotation;
            }
            else if (bpAvatar.Contains("R Hand"))
            {
                EndEffector.transform.parent = rbMapping["Hand_R_Blue"].transform;
                EndEffector.transform.localPosition = Vector3.zero;
                EndEffector.transform.localRotation = Tr.localRotation;
            }
            else if (bpAvatar.Contains("L Foot"))
            {
                EndEffector.transform.parent = rbMapping["Foot_L_Blue"].transform;
                EndEffector.transform.localPosition = Vector3.zero;
                EndEffector.transform.localRotation = Tr.localRotation;
            }
            else if (bpAvatar.Contains("R Foot"))
            {
                EndEffector.transform.parent = rbMapping["Foot_R_Blue"].transform;
                EndEffector.transform.localPosition = Vector3.zero;
                EndEffector.transform.localRotation = Tr.localRotation;
            }


            EndEffectorsList.Add(EndEffector);
        }

        */

        /*
        List<Transform> Trs = Pilot.GetComponentsInChildren<Transform>().ToList();
        List<GameObject> EndEffectorsList = new List<GameObject>();
        */
        /*
        foreach (Transform Tr in Trs)
        {
            if (Tr.name.Contains("Mock"))
            {
                GameObject EndEffector = Instantiate(Tr.gameObject);


                if (Tr.gameObject.name.Contains())

                Debug.LogFormat("{0}", Tr.gameObject.name);
                EndEffectorsList.Add(EndEffector);
            }
        }
        */
    }

    string PrintEigenLibMat(IntPtr mat)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < Eigen.GetRows(mat); ++i)
        {
            for (int j = 0; j < Eigen.GetCols(mat); ++j)
            {
                sb.AppendFormat("{0:F6}\t", Eigen.GetValue(mat, i, j));
            }
            sb.Append(Environment.NewLine);
        }

        return sb.ToString();
    }

    void DefineEndEffectors2()
    {

        VRIK ik = Avatar.GetComponentInChildren<VRIK>();
        List<Transform> AvatarTrs = Avatar.GetComponentsInChildren<Transform>().ToList();

        // Registering Oculus
        AvatarTrs.Where(obj => obj.transform.name == "BipDummy").SingleOrDefault().position = GameObject.Find("Pelvis_Blue").transform.position;
        AvatarTrs.Where(obj => obj.transform.name == "BipDummy").SingleOrDefault().rotation = GameObject.Find("Pelvis_Blue").transform.rotation * Quaternion.Euler(-90f, 0f, 0f) * Quaternion.Euler(0f, 0f, -90f);




        for (int id = 0; id < AvatarBodyPartsList.Count(); ++id)
        {
            string bpAvatar = AvatarBodyPartsList[id];
            string bpVicon = ViconBodyPartsList[id] + "Blue";

            Transform Tr = AvatarTrs.Where(obj => obj.transform.name == bpAvatar).SingleOrDefault();

            GameObject EndEffector = Instantiate(Tr.gameObject);
            EndEffector.transform.DetachChildren();
            EndEffector.name = Tr.gameObject.name + "_EndEffector";
            EndEffector.transform.parent = GameObject.Find(bpVicon).transform;
            EndEffector.transform.localPosition = Vector3.zero;
            EndEffector.transform.rotation = Tr.rotation;


            InitialPosesDict.Add(bpVicon, EndEffector.transform);



            Debug.LogFormat("id: {0} | {1} | {2}", id, bpVicon, EndEffector.name);


            if (id == 0)
            {
                ik.solver.spine.headTarget = EndEffector.transform;
            }
            else if (id == 1)
            {
                ik.solver.spine.pelvisTarget = EndEffector.transform;
            }
            else if (id == 2)
            {
                ik.solver.leftArm.target = EndEffector.transform;
            }
            else if (id == 3)
            {
                ik.solver.rightArm.target = EndEffector.transform;
            }
            else if (id == 4)
            {
                ik.solver.leftLeg.target = EndEffector.transform;
            }
            else if (id == 5)
            {
                ik.solver.rightLeg.target = EndEffector.transform;
            }
        }

        ik.references.root.localScale /= AvatarScaleFactor;


        Transform TrCenterEyes = AvatarTrs.Where(obj => obj.transform.name == "CenterEyes").SingleOrDefault();
        Transform TrFoot = AvatarTrs.Where(obj => obj.transform.name == "Bip002 L Foot").SingleOrDefault();

        // Avatar Scaling
        float PlayerEyeHeightIdle = (GameObject.Find("Oculus_Blue").transform.position.y);

        AvatarScaleFactor = TrCenterEyes.position.y / PlayerEyeHeightIdle;
        ik.references.root.localScale *= AvatarScaleFactor;

        Debug.LogFormat("PlayerEyeHeightIdle: {0}  | Avatar height: {1}", PlayerEyeHeightIdle, TrCenterEyes.position.y - TrFoot.position.y);



        /*
        Vector3 targetForward = targetRot * Vector3.forward;
        Vector3 targetUp = targetRot * Vector3.up;
        */



        //ik.solver.spine.headTarget =


        /*

        List<string> Players = new List<string>() { "Blue" };
        foreach (var bp in ViconBodyPartsList)
        {
            string rbName = bp + Players[0];
            if (GameObject.Find(rbName))
            {
                Transform Tr = GameObject.Find(bp + Players[0]).transform;
                InitialPosesDict.Add(rbName, Tr);
                Debug.Log(rbName);
            }
        }
        */

        /*

        GameObject.Find("Tracker Mock (Right Foot)").transform.parent = rbMapping["Foot_R_Blue"].transform;
        GameObject.Find("Tracker Mock (Right Foot)").transform.localPosition = Vector3.zero;
        GameObject.Find("Tracker Mock (Right Foot)").transform.localRotation = GameObject.Find("Tracker Mock (Right Foot)").transform.localRotation;

        GameObject.Find("Tracker Mock (Left Foot)").transform.parent = rbMapping["Foot_L_Blue"].transform;
        GameObject.Find("Tracker Mock (Left Foot)").transform.localPosition = Vector3.zero;
        GameObject.Find("Tracker Mock (Left Foot)").transform.localRotation = GameObject.Find("Tracker Mock (Left Foot)").transform.localRotation;


        GameObject.Find("Tracker Mock (Body)").transform.parent = rbMapping["Pelvis_Blue"].transform;
        GameObject.Find("Tracker Mock (Body)").transform.localPosition = Vector3.zero;
        GameObject.Find("Tracker Mock (Body)").transform.localRotation = GameObject.Find("Tracker Mock (Body)").transform.localRotation;

        GameObject.Find("Tracker Mock (CenterEyeAnchor)").transform.parent = rbMapping["Oculus_Blue"].transform;
        GameObject.Find("Tracker Mock (CenterEyeAnchor)").transform.localPosition = Vector3.zero;
        GameObject.Find("Tracker Mock (CenterEyeAnchor)").transform.localRotation = GameObject.Find("Tracker Mock (CenterEyeAnchor)").transform.localRotation;


        GameObject.Find("Tracker Mock (Left Hand)").transform.parent = rbMapping["Hand_L_Blue"].transform;
        GameObject.Find("Tracker Mock (Left Hand)").transform.localPosition = Vector3.zero;
        GameObject.Find("Tracker Mock (Left Hand)").transform.localRotation = GameObject.Find("Tracker Mock (Left Hand)").transform.localRotation;

        GameObject.Find("Tracker Mock (Right Hand)").transform.parent = rbMapping["Hand_R_Blue"].transform;
        GameObject.Find("Tracker Mock (Right Hand)").transform.localPosition = Vector3.zero;
        GameObject.Find("Tracker Mock (Right Hand)").transform.localRotation = GameObject.Find("Tracker Mock (Right Hand)").transform.localRotation;

        */


        //List<string> AvatarBodyPartsList = new List<string>() { "Bip002 Head", "Bip002 Pelvis", "Bip002 L Hand", "Bip002 R Hand", "Bip002 L Foot", "Bip002 R Foot" };

        //List<Transform> AvatarTrs = Avatar.GetComponentsInChildren<Transform>().ToList();

        //Transform Tr = AvatarTrs.Where(obj => obj.transform.name == "CenterEyes").SingleOrDefault();









        /*
        List<GameObject> EndEffectorsGOs = new List<GameObject>();
        for (int id = 0; id < AvatarBodyPartsList.Count; ++id)
        {
            string rbName = AvatarBodyPartsList[id];

            Transform Tr = AvatarTrs.Where(obj => obj.transform.name == rbName).SingleOrDefault();

            GameObject EndEffector = new GameObject() as GameObject;
            EndEffector.name = AvatarBodyPartsList[id] + "_EndEffector";

            if (id > 0)
            {
                EndEffector.transform.parent = EndEffectorsGOs[0].transform;
            }
            EndEffector.transform.position = Tr.position;
            EndEffector.transform.rotation = Tr.rotation;

            EndEffector.transform.parent = GameObject.Find(ViconBodyPartsList[id] + "Blue").transform;
            EndEffector.transform.localPosition = Vector3.zero;

            EndEffectorsGOs.Add(EndEffector);


        }
        */





        /*
       
        GameObject.Find("Tracker Mock (Right Foot)").transform.parent = rbMapping["Foot_R_Blue"].transform;
        GameObject.Find("Tracker Mock (Right Foot)").transform.localPosition = Vector3.zero;

        GameObject.Find("Tracker Mock (Left Foot)").transform.parent = rbMapping["Foot_L_Blue"].transform;
        GameObject.Find("Tracker Mock (Left Foot)").transform.localPosition = Vector3.zero;



        GameObject.Find("Tracker Mock (Body)").transform.parent = rbMapping["Pelvis_Blue"].transform;
        GameObject.Find("Tracker Mock (Body)").transform.localPosition = Vector3.zero;

        GameObject.Find("Tracker Mock (CenterEyeAnchor)").transform.parent = rbMapping["Oculus_Blue"].transform;
        GameObject.Find("Tracker Mock (CenterEyeAnchor)").transform.localPosition = Vector3.zero;        


        GameObject.Find("Tracker Mock (Left Hand)").transform.parent = rbMapping["Hand_L_Blue"].transform;
        GameObject.Find("Tracker Mock (Left Hand)").transform.localPosition = Vector3.zero;

        GameObject.Find("Tracker Mock (Right Hand)").transform.parent = rbMapping["Hand_R_Blue"].transform;
        GameObject.Find("Tracker Mock (Right Hand)").transform.localPosition = Vector3.zero;
        
        */

        /*
        List<Transform> AvatarTrs = Avatar.GetComponentsInChildren<Transform>().ToList();

        List<GameObject> EndEffectorsGOs = new List<GameObject>();


        for (int id = 0; id < AvatarBodyPartsList.Count; ++id)
        {
            string rbName = AvatarBodyPartsList[id];

            Transform Tr = AvatarTrs.Where(obj => obj.transform.name == rbName).SingleOrDefault();

            GameObject EndEffector = new GameObject() as GameObject;
            EndEffector.name = AvatarBodyPartsList[id] + "_EndEffector";

            if (id > 0)
            {
                EndEffector.transform.parent = EndEffectorsGOs[0].transform;               
            }
            EndEffector.transform.position = Tr.position;
            EndEffector.transform.rotation = Tr.rotation;

            EndEffector.transform.parent = GameObject.Find(ViconBodyPartsList[id] + "Blue").transform;
            EndEffector.transform.localPosition = Vector3.zero;

            EndEffectorsGOs.Add(EndEffector);

            
        }

    */




        //        ViconTrs[0].position = Vector3.zero;




        /*
        for (int id = 0; id < ViconRBsList.Count; ++id)
        {
            string rbName = ViconRBsList[id];

            if (GameObject.Find(rbName) != null)
            {
                Transform Tr = GameObject.Find(rbName).transform;
                ViconTrs.Add(Tr);

                GameObject EndEffector = new GameObject() as GameObject;
                EndEffector.name = rbName + "_EndEffector";

                if (id > 0)
                {
                    ViconTrs[id].parent = ViconTrs[0];

                    // End eddfectors definition
                    // ViconTrs[id].localRotation * Q = AvatarTransfList[id].Rotation *  AvatarTransfList[0].localRotation;
                    // Q = Quaternion.Inverse(ViconTrs[id].rotation) * AvatarTransfList[id].rotation * Quaternion.Inverse(AvatarTransfList[0].rotation);

                 
                    EndEffector.transform.parent = ViconTrs[id];
                    EndEffector.transform.localPosition = Vector3.zero;
                    EndEffector.transform.localRotation = ViconTrs[id].rotation * Quaternion.Inverse(ViconTrs[id].rotation) * AvatarTransfList[id].rotation * Quaternion.Inverse(AvatarTransfList[0].rotation);
                }
                else
                {
                    // ViconTrs[0].rotation * q = AvatarTransfList[0].Rotation;
                    // q = AvatarTransfList[0].Rotation * Quaternion.Inverse(ViconTrs[0].rotation);

                }
            }
        }

        */







        /*
        AvatarTransfList[0].rotation * Quaternion.Inverse(AvatarTransfList[1].rotation);
        AvatarTransfList[0].rotation * Quaternion.Inverse(AvatarTransfList[2].rotation);
        AvatarTransfList[0].rotation * Quaternion.Inverse(AvatarTransfList[3].rotation);
        AvatarTransfList[0].rotation * Quaternion.Inverse(AvatarTransfList[4].rotation);
        AvatarTransfList[0].rotation * Quaternion.Inverse(AvatarTransfList[5].rotation);
        */


        /*
        VRIK ik = Avatar.GetComponentInChildren<VRIK>();

        ik.solver.spine.pelvisTarget = new GameObject("Pelvis_EndEffector").transform;
        ik.solver.spine.pelvisTarget.parent = GameObject.Find("Pelvis_Blue").transform;
        ik.solver.spine.pelvisTarget.localPosition = Vector3.zero;
        ik.solver.spine.pelvisTarget.localRotation = AvatarTransfList[0].localRotation;

        // Build and assign VRIK targets
        ik.solver.spine.headTarget = new GameObject("Head_EndEffector").transform;
        ik.solver.spine.headTarget.parent = GameObject.Find("Oculus_Blue").transform;
        ik.solver.spine.headTarget.localPosition = Vector3.zero;
        ik.solver.spine.headTarget.localRotation = AvatarTransfList[1].localRotation;
        

        ik.solver.leftArm.target = new GameObject("Hand_L_EndEffector").transform;
        ik.solver.leftArm.target.parent = GameObject.Find("Hand_L_Blue").transform;
        ik.solver.leftArm.target.localPosition = Vector3.zero;
        ik.solver.leftArm.target.localRotation = AvatarTransfList[2].localRotation;

        ik.solver.rightArm.target = new GameObject("Hand_R_EndEffector").transform;
        ik.solver.rightArm.target.parent = GameObject.Find("Hand_R_Blue").transform; ;
        ik.solver.rightArm.target.localPosition = Vector3.zero;
        ik.solver.rightArm.target.localRotation = AvatarTransfList[3].localRotation;

        ik.solver.leftLeg.target = new GameObject("Foot_L_EndEffector").transform;
        ik.solver.leftLeg.target.parent = GameObject.Find("Foot_L_Blue").transform; ;
        ik.solver.leftLeg.target.localPosition = Vector3.zero;
        ik.solver.leftLeg.target.localRotation = AvatarTransfList[4].localRotation;


        ik.solver.rightLeg.target = new GameObject("Foot_R_EndEffector").transform;
        ik.solver.rightLeg.target.parent = GameObject.Find("Foot_R_Blue").transform; ;
        ik.solver.rightLeg.target.localPosition = Vector3.zero;
        ik.solver.rightLeg.target.localRotation = AvatarTransfList[5].localRotation;

        //ik.references.root.localScale *= (ik.solver.spine.headTarget.position.y / (ik.references.head.position.y));

        //ik.enabled = true;

        */


        /*
        ik.solver.solveSpine = true;
        ik.solver.solveLegs = true;
        ik.solver.solveArms = true;
        */

        Quaternion q0 = Quaternion.LookRotation(new Vector3(0f, 0f, 1f), new Vector3(0f, 1f, 0f));
        Debug.LogFormat("q0: {0}", q0);

        /*
        List<string> ListEndEffector = new List<string>() { "NeckHead", "Pelvis", "LArmHand", "RArmHand", "LLegAnkle", "RLegAnkle" };
        List<string> ListViconMarker = new List<string>() { "Oculus_Blue", "Pelvis_Blue", "Hand_L_Blue", "Hand_R_Blue", "Foot_L_Blue", "Foot_R_Blue" };

        List<GameObject> EndEffectorsList = new List<GameObject>();
        

        List<Transform> AvatarTrs = Avatar.GetComponentsInChildren<Transform>().ToList();
        for(int i = 0;  i < ListEndEffector.Count; ++i)
        {
            string efName = ListEndEffector[i];
            Transform Tr = AvatarTrs.Where(obj => obj.gameObject.name == efName).SingleOrDefault();

            GameObject EndEffector = new GameObject();
            EndEffector.name = efName + "_EndEffector";

            EndEffector.transform.parent = GameObject.Find(ListViconMarker[i]).transform;
            EndEffector.transform.localPosition = Vector3.zero;
            //EndEffector.transform.localRotation = Tr.localRotation;
            EndEffector.transform.rotation = Tr.rotation;
            EndEffectorsList.Add(EndEffector);
            Debug.LogFormat("{0}", Tr.gameObject.name);            
        }

        VRIK ik = Avatar.GetComponentInChildren<VRIK>();
        ik.solver.spine.headTarget = EndEffectorsList[0].transform;
        ik.solver.spine.pelvisTarget = EndEffectorsList[1].transform;
        ik.solver.leftArm.target = EndEffectorsList[2].transform;
        //ik.solver.leftArm.bendGoal = EndEffectorsList[2].transform;
        ik.solver.rightArm.target = EndEffectorsList[3].transform;
        //ik.solver.rightArm.bendGoal = EndEffectorsList[3].transform;
        ik.solver.leftLeg.target = EndEffectorsList[4].transform;
        //ik.solver.leftLeg.bendGoal = EndEffectorsList[4].transform;
        ik.solver.rightLeg.target = EndEffectorsList[5].transform;
        //ik.solver.rightLeg.bendGoal = EndEffectorsList[5].transform;
        */


    }

    void DefineEndEffectors()
    {
        List<Transform> ListAvatarTrsBodyParts = new List<Transform>();
        foreach (var rbName in AvatarBodyPartsList)
        {
            Transform Tr = Avatar.GetComponentsInChildren<Transform>().ToList().Where(obj => obj.name == rbName).SingleOrDefault();
            ListAvatarTrsBodyParts.Add(Tr);

        }


        foreach (var rbName in ViconBodyPartsList)
        {
            Transform Tr = GameObject.Find(rbName + "Red").transform;

            if (Tr != null)
            {
                ListViconTrsBodyParts.Add(Tr);
            }
        }


        for (int id = 0; id < ListViconTrsBodyParts.Count; ++id)
        {
            Debug.LogFormat("{0}  |  {1}", ListAvatarTrsBodyParts[id].gameObject.name, ListViconTrsBodyParts[id].gameObject.name);

            GameObject EndEffector = new GameObject() as GameObject;
            EndEffector.name = ListAvatarTrsBodyParts[id].gameObject.name + "_EndEffector";
            EndEffector.transform.parent = ListViconTrsBodyParts[id];
            EndEffector.transform.localPosition = Vector3.zero;
            EndEffector.transform.rotation = ListAvatarTrsBodyParts[id].rotation;
            EndEffector.transform.localRotation = ListAvatarTrsBodyParts[id].localRotation;
            ListViconTrsEndEffectors.Add(EndEffector.transform);
        }

        /*
        VRIK ik = Avatar.GetComponentInChildren<VRIK>();
        ik.solver.spine.headTarget = ListViconTrsEndEffectors[0];
        ik.solver.spine.pelvisTarget = ListViconTrsEndEffectors[1];

        ik.solver.leftArm.target = ListViconTrsEndEffectors[2];
        ik.solver.rightArm.target = ListViconTrsEndEffectors[3];

        ik.solver.leftLeg.target = ListViconTrsEndEffectors[4];
        ik.solver.leftLeg.positionWeight = 1f;
        ik.solver.leftLeg.rotationWeight = 1f;

        ik.solver.rightLeg.target = ListViconTrsEndEffectors[5];
        ik.solver.rightLeg.positionWeight = 1f;
        ik.solver.rightLeg.rotationWeight = 1f;

        ik.solver.solveSpine = true;
        ik.solver.solveLegs = true;
        ik.solver.solveArms = true;
        */

        VRIKCalibrationController CalibCtrl = Avatar.GetComponentInChildren<VRIKCalibrationController>();

        CalibCtrl.settings.headTrackerForward = new Vector3(0f, 1f, 0f);
        CalibCtrl.settings.headTrackerUp = new Vector3(-1f, 0f, 0f);

        CalibCtrl.settings.bodyTrackerForward = new Vector3(0f, 1f, 0f);
        CalibCtrl.settings.bodyTrackerUp = new Vector3(-1f, 0f, 0f);

        CalibCtrl.settings.handTrackerForward = new Vector3(0f, -1f, 0f);
        CalibCtrl.settings.handTrackerUp = new Vector3(1f, 0f, 0f);

        CalibCtrl.settings.footTrackerForward = new Vector3(0f, 1f, 0f);
        CalibCtrl.settings.footTrackerUp = new Vector3(1f, 0f, 0f);



        CalibCtrl.headTracker = ListViconTrsEndEffectors[0];
        CalibCtrl.bodyTracker = ListViconTrsEndEffectors[1];
        CalibCtrl.leftHandTracker = ListViconTrsEndEffectors[2];
        CalibCtrl.rightHandTracker = ListViconTrsEndEffectors[3];
        CalibCtrl.leftFootTracker = ListViconTrsEndEffectors[4];
        CalibCtrl.rightFootTracker = ListViconTrsEndEffectors[5];





    }


    void LateUpdate()
    {



        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (Time.realtimeSinceStartup > LastKeyPressedTime + 2f)
            {
                DefineEndEffectorsTux();
                //GetInitialTransform();
                //DefineEndEffectors2();
                LastKeyPressedTime = Time.realtimeSinceStartup;
            }
        }


        if (m_Client.IsConnected().Connected)
        {
            //m_Client.UpdateFrame(Offset);

            //Retrieve Vicon frame
            Output_GetFrame Frame = m_Client.GetFrame();

            if (Frame.Result == Result.Success)
            {
                float CurrentTime = Time.realtimeSinceStartup;

                for (uint id = 0; id < m_Client.GetSubjectCount().SubjectCount; id++)
                {
                    var rbname = m_Client.GetSubjectName(id).SubjectName;
                    Output_GetSubjectRootSegmentName RootName = m_Client.GetSubjectRootSegmentName(rbname);


                    //Output_GetSegmentGlobalTranslation Pos = m_Client.GetSegmentGlobalTranslation(RootName.SegmentName, rbname);
                    //Output_GetSegmentGlobalRotationEulerXYZ Euler = m_Client.GetSegmentGlobalRotationEulerXYZ(RootName.SegmentName, rbname);
                    Output_GetSegmentGlobalTranslation Pos = m_Client.GetSegmentGlobalTranslation(rbname, RootName.SegmentName);
                    Output_GetSegmentGlobalRotationEulerXYZ Euler = m_Client.GetSegmentGlobalRotationEulerXYZ(rbname, RootName.SegmentName);
                    Output_GetSegmentGlobalRotationQuaternion Rot = m_Client.GetSegmentGlobalRotationQuaternion(rbname, RootName.SegmentName);


                    Vector3 pos = TransformPositionViconToUnity(Pos.Translation);
                    //Quaternion rot = TransformRotationViconToUnity(Euler.Rotation);
                    Quaternion rot = TransformRotationViconToUnity2(Rot.Rotation);
                    //Vector3 pos = new Vector3((float)Pos.Translation[0], (float)Pos.Translation[1], (float)Pos.Translation[2]) * ViconToUnityScale;
                    //Quaternion rot = Quaternion.Euler(new Vector3((float)Euler.Rotation[0], (float)Euler.Rotation[1], (float)Euler.Rotation[2]) * Mathf.Rad2Deg);
                    //Quaternion rot = new Quaternion((float)Rot.Rotation[0], (float)Rot.Rotation[1], (float)Rot.Rotation[2], (float)Rot.Rotation[3]);




                    if (!rbMapping.ContainsKey(rbname))
                    {
                        createRigidBody(rbname, pos, rot);
                    }

                    rbMapping[rbname].position = pos;
                    rbMapping[rbname].rotation = rot;

                }
            }
        }

    }




    void createRigidBody(string rbName, Vector3 pos, Quaternion rot)
    {

        //GameObject rb = GameObject.CreatePrimitive(PrimitiveType.Cube) as GameObject;
        GameObject rb = new GameObject() as GameObject;
        //rb.transform.parent = FloorGameObject.transform;
        rb.name = rbName;
        rb.transform.position = pos;
        rb.transform.rotation = rot;
        //rb.transform.localScale = new Vector3(0.1f, 0.05f, 0.025f);
        //rb.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);

        rbMapping.Add(rbName, rb.transform);
    }



    Vector3 TransformPositionViconToUnity(double[] pos)
    {
        //return (new Vector3(-(float)pos[1], (float)pos[2], (float)pos[0]) * ViconToUnityScale);
        return (new Vector3(-(float)pos[0], (float)pos[1], (float)pos[2]) * ViconToUnityScale);
    }


    Quaternion TransformRotationViconToUnity(double[] rot)
    {
        Quaternion rZ = Quaternion.AngleAxis(-(float)rot[0] * Mathf.Rad2Deg, Vector3.forward);
        Quaternion rX = Quaternion.AngleAxis((float)rot[1] * Mathf.Rad2Deg, Vector3.left);
        Quaternion rY = Quaternion.AngleAxis(-(float)rot[2] * Mathf.Rad2Deg, Vector3.up);
        return rZ * rX * rY;
    }

    Quaternion TransformRotationViconToUnity2(double[] rot)
    {
        return new Quaternion((float)-rot[0], (float)rot[1], (float)rot[2], (float)-rot[3]);
    }
}
