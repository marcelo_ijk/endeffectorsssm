﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Artanim
{
    public class QuaternionLib
    {
        public static void DecomposeMatrix(ref Matrix4x4 matrix, out Vector3 localPosition, out Quaternion localRotation, out Vector3 localScale)
        {
            localPosition = ExtractTranslationFromMatrix(ref matrix);
            localRotation = ExtractRotationFromMatrix(ref matrix);
            localScale = ExtractScaleFromMatrix(ref matrix);
        }


        static Vector3 ExtractTranslationFromMatrix(ref Matrix4x4 matrix)
        {
            Vector3 translate;
            translate.x = matrix.m03;
            translate.y = matrix.m13;
            translate.z = matrix.m23;
            return translate;
        }


        /// Sets this matrix to a translation, rotation and scaling matrix.
        //static Matrix4x4 GetTRS(ref Vector3 translation, ref Quaternion rotation, ref Vector3 scale) {
        public static Matrix4x4 GetTRS(GameObject Obj)
        {
            Matrix4x4 m = Matrix4x4.identity;

            m.SetTRS(Obj.transform.position, Obj.transform.rotation, Obj.transform.localScale);
            return m;
        }


        public static Quaternion ExtractRotationFromMatrix_(ref Matrix3x3 matrix)
        {
            Vector3 forward;
            forward.x = matrix.m02;
            forward.y = matrix.m12;
            forward.z = matrix.m22;

            Vector3 upwards;
            upwards.x = matrix.m01;
            upwards.y = matrix.m11;
            upwards.z = matrix.m21;

            return Quaternion.LookRotation(forward, upwards);
        }

        public static Quaternion ExtractRotationFromMatrix(ref Matrix4x4 matrix)
        {
            Vector3 forward;
            forward.x = matrix.m02;
            forward.y = matrix.m12;
            forward.z = matrix.m22;

            Vector3 upwards;
            upwards.x = matrix.m01;
            upwards.y = matrix.m11;
            upwards.z = matrix.m21;

            return Quaternion.LookRotation(forward, upwards);
        }


        static Vector3 ExtractScaleFromMatrix(ref Matrix4x4 matrix)
        {
            Vector3 scale;
            scale.x = new Vector4(matrix.m00, matrix.m10, matrix.m20, matrix.m30).magnitude;
            scale.y = new Vector4(matrix.m01, matrix.m11, matrix.m21, matrix.m31).magnitude;
            scale.z = new Vector4(matrix.m02, matrix.m12, matrix.m22, matrix.m32).magnitude;
            return scale;
        }



        public static Quaternion QuaternionFromMatrix(Matrix4x4 m)
        {
            // Adapted from: http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
            Quaternion q = new Quaternion();
            q.w = Mathf.Sqrt(Mathf.Max(0, 1 + m[0, 0] + m[1, 1] + m[2, 2])) / 2;
            q.x = Mathf.Sqrt(Mathf.Max(0, 1 + m[0, 0] - m[1, 1] - m[2, 2])) / 2;
            q.y = Mathf.Sqrt(Mathf.Max(0, 1 - m[0, 0] + m[1, 1] - m[2, 2])) / 2;
            q.z = Mathf.Sqrt(Mathf.Max(0, 1 - m[0, 0] - m[1, 1] + m[2, 2])) / 2;
            q.x *= Mathf.Sign(q.x * (m[2, 1] - m[1, 2]));
            q.y *= Mathf.Sign(q.y * (m[0, 2] - m[2, 0]));
            q.z *= Mathf.Sign(q.z * (m[1, 0] - m[0, 1]));
            return q;
        }


        // Normalising Quaternions        
        public static Quaternion Normalize(Quaternion q)
        {
            Quaternion _q = new Quaternion();

            float L2 = Mathf.Sqrt(q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w);
            _q.x = q.x / L2;
            _q.y = q.y / L2;
            _q.z = q.z / L2;
            _q.w = q.w / L2;

            return _q;
        }


        // Quaternion Scalar Multiplication        
        public static Quaternion ScalarMultiplication(Quaternion q, float s)
        {
            Quaternion _q = new Quaternion();
            _q.x = q.x * s;
            _q.y = q.y * s;
            _q.z = q.z * s;
            _q.w = q.w * s;

            return _q;
        }


        public static Quaternion Add(Quaternion q1, Quaternion q2)
        {
            Quaternion _q = new Quaternion();
            _q.x = q1.x + q2.x;
            _q.y = q1.y + q2.y;
            _q.z = q1.z + q2.z;
            _q.w = q1.w + q2.w;
                       

            return _q;
        }

        public static Quaternion Subtraction(Quaternion q1, Quaternion q0)
        {
            Quaternion _q = new Quaternion();
            _q.x = q1.x - q0.x;
            _q.y = q1.y - q0.y;
            _q.z = q1.z - q0.z;
            _q.w = q1.w - q0.w;

            return _q;
        }



        // Create Orthonormal Vector Basis: OVB
        public static bool CreateOVB(ref List<Vector3> positions, out Vector3 lPosition, out Quaternion lRotation, out Vector3 lScale)
        {

            Vector3 u = (positions[positions.Count - 2] - positions[positions.Count - 3]).normalized;
            Vector3 w = (positions[positions.Count - 1] - positions[positions.Count - 3]).normalized;
            Vector3 v = Vector3.Cross(w, u);
            u = Vector3.Cross(v, w);

            Matrix4x4 Transf = new Matrix4x4();
            Transf.m00 = u.x; Transf.m01 = v.x; Transf.m02 = w.x; Transf.m03 = positions[positions.Count - 3].x;
            Transf.m10 = u.y; Transf.m11 = v.y; Transf.m12 = w.y; Transf.m13 = positions[positions.Count - 3].y;
            Transf.m20 = u.z; Transf.m21 = v.z; Transf.m22 = w.z; Transf.m23 = positions[positions.Count - 3].z;
            Transf.m30 = 0.0f; Transf.m31 = 0.0f; Transf.m32 = 0.0f; Transf.m33 = 1.0f;

            DecomposeMatrix(ref Transf, out lPosition, out lRotation, out lScale);

            // Checking singularity 
            return true;
        }
    }
}