﻿using UnityEngine;
using System.Collections;
using Artanim;

public class LorentzDynSys : MonoBehaviour
{
    public float sigma = 10.0f;
    public float beta = 8.0f / 3.0f;
    public float rho = 28.0f;
    public float dt = 0.01f;
    public float[] Xi = new float[] { 1.0f, 1.0f, 1.0f };
        
    private Vector3 xd;
    private Vector3 x;
    private Vector3 xp;
    private Vector4 k;


    // Use this for initialization
    void Start()
    {
        k = Vector4.zero;
        x = Vector3.zero;
        xp = Vector3.zero;
        xd = Vector3.zero;

    
        SetInitialStates();
    }

    public void SetIntegrationStep(float _dt)
    {
        dt = _dt;
    }

    void SetInitialStates()
    {
        x.x = Xi[0];
        x.y = Xi[1];
        x.z = Xi[2];
    }


    public Vector3 GetStates()
    {
        return xd;
    }

    void SimulateSystem()
    {
        xd.x = sigma * (x.y - x.x);
        xd.y = (rho - x.z) * x.x - x.y;
        xd.z = x.x * x.y - beta * x.z;
    }

    // Simple Euller integration just for fun
    void ODEIntegration()
    {
        for (int j = 0; j < 3; ++j)
        {
            xp[j] = x[j];
            x[j] += xd[j] * dt;
        }
    }



    // Update is called once per frame
    void Update()
    {
        SimulateSystem();
        ODEIntegration();
    }    

}
