﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



using System.Runtime.InteropServices;
using System;
using System.Text;

public class EigenTest : MonoBehaviour
{


    // Use this for initialization
    void Start()
    {

        float[,] data = new float[5, 3] {
            { 0.996574f,   0.179624f,   0.085959f },
            { 0.262170f,   0.244073f,   0.431850f },
            { 0.876557f,   0.196126f,   0.932677f },
            { 0.259458f,   0.270316f,   0.480032f },
            { 0.110504f,   0.395519f,   0.567647f }};

        IntPtr X = Eigen.Create(5, 3);
        IntPtr Y = Eigen.Create(5, 3);
        IntPtr Cov = Eigen.Create(3, 3);
        IntPtr RT = Eigen.Create(4, 4);
        IntPtr matU = Eigen.Create(3, 3);
        IntPtr matV = Eigen.Create(3, 3);
        IntPtr matSingVals = Eigen.Create(3, 3);

        for (int i = 0; i < 5; ++i)
        {
            for (int j = 0; j < 3; ++j)
            {
                Eigen.SetValue(X, i, j, data[i, j]);
                Eigen.SetValue(Y, i, j, data[i, j]);
            }
        }
        

        
        Eigen.CovarianceMat(X, Y, Cov, matU, matV, matSingVals, RT);
        Debug.Log("Cov:\n" + PrintEigenLibMat(Cov));
        Debug.Log("MatU:\n" + PrintEigenLibMat(matU));
        Debug.Log("MatV:\n" + PrintEigenLibMat(matV));
        Debug.Log("Singular Values:\n" + PrintEigenLibMat(matSingVals));
        Debug.Log("RT:\n" + PrintEigenLibMat(RT));


    }

    string PrintEigenLibMat(IntPtr mat)
    {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < Eigen.GetRows(mat); ++i)
        {
            for (int j = 0; j < Eigen.GetCols(mat); ++j)
            {
                sb.AppendFormat("{0:F6}\t", Eigen.GetValue(mat, i, j));
            }
            sb.Append(Environment.NewLine);
        }

        return sb.ToString();
    }

}
