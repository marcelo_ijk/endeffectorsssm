﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class viewOrthoBasis : MonoBehaviour
{

    //public GameObject Dummy;
    //public GameObject Pilot;
    public GameObject Pilot;
    public GameObject Avatar;



    List<string> ViconMarkersList = new List<string>();
    List<string> PlayersList = new List<string>() { "Red", "Green", "Blue", "Yellow"};
    List<string> ViconBodyMarkers = new List<string>() { "Oculus_", "Pelvis_", "Hand_L_", "Hand_R_", "Foot_L_", "Foot_R_" };
        
    List<string> EndEffectorsList = new List<string>() { "Head_EndEffectors", "Pelvis_EndEffectors", "Hand_L_EndEffectors", "Hand_R_EndEffectors", "Foot_L_EndEffectors", "Foot_R_EndEffectors" };
    List<string> AvatarRBsList = new List<string>() { "Bip002 Head", "CenterEyes", "Bip002 Pelvis", "Bip002 L Hand", "Bip002 R Hand", "Bip002 L Foot", "Bip002 R Foot" };

    List<Transform> PilotTransfList = new List<Transform>();
    List<Transform> AvatarTransfList = new List<Transform>();
    


    Material lineMaterial;
    Vector3 startPoint = Vector3.zero;
    Vector3 endPoint = Vector3.one;
    float scale = 0.25f;

    // Use this for initialization
    void Start()
    {
        CreateLineMaterial();

        foreach (var Player in PlayersList)
            foreach (var bodyPart in ViconBodyMarkers)
                ViconMarkersList.Add(bodyPart + Player);


        List<Transform> PilotTrs = Pilot.GetComponentsInChildren<Transform>().ToList();
        foreach (Transform Tr in PilotTrs)
        {
        

            if (Tr.gameObject.name.Contains("Mock"))
            {
                PilotTransfList.Add(Tr);
            }
        }


        
        //List<string> AvatarRBsList = new List<string>() { "Bip002 Head", "Bip002 Pelvis", "Bip002 L Hand", "Bip002 R Hand", "Bip002 L Foot", "Bip002 R Foot" };
        List<Transform> AvatarTrs = Avatar.GetComponentsInChildren<Transform>().ToList();

        foreach (var bp in AvatarRBsList)
        {
            Transform Tr = AvatarTrs.Where(obj => obj.transform.name == bp).SingleOrDefault();
            AvatarTransfList.Add(Tr);
        }
    }



    void CreateLineMaterial()
    {

        // Unity has a built-in shader that is useful for drawing simple colored things
        var shader = Shader.Find("Hidden/Internal-Colored");
        lineMaterial = new Material(shader);
        lineMaterial.hideFlags = HideFlags.HideAndDontSave;
        // Turn on alpha blending
        lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
        lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        // Turn backface culling off
        lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
        // Turn off depth writes
        lineMaterial.SetInt("_ZWrite", 0);
    }

    void OnRenderObject()
    {
        lineMaterial.SetPass(0);

        // View end effectors


        foreach (var bp in AvatarRBsList)
        {
            GameObject go = GameObject.Find(bp + "_EndEffector");
            if (go != null)
            {
                Transform Tr = go.transform;

                startPoint = Tr.transform.position;
                GL.PushMatrix();
                //GL.MultMatrix(transform.localToWorldMatrix);
                //GL.MultMatrix(transform.worldToLocalMatrix);
                GL.Begin(GL.LINES);

                endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(1f, 0f, 0f) * scale);
                GL.Color(Color.red);
                GL.Vertex(startPoint);
                GL.Vertex(endPoint);

                endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(0f, 1f, 0f) * scale);
                GL.Color(Color.green);
                GL.Vertex(startPoint);
                GL.Vertex(endPoint);

                endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(0f, 0f, 1f) * scale);
                GL.Color(Color.blue);
                GL.Vertex(startPoint);
                GL.Vertex(endPoint);

                GL.End();
                GL.PopMatrix();
            }
        }


        foreach (Transform Tr in PilotTransfList)
        {
            startPoint = Tr.transform.position;
            GL.PushMatrix();
            //GL.MultMatrix(transform.localToWorldMatrix);
            //GL.MultMatrix(transform.worldToLocalMatrix);
            GL.Begin(GL.LINES);

            endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(1f, 0f, 0f) * scale);
            GL.Color(Color.red);
            GL.Vertex(startPoint);
            GL.Vertex(endPoint);

            endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(0f, 1f, 0f) * scale);
            GL.Color(Color.green);
            GL.Vertex(startPoint);
            GL.Vertex(endPoint);

            endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(0f, 0f, 1f) * scale);
            GL.Color(Color.blue);
            GL.Vertex(startPoint);
            GL.Vertex(endPoint);

            GL.End();
            GL.PopMatrix();
        }


        foreach (Transform Tr in AvatarTransfList)
        {
            startPoint = Tr.transform.position;
            GL.PushMatrix();
            //GL.MultMatrix(transform.localToWorldMatrix);
            //GL.MultMatrix(transform.worldToLocalMatrix);
            GL.Begin(GL.LINES);

            endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(1f, 0f, 0f) * scale);
            GL.Color(Color.red);
            GL.Vertex(startPoint);
            GL.Vertex(endPoint);

            endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(0f, 1f, 0f) * scale);
            GL.Color(Color.green);
            GL.Vertex(startPoint);
            GL.Vertex(endPoint);

            endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(0f, 0f, 1f) * scale);
            GL.Color(Color.blue);
            GL.Vertex(startPoint);
            GL.Vertex(endPoint);

            GL.End();
            GL.PopMatrix();
        }


        foreach (string s in ViconMarkersList)
        {
            GameObject eF = GameObject.Find(s);
            if (eF != null)
            {
                Transform[] Trs = eF.GetComponentsInChildren<Transform>();
                Transform Tr = (Trs.Count() > 1) ? Trs[1] : Trs[0];                

                
                //foreach (Transform Tr in Trs)
                {
                    startPoint = Tr.transform.position;
                    GL.PushMatrix();
                    //GL.MultMatrix(transform.localToWorldMatrix);
                    //GL.MultMatrix(transform.worldToLocalMatrix);
                    GL.Begin(GL.LINES);

                    endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(1f, 0f, 0f) * scale);
                    GL.Color(Color.red);
                    GL.Vertex(startPoint);
                    GL.Vertex(endPoint);

                    endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(0f, 1f, 0f) * scale);
                    GL.Color(Color.green);
                    GL.Vertex(startPoint);
                    GL.Vertex(endPoint);

                    endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(0f, 0f, 1f) * scale);
                    GL.Color(Color.blue);
                    GL.Vertex(startPoint);
                    GL.Vertex(endPoint);

                    GL.End();
                    GL.PopMatrix();
                }
            }
        }
       
    }
}
