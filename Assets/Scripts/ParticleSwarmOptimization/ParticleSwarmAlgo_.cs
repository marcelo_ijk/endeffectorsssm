﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ParticleSwarmOptimization_
{

    public class ParticleSwarmAlgo : MonoBehaviour
    {

        public class Swarm
        {

            public class Particle
            {
                public Vector3 pos;
                public Vector3 vel;
                public KeyValuePair<float, Vector3> lbest;
                public float[,] previousObsevations;
                public int NumberOfObservations = 25;
                public Color ColorID = Color.white;


                public Particle()
                {
                    pos = Vector3.zero;
                    vel = Vector3.zero;
                    lbest = new KeyValuePair<float, Vector3>(1e12f, Vector3.zero);

                    previousObsevations = new float[NumberOfObservations, 3];
                }

                public void SetColor(Color cID)
                {
                    ColorID = cID;
                }

                public Color GetColorID()
                {
                    return ColorID;
                }
            }

            public int NumberOfParticles;
            Vector3 dxyz = Vector3.zero;
            public List<Particle> ListParticles = new List<Particle>();
            public KeyValuePair<float, Vector3> gbest;
            public Vector3 TargetVec = new Vector3();
            public float C1 = 0.01f;
            public float C2 = 0.01f;



            public Swarm(int NOfP)
            {
                NumberOfParticles = NOfP;
                gbest = new KeyValuePair<float, Vector3>(1e12f, Vector3.zero);
            }


            public float ComputeCostFunction(Vector3 p)
            {
                return (p - TargetVec).magnitude;

            }

            public void CreateInitialPopulation(Vector3 p0, Vector3 v0)
            {

                for (int i = 0; i < NumberOfParticles; ++i)
                {
                    Particle p = new Particle();

                    p.pos = p0 + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
                    p.vel = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)) / 10f;
                    p.lbest = new KeyValuePair<float, Vector3>(1e12f, Vector3.zero);
                    p.SetColor(Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f));
                    ListParticles.Add(p);
                }
            }


            public void UpdateSolutions()
            {
                // v[] = v[] + c1 * rand() * (pbest[] - present[]) + c2 * rand() * (gbest[] - present[])(a)
                // present[] = persent[] + v[](b)

                foreach (var p in ListParticles)
                {
                    Vector3 r1 = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
                    Vector3 r2 = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));

                    p.vel = p.vel + C1 * Vector3.Scale((p.lbest.Value - p.pos), r1) + C2 * Vector3.Scale((gbest.Value - p.pos), r2);
                    //p.vel = p.vel + C1 * Random.Range(0f, 1f) * (p.lbest.Value - p.pos) + C2 * Random.Range(0f, 1f) * (gbest.Value - p.pos);

                    // Check max. vel
                    if (p.vel.magnitude > 1f)
                    {
                        //p.vel = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)) / 10f;
                        p.vel = Vector3.zero;

                    }
                    p.pos = p.pos + p.vel;


                    p.previousObsevations[0, 0] = p.pos.x;
                    p.previousObsevations[0, 1] = p.pos.y;
                    p.previousObsevations[0, 2] = p.pos.z;


                    float[,] dummy = new float[p.NumberOfObservations, 3];

                    for (int i = 0; i < p.NumberOfObservations; ++i)
                    {
                        dummy[i, 0] = p.previousObsevations[(i + 1) % p.NumberOfObservations, 0];
                        dummy[i, 1] = p.previousObsevations[(i + 1) % p.NumberOfObservations, 1];
                        dummy[i, 2] = p.previousObsevations[(i + 1) % p.NumberOfObservations, 2];
                    }

                    p.previousObsevations = dummy;
                }
            }

            public void SetTargetSolution(Vector3 _Target)
            {
                TargetVec = _Target;
            }


            public void SearchingSol()
            {

                for (int i = 0; i < ListParticles.Count; ++i)
                {
                    float f = ComputeCostFunction(ListParticles[i].pos);


                    if (ListParticles[i].lbest.Key > f)
                    {
                        ListParticles[i].lbest = new KeyValuePair<float, Vector3>(f, ListParticles[i].pos);

                        if (gbest.Key > f)
                        {
                            gbest = new KeyValuePair<float, Vector3>(f, ListParticles[i].pos);
                            //Debug.LogFormat("{0}  :  {1}", f, gbest.Value);
                        }
                    }
                }

                UpdateSolutions();


                /*
                for (int i = 0; i < ListParticles.Count; ++i)
                {
                    Debug.LogFormat("{0}  :  {1}", i, ListParticles[i].pos);
                }
                */

            }
        }


        // Past states
        public List<List<Vector3>> StatesParticlesList = new List<List<Vector3>>();

        public static int NumberOfParticles = 30;

        GameObject[] p = new GameObject[NumberOfParticles];


        public GameObject GoalGO;
        public Swarm PSO = new Swarm(NumberOfParticles);


        /*
        // Use this for initialization
        void Start()
        {
            //this.gameObject.transform.parent = Camera.main.transform;

            PSO.CreateInitialPopulation(Vector3.zero, Vector3.zero);
            PSO.SetTargetSolution(Vector3.zero);
            //PSO.SetTargetSolution(new Vector3(1f, 0f, 1.1f));

            for (int i = 0; i < NumberOfParticles; ++i)
            {
                List<Vector3> ListStates = new List<Vector3>();
                StatesParticlesList.Add(ListStates);

                p[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere) as GameObject;
                p[i].GetComponent<Renderer>().material.color = PSO.ListParticles[i].ColorID;// Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);            
                p[i].transform.localScale = Vector3.one / 3f;
            }
        }
        */

        /*
        // Update is called once per frame
        void Update()
        {

            if (Input.GetMouseButton(0))
            {
                for (int i = 0; i < PSO.ListParticles.Count; ++i)
                {
                    PSO.ListParticles[i].lbest = new KeyValuePair<float, Vector3>(1e12f, PSO.ListParticles[i].pos);
                }

                //Debug.Log("Pressed left click.");
                //PSO.SetTargetSolution(Input.mousePosition);
                PSO.SetTargetSolution(GoalGO.transform.position);
                PSO.gbest = new KeyValuePair<float, Vector3>(1e12f, Vector3.zero);
                //GoalGO.transform.position = Input.mousePosition;
            }


            if (PSO.gbest.Key < 0.1f)
            {
                for (int i = 0; i < PSO.ListParticles.Count; ++i)
                {
                    PSO.ListParticles[i].lbest = new KeyValuePair<float, Vector3>(1e12f, PSO.ListParticles[i].pos);

                }

                //Debug.Log("Pressed left click.");
                //PSO.SetTargetSolution(Input.mousePosition);

                Vector3 rpos = new Vector3(Random.Range(-5f, 5f), Random.Range(2f, 8f), Random.Range(-5f, 5f));
                PSO.SetTargetSolution(rpos);
                //PSO.gbest = new KeyValuePair<float, Vector3>(1e12f, Vector3.zero);
                PSO.gbest = new KeyValuePair<float, Vector3>(1e12f, PSO.gbest.Value);
                GoalGO.transform.position = rpos;

                //Stack myStack = new Stack();                      

            }




            //Debug.Log("asksdölksaölda");
            PSO.SearchingSol();

            for (int i = 0; i < PSO.ListParticles.Count; ++i)
            {
                p[i].transform.position = PSO.ListParticles[i].pos;
            }
        }
        */

    }
}