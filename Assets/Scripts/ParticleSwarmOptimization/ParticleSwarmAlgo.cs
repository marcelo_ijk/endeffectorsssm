﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using LightweightMatrixCSharp;
using System;

using Random = UnityEngine.Random;


public class ParticleSwarmAlgo : MonoBehaviour
{

    public class Particle
    {
        //public delegate float EvaluateCostFunc_();
        //public EvaluateCostFunc_ EvaluateCostFunc;
        
        static public int dim;
        public float[] pos;
        //public float[] previousPos;
        public float[] vel;
        public float _maxVel = 1f;
        //public KeyValuePair<float, float[]> lbest;
        public float[] lbest;


        public Particle(int _dim)
        {
            dim = _dim;
            pos = new float[dim];
            //previousPos = new float[dim];
            vel = new float[dim];
            //lbest = new KeyValuePair<float, float[]>();
            lbest = new float[dim + 1];     // Last component: Error value 

            InitializeParticle();
        }

        ~Particle()
        {

        }

        public void InitializeParticle(params float[] pos0)
        {

            for (int id = 0; id < dim; ++id)
            {

                //pos[id] = Random.Range(-1f, 1f) / 1e10f;
                pos[id] = Random.Range(0, 2f);

                vel[id] = Random.Range(-_maxVel, _maxVel);

                if (pos0.Length == dim)
                {
                    pos[id] += pos0[id];
                }
                lbest[id] = pos[id];
            }
            lbest[dim] = 1e12f;
            //KeyValuePair<float, float[]> kv = new KeyValuePair<float, float[]>(1e12f, pos);
            //lbest = kv;
        }
    }

    public bool _isGBestUpdated = false;
    ulong iter = 0;
    public int NumberOfParticles;
    public int dim;
    //public List<Particle> ListParticles = new List<Particle>();
    Particle[] Particles;

    //public KeyValuePair<float, float[]> gbest;
    public float[] gbest;
    public float[] SearchingSpaceOrigin;
    public float[] TargetVec;

    public float C1 = 2f;
    public float C2 = 2f;

    //public float[] r1;
    //public float[] r2;
    public float r1;
    public float r2;

    /*
    public delegate Matrix StatisticalShapeModel_(Matrix ShapeParameter);
    public StatisticalShapeModel_ StatisticalShapeModel;

    public delegate Matrix GetPhysicalMarkers_();
    public GetPhysicalMarkers_ GetPhysicalMarkers;
    */

    public delegate float EvaluateCostFunc_();
    public EvaluateCostFunc_ EvaluateCostFunc;

    public ParticleSwarmAlgo(int _NumberOfPartciles, int _dim)
    {

        NumberOfParticles = _NumberOfPartciles;
        dim = _dim;

        Particles = new Particle[NumberOfParticles];

        //r1 = new float[dim];
        //r2 = new float[dim];

        SearchingSpaceOrigin = new float[dim];
        gbest = new float[dim + 1];
        gbest[dim] = 1e12f;

        for (int i = 0; i < NumberOfParticles; ++i)
        {
            Particles[i] = new Particle(dim);
            Particles[i].InitializeParticle();
        }
    }


    ~ParticleSwarmAlgo() { }

    public Matrix GetOptimalSolution()
    {
        Matrix gBestMat = Matrix.ZeroMatrix(dim, 1);

        for (int s = 0; s < dim; s++)
        {
            gBestMat[s, 0] = gbest[s];
        }
        return gBestMat;

    }

    public void Optimize()
    {
        // v[] = v[] + c1 * rand() * (pbest[] - present[]) + c2 * rand() * (gbest[] - present[])(a)
        // present[] = persent[] + v[](b)        

        SearchingForGlobalBestSol();

        for (int id = 0; id < NumberOfParticles; ++id)
        {
            r1 = Random.Range(0f, 1f);
            r2 = Random.Range(0f, 1f);

            float Speed = 0f;
            for (int d = 0; d < dim; ++d)
            {

                Particles[id].vel[d] += C1 * (Particles[id].lbest[d] - Particles[id].pos[d]) * r1 + C2 * (gbest[d] - Particles[id].pos[d]) * r2;
                Speed += Particles[id].vel[d] * Particles[id].vel[d];

                /*
                if (Particles[id].vel[d] > Particles[id]._maxVel)
                {
                    Particles[id].vel[d] = Random.Range(-Particles[id]._maxVel, Particles[id]._maxVel);
                }
                Particles[id].pos[d] += Particles[id].vel[d];
                */
            }

            
            if (Mathf.Sqrt(Speed) > 2f)
            {                
                for (int d = 0; d < dim; ++d)
                {
                    //Particles[id].vel[d] = Random.Range(-Particles[id]._maxVel, Particles[id]._maxVel);
                    Particles[id].vel[d] = Random.Range(0f, Particles[id]._maxVel);
                }                
            }
            

            if (Particles[id].lbest[dim] / gbest[dim] > 1f)
            {
                //Particles[id].InitializeParticle();
            }

            for (int d = 0; d < dim; ++d)
            {
                Particles[id].pos[d] += Particles[id].vel[d];
            }


            // Verify cost function
            // Input solution in the statistical shape model
            // Physical tracking markers
            // e = 1/n * (phys - SSM) * (phys - SSM)'
            //SearchingForGlobalBestSol();
        }


        /*
        foreach (var p in ListParticles)
        {
            Vector3 r1 = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));
            Vector3 r2 = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f));

            p.vel = p.vel + C1 * Vector3.Scale((p.lbest.Value - p.pos), r1) + C2 * Vector3.Scale((gbest.Value - p.pos), r2);
            //p.vel = p.vel + C1 * Random.Range(0f, 1f) * (p.lbest.Value - p.pos) + C2 * Random.Range(0f, 1f) * (gbest.Value - p.pos);

            // Check max. vel
            if (p.vel.magnitude > 1f)
            {
                //p.vel = new Vector3(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)) / 10f;
                p.vel = Vector3.zero;

            }
            p.pos = p.pos + p.vel;


            p.previousObsevations[0, 0] = p.pos.x;
            p.previousObsevations[0, 1] = p.pos.y;
            p.previousObsevations[0, 2] = p.pos.z;


            float[,] dummy = new float[p.NumberOfObservations, 3];

            for (int i = 0; i < p.NumberOfObservations; ++i)
            {
                dummy[i, 0] = p.previousObsevations[(i + 1) % p.NumberOfObservations, 0];
                dummy[i, 1] = p.previousObsevations[(i + 1) % p.NumberOfObservations, 1];
                dummy[i, 2] = p.previousObsevations[(i + 1) % p.NumberOfObservations, 2];
            }

            p.previousObsevations = dummy;
        }
        */
    }


    void SearchingForGlobalBestSol()
    {
        _isGBestUpdated = false;
        for (int i = 0; i < NumberOfParticles; ++i)
        {
            Particles[i].lbest[dim] = EvaluateCostFunc();

            if (Particles[i].lbest[dim] < gbest[dim])
            {
                gbest[dim] = Particles[i].lbest[dim];
                iter++;
                Debug.LogFormat("RMSE [{0:D6}]: {1:F6}", iter,  gbest[dim]);

                for (int d = 0; d < dim; ++d)
                {
                    gbest[d] = Particles[i].lbest[d];
                }
                _isGBestUpdated = true;
            }
        }
    }
}
