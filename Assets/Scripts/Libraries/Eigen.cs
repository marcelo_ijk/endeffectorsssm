﻿using System;
using System.Runtime.InteropServices;
using System.Text;

public static class Eigen
{
    //Default
    [DllImport("libEigen")]
    public static extern IntPtr Create(int rows, int cols);
    [DllImport("libEigen")]
    public static extern void CovarianceMat(IntPtr X, IntPtr Y, IntPtr Cov, IntPtr matU, IntPtr matV, IntPtr matSingVals, IntPtr RT);
    [DllImport("libEigen")]
    public static extern IntPtr Delete(IntPtr T);
    [DllImport("libEigen")]    
    public static extern bool _msgInfo(StringBuilder sStrBuilder, int nCapacity);



    //Setters and Getters
    [DllImport("libEigen")]
    public static extern int GetRows(IntPtr T);
    [DllImport("libEigen")]
    public static extern int GetCols(IntPtr T);
    [DllImport("libEigen")]
    public static extern void SetZero(IntPtr T);
    [DllImport("libEigen")]
    public static extern void SetSize(IntPtr T, int rows, int cols);
    [DllImport("libEigen")]
    public static extern void SetValue(IntPtr T, int row, int col, float value);
    [DllImport("libEigen")]
    public static extern float GetValue(IntPtr T, int row, int col);

    //Arithmetics
    [DllImport("libEigen")]
    public static extern void Add(IntPtr lhs, IntPtr rhs, IntPtr OUT);
    [DllImport("libEigen")]
    public static extern void Subtract(IntPtr lhs, IntPtr rhs, IntPtr OUT);
    [DllImport("libEigen")]
    public static extern void Product(IntPtr lhs, IntPtr rhs, IntPtr OUT);
    [DllImport("libEigen")]
    public static extern void Scale(IntPtr lhs, float value, IntPtr OUT);
    [DllImport("libEigen")]
    public static extern void PointwiseProduct(IntPtr lhs, IntPtr rhs, IntPtr OUT);
    [DllImport("libEigen")]
    public static extern void PointwiseQuotient(IntPtr lhs, IntPtr rhs, IntPtr OUT);
    [DllImport("libEigen")]
    public static extern void PointwiseAbsolute(IntPtr IN, IntPtr OUT);
    [DllImport("libEigen")]
    public static extern float RowSum(IntPtr T, int row);
    [DllImport("libEigen")]
    public static extern float ColSum(IntPtr T, int col);
    [DllImport("libEigen")]
    public static extern float RowMean(IntPtr T, int row);
    [DllImport("libEigen")]
    public static extern float ColMean(IntPtr T, int col);
    [DllImport("libEigen")]
    public static extern float RowStd(IntPtr T, int row);
    [DllImport("libEigen")]
    public static extern float ColStd(IntPtr T, int col);

    //Deep Learning Functions
    [DllImport("libEigen")]
    public static extern void Normalise(IntPtr IN, IntPtr mean, IntPtr std, IntPtr OUT);
    [DllImport("libEigen")]
    public static extern void Renormalise(IntPtr IN, IntPtr mean, IntPtr std, IntPtr OUT);
    [DllImport("libEigen")]
    public static extern void Layer(IntPtr IN, IntPtr W, IntPtr b, IntPtr OUT);
    [DllImport("libEigen")]
    public static extern void Blend(IntPtr T, IntPtr W, float w);
    [DllImport("libEigen")]
    public static extern void ELU(IntPtr T);
    [DllImport("libEigen")]
    public static extern void Sigmoid(IntPtr T);
    [DllImport("libEigen")]
    public static extern void TanH(IntPtr T);
    [DllImport("libEigen")]
    public static extern void SoftMax(IntPtr T);
    [DllImport("libEigen")]
    public static extern void LogSoftMax(IntPtr T);
    [DllImport("libEigen")]
    public static extern void SoftSign(IntPtr T);
    [DllImport("libEigen")]
    public static extern void Exp(IntPtr T);
}
