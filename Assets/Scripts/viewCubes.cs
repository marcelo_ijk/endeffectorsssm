﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class viewCubes : MonoBehaviour
{   
    Material lineMaterial;
    Vector3 startPoint = Vector3.zero;
    Vector3 endPoint = Vector3.one;
    float scale = 1.5f;

    List<string> GameObjectsOrthoBasis = new List<string>() { "Cube0", "Cube1" };

    // Use this for initialization
    void Start()
    {
        CreateLineMaterial();


    }



    void CreateLineMaterial()
    {

        // Unity has a built-in shader that is useful for drawing simple colored things
        var shader = Shader.Find("Hidden/Internal-Colored");
        lineMaterial = new Material(shader);
        lineMaterial.hideFlags = HideFlags.HideAndDontSave;
        // Turn on alpha blending
        lineMaterial.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
        lineMaterial.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
        // Turn backface culling off
        lineMaterial.SetInt("_Cull", (int)UnityEngine.Rendering.CullMode.Off);
        // Turn off depth writes
        lineMaterial.SetInt("_ZWrite", 0);
    }

    void OnRenderObject()
    {
        lineMaterial.SetPass(0);

        // View end effectors



        foreach (var go in GameObjectsOrthoBasis)
        {
            GameObject c = GameObject.Find(go);            

            if (c != null)
            {
                Transform Tr = c.transform;

                startPoint = Tr.transform.position;
                GL.PushMatrix();
                //GL.MultMatrix(transform.localToWorldMatrix);
                //GL.MultMatrix(transform.worldToLocalMatrix);
                GL.Begin(GL.LINES);

                endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(1f, 0f, 0f) * scale);
                GL.Color(Color.red);
                GL.Vertex(startPoint);
                GL.Vertex(endPoint);

                endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(0f, 1f, 0f) * scale);
                GL.Color(Color.green);
                GL.Vertex(startPoint);
                GL.Vertex(endPoint);

                endPoint = Tr.transform.localToWorldMatrix.MultiplyPoint(new Vector3(0f, 0f, 1f) * scale);
                GL.Color(Color.blue);
                GL.Vertex(startPoint);
                GL.Vertex(endPoint);

                GL.End();
                GL.PopMatrix();
            }
        }

    }
}
