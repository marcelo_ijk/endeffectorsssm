﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.IO;
using System.Xml.Linq;
using System.Linq;

using LightweightMatrixCSharp;
using System;
using UnityEngine.UI;

public class SSM_ANSUR : MonoBehaviour
{

    private GameObject Dummy;
    private GameObject Dummy0;
    //{ "CenterEyes", "LeftFoot", "RightFoot", "Bip002 Pelvis", "LeftHand", "RightHand" };
    private List<string> VirtualEndEffectorsIDs = new List<string>() { "CenterEyes", "LeftFoot", "RightFoot", "LeftHand", "RightHand" };

    Matrix MeanShapeMat;
    Matrix EigVecsMat;
    Matrix EigValsMat;
    public IEnumerator coroutineShapeParameter;
    bool _isCoroutineShapeParameter = false;
    public Text ShapeParameter;


    // Use this for initialization
    void Start()
    {

        LoadSSM();

        coroutineShapeParameter = SSM_SetShapeParameterRandom(0.25f);
        _isCoroutineShapeParameter = false;


        // Starting
        //_isCoroutineShapeParameter = true;
        //StartCoroutine(coroutineShapeParameter);
        //StartCoroutineSSM();

        // Stoping
        //_isCoroutineShapeParameter = false;
        //StopCoroutine(coroutineShapeParameter);
    }



    public void StartCoroutineSSM()
    {
        _isCoroutineShapeParameter = true;
        StartCoroutine(coroutineShapeParameter);
    }

    void LoadSSM()
    {
        // Load the XML file from our project directory containing the purchase orders
        var filenameEigVals = "EigenValsSSM_ANSUR.xml";
        var filenameEigVecs = "EigenVecsSSM_ANSUR.xml";
        var filenameMeanShape = "MeanModelSSM_ANSUR.xml";
        var currentDirectory = Directory.GetCurrentDirectory();


        var EigenValsFilePath = Path.Combine(currentDirectory + @"\Assets\Resources\SSM_ANSUR\", filenameEigVals);
        var EigenVecsFilePath = Path.Combine(currentDirectory + @"\Assets\Resources\SSM_ANSUR\", filenameEigVecs);
        var MeanShapeFilePath = Path.Combine(currentDirectory + @"\Assets\Resources\SSM_ANSUR\", filenameMeanShape);
        //Debug.LogFormat("{0} | {1}", EigenValsFilePath.ToString(), filename.ToString());

        XElement xElemEigenVals_SSM = XElement.Load(EigenValsFilePath);
        XElement xElemEigenVecs_SSM = XElement.Load(EigenVecsFilePath);
        XElement xElemMeanShape_SSM = XElement.Load(MeanShapeFilePath);


        IEnumerable<XElement> eigVals =
             from el in xElemEigenVals_SSM.Descendants()
             where el.Name.LocalName.StartsWith("scalar")
             select el;

        IEnumerable<XElement> eigVecs =
             from el in xElemEigenVecs_SSM.Descendants()
             where el.Name.LocalName.StartsWith("scalar")
             select el;


        IEnumerable<XElement> meanShape =
            from el in xElemMeanShape_SSM.Descendants()
            where el.Name.LocalName.StartsWith("scalar")
            select el;


        MeanShapeMat = Matrix.ZeroMatrix(54, 1);
        int id = 0;
        foreach (XElement xElem in meanShape)
        {
            double val;
            Double.TryParse(xElem.Value, out val);

            MeanShapeMat[id, 0] = val;
            id++;
        }


        EigValsMat = Matrix.ZeroMatrix(54, 1);
        id = 0;
        foreach (XElement xElem in eigVals)
        {
            double val;
            Double.TryParse(xElem.Value, out val);

            EigValsMat[id, 0] = val;
            id++;
        }


        Matrix dummyMat = Matrix.ZeroMatrix(54 * 54, 1);
        id = 0;
        foreach (XElement xElem in eigVecs)
        {
            double val;
            Double.TryParse(xElem.Value, out val);
            dummyMat[id, 0] = val;
            id++;
        }

        // Reshaping vector to matrix
        EigVecsMat = Matrix.ZeroMatrix(54, 54);

        for (int i = 0; i < 54; ++i)
        {
            for (int j = 0; j < 54; ++j)
            {
                EigVecsMat[i, j] = dummyMat[54 * i + j, 0];
            }
        }


        Debug.LogFormat("Rows: {0} | Cols: {1}", MeanShapeMat.rows, MeanShapeMat.cols);


    }


    public void SetAvatarModel(GameObject _Dummy)
    {
        Dummy = _Dummy;
        Dummy0 = _Dummy;
        LoadSSM();
    }



    public Matrix SSM_SetShapeParameter(Matrix b)
    {        

        float neckLenght = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head).position);

        float chestLenght = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperArm).position);

        float femurLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg).position);
        float tibiaLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftFoot).position);

        float armLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm).position);
        float forearmLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftHand).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm).position);


        //Debug.LogFormat("LeftHand: {0}\n", Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftHand).gameObject.name);

        Debug.Assert(b.rows == MeanShapeMat.rows, "Number of shape parameters should be equal the number of eigenvectors");
        //Matrix b = Matrix.ZeroMatrix(MeanShapeMat.rows, 1);
        Matrix x = Matrix.ZeroMatrix(MeanShapeMat.rows, 1);

        // [-3*Sigma, 3*Sigma]
        //double dS = 0.1;
        //int NumberOfSteps = 11;

        // Head
        Transform Head0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);
        Vector3 HeadS0 = Head0.localScale;
        Vector3 HeadChildrenScale = Head0.GetComponentsInChildren<Transform>()[1].localScale;


        // Neck        
        Transform Neck0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);
        //Vector3 NeckS0 = Neck0.localScale;
        Vector3 NeckS0 = Neck0.localPosition;
        Vector3 NeckChildrenScale = Neck0.GetComponentsInChildren<Transform>()[1].localScale;


        // Pelvis        
        Transform Pelvis0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Hips);
        Vector3 PelvisS0 = Pelvis0.localScale;
        Vector3 PelvisChildrenScale = Pelvis0.GetComponentsInChildren<Transform>()[1].localScale;
        Debug.LogFormat("Pelvis: {0}  |  Pelvis Child: {1}", Pelvis0.gameObject.name, Pelvis0.GetComponentsInChildren<Transform>()[1].gameObject.name);


        // Chest
        //Debug.LogFormat("{0}", Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest).gameObject.name);
        Transform Chest0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest);
        Vector3 ChestS0 = Chest0.localScale;
        Vector3 ChestChildrenScale = Chest0.GetComponentsInChildren<Transform>()[1].localScale;

        // Forearm
        Transform ForearmLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm);
        Transform ForearmRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerArm);
        Vector3 forearmS0 = ForearmLeft0.localScale;

        // Arm
        Transform ArmLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm);
        Transform ArmRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperArm);
        Vector3 armS0 = ArmLeft0.localScale;

        // Femur
        Transform FemurLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg);
        Transform FemurRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperLeg);
        Vector3 femurS0 = FemurLeft0.localScale;

        // Tibia
        Transform TibiaLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg);
        Transform TibiaRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerLeg);
        Vector3 tibiaS0 = TibiaLeft0.localScale;

        
        
        for (int ev = 0; ev < MeanShapeMat.rows; ++ev)
        {
            if (EigValsMat[ev, 0] > 1e-6)
                b[ev, 0] *= Math.Sqrt(EigValsMat[ev, 0]);
        }        
        x = MeanShapeMat + EigVecsMat * b;

        ShapeParameter.text = String.Format("Parameters: < {0:F12}, {1:F12}, {2:F12}, {3:F12}, {4:F12} >", b[0, 0], b[1, 0], b[2, 0], b[3, 0], b[4, 0] );


        //******* Head *******                         
        Transform Head = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);

        float sHead = Vector3.Distance(
            new Vector3((float)x[51, 0], (float)x[52, 0], (float)x[53, 0]),
            new Vector3((float)x[48, 0], (float)x[49, 0], (float)x[50, 0])) /
            Vector3.Distance(
            new Vector3((float)MeanShapeMat[51, 0], (float)MeanShapeMat[52, 0], (float)MeanShapeMat[53, 0]),
            new Vector3((float)MeanShapeMat[48, 0], (float)MeanShapeMat[49, 0], (float)MeanShapeMat[50, 0]));

        Head.localScale = HeadS0 * sHead;

        // Children Scaling Correction
        Head.GetComponentsInChildren<Transform>()[1].localScale = HeadChildrenScale / sHead;


        //******* Neck *******                         
        Transform Neck = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);

        float sNeck = Vector3.Distance(
            new Vector3((float)x[45, 0], (float)x[46, 0], (float)x[47, 0]),
            new Vector3((float)x[42, 0], (float)x[43, 0], (float)x[44, 0])) /
            Vector3.Distance(
            new Vector3((float)MeanShapeMat[45, 0], (float)MeanShapeMat[46, 0], (float)MeanShapeMat[47, 0]),
            new Vector3((float)MeanShapeMat[42, 0], (float)MeanShapeMat[43, 0], (float)MeanShapeMat[44, 0]));

        //Neck.localScale = NeckS0 * sNeck;
        Neck.localPosition = NeckS0 * sNeck;

        // Children Scaling Correction
        Neck.GetComponentsInChildren<Transform>()[1].localScale = NeckChildrenScale / sNeck;



        //******* Pelvis *******                     
        Transform Pelvis = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Hips);
        //Debug.LogFormat("Pelvis Tr: {0}  |  Child: {1}", Pelvis.gameObject.name, Pelvis.GetComponentsInChildren<Transform>()[1].gameObject.name);

        //float sPelvis = (Math.Abs((float)x[4, 0] - (float)x[3, 0])) / (Math.Abs((float)MeanShapeMat[4, 0] - (float)MeanShapeMat[3, 0]));
        float sPelvis = Vector3.Distance(
           new Vector3((float)x[12, 0], (float)x[13, 0], (float)x[14, 0]),
           new Vector3((float)x[9, 0], (float)x[10, 0], (float)x[11, 0])) /
           Vector3.Distance(
           new Vector3((float)MeanShapeMat[12, 0], (float)MeanShapeMat[13, 0], (float)MeanShapeMat[14, 0]),
           new Vector3((float)MeanShapeMat[9, 0], (float)MeanShapeMat[10, 0], (float)MeanShapeMat[11, 0]));

        Pelvis.localScale = PelvisS0 * sPelvis;

        // Children Scaling Correction
        Pelvis.GetComponentsInChildren<Transform>()[1].localScale = PelvisChildrenScale / sPelvis;


        //******* Chest *******                     
        Transform Chest = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest);

        //float sChest = (Math.Abs((float)x[13, 0] - (float)x[12, 0])) / (Math.Abs((float)MeanShapeMat[13, 0] - (float)MeanShapeMat[12, 0]));
        float sChest = Vector3.Distance(
          new Vector3((float)x[39, 0], (float)x[40, 0], (float)x[41, 0]),
          new Vector3((float)x[36, 0], (float)x[37, 0], (float)x[38, 0])) /
          Vector3.Distance(
          new Vector3((float)MeanShapeMat[39, 0], (float)MeanShapeMat[40, 0], (float)MeanShapeMat[41, 0]),
          new Vector3((float)MeanShapeMat[36, 0], (float)MeanShapeMat[37, 0], (float)MeanShapeMat[38, 0]));
        Chest.localScale = ChestS0 * sChest;

        // Children Scaling Correction
        Chest.GetComponentsInChildren<Transform>()[1].localScale = ChestChildrenScale / sChest;


        //******* Arm *******                     
        Transform LeftArm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm);
        Transform RightArm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperArm);

        //float sArm = (Math.Abs((float)x[12, 0] - (float)x[9, 0])) / (Math.Abs((float)MeanShapeMat[12, 0] - (float)MeanShapeMat[9, 0]));
        float sArm = Vector3.Distance(
         new Vector3((float)x[27, 0], (float)x[28, 0], (float)x[29, 0]),
         new Vector3((float)x[36, 0], (float)x[37, 0], (float)x[38, 0])) /
         Vector3.Distance(
         new Vector3((float)MeanShapeMat[27, 0], (float)MeanShapeMat[28, 0], (float)MeanShapeMat[29, 0]),
         new Vector3((float)MeanShapeMat[36, 0], (float)MeanShapeMat[37, 0], (float)MeanShapeMat[38, 0]));
        LeftArm.localScale = armS0 * sArm;
        RightArm.localScale = armS0 * sArm;

        //******* Forearm *******       
        // Children Scaling Correction
        Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm).localScale = forearmS0 / sArm;
        Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerArm).localScale = forearmS0 / sArm;

        Transform LeftForearm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm);
        Transform RightForearm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerArm);

        //float sForearm = (Math.Abs((float)x[9, 0] - (float)x[8, 0])) / (Math.Abs((float)MeanShapeMat[9, 0] - (float)MeanShapeMat[8, 0]));
        float sForearm = Vector3.Distance(
         new Vector3((float)x[27, 0], (float)x[28, 0], (float)x[29, 0]),
         new Vector3((float)x[24, 0], (float)x[25, 0], (float)x[26, 0])) /
         Vector3.Distance(
         new Vector3((float)MeanShapeMat[27, 0], (float)MeanShapeMat[28, 0], (float)MeanShapeMat[29, 0]),
         new Vector3((float)MeanShapeMat[24, 0], (float)MeanShapeMat[25, 0], (float)MeanShapeMat[26, 0]));
        LeftForearm.localScale = forearmS0 * sForearm;
        RightForearm.localScale = forearmS0 * sForearm;


        //******* Femur *******                     
        Transform LeftFemur = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg);
        Transform RightFemur = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperLeg);

        //float sFemur = (Math.Abs((float)x[3, 0] - (float)x[2, 0])) / (Math.Abs((float)MeanShapeMat[3, 0] - (float)MeanShapeMat[2, 0]));
        float sFemur = Vector3.Distance(
            new Vector3((float)x[9, 0], (float)x[10, 0], (float)x[11, 0]),
            new Vector3((float)x[6, 0], (float)x[7, 0], (float)x[8, 0])) /
            Vector3.Distance(
            new Vector3((float)MeanShapeMat[9, 0], (float)MeanShapeMat[10, 0], (float)MeanShapeMat[11, 0]),
            new Vector3((float)MeanShapeMat[6, 0], (float)MeanShapeMat[7, 0], (float)MeanShapeMat[8, 0]));

        LeftFemur.localScale = femurS0 * sFemur;
        RightFemur.localScale = femurS0 * sFemur;


        //******* Tibia *******                     
        // Children Scaling Correction
        Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg).localScale = tibiaS0 / sFemur;
        Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerLeg).localScale = tibiaS0 / sFemur;

        Transform LeftTibia = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg);
        Transform RightTibia = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerLeg);

        //float sTibia = (Math.Abs((float)x[2, 0] - (float)x[1, 0])) / (Math.Abs((float)MeanShapeMat[2, 0] - (float)MeanShapeMat[1, 0]));
        float sTibia = Vector3.Distance(
            new Vector3((float)x[3, 0], (float)x[4, 0], (float)x[5, 0]),
            new Vector3((float)x[6, 0], (float)x[7, 0], (float)x[8, 0])) /
            Vector3.Distance(
            new Vector3((float)MeanShapeMat[3, 0], (float)MeanShapeMat[4, 0], (float)MeanShapeMat[5, 0]),
            new Vector3((float)MeanShapeMat[6, 0], (float)MeanShapeMat[7, 0], (float)MeanShapeMat[8, 0]));

        LeftFemur.localScale = femurS0 * sFemur;
        RightFemur.localScale = femurS0 * sFemur;
        LeftTibia.localScale = tibiaS0 * sTibia;
        RightTibia.localScale = tibiaS0 * sTibia;


        //******* Feet *******                     
        Debug.LogFormat("Foot pos: {0}", Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftFoot).transform.localToWorldMatrix.GetColumn(3));
        Dummy.transform.localPosition += new Vector3(0f, (float)x[1, 0], 0f);

               

        Matrix EndEffectors = Matrix.ZeroMatrix(VirtualEndEffectorsIDs.Count, 3);

        for (int id = 0; id < VirtualEndEffectorsIDs.Count; ++id)
        {
            Transform TargetTr = Dummy.GetComponentsInChildren<Transform>().ToList().Where(obj => obj.name == VirtualEndEffectorsIDs[id]).SingleOrDefault();
            EndEffectors[id, 0] = TargetTr.position.x;
            EndEffectors[id, 1] = TargetTr.position.y;
            EndEffectors[id, 2] = TargetTr.position.z;

            Debug.LogFormat("{0} dist(Virtual end-effectors) < {1:F6} >", TargetTr.gameObject.name, TargetTr.position);
        }

        return EndEffectors;
    }

    public void SSM_SetShapeParameterA_(Matrix _b)
    {
        

        float neckLenght = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head).position);

        float chestLenght = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperArm).position);

        float femurLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg).position);
        float tibiaLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftFoot).position);

        float armLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm).position);
        float forearmLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftHand).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm).position);


        Matrix b = Matrix.ZeroMatrix(MeanShapeMat.rows, 1);
        Matrix x = Matrix.ZeroMatrix(MeanShapeMat.rows, 1);


        // [-3*Sigma, 3*Sigma]
        //double dS = 0.1;
        //int NumberOfSteps = 11;

        // Head
        Transform Head0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);
        Vector3 HeadS0 = Head0.localScale;
        Vector3 HeadChildrenScale = Head0.GetComponentsInChildren<Transform>()[1].localScale;


        // Neck        
        Transform Neck0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);
        //Vector3 NeckS0 = Neck0.localScale;
        Vector3 NeckS0 = Neck0.localPosition;
        Vector3 NeckChildrenScale = Neck0.GetComponentsInChildren<Transform>()[1].localScale;


        // Pelvis        
        Transform Pelvis0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Hips);
        Vector3 PelvisS0 = Pelvis0.localScale;
        Vector3 PelvisChildrenScale = Pelvis0.GetComponentsInChildren<Transform>()[1].localScale;
        Debug.LogFormat("Pelvis: {0}  |  Pelvis Child: {1}", Pelvis0.gameObject.name, Pelvis0.GetComponentsInChildren<Transform>()[1].gameObject.name);


        // Chest
        //Debug.LogFormat("{0}", Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest).gameObject.name);
        Transform Chest0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest);
        Vector3 ChestS0 = Chest0.localScale;
        Vector3 ChestChildrenScale = Chest0.GetComponentsInChildren<Transform>()[1].localScale;

        // Forearm
        Transform ForearmLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm);
        Transform ForearmRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerArm);
        Vector3 forearmS0 = ForearmLeft0.localScale;

        // Arm
        Transform ArmLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm);
        Transform ArmRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperArm);
        Vector3 armS0 = ArmLeft0.localScale;

        // Femur
        Transform FemurLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg);
        Transform FemurRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperLeg);
        Vector3 femurS0 = FemurLeft0.localScale;

        // Tibia
        Transform TibiaLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg);
        Transform TibiaRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerLeg);
        Vector3 tibiaS0 = TibiaLeft0.localScale;


        List<double> shapeParam = new List<double>();
        for (int i = 0; i > -11; --i) shapeParam.Add((double)i / 10);
        for (int i = -9; i < 11; ++i) shapeParam.Add((double)i / 10);
        for (int i = 9; i > 0; --i) shapeParam.Add((double)i / 10);



        for (int i = 0; i < EigValsMat.rows; ++i)
        {
            for (int n = 0; n < b.rows; ++n)
            {
                b[n, 0] = 0;
            }

            double tux = Math.Sqrt(EigValsMat[i, 0]);


            for (int j = 0; j < shapeParam.Count; j++)
            {
                b[i, 0] = 3 * shapeParam[j] * tux;
                x = MeanShapeMat + EigVecsMat * b;

                ShapeParameter.text = String.Format("Mode: {0}\nParameter: {1:F2}", i, b[i, 0]);


                //******* Head *******                         
                Transform Head = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);

                float sHead = Vector3.Distance(
                    new Vector3((float)x[51, 0], (float)x[52, 0], (float)x[53, 0]),
                    new Vector3((float)x[48, 0], (float)x[49, 0], (float)x[50, 0])) /
                    Vector3.Distance(
                    new Vector3((float)MeanShapeMat[51, 0], (float)MeanShapeMat[52, 0], (float)MeanShapeMat[53, 0]),
                    new Vector3((float)MeanShapeMat[48, 0], (float)MeanShapeMat[49, 0], (float)MeanShapeMat[50, 0]));

                Head.localScale = HeadS0 * sHead;

                // Children Scaling Correction
                Head.GetComponentsInChildren<Transform>()[1].localScale = HeadChildrenScale / sHead;


                //******* Neck *******                         
                Transform Neck = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);

                float sNeck = Vector3.Distance(
                    new Vector3((float)x[45, 0], (float)x[46, 0], (float)x[47, 0]),
                    new Vector3((float)x[42, 0], (float)x[43, 0], (float)x[44, 0])) /
                    Vector3.Distance(
                    new Vector3((float)MeanShapeMat[45, 0], (float)MeanShapeMat[46, 0], (float)MeanShapeMat[47, 0]),
                    new Vector3((float)MeanShapeMat[42, 0], (float)MeanShapeMat[43, 0], (float)MeanShapeMat[44, 0]));

                //Neck.localScale = NeckS0 * sNeck;
                Neck.localPosition = NeckS0 * sNeck;

                // Children Scaling Correction
                Neck.GetComponentsInChildren<Transform>()[1].localScale = NeckChildrenScale / sNeck;



                //******* Pelvis *******                     
                Transform Pelvis = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Hips);
                //Debug.LogFormat("Pelvis Tr: {0}  |  Child: {1}", Pelvis.gameObject.name, Pelvis.GetComponentsInChildren<Transform>()[1].gameObject.name);

                //float sPelvis = (Math.Abs((float)x[4, 0] - (float)x[3, 0])) / (Math.Abs((float)MeanShapeMat[4, 0] - (float)MeanShapeMat[3, 0]));
                float sPelvis = Vector3.Distance(
                   new Vector3((float)x[12, 0], (float)x[13, 0], (float)x[14, 0]),
                   new Vector3((float)x[9, 0], (float)x[10, 0], (float)x[11, 0])) /
                   Vector3.Distance(
                   new Vector3((float)MeanShapeMat[12, 0], (float)MeanShapeMat[13, 0], (float)MeanShapeMat[14, 0]),
                   new Vector3((float)MeanShapeMat[9, 0], (float)MeanShapeMat[10, 0], (float)MeanShapeMat[11, 0]));

                Pelvis.localScale = PelvisS0 * sPelvis;

                // Children Scaling Correction
                Pelvis.GetComponentsInChildren<Transform>()[1].localScale = PelvisChildrenScale / sPelvis;


                //******* Chest *******                     
                Transform Chest = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest);

                //float sChest = (Math.Abs((float)x[13, 0] - (float)x[12, 0])) / (Math.Abs((float)MeanShapeMat[13, 0] - (float)MeanShapeMat[12, 0]));
                float sChest = Vector3.Distance(
                  new Vector3((float)x[39, 0], (float)x[40, 0], (float)x[41, 0]),
                  new Vector3((float)x[36, 0], (float)x[37, 0], (float)x[38, 0])) /
                  Vector3.Distance(
                  new Vector3((float)MeanShapeMat[39, 0], (float)MeanShapeMat[40, 0], (float)MeanShapeMat[41, 0]),
                  new Vector3((float)MeanShapeMat[36, 0], (float)MeanShapeMat[37, 0], (float)MeanShapeMat[38, 0]));
                Chest.localScale = ChestS0 * sChest;

                // Children Scaling Correction
                Chest.GetComponentsInChildren<Transform>()[1].localScale = ChestChildrenScale / sChest;


                //******* Arm *******                     
                Transform LeftArm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm);
                Transform RightArm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperArm);

                //float sArm = (Math.Abs((float)x[12, 0] - (float)x[9, 0])) / (Math.Abs((float)MeanShapeMat[12, 0] - (float)MeanShapeMat[9, 0]));
                float sArm = Vector3.Distance(
                 new Vector3((float)x[27, 0], (float)x[28, 0], (float)x[29, 0]),
                 new Vector3((float)x[36, 0], (float)x[37, 0], (float)x[38, 0])) /
                 Vector3.Distance(
                 new Vector3((float)MeanShapeMat[27, 0], (float)MeanShapeMat[28, 0], (float)MeanShapeMat[29, 0]),
                 new Vector3((float)MeanShapeMat[36, 0], (float)MeanShapeMat[37, 0], (float)MeanShapeMat[38, 0]));
                LeftArm.localScale = armS0 * sArm;
                RightArm.localScale = armS0 * sArm;

                //******* Forearm *******       
                // Children Scaling Correction
                Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm).localScale = forearmS0 / sArm;
                Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerArm).localScale = forearmS0 / sArm;

                Transform LeftForearm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm);
                Transform RightForearm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerArm);

                //float sForearm = (Math.Abs((float)x[9, 0] - (float)x[8, 0])) / (Math.Abs((float)MeanShapeMat[9, 0] - (float)MeanShapeMat[8, 0]));
                float sForearm = Vector3.Distance(
                 new Vector3((float)x[27, 0], (float)x[28, 0], (float)x[29, 0]),
                 new Vector3((float)x[24, 0], (float)x[25, 0], (float)x[26, 0])) /
                 Vector3.Distance(
                 new Vector3((float)MeanShapeMat[27, 0], (float)MeanShapeMat[28, 0], (float)MeanShapeMat[29, 0]),
                 new Vector3((float)MeanShapeMat[24, 0], (float)MeanShapeMat[25, 0], (float)MeanShapeMat[26, 0]));
                LeftForearm.localScale = forearmS0 * sForearm;
                RightForearm.localScale = forearmS0 * sForearm;


                //******* Femur *******                     
                Transform LeftFemur = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg);
                Transform RightFemur = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperLeg);

                //float sFemur = (Math.Abs((float)x[3, 0] - (float)x[2, 0])) / (Math.Abs((float)MeanShapeMat[3, 0] - (float)MeanShapeMat[2, 0]));
                float sFemur = Vector3.Distance(
                    new Vector3((float)x[9, 0], (float)x[10, 0], (float)x[11, 0]),
                    new Vector3((float)x[6, 0], (float)x[7, 0], (float)x[8, 0])) /
                    Vector3.Distance(
                    new Vector3((float)MeanShapeMat[9, 0], (float)MeanShapeMat[10, 0], (float)MeanShapeMat[11, 0]),
                    new Vector3((float)MeanShapeMat[6, 0], (float)MeanShapeMat[7, 0], (float)MeanShapeMat[8, 0]));

                LeftFemur.localScale = femurS0 * sFemur;
                RightFemur.localScale = femurS0 * sFemur;


                //******* Tibia *******                     
                // Children Scaling Correction
                Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg).localScale = tibiaS0 / sFemur;
                Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerLeg).localScale = tibiaS0 / sFemur;

                Transform LeftTibia = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg);
                Transform RightTibia = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerLeg);

                //float sTibia = (Math.Abs((float)x[2, 0] - (float)x[1, 0])) / (Math.Abs((float)MeanShapeMat[2, 0] - (float)MeanShapeMat[1, 0]));
                float sTibia = Vector3.Distance(
                    new Vector3((float)x[3, 0], (float)x[4, 0], (float)x[5, 0]),
                    new Vector3((float)x[6, 0], (float)x[7, 0], (float)x[8, 0])) /
                    Vector3.Distance(
                    new Vector3((float)MeanShapeMat[3, 0], (float)MeanShapeMat[4, 0], (float)MeanShapeMat[5, 0]),
                    new Vector3((float)MeanShapeMat[6, 0], (float)MeanShapeMat[7, 0], (float)MeanShapeMat[8, 0]));

                LeftFemur.localScale = femurS0 * sFemur;
                RightFemur.localScale = femurS0 * sFemur;
                LeftTibia.localScale = tibiaS0 * sTibia;
                RightTibia.localScale = tibiaS0 * sTibia;


                //******* Feet *******                     



                Debug.LogFormat("Foot pos: {0}", Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftFoot).transform.localToWorldMatrix.GetColumn(3));



                Dummy.transform.localPosition += new Vector3(0f, (float)x[1, 0], 0f);
            }

        }


        /*
         * 
         * 
        _scalar = -3:0.1:3;
        frame = 0;
        gcf2 = figure('Position',[ 406    244   1257    933]); 
        for i=1:length(Lambda)
        %[-3 * Sigma, 3Sigma]
        for j = 1:length(_scalar)
        x = [];
        xy = [];
        b = zeros(size(MeanShape,1),1);
        b(i) = _scalar(j);		
        x = MeanShape + V * b .* sqrt(Lambda(i));		

        */


    }


    public void SSM_SetShapeParameterTux(Matrix _b)
    {
        

        float neckLenght = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head).position);

        float chestLenght = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperArm).position);

        float femurLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg).position);
        float tibiaLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftFoot).position);

        float armLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm).position);
        float forearmLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftHand).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm).position);


        Matrix b = Matrix.ZeroMatrix(MeanShapeMat.rows, 1);
        Matrix x = Matrix.ZeroMatrix(MeanShapeMat.rows, 1);


        // [-3*Sigma, 3*Sigma]
        //double dS = 0.1;
        //int NumberOfSteps = 11;

        // Head
        Transform Head0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);
        Vector3 HeadS0 = Head0.localScale;
        Vector3 HeadChildrenScale = Head0.GetComponentsInChildren<Transform>()[1].localScale;


        // Neck        
        Transform Neck0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);
        //Vector3 NeckS0 = Neck0.localScale;
        Vector3 NeckS0 = Neck0.localPosition;
        Vector3 NeckChildrenScale = Neck0.GetComponentsInChildren<Transform>()[1].localScale;


        // Pelvis        
        Transform Pelvis0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Hips);
        Vector3 PelvisS0 = Pelvis0.localScale;
        Vector3 PelvisChildrenScale = Pelvis0.GetComponentsInChildren<Transform>()[1].localScale;
        Debug.LogFormat("Pelvis: {0}  |  Pelvis Child: {1}", Pelvis0.gameObject.name, Pelvis0.GetComponentsInChildren<Transform>()[1].gameObject.name);


        // Chest
        //Debug.LogFormat("{0}", Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest).gameObject.name);
        Transform Chest0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest);
        Vector3 ChestS0 = Chest0.localScale;
        Vector3 ChestChildrenScale = Chest0.GetComponentsInChildren<Transform>()[1].localScale;

        // Forearm
        Transform ForearmLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm);
        Transform ForearmRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerArm);
        Vector3 forearmS0 = ForearmLeft0.localScale;

        // Arm
        Transform ArmLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm);
        Transform ArmRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperArm);
        Vector3 armS0 = ArmLeft0.localScale;

        // Femur
        Transform FemurLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg);
        Transform FemurRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperLeg);
        Vector3 femurS0 = FemurLeft0.localScale;

        // Tibia
        Transform TibiaLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg);
        Transform TibiaRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerLeg);
        Vector3 tibiaS0 = TibiaLeft0.localScale;


        List<double> shapeParam = new List<double>();
        for (int i = 0; i > -11; --i) shapeParam.Add((double)i / 10);
        for (int i = -9; i < 11; ++i) shapeParam.Add((double)i / 10);
        for (int i = 9; i > 0; --i) shapeParam.Add((double)i / 10);


        //b[i, 0] = 3 * shapeParam[j] * tux;                
        x = MeanShapeMat + EigVecsMat * _b;
        ShapeParameter.text = String.Format("Mode: {0}\nParameter: {1:F2}", 0, _b[0, 0]);
        //ShapeParameters.text = String.Format("Mode: {0}\nParameter: {1:F2}", i, b[i, 0]);


        //******* Head *******                         
        Transform Head = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);

        float sHead = Vector3.Distance(
            new Vector3((float)x[51, 0], (float)x[52, 0], (float)x[53, 0]),
            new Vector3((float)x[48, 0], (float)x[49, 0], (float)x[50, 0])) /
            Vector3.Distance(
            new Vector3((float)MeanShapeMat[51, 0], (float)MeanShapeMat[52, 0], (float)MeanShapeMat[53, 0]),
            new Vector3((float)MeanShapeMat[48, 0], (float)MeanShapeMat[49, 0], (float)MeanShapeMat[50, 0]));

        Head.localScale = HeadS0 * sHead;

        // Children Scaling Correction
        Head.GetComponentsInChildren<Transform>()[1].localScale = HeadChildrenScale / sHead;


        //******* Neck *******                         
        Transform Neck = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);

        float sNeck = Vector3.Distance(
            new Vector3((float)x[45, 0], (float)x[46, 0], (float)x[47, 0]),
            new Vector3((float)x[42, 0], (float)x[43, 0], (float)x[44, 0])) /
            Vector3.Distance(
            new Vector3((float)MeanShapeMat[45, 0], (float)MeanShapeMat[46, 0], (float)MeanShapeMat[47, 0]),
            new Vector3((float)MeanShapeMat[42, 0], (float)MeanShapeMat[43, 0], (float)MeanShapeMat[44, 0]));

        //Neck.localScale = NeckS0 * sNeck;
        Neck.localPosition = NeckS0 * sNeck;

        // Children Scaling Correction
        Neck.GetComponentsInChildren<Transform>()[1].localScale = NeckChildrenScale / sNeck;



        //******* Pelvis *******                     
        Transform Pelvis = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Hips);
        //Debug.LogFormat("Pelvis Tr: {0}  |  Child: {1}", Pelvis.gameObject.name, Pelvis.GetComponentsInChildren<Transform>()[1].gameObject.name);

        //float sPelvis = (Math.Abs((float)x[4, 0] - (float)x[3, 0])) / (Math.Abs((float)MeanShapeMat[4, 0] - (float)MeanShapeMat[3, 0]));
        float sPelvis = Vector3.Distance(
           new Vector3((float)x[12, 0], (float)x[13, 0], (float)x[14, 0]),
           new Vector3((float)x[9, 0], (float)x[10, 0], (float)x[11, 0])) /
           Vector3.Distance(
           new Vector3((float)MeanShapeMat[12, 0], (float)MeanShapeMat[13, 0], (float)MeanShapeMat[14, 0]),
           new Vector3((float)MeanShapeMat[9, 0], (float)MeanShapeMat[10, 0], (float)MeanShapeMat[11, 0]));

        Pelvis.localScale = PelvisS0 * sPelvis;

        // Children Scaling Correction
        Pelvis.GetComponentsInChildren<Transform>()[1].localScale = PelvisChildrenScale / sPelvis;


        //******* Chest *******                     
        Transform Chest = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest);

        //float sChest = (Math.Abs((float)x[13, 0] - (float)x[12, 0])) / (Math.Abs((float)MeanShapeMat[13, 0] - (float)MeanShapeMat[12, 0]));
        float sChest = Vector3.Distance(
          new Vector3((float)x[39, 0], (float)x[40, 0], (float)x[41, 0]),
          new Vector3((float)x[36, 0], (float)x[37, 0], (float)x[38, 0])) /
          Vector3.Distance(
          new Vector3((float)MeanShapeMat[39, 0], (float)MeanShapeMat[40, 0], (float)MeanShapeMat[41, 0]),
          new Vector3((float)MeanShapeMat[36, 0], (float)MeanShapeMat[37, 0], (float)MeanShapeMat[38, 0]));
        Chest.localScale = ChestS0 * sChest;

        // Children Scaling Correction
        Chest.GetComponentsInChildren<Transform>()[1].localScale = ChestChildrenScale / sChest;


        //******* Arm *******                     
        Transform LeftArm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm);
        Transform RightArm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperArm);

        //float sArm = (Math.Abs((float)x[12, 0] - (float)x[9, 0])) / (Math.Abs((float)MeanShapeMat[12, 0] - (float)MeanShapeMat[9, 0]));
        float sArm = Vector3.Distance(
         new Vector3((float)x[27, 0], (float)x[28, 0], (float)x[29, 0]),
         new Vector3((float)x[36, 0], (float)x[37, 0], (float)x[38, 0])) /
         Vector3.Distance(
         new Vector3((float)MeanShapeMat[27, 0], (float)MeanShapeMat[28, 0], (float)MeanShapeMat[29, 0]),
         new Vector3((float)MeanShapeMat[36, 0], (float)MeanShapeMat[37, 0], (float)MeanShapeMat[38, 0]));
        LeftArm.localScale = armS0 * sArm;
        RightArm.localScale = armS0 * sArm;

        //******* Forearm *******       
        // Children Scaling Correction
        Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm).localScale = forearmS0 / sArm;
        Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerArm).localScale = forearmS0 / sArm;

        Transform LeftForearm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm);
        Transform RightForearm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerArm);

        //float sForearm = (Math.Abs((float)x[9, 0] - (float)x[8, 0])) / (Math.Abs((float)MeanShapeMat[9, 0] - (float)MeanShapeMat[8, 0]));
        float sForearm = Vector3.Distance(
         new Vector3((float)x[27, 0], (float)x[28, 0], (float)x[29, 0]),
         new Vector3((float)x[24, 0], (float)x[25, 0], (float)x[26, 0])) /
         Vector3.Distance(
         new Vector3((float)MeanShapeMat[27, 0], (float)MeanShapeMat[28, 0], (float)MeanShapeMat[29, 0]),
         new Vector3((float)MeanShapeMat[24, 0], (float)MeanShapeMat[25, 0], (float)MeanShapeMat[26, 0]));
        LeftForearm.localScale = forearmS0 * sForearm;
        RightForearm.localScale = forearmS0 * sForearm;


        //******* Femur *******                     
        Transform LeftFemur = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg);
        Transform RightFemur = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperLeg);

        //float sFemur = (Math.Abs((float)x[3, 0] - (float)x[2, 0])) / (Math.Abs((float)MeanShapeMat[3, 0] - (float)MeanShapeMat[2, 0]));
        float sFemur = Vector3.Distance(
            new Vector3((float)x[9, 0], (float)x[10, 0], (float)x[11, 0]),
            new Vector3((float)x[6, 0], (float)x[7, 0], (float)x[8, 0])) /
            Vector3.Distance(
            new Vector3((float)MeanShapeMat[9, 0], (float)MeanShapeMat[10, 0], (float)MeanShapeMat[11, 0]),
            new Vector3((float)MeanShapeMat[6, 0], (float)MeanShapeMat[7, 0], (float)MeanShapeMat[8, 0]));

        LeftFemur.localScale = femurS0 * sFemur;
        RightFemur.localScale = femurS0 * sFemur;


        //******* Tibia *******                     
        // Children Scaling Correction
        Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg).localScale = tibiaS0 / sFemur;
        Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerLeg).localScale = tibiaS0 / sFemur;

        Transform LeftTibia = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg);
        Transform RightTibia = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerLeg);

        //float sTibia = (Math.Abs((float)x[2, 0] - (float)x[1, 0])) / (Math.Abs((float)MeanShapeMat[2, 0] - (float)MeanShapeMat[1, 0]));
        float sTibia = Vector3.Distance(
            new Vector3((float)x[3, 0], (float)x[4, 0], (float)x[5, 0]),
            new Vector3((float)x[6, 0], (float)x[7, 0], (float)x[8, 0])) /
            Vector3.Distance(
            new Vector3((float)MeanShapeMat[3, 0], (float)MeanShapeMat[4, 0], (float)MeanShapeMat[5, 0]),
            new Vector3((float)MeanShapeMat[6, 0], (float)MeanShapeMat[7, 0], (float)MeanShapeMat[8, 0]));

        LeftFemur.localScale = femurS0 * sFemur;
        RightFemur.localScale = femurS0 * sFemur;
        LeftTibia.localScale = tibiaS0 * sTibia;
        RightTibia.localScale = tibiaS0 * sTibia;


        //******* Feet *******                     



        Debug.LogFormat("Foot pos: {0}", Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftFoot).transform.localToWorldMatrix.GetColumn(3));



        Dummy.transform.localPosition += new Vector3(0f, (float)x[1, 0], 0f);

        /*
         * 
         * 
        _scalar = -3:0.1:3;
        frame = 0;
        gcf2 = figure('Position',[ 406    244   1257    933]); 
        for i=1:length(Lambda)
        %[-3 * Sigma, 3Sigma]
        for j = 1:length(_scalar)
        x = [];
        xy = [];
        b = zeros(size(MeanShape,1),1);
        b(i) = _scalar(j);		
        x = MeanShape + V * b .* sqrt(Lambda(i));		

        */


    }

    public IEnumerator SSM_SetShapeParameterRandom(float waitTime)
    {

        /*
        GameObject p0 = GameObject.CreatePrimitive(PrimitiveType.Sphere) as GameObject;
        p0.transform.position = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftFoot).position;
        p0.transform.localScale = Vector3.one / 5f;




        GameObject p2 = GameObject.CreatePrimitive(PrimitiveType.Sphere) as GameObject;
        p2.transform.position = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg).position;
        p2.transform.localScale = Vector3.one / 5f;


        GameObject p1 = GameObject.CreatePrimitive(PrimitiveType.Sphere) as GameObject;
        p1.transform.position = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg).position;
        //p2.transform.localScale = 
        */

        //Vector3 pos = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg).worldToLocalMatrix.MultiplyPoint( Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg).position);
        //Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg).transform.position = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg).transform.position;


        //Vector3 pos = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg).root.worldToLocalMatrix.MultiplyPoint(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg).position);


        /*
        GameObject p0 = GameObject.CreatePrimitive(PrimitiveType.Sphere) as GameObject;
        p0.transform.position = pos;
        p0.transform.localScale = Vector3.one / 8f;


        GameObject p1 = GameObject.CreatePrimitive(PrimitiveType.Sphere) as GameObject;
        p1.GetComponent<Renderer>().material.color = Color.red;
        p1.transform.position = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg).localToWorldMatrix.MultiplyPoint(new Vector3(femurLength*0.2f,0f,0f));
        p1.transform.localScale = Vector3.one / 8f;

        Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg).transform.position = p1.transform.position;
        */

        /*
         *  upperArm = animator.GetBoneTransform(HumanBodyBones.LeftUpperArm);
            forearm = animator.GetBoneTransform(HumanBodyBones.LeftLowerArm);
            hand = animator.GetBoneTransform(HumanBodyBones.LeftHand);

        float upperArmLength = Vector3.Distance(upperArm.position, forearm.position);
        float forearmLength = Vector3.Distance(forearm.position, hand.position);
        length = upperArmLength + forearmLength;
        */

        

        float neckLenght = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head).position);

        float chestLenght = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperArm).position);

        float femurLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg).position);
        float tibiaLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftFoot).position);

        float armLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm).position);
        float forearmLength = Vector3.Distance(Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftHand).position, Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm).position);


        //Debug.LogFormat("LeftHand: {0}\n", Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftHand).gameObject.name);

        Matrix b = Matrix.ZeroMatrix(MeanShapeMat.rows, 1);
        Matrix x = Matrix.ZeroMatrix(MeanShapeMat.rows, 1);

        // [-3*Sigma, 3*Sigma]
        //double dS = 0.1;
        //int NumberOfSteps = 11;

        // Head
        Transform Head0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);
        Vector3 HeadS0 = Head0.localScale;
        Vector3 HeadChildrenScale = Head0.GetComponentsInChildren<Transform>()[1].localScale;


        // Neck        
        Transform Neck0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);
        //Vector3 NeckS0 = Neck0.localScale;
        Vector3 NeckS0 = Neck0.localPosition;
        Vector3 NeckChildrenScale = Neck0.GetComponentsInChildren<Transform>()[1].localScale;


        // Pelvis        
        Transform Pelvis0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Hips);
        Vector3 PelvisS0 = Pelvis0.localScale;
        Vector3 PelvisChildrenScale = Pelvis0.GetComponentsInChildren<Transform>()[1].localScale;
        Debug.LogFormat("Pelvis: {0}  |  Pelvis Child: {1}", Pelvis0.gameObject.name, Pelvis0.GetComponentsInChildren<Transform>()[1].gameObject.name);


        // Chest
        //Debug.LogFormat("{0}", Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest).gameObject.name);
        Transform Chest0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest);
        Vector3 ChestS0 = Chest0.localScale;
        Vector3 ChestChildrenScale = Chest0.GetComponentsInChildren<Transform>()[1].localScale;

        // Forearm
        Transform ForearmLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm);
        Transform ForearmRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerArm);
        Vector3 forearmS0 = ForearmLeft0.localScale;

        // Arm
        Transform ArmLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm);
        Transform ArmRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperArm);
        Vector3 armS0 = ArmLeft0.localScale;

        // Femur
        Transform FemurLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg);
        Transform FemurRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperLeg);
        Vector3 femurS0 = FemurLeft0.localScale;

        // Tibia
        Transform TibiaLeft0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg);
        Transform TibiaRight0 = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerLeg);
        Vector3 tibiaS0 = TibiaLeft0.localScale;


        List<double> shapeParam = new List<double>();
        for (int i = 0; i > -11; --i) shapeParam.Add((double)i / 10);
        for (int i = -9; i < 11; ++i) shapeParam.Add((double)i / 10);
        for (int i = 9; i > 0; --i) shapeParam.Add((double)i / 10);



        //for (int i = 0; i < EigValsMat.rows; ++i)
        for (int i = 0; i < 2; ++i)
        {
            for (int n = 0; n < b.rows; ++n)
            {
                b[n, 0] = 0;
            }

            double tux = Math.Sqrt(EigValsMat[i, 0]);

            if (Math.Abs(tux) > 1e-6)
            {
                for (int j = 0; j < shapeParam.Count; j++)
                {
                    //b[i, 0] = 3 * shapeParam[j] * tux;
                    b[i, 0] = 2 * shapeParam[j] * tux;
                    x = MeanShapeMat + EigVecsMat * b;

                    ShapeParameter.text = String.Format("Mode: {0}\nParameter: {1:F2}", i, b[i, 0]);


                    //******* Head *******                         
                    Transform Head = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);

                    float sHead = Vector3.Distance(
                        new Vector3((float)x[51, 0], (float)x[52, 0], (float)x[53, 0]),
                        new Vector3((float)x[48, 0], (float)x[49, 0], (float)x[50, 0])) /
                        Vector3.Distance(
                        new Vector3((float)MeanShapeMat[51, 0], (float)MeanShapeMat[52, 0], (float)MeanShapeMat[53, 0]),
                        new Vector3((float)MeanShapeMat[48, 0], (float)MeanShapeMat[49, 0], (float)MeanShapeMat[50, 0]));

                    Head.localScale = HeadS0 * sHead;

                    // Children Scaling Correction
                    Head.GetComponentsInChildren<Transform>()[1].localScale = HeadChildrenScale / sHead;


                    //******* Neck *******                         
                    Transform Neck = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Head);

                    float sNeck = Vector3.Distance(
                        new Vector3((float)x[45, 0], (float)x[46, 0], (float)x[47, 0]),
                        new Vector3((float)x[42, 0], (float)x[43, 0], (float)x[44, 0])) /
                        Vector3.Distance(
                        new Vector3((float)MeanShapeMat[45, 0], (float)MeanShapeMat[46, 0], (float)MeanShapeMat[47, 0]),
                        new Vector3((float)MeanShapeMat[42, 0], (float)MeanShapeMat[43, 0], (float)MeanShapeMat[44, 0]));

                    //Neck.localScale = NeckS0 * sNeck;
                    Neck.localPosition = NeckS0 * sNeck;

                    // Children Scaling Correction
                    Neck.GetComponentsInChildren<Transform>()[1].localScale = NeckChildrenScale / sNeck;



                    //******* Pelvis *******                     
                    Transform Pelvis = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Hips);
                    //Debug.LogFormat("Pelvis Tr: {0}  |  Child: {1}", Pelvis.gameObject.name, Pelvis.GetComponentsInChildren<Transform>()[1].gameObject.name);

                    //float sPelvis = (Math.Abs((float)x[4, 0] - (float)x[3, 0])) / (Math.Abs((float)MeanShapeMat[4, 0] - (float)MeanShapeMat[3, 0]));
                    float sPelvis = Vector3.Distance(
                       new Vector3((float)x[12, 0], (float)x[13, 0], (float)x[14, 0]),
                       new Vector3((float)x[9, 0], (float)x[10, 0], (float)x[11, 0])) /
                       Vector3.Distance(
                       new Vector3((float)MeanShapeMat[12, 0], (float)MeanShapeMat[13, 0], (float)MeanShapeMat[14, 0]),
                       new Vector3((float)MeanShapeMat[9, 0], (float)MeanShapeMat[10, 0], (float)MeanShapeMat[11, 0]));

                    Pelvis.localScale = PelvisS0 * sPelvis;

                    // Children Scaling Correction
                    Pelvis.GetComponentsInChildren<Transform>()[1].localScale = PelvisChildrenScale / sPelvis;


                    //******* Chest *******                     
                    Transform Chest = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.Chest);

                    //float sChest = (Math.Abs((float)x[13, 0] - (float)x[12, 0])) / (Math.Abs((float)MeanShapeMat[13, 0] - (float)MeanShapeMat[12, 0]));
                    float sChest = Vector3.Distance(
                      new Vector3((float)x[39, 0], (float)x[40, 0], (float)x[41, 0]),
                      new Vector3((float)x[36, 0], (float)x[37, 0], (float)x[38, 0])) /
                      Vector3.Distance(
                      new Vector3((float)MeanShapeMat[39, 0], (float)MeanShapeMat[40, 0], (float)MeanShapeMat[41, 0]),
                      new Vector3((float)MeanShapeMat[36, 0], (float)MeanShapeMat[37, 0], (float)MeanShapeMat[38, 0]));
                    Chest.localScale = ChestS0 * sChest;

                    // Children Scaling Correction
                    Chest.GetComponentsInChildren<Transform>()[1].localScale = ChestChildrenScale / sChest;


                    //******* Arm *******                     
                    Transform LeftArm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperArm);
                    Transform RightArm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperArm);

                    //float sArm = (Math.Abs((float)x[12, 0] - (float)x[9, 0])) / (Math.Abs((float)MeanShapeMat[12, 0] - (float)MeanShapeMat[9, 0]));
                    float sArm = Vector3.Distance(
                     new Vector3((float)x[27, 0], (float)x[28, 0], (float)x[29, 0]),
                     new Vector3((float)x[36, 0], (float)x[37, 0], (float)x[38, 0])) /
                     Vector3.Distance(
                     new Vector3((float)MeanShapeMat[27, 0], (float)MeanShapeMat[28, 0], (float)MeanShapeMat[29, 0]),
                     new Vector3((float)MeanShapeMat[36, 0], (float)MeanShapeMat[37, 0], (float)MeanShapeMat[38, 0]));
                    LeftArm.localScale = armS0 * sArm;
                    RightArm.localScale = armS0 * sArm;

                    //******* Forearm *******       
                    // Children Scaling Correction
                    Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm).localScale = forearmS0 / sArm;
                    Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerArm).localScale = forearmS0 / sArm;

                    Transform LeftForearm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerArm);
                    Transform RightForearm = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerArm);

                    //float sForearm = (Math.Abs((float)x[9, 0] - (float)x[8, 0])) / (Math.Abs((float)MeanShapeMat[9, 0] - (float)MeanShapeMat[8, 0]));
                    float sForearm = Vector3.Distance(
                     new Vector3((float)x[27, 0], (float)x[28, 0], (float)x[29, 0]),
                     new Vector3((float)x[24, 0], (float)x[25, 0], (float)x[26, 0])) /
                     Vector3.Distance(
                     new Vector3((float)MeanShapeMat[27, 0], (float)MeanShapeMat[28, 0], (float)MeanShapeMat[29, 0]),
                     new Vector3((float)MeanShapeMat[24, 0], (float)MeanShapeMat[25, 0], (float)MeanShapeMat[26, 0]));
                    LeftForearm.localScale = forearmS0 * sForearm;
                    RightForearm.localScale = forearmS0 * sForearm;


                    //******* Femur *******                     
                    Transform LeftFemur = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftUpperLeg);
                    Transform RightFemur = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightUpperLeg);

                    //float sFemur = (Math.Abs((float)x[3, 0] - (float)x[2, 0])) / (Math.Abs((float)MeanShapeMat[3, 0] - (float)MeanShapeMat[2, 0]));
                    float sFemur = Vector3.Distance(
                        new Vector3((float)x[9, 0], (float)x[10, 0], (float)x[11, 0]),
                        new Vector3((float)x[6, 0], (float)x[7, 0], (float)x[8, 0])) /
                        Vector3.Distance(
                        new Vector3((float)MeanShapeMat[9, 0], (float)MeanShapeMat[10, 0], (float)MeanShapeMat[11, 0]),
                        new Vector3((float)MeanShapeMat[6, 0], (float)MeanShapeMat[7, 0], (float)MeanShapeMat[8, 0]));

                    LeftFemur.localScale = femurS0 * sFemur;
                    RightFemur.localScale = femurS0 * sFemur;


                    //******* Tibia *******                     
                    // Children Scaling Correction
                    Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg).localScale = tibiaS0 / sFemur;
                    Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerLeg).localScale = tibiaS0 / sFemur;

                    Transform LeftTibia = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftLowerLeg);
                    Transform RightTibia = Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.RightLowerLeg);

                    //float sTibia = (Math.Abs((float)x[2, 0] - (float)x[1, 0])) / (Math.Abs((float)MeanShapeMat[2, 0] - (float)MeanShapeMat[1, 0]));
                    float sTibia = Vector3.Distance(
                        new Vector3((float)x[3, 0], (float)x[4, 0], (float)x[5, 0]),
                        new Vector3((float)x[6, 0], (float)x[7, 0], (float)x[8, 0])) /
                        Vector3.Distance(
                        new Vector3((float)MeanShapeMat[3, 0], (float)MeanShapeMat[4, 0], (float)MeanShapeMat[5, 0]),
                        new Vector3((float)MeanShapeMat[6, 0], (float)MeanShapeMat[7, 0], (float)MeanShapeMat[8, 0]));

                    LeftFemur.localScale = femurS0 * sFemur;
                    RightFemur.localScale = femurS0 * sFemur;
                    LeftTibia.localScale = tibiaS0 * sTibia;
                    RightTibia.localScale = tibiaS0 * sTibia;


                    //******* Feet *******                     



                    Debug.LogFormat("Foot pos: {0}", Dummy.GetComponent<Animator>().GetBoneTransform(HumanBodyBones.LeftFoot).transform.localToWorldMatrix.GetColumn(3));



                    Dummy.transform.localPosition += new Vector3(0f, (float)x[1, 0], 0f);


                    yield return new WaitForSeconds(waitTime);
                }
            }
            else
            {
                _isCoroutineShapeParameter = false;
                StopCoroutine(coroutineShapeParameter);
                i = -1;
                yield return new WaitForSeconds(waitTime);
            }
        }


        /*
         * 
         * 
        _scalar = -3:0.1:3;
        frame = 0;
        gcf2 = figure('Position',[ 406    244   1257    933]); 
        for i=1:length(Lambda)
        %[-3 * Sigma, 3Sigma]
        for j = 1:length(_scalar)
        x = [];
        xy = [];
        b = zeros(size(MeanShape,1),1);
        b(i) = _scalar(j);		
        x = MeanShape + V * b .* sqrt(Lambda(i));		

        */


    }





}
