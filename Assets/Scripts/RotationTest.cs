﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationTest : MonoBehaviour {

    public GameObject RefGO;
    GameObject Cube0, Cube1;

	// Use this for initialization
	void Start () {

        Cube0 = GameObject.CreatePrimitive(PrimitiveType.Cube) as GameObject;

        Cube0.name = "Cube0";
        Cube0.GetComponent<Renderer>().material.color = Color.red;       
        Cube0.transform.position = Vector3.zero;
        Cube0.transform.rotation = Quaternion.identity;

        //Cube0.transform.rotation = Quaternion.LookRotation(new Vector3(0f, 0f, 1f), new Vector3(0f, 1f, 0f));


        //Cube0.transform.TransformDirection();


        Cube1 = GameObject.CreatePrimitive(PrimitiveType.Cube) as GameObject;
        Cube1.name = "Cube1";
        Cube1.GetComponent<Renderer>().material.color = Color.green;
        Cube1.transform.parent = Cube0.transform;
        Cube1.transform.localPosition = new Vector3(2f, 0f, 0f);// Vector3.zero;
        Cube1.transform.localRotation = Quaternion.LookRotation(new Vector3(0f, 0f, 1f), new Vector3(0f, 1f, 0f));
        //Cube1.transform.localRotation = Quaternion.identity;



    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKey(KeyCode.UpArrow))
        {
            Cube0.transform.parent = RefGO.transform;
            Cube0.transform.localRotation = Quaternion.identity;
            Cube0.transform.localPosition = Vector3.zero;


            Cube1.transform.localRotation = Quaternion.Euler(90f,90f,0f);
        }
        else
        {
            float _angle = Mathf.Cos(Mathf.PI * Time.realtimeSinceStartup) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.Euler(_angle, 0f, 0f);
            Cube0.transform.rotation = q;
        }



        //Cube1.transform.localRotation = Quaternion.Inverse(q) * Quaternion.Euler(0f,_angle,0f);
        //Cube1.transform.localRotation = Quaternion.Inverse(q);// * Quaternion.LookRotation(new Vector3(1f,0f,0f), new Vector3(0f,1f,0f)) * Quaternion.Euler(_angle, 0f, 0f);



        //Cube1.transform.localRotation = q;

        //Debug.LogFormat("Local: {0}  |  {1}", Cube1.transform.localRotation, Cube1.transform.rotation);

        Debug.LogFormat("Look Rot.: {0} ",
        Quaternion.LookRotation(new Vector3(0f, 0f, 1f), new Vector3(0f, 1f, 0f)));



    }
}
