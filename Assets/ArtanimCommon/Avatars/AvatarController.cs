﻿using UnityEngine;
using System.Collections;
using Artanim.Location.Data;
using Artanim.Vicon;
using System;
using System.Linq;
using Artanim.Location.Messages;
using Artanim.Location.Network;
using System.Collections.Generic;

namespace Artanim
{

	/// <summary>
	/// Main controller for an actor.
	/// This class is responsible to init all actor and skeleton based functionality of an actor.
	/// </summary>
	[RequireComponent(typeof(IKListener))]
	[AddComponentMenu("Artanim/Avatar Controller")]
	public class AvatarController : MonoBehaviour
	{
		//PlayerId is not used here but used in Real Virtuality 2 to store the player state
		public Guid PlayerId
		{
			get
			{
				return Player != null ? Player.ComponentId : Guid.Empty;
			}
		}

		public Guid SkeletonId
		{
			get
			{
				return Player != null ? Player.SkeletonId : Guid.Empty;
			}
		}

		public string Initials
		{
			get
			{
				return Player != null ? Player.Initials : string.Empty;
			}
		}

		public Transform HeadBone;

		[HideInInspector]
		public GameObject wheelChair;
		public GameObject wheelChairTemplate;

		[Header("Rig Calibration")]
		[Tooltip("")]
		public float BackpackToPelvisOffset = 0.14f;

		[Tooltip("")]
		public float DefaultArmStretchFactor = 1.0f;

		[Tooltip("")]
		public Vector3 HeadTargetOffset = new Vector3(0f, -0.07f, -0.20f);

		[Tooltip("")]
		public Vector3 HandTargetOffset = new Vector3(0f, -0.04f, 0.06f);

		/// <summary>
		/// True if the avatar is the clients main player.
		/// </summary>
		public bool IsMainPlayer { get; private set; }


		private IKListener _ikListener;
		private IKListener IKListener
		{
			get
			{
				if (_ikListener == null)
					_ikListener = GetComponent<IKListener>();
				return _ikListener;
			}
		}
		
		private bool ShowAvatarOnNextUpdate = false;
		private Player Player;

		private AvatarDisplayController _AvatarDisplayController;
		private AvatarDisplayController AvatarDisplayController
		{
			get
			{
				if (!_AvatarDisplayController)
				{
					_AvatarDisplayController = GetComponent<AvatarDisplayController>();
					if (!_AvatarDisplayController)
						Debug.LogErrorFormat("Avatar {0} is not setup correctly. No AvatarDisplayController found. Add an implementation of AvatarDisplayController to the root of the avatar.", name);
				}
				return _AvatarDisplayController;
			}
		}

		private List<BoneTransform> InitialBones;
		private bool Initialized;

		private AvatarBodyPart[] BodyParts;

		void Awake()
		{
			//Hide avatar
			//AvatarDisplayController.HideAvatar();
			BodyParts = FindAvatarBodyParts();
		}

		private void OnDestroy()
		{
			if (Player != null)
			{
				//Detach player state update
				Player.PropertyChanged -= Player_PropertyChanged;
			}
		}

		#region Public interface

		/// <summary>
		/// 
		/// </summary>
		public void SetAsMainPlayer()
		{
			IsMainPlayer = true;
			InitAvatarVisuals();
		}

		/// <summary>
		/// 
		/// </summary>
		public void SetAsPassivePlayer()
		{
			IsMainPlayer = false;
			InitAvatarVisuals();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="playerId"></param>
		/// <param name="skeletonId"></param>
		/// <param name="isMainPlayer"></param>
		/// <param name="showAvatar"></param>
		public void InitAvatar(Player player, bool isMainPlayer, bool showAvatar = false)
		{
			if(player != null)
			{
				//Detach player state update if needed
				if(Player != null)
				{
					Player.PropertyChanged -= Player_PropertyChanged;
				}
				
				IsMainPlayer = isMainPlayer;
				Player = player;

				//Attach player state udpates
				Player.PropertyChanged += Player_PropertyChanged;

				//Init IK listener
				IKListener.Init(SkeletonId, isMainPlayer);

				InitAvatarVisuals();

				ShowAvatar(showAvatar);

				Initialized = true;
			}
		}

		public void InitWheelChair(EPlayerStatus playerStatus, ECalibrationMode calibrationMode)
		{
			// Instantiate wheel chair template
			if (wheelChairTemplate)
			{
				wheelChair = Instantiate(wheelChairTemplate, transform);

				// Make the wheel chair root follow the avatar root transform but ignores it's scale
				FollowTransform ft = wheelChair.AddComponent<FollowTransform>();
				ft.target = GetAvatarRoot();
				ft.UpdatePosition = true;
				ft.UpdateRotation = true;
				ft.UpdateScale = false;

				if (wheelChair)
					wheelChair.SetActive(playerStatus == EPlayerStatus.Calibrated); //Show wheel chair if player is already calibrated.
				else
					Debug.LogErrorFormat("<color=lightblue>Failed to create wheel chair. Could not instantiate wheel chair resource={0}</color>", wheelChair);
			}
			else
				Debug.LogErrorFormat("<color=lightblue>Failed to create wheel chair. Could not find a wheel chair template</color>");

			// [TODO] Handle here specific changes between TrackedWheelChair and UserWheelChair
			switch (calibrationMode)
			{
				case (ECalibrationMode.TrackedWheelchair):
					break;
				case (ECalibrationMode.UserWheelchair):
					break;
				default:
					Debug.LogWarning("Unrecognized wheel chair calibration mode");
					return;
			}

		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="skeleton"></param>
		/// <param name="frameNumber"></param>
		public void UpdateSkeleton(SkeletonTransforms skeleton, uint frameNumber)
		{
			if (Initialized && Player != null && Player.Status == EPlayerStatus.Calibrated)
			{
				IKListener.UpdateSkeleton(skeleton, frameNumber);

				//Show avatar is needed
				if (ShowAvatarOnNextUpdate)
				{
					AvatarDisplayController.ShowAvatar();

					// Add wheel chair if requested
					if (!wheelChair)
					{
						if (Player.CalibrationMode == ECalibrationMode.TrackedWheelchair || Player.CalibrationMode == ECalibrationMode.UserWheelchair)
							InitWheelChair(Player.Status, Player.CalibrationMode);
					}
					if (wheelChair) wheelChair.SetActive(true);

					ShowAvatarOnNextUpdate = false;
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="show"></param>
		public void ShowAvatar(bool show)
		{
			if (AvatarDisplayController)
			{
				//Delay avatar display to next ik updates to avoid showing the avatar in T-pose until next update
				if (show)
				{
					ShowAvatarOnNextUpdate = true;
				}
				else
				{ 
					AvatarDisplayController.HideAvatar();
					if (wheelChair) wheelChair.SetActive(false);
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		public void StartCalibration(bool sendRigSetup, Player player)
		{
			//Client send RigSetup to IK server... needs to be the clients since in some cases no server is preset at calibration time!
			if (sendRigSetup)
			{
				if (InitialBones == null)
				{
					Debug.Log("Storing initial bones");

					//Initialize initial bones
					InitialBones = new List<BoneTransform>();
					foreach (ERigBones bone in Enum.GetValues(typeof(ERigBones)))
					{
						var boneTransform = IKListener.GetBoneTransform(bone);
						if (boneTransform != null)
						{
							InitialBones.Add(new BoneTransform
							{
								BoneIndex = (int)bone,
								LocalPosition = NetworkTransformUtils.ToPosition(boneTransform.localPosition),
								LocalRotation = NetworkTransformUtils.ToRotation(boneTransform.localRotation),
							});
						}
					}
				}

				//Send rig calibration message to IK with the avatar bone setup
				var msgRigCalibration = new RecalibrateRig
				{
					ExperienceClientId = NetworkInterface.Instance.NetworkGuid,

					RigSetup = new RigSetup
					{
						BackpackToPelvisOffset = BackpackToPelvisOffset,
						DefaultArmStretchFactor = DefaultArmStretchFactor,
						HeadTargetOffset = NetworkTransformUtils.ToPosition(HeadTargetOffset),
						HandTargetOffset = NetworkTransformUtils.ToPosition(HandTargetOffset),

						Bones = InitialBones.ToArray(),
					},
				};

				//Send rig calibration to IK
				NetworkInterface.Instance.SendMessage(msgRigCalibration);
			}
		}

		/// <summary>
		/// Returns the requested body part of the avatar if available.
		/// </summary>
		/// <param name="bodyPart"></param>
		/// <returns>AvatarBodyPart instance. If not available, null.</returns>
		public AvatarBodyPart GetAvatarBodyPart(EAvatarBodyPart bodyPart)
		{
			return BodyParts[(int)bodyPart];
		}

		/// <summary>
		/// Return the root node of the avatar (i.e. where the root motion is applied)
		/// </summary>
		public Transform GetAvatarRoot()
		{
			return _ikListener.AvatarAnimator.transform;
		}

		#endregion

		#region Events

		private void Player_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			//Initials updated?
			if(e.PropertyName == "Initials" && AvatarDisplayController)
			{
				AvatarDisplayController.InitializePlayer(Player.Initials);
			}
		}

		#endregion

		#region Internals

		private void InitAvatarVisuals()
		{
			if (AvatarDisplayController)
			{
				//Initialize player
				var playerInitials = "";
				if (Player != null && !string.IsNullOrEmpty(Player.Initials))
					playerInitials = Player.Initials;
				AvatarDisplayController.InitializePlayer(playerInitials);
				
				if (IsMainPlayer)
					AvatarDisplayController.HideHead();
				else
					AvatarDisplayController.ShowHead();
			}
			else
			{
				Debug.LogError("No display controller");
			}
		}

		private AvatarBodyPart[] FindAvatarBodyParts()
		{
			var bodyParts = new AvatarBodyPart[Enum.GetValues(typeof(EAvatarBodyPart)).Length];
			foreach (EAvatarBodyPart bodyPart in Enum.GetValues(typeof(EAvatarBodyPart)))
			{
				bodyParts[(int)bodyPart] = GetComponentsInChildren<AvatarBodyPart>().FirstOrDefault(p => p.BodyPart == bodyPart);
			}
			return bodyParts;
		}

		#endregion
	}

}