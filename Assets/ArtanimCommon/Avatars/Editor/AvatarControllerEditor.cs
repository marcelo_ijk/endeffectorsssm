﻿using Artanim.Location.Messages;
using Artanim.Vicon;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

namespace Artanim
{
	[CustomEditor(typeof(AvatarController))]
	[CanEditMultipleObjects]
	public class AvatarControllerEditor : Editor
	{
		private static Dictionary<EAvatarBodyPart, float> BODY_PART_COLLIDER_RADIUS = new Dictionary<EAvatarBodyPart, float>
		{
			{ EAvatarBodyPart.Head, 0.2f },
			{ EAvatarBodyPart.LeftFoot, 0.08f },
			{ EAvatarBodyPart.RightFoot, 0.08f },
			{ EAvatarBodyPart.LeftHand, 0.08f },
			{ EAvatarBodyPart.RightHand, 0.08f },

		};

		private string Log;

		public override void OnInspectorGUI()
		{
			serializedObject.Update();
			
			DrawDefaultInspector();
			
			if(GUILayout.Button("Initialize Avatar"))
			{
				InitAvatar();

				if (string.IsNullOrEmpty(Log))
					Log = "Avatar already initialized.";
			}

			if(!string.IsNullOrEmpty(Log))
			{
				GUILayout.Label(Log);
			}

			serializedObject.ApplyModifiedProperties();
		}

		#region Internals

		private AvatarController GetAvatarController()
		{
			return (AvatarController)target;
		}

		private void InitAvatar()
		{
			Log = null;

			//Validate body parts (AvatarBodyPart)
			var bodyParts = GetAvatarController().GetComponentsInChildren<AvatarBodyPart>();
			foreach (EAvatarBodyPart bodyPartType in Enum.GetValues(typeof(EAvatarBodyPart)))
			{
				//Do we have more than one?
				if (bodyParts.Count(p => p.BodyPart == bodyPartType) > 1)
				{
					Log += string.Format("More than one body part of type {0} found. Make sure you have only one body part per type.\n", bodyPartType.ToString());
				}

				//Is body part available?
				else if (!bodyParts.Any(p => p.BodyPart == bodyPartType))
				{
					//Nope... create one
					Log += string.Format("AvatarBodyPart added: {0}\n", bodyPartType.ToString());
					InitBodyPart(bodyPartType);
				}
			}
		}

		private void InitBodyPart(EAvatarBodyPart bodyPart)
		{
			var boneTransform = GetBodyPartTransform(bodyPart);
			if (boneTransform)
			{
				//Create body part
				var bodyPartInst = GetOrCreateComponent<AvatarBodyPart>(boneTransform.gameObject);
				if (!bodyPartInst)
				{
					Debug.LogErrorFormat("Failed to initialized avatar body part. Failed to create AvatarBodyPart. Avatar={0}, BodyPart={1}", name, bodyPart.ToString());
					return;
				}

				var bodyPartRigidbody = GetOrCreateComponent<Rigidbody>(boneTransform.gameObject);
				if (!bodyPartRigidbody)
				{
					Debug.LogErrorFormat("Failed to initialized avatar body part. Failed to create Rigidbody. Avatar={0}, BodyPart={1}", name, bodyPart.ToString());
					return;
				}

				var bodyPartCollider = GetOrCreateComponent<SphereCollider>(boneTransform.gameObject);
				if (!bodyPartCollider)
				{
					Debug.LogErrorFormat("Failed to initialized avatar body part. Failed to create Collider. Avatar={0}, BodyPart={1}", name, bodyPart.ToString());
					return;
				}

				//Init components
				bodyPartInst.BodyPart = bodyPart;
				bodyPartRigidbody.useGravity = false;
				bodyPartRigidbody.isKinematic = true;
				bodyPartCollider.isTrigger = true;
				bodyPartCollider.radius = BODY_PART_COLLIDER_RADIUS[bodyPart];

				Debug.LogFormat("Create AvatarBodyPart. Avatar={0}, BodyPart={1}, Bone={2}", name, bodyPart.ToString(), boneTransform.name);
			}
			else
			{
				Debug.LogErrorFormat("Failed to initialized avatar body part. Bone transform not found. Avatar={0}, BodyPart={1}", name, bodyPart.ToString());
			}
		}

		private Transform GetBodyPartTransform(EAvatarBodyPart bodyPart)
		{
			var humanBone = HumanBodyBones.LastBone;
			switch (bodyPart)
			{
				case EAvatarBodyPart.LeftFoot:
					humanBone = HumanBodyBones.LeftToes;
					break;
				case EAvatarBodyPart.RightFoot:
					humanBone = HumanBodyBones.RightToes;
					break;
				case EAvatarBodyPart.LeftHand:
					humanBone = HumanBodyBones.LeftHand;
					break;
				case EAvatarBodyPart.RightHand:
					humanBone = HumanBodyBones.RightHand;
					break;
				case EAvatarBodyPart.Head:
					humanBone = HumanBodyBones.Head;
					break;
			}

			if (humanBone != HumanBodyBones.LastBone)
				return GetAvatarAnimator().GetBoneTransform(humanBone);
			else
				return null;
		}

		private T GetOrCreateComponent<T>(GameObject target) where T : Component
		{
			var instance = target.GetComponent<T>();
			if (instance == null)
			{
				//Create it
				instance = target.gameObject.AddComponent<T>();
			}

			return instance;
		}

		private Animator GetAvatarAnimator()
		{
			var ikListener = GetAvatarController().GetComponent<IKListener>();
			return ikListener.AvatarAnimator;
		}

		#endregion


	}
}