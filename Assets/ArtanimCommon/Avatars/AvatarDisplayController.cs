﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Artanim
{
	public abstract class AvatarDisplayController : MonoBehaviour
	{
        public abstract void InitializePlayer(string initials);

		/// <summary>
		/// Shows the avatar body.
		/// </summary>
		public abstract void ShowAvatar();

		/// <summary>
		/// Hides the avatar body.
		/// </summary>
		public abstract void HideAvatar();

		/// <summary>
		/// Shows the avatar head.
		/// </summary>
		public abstract void ShowHead();

		/// <summary>
		/// Hides the avatar head.
		/// </summary>
		public abstract void HideHead();

	}
}