﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Artanim
{

	[CreateAssetMenu(fileName = "Experience Settings", menuName = "Artanim/Experience Settings")]
	public class ExperienceSettings : ScriptableObject
	{
		public enum ESceneLoadingType { DontWaitForSync, WaitForSync }

		/// ------------------------------------------------------------------------
		/// Experience Settings
		/// ------------------------------------------------------------------------
		[Header("Experience Settings")]
		[Tooltip("Name of the experience in the config.")]
		public string ConfigExperienceName;

		[Tooltip("Default camera template created at startup. Leave empty to use the original default camera. The template must have a VRCameraFader behaviour attached to it.")]
		public GameObject DefaultCameraTemplate;

		[Tooltip("Default camera template used for observer views (used in observer and server). Leave empty to use the original camera. The template must have a VRCameraFader behaviour attached to it.")]
		public GameObject DefaultObserverCameraTemplate;

		[Tooltip("Default camera template used for server view. If not set the server will use the observer or default camera.")]
		public GameObject DefaultServerCameraTemplate;

		[Tooltip("Default prefab template used to display messages to the player. Leav empty to use the original SDK template. The given template must have a behaviour attached implementing the IUserMessageDisplayer interface.")]
		public GameObject DefaultUserMessageDisplayer;

		[Tooltip("Name of the scene used in the sesssion initialization phase. Leave empty to use the original construct scene.")]
		public string ConstructSceneName;

		/// ------------------------------------------------------------------------
		/// Teamspeak
		/// ------------------------------------------------------------------------
		[Header("Teamspeak")]
		[Tooltip("Default audio source used for Teamspeak player voices. If not set, the default SDK audio source is used. The provided prefab must have a TeamSpeakAudioSource behaviour in the root.")]
		public GameObject TeamspeakAudioSource;

		[Tooltip("Default audio source used for Teamspeak hostess voice. If not set, the TeamspeakAudioSource or the default SDK source is used instead. The provided prefab must have a TeamSpeakAudioSource behaviour in the root.")]
		public GameObject TeamspeakHostessAudioSource;

		[Header("Scene loading")]
		[Tooltip("Default scene loading behaviour. WaitForSync forces all components to wait until all have loaded a scene before fading in")]
		public ESceneLoadingType DefaultSceneLoadingType = ESceneLoadingType.DontWaitForSync;

		/// ------------------------------------------------------------------------
		/// Text and translation
		/// ------------------------------------------------------------------------
		[Header("Text and translation")]
		[Tooltip("2 character language to be used in the editor. The runtime value is provided by the hostess depending on the players choice.")]
		public string EditorLanguage = "en";

		[Tooltip("Show key and text in the editor.")]
		public bool ShowKeysInEditor = false;

		[Tooltip("2 character language to be used as fallback when the requested language is not found.")]
		public string FallbackLanguage = "en";

		[Tooltip("Fully qualified type of text provider used to retrieve translated texts. If not set, the SDK default implementation is used.")]
		public string TextProviderTypeName = "Artanim.CsvTextProvider";

		[Tooltip("Path relative to StreamingAssets where the text resources are located.")]
		public string TextAssetPath = "Texts/texts.csv";

		/// ------------------------------------------------------------------------
		/// Subtitles
		/// ------------------------------------------------------------------------
		[Header("Subtitles")]
		[Tooltip("Prefab used to display subtitles in observers. The given prefab must have a behaviour implementing the ISubtitleDisplayer interface attached to its root. If not set the SDK default is used.")]
		public GameObject ObserverSubtitleDisplayer;

		[Tooltip("Prefab used to display subtitles in VR. The given prefab must have a behaviour implementing the ISubtitleDisplayer interface attached to its root. If not set the SDK default is used.")]
		public GameObject VRSubtitleDisplayer;

		#region Editor events

#if UNITY_EDITOR
		private string PrevEditorLanguage;

		private void OnValidate()
		{
			if (string.IsNullOrEmpty(PrevEditorLanguage))
			{
				PrevEditorLanguage = EditorLanguage;
			}
			else if (PrevEditorLanguage != EditorLanguage)
			{
				TextService.Instance.SetLanguage(EditorLanguage);
				PrevEditorLanguage = EditorLanguage;
			}
		}

#endif
		#endregion
	}

}