﻿//#define SINGLE_EXP_CONFIG

using UnityEngine;
using System.Collections;
using System.IO;
using System.Xml.Serialization;
using Artanim.Location.Config;
using System.Linq;
using Artanim.Location.Network;

namespace Artanim
{

	public class ConfigService
	{
		private const string EXPERIENCE_SETTINGS_RESOURCE = "Experience Settings";

		#region Factory

		private static ConfigService _instance;

		public static ConfigService Instance
		{
			get
			{
				if(_instance == null)
				{
					_instance = new ConfigService();
				}
				return _instance;
			}
		}

		#endregion

		public Config Config { get { return Config.Instance; } }

		public readonly ExperienceSettings ExperienceSettings;
		public readonly ExperienceConfig ExperienceConfig;

		public string ExpConfigPath
		{
			get { return Path.Combine(Application.streamingAssetsPath, "Config/ExpConfig.xml"); }
		}

		private ConfigService()
		{
			//Config is already loaded by NetworkInterface
			if (Config.Instance == null)
			{
				Debug.LogError("Config not loaded!");
			}

			//Load experience settings
#if !UNITY_EDITOR
			var componentType = NetworkInterface.Instance.ComponentType;
			if(componentType != Location.Data.ELocationComponentType.IKServer)
			{
#endif
				ExperienceSettings = ResourceUtils.LoadResources<ExperienceSettings>(EXPERIENCE_SETTINGS_RESOURCE);
				if (!ExperienceSettings)
					Debug.LogWarning("No experience settings found. Create a new ExperienceSettings asset in a resource folder of your project called 'Experience Settings'");

				//Load experience config
				if (ExperienceSettings)
				{
#if !SINGLE_EXP_CONFIG
					ExperienceConfig = Config.Instance.ExperienceConfigs.FirstOrDefault(c => c.ExperienceName == ExperienceSettings.ConfigExperienceName);
#else
					try
					{
						var serializer = new XmlSerializer(typeof(ExperienceConfig));
						using (var stream = new FileStream(ExpConfigPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
						{
							ExperienceConfig = (ExperienceConfig)serializer.Deserialize(stream);
						}
					}
					catch
					{
						Debug.LogError("Failed to read experience config at path " + ExpConfigPath);
						throw;
					}
#endif
				}
				else
				{
					Debug.LogWarning("Cannot load experience config. No settings found. (See previous error)");
				}
#if !UNITY_EDITOR
			}
#endif
		}
	}
}