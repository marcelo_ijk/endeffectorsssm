﻿using Artanim.Monitoring;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
public class Snapshot : MonoBehaviour {

	#region Internals

	private int _resWidth = 240;
	private int _resHeight = 135;
	private float _xfactor = 1;
	private float _yfactor = 1;

	private MetricsRawChannel _metrics;
	private bool _doSnapShot = false;
	// Ensure callback does not get garbage collected
	private MetricsManager.AskSendNowCallbackDelegate _askNowCallback;
	Texture2D _screenShot;
	Texture2D _reducedScreenShot;

	IEnumerator Start()
	{
		var waitEndOfFrame = new WaitForEndOfFrame();
		while(true)
		{
			yield return waitEndOfFrame;

			if(enabled && _doSnapShot && _metrics != null)
			{
				DoSnapShot();
			}

			_doSnapShot = false;
		}
	}

	void OnEnable()
	{
		_screenShot = new Texture2D(1, 1, TextureFormat.RGB24, false);
		_reducedScreenShot = new Texture2D(_resWidth, _resHeight, TextureFormat.RGB24, false);

		// Create metrics channels
		_metrics = MetricsManager.Instance.GetRawChannelInstance(MetricsAction.Create, "Snapshot", (ulong)_resWidth * (ulong)_resHeight * 3);
		_askNowCallback = (m) => { _doSnapShot = true; };
		_metrics.SetAskSendDataNowCallback(_askNowCallback);
	}

	void OnDisable()
	{
		// Destroy metrics channels
		if(_metrics != null)
		{
			_metrics.Dispose();
			_metrics = null;
		}

		_doSnapShot = false;
	}

	void DoSnapShot()
	{
		IntPtr buffer = _metrics.BeginSend();

		if (buffer != IntPtr.Zero)
		{
			if (_screenShot.width != Screen.width || _screenShot.height != Screen.height)
			{
				// screen size has changed. Resize our texture object.
				_screenShot.Resize(Screen.width, Screen.height);
				_screenShot.Apply();

				// Also needs to recompute transform factors.
				_xfactor = (float)Screen.width / (float)_resWidth;
				_yfactor = (float)Screen.height / (float)_resHeight;
			}

			// Full screen snapshot
			_screenShot.ReadPixels(new Rect(0, 0, _screenShot.width, _screenShot.height), 0, 0, false);
			_screenShot.Apply();

			// reduce snapshot
			for (int i = 0; i < _resWidth; ++i)
			{
				for (int j = 0; j < _resHeight; ++j)
				{
					//Color c = _screenShot.GetPixelBilinear(i / (float)_resWidth, j / (float)_resHeight);
					//_reducedScreenShot.SetPixel(i, j, c);
					Color c = _screenShot.GetPixel((int)(i * _xfactor), (int)(j * _yfactor));
					_reducedScreenShot.SetPixel(i, j, c);
				}
			}

			_reducedScreenShot.Apply();

			//byte[] bytes = screenShot.GetRawTextureData();
			//Marshal.Copy(bytes, 0, buffer, bytes.Length);
			byte[] bytes = _reducedScreenShot.EncodeToJPG();
			Marshal.WriteInt64(buffer, bytes.Length);
			Marshal.Copy(bytes, 0, new IntPtr(buffer.ToInt64() + 8), bytes.Length);
			_metrics.EndSend(buffer, (ulong)bytes.Length + 8);

			//ulong ticket = _metrics.OpenQualityTicket("Snapshot");
			//_metrics.SendQualityTicketEvent(ticket, 100, "Ok");
			//_metrics.CloseQualityTicket(ticket, "Done");
			_metrics.SendQualityEvent("Snapshot", 0, "Done");
		}
	}

	#endregion
}
