﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Artanim
{
    /// <summary>
    /// Latency estimation based on the cross-correlation function
    /// Measuring the similarity between the Vicon and Headset systems, i.e., {X} and {Y}, respectively    
    /// The estimates were obtained by using a FIFO (First in, first out) approach
    /// </summary>
    /// <typeparam name="T"></typeparam>    
    
    public class LatencyEstimation
    {

        public readonly int WindowSize;
        //public int timeLag;
        public int MaxTimeLag = 20;
        private readonly List<float> _fifoX;
        private readonly List<float> _fifoY;
        //private readonly List<float> _Dt;
        //private List<int, float> _xcorr;

        /// <summary>
        ///  Mean values for the {X} and {Y} time-series
        /// </summary>
        private float muX;
        private float muY;
                
        private float NormX;
        private float NormY;
        private float timeLag;
                
        // Time interval between UDP packages
        private float Dt;

        // Latency analytics
        // Mean time interval between consecutive packages
        private float muDt;

        // Standard deviation of Dt
        private float stdDt;

        // Last received package
        private float RcvLastPackage;        
        

        public LatencyEstimation(int _WindowSize = 32)
        {
            // Vicon and Headset time series {X} and {Y}, respectively
            muX = 0f;
            muY = 0f;

            NormX = 0f;
            NormY = 0f;

            // Latency analytics
            muDt = 0;
            stdDt = 0;

            // The window size depends on the fundamental frequency, intensity and changes of the signal            
            WindowSize = _WindowSize;

            _fifoX = new List<float>(WindowSize);
            _fifoY = new List<float>(WindowSize);
            //_Dt = new List<float>(WindowSize);
        }

        public void Push(float itemX, float itemY, float _time)
        {            
            if (_fifoX.Count == WindowSize)
            {
                NormX = Mathf.Sqrt(NormX);
                NormY = Mathf.Sqrt(NormY);
                LastGoodTimeLag = timeLag = XCORR();
                LastGoodMuDt = muDt;

                /// 
                /// UDP Send (int)
                /// muDt ¦ sdtDt  timeLag
                //Debug.Log("Time lag: " + timeLag.ToString() + "  MuDt: " + muDt);


                _fifoX.Clear();
                _fifoY.Clear();

                muX = 0f;
                muY = 0f;

                NormX = 0f;
                NormY = 0f;

                muDt = 0;
                timeLag = 0;
            }
            _fifoX.Add(itemX);
            _fifoY.Add(itemY);

            int _size = _fifoX.Count;

            if (_size > 1)
            {                
                muX = ( muX * ((float)_size - 1f) + itemX ) / (float)_size;
                muY = ( muY * ((float)_size - 1f) + itemY)  / (float)_size;

                
                Dt = _time - RcvLastPackage;
                muDt = ( muDt * ( (float)_size - 1f) + Dt) / (float)_size;                

                RcvLastPackage = _time;
            }
            else
            {
                muX = itemX;
                muY = itemY;
                               
                
                // Initial assumption to avoid additional statements                 
                RcvLastPackage = _time;
                muDt = 0;
            }
            NormX += itemX * itemX;
            NormY += itemY * itemY;                        
            
        }

        
        public float Peek()
        {
            return _fifoX[_fifoX.Count - 1];
        }

        public void Pop()
        {
            _fifoX.RemoveAt(_fifoX.Count - 1);
        }
        

        public int Count
        {
            // Time series {X} and {Y} have the same size
            get { return _fifoX.Count; }
        }

        public double GetDt
        {
            get { return Dt; }
        }

        public float GetStdDt
        {
            get { return stdDt; }
        }


        public float GetMuDt
        {
            get { return muDt; }
        }

        public float GetTimeLag
        {
            get { return timeLag;  }
        }

        public int SetMaxTimeLag
        {         
            set { MaxTimeLag = value; }
        }        

        public float LastGoodTimeLag
        {
            get; private set;
        }

        public float LastGoodMuDt
        {
            get; private set;
        }

        /// <summary>
        /// XCORR: Cross-correlation 
        /// Similarity between the two series as a function of the system's latency, e.g., Vicon - Headset
        /// </summary>
        /// <returns></returns>
        public float XCORR()
        {
            timeLag = 0;

            float _xcorr = 0f;
            for (int tau = -(WindowSize - 1); tau < (WindowSize - 1); ++tau)
            {
                float dummy = 0f;
                for (int i = 0; i < WindowSize; ++i)
                {
                    int _shift = i + tau;
                    if (_shift >= 0 && _shift < WindowSize)
                    {                        
                        dummy += _fifoX[_shift] * _fifoY[i];
                    }
                }
                dummy /= (NormX * NormY);                
                
                if (dummy > _xcorr)
                {
                    timeLag = tau;
                    _xcorr = dummy;
                } 
            }            
            return timeLag * muDt;
        }
    }    
}