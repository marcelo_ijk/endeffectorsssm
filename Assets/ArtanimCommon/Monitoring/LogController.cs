﻿using UnityEngine;
using System.Collections;
using System.IO;
using Artanim.LogSystem;
using System.Collections.Generic;
using System.Threading;

namespace Artanim
{
	// Initialize our logging system.
	// It's recommended to give script a high priority so it runs as soon as possible in order
	// to ensure that even early Unity logs are forwarded to our logger
	public class LogController : MonoBehaviour
	{
		#region Internals
		private Artanim.LogSystem.Logger logger;
		private static LogController instance;
		private ILogHandler _defaultlogger;
		private IEnumerator _logcoroutine = null;

		internal static LogController Instance { get { return instance; } }


		void Awake()
		{
			instance = this;

#if UNITY_5
			_defaultlogger = Debug.logger.logHandler;
#else
			_defaultlogger = Debug.unityLogger.logHandler;
#endif

			if(Debug.isDebugBuild)
			{
				LogManager.Instance.Init("Unity.Console", activatePolling: true);

				_logcoroutine = LogConsoleCoroutine();

				StartCoroutine(_logcoroutine);
			}
			else
			{
				LogManager.Instance.Init("Unity.Console");
			}

			logger = LogManager.Instance.GetLogger("Unity");

			// We want to log every message from now on, so we never unsubscribe from this event
			Application.logMessageReceivedThreaded += HandleLogCallback;
		}

		void OnDestroy()
		{
			if(_logcoroutine != null)
			{
				StopCoroutine(_logcoroutine);
				_logcoroutine = null;
			}
		}

		private void HandleLogCallback(string msg, string stackTrace, LogType type)
		{
			// Remove color tags
			if (msg.StartsWith("<color=") && msg.EndsWith("</color>"))
			{
				int endTag = msg.IndexOf('>');
				if (endTag > 7)
				{
					msg = msg.Substring(endTag + 1, msg.Length - endTag - 9);
				}
			}

			switch (type)
			{
				case LogType.Log:
				case LogType.Assert:
					if(logger.IsInfoEnabled)
						logger.Info(msg);
					break;
				case LogType.Warning:
					if(logger.IsWarnEnabled)
						logger.Warn(msg);
					break;
				case LogType.Error:
					if(logger.IsErrorEnabled)
						logger.Error(msg);
					break;
				case LogType.Exception:
					if(logger.IsExceptionEnabled)
						logger.Exception(stackTrace, msg);
					break;
			}
		}

		// This callback allows Console.Write messages to be pushed on Unity console.
		private void LogConsoleCallback(string loggername, int level, string message)
		{
			//lock(_logMessageQueue)
			//{
			//	_logMessageQueue.Enqueue(new _logMessage() { loggername = loggername, level = level, message = message });
			//}
		}

		private IEnumerator LogConsoleCoroutine()
		{
			while(true)
			{
				while(true)
				{
					LogManager.LogEntry logentry = LogManager.Instance.PopMessage();

					if(logentry == null)
						break;

					LogType type = LogType.Log;

					switch(logentry.level)
					{
						case LogManager.Levels.DEBUG:
						case LogManager.Levels.INFO:
							type = LogType.Log;
							break;
						case LogManager.Levels.WARNING:
							type = LogType.Warning;
							break;
						case LogManager.Levels.ERROR:
						case LogManager.Levels.EXCEPTION:
							type = LogType.Error;
							break;
					}

					// By default we only want to see the errors in the Unity Editor console
					if(type == LogType.Error)
					{
						_defaultlogger.LogFormat(type, this, "{0}: {1} - {2}", new string[] { logentry.loggername, logentry.level.ToString(), logentry.message });
					}
				}

				// Wait until next frame
				yield return null;
			}
		}
#endregion
	}
}