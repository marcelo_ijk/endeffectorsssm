﻿using CsvHelper;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using UnityEngine;
using System;

namespace Artanim
{
	public class CsvTextProvider :  ITextProvider
	{
		private Dictionary<string, Text> Texts = new Dictionary<string, Text>();

		#region Public interface

		public string GetText(string key, string language, string defaultLanguage)
		{
			string translation = null;
			if (HasKey(key))
			{
				var text = Texts[key];
				if (!text.LanguageTexts.TryGetValue(language.ToLower(), out translation))
				{
					//Try default language
					text.LanguageTexts.TryGetValue(defaultLanguage.ToLower(), out translation);
				}
			}
			return translation;
		}

		public bool ReloadTexts(string textAssetPath)
		{
			try
			{
				//Clear old content
				Texts.Clear();

				//Build streaming assets path
				var path = Path.Combine(Application.streamingAssetsPath, textAssetPath);
				Debug.LogFormat("Reading texts from {0}...", path);

				//Read CSV file
				using (var csv = new CsvReader(new StreamReader(path)))
				{
					csv.Configuration.Delimiter = ";";
					
					//Read line
					while (csv.Read())
					{
						//Read languages
						var text = new Text() { Id = csv.GetField("id"), };
						foreach (var header in csv.FieldHeaders)
						{
							if (header != "id")
								text.LanguageTexts.Add(header.ToLower().Trim(), csv.GetField(header));
						}
						Texts.Add(text.Id, text);
					}
				}

				Debug.LogFormat("Loaded {0} texts from {1}", Texts.Count, path);
				return true;
			}
			catch(Exception ex)
			{
				Debug.LogWarningFormat("Failed to read text resources from {0}. {1}:{2}", textAssetPath, ex.GetType().Name, ex.Message);
				return false;
			}
		}

		public void SetLanguage(string language)
		{
			//Ignore, since we already cache all texts
		}

		public bool HasKey(string key)
		{
			return Texts.ContainsKey(key);
		}

		#endregion

		private class Text
		{
			public string Id { get; set; }
			public Dictionary<string, string> LanguageTexts = new Dictionary<string, string>();
		}
	}
}