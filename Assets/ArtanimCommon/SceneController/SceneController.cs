﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System;
using System.Linq;
using Artanim.Location.Network;
using Artanim.Location.Messages;
using Artanim.Location.Data;
using UnityEngine.UI;
using System.Text;

namespace Artanim
{

	/// <summary>
	/// Controls scene loading and screen fade between scene transitions.
	/// </summary>
	public class SceneController : SingletonBehaviour<SceneController>
	{
		private const string EMERGENCY_SCENE_NAME = "Emergency Scene";
		private const string TAG_DEVELOPMENT_CAMERA = "Development Camera";

		private const string MAIN_SCENE_NAME = "Main Scene";
		private const EScene DEFAULT_SCENE = EScene.MainMenu;

		public enum EScene
		{
			MainMenu,
			IK,
			ExperienceServer,
			ExperienceClient,
			ExperienceObserver,
		}

		public ELocationComponentType SceneComponentType
		{
			get
			{
				if (CurrentScene != null)
					return CurrentScene.ComponentType;
				else
					return ELocationComponentType.Undefined;
			}
		}

		private static readonly Dictionary<EScene, SceneConfig> SceneConfigs = new Dictionary<EScene, SceneConfig>()
		{
			{ EScene.MainMenu, new SceneConfig { SceneName = "Main Menu Scene", ComponentType = ELocationComponentType.Undefined } },

			{ EScene.IK, new SceneConfig { SceneName = "IK Scene", ComponentType = ELocationComponentType.IKServer } },


			{ EScene.ExperienceServer, new SceneConfig { SceneName = "Experience Controller Scene", ComponentType = ELocationComponentType.ExperienceServer } },
			{ EScene.ExperienceClient, new SceneConfig { SceneName = "Experience Controller Scene", ComponentType = ELocationComponentType.ExperienceClient } },
			{ EScene.ExperienceObserver, new SceneConfig { SceneName = "Experience Observer Scene", ComponentType = ELocationComponentType.ExperienceObserver } },
		};

		public Action<string, Scene, bool> OnSceneLoaded;
		public Action<string, Scene, bool> OnSceneUnLoaded;
		public Action<string> OnSceneLoadFailed;

		private SceneConfig CurrentScene;
		public string MainChildSceneName { get; private set; }

		private string SceneToActivate;

		void Start()
		{
			SceneManager.sceneLoaded += SceneManager_sceneLoaded;
			SceneManager.sceneUnloaded += SceneManager_sceneUnloaded;

			//Start scene
#if !UNITY_EDITOR
			EScene scene = EScene.MainMenu;
			for (var i = 0; i < Environment.GetCommandLineArgs().Length; ++i)
			{
				try
				{
					scene = (EScene)Enum.Parse(typeof(EScene), Environment.GetCommandLineArgs()[i], true);
					break;
				}
				catch (Exception)
				{
					continue;
				}
			}
			LoadScene(scene, Transition.None, true);
#else
			//Load default scene
			LoadDefaultScene(true);
#endif
		}

		private void SceneManager_sceneLoaded(Scene scene, LoadSceneMode mode)
		{
			if (!string.IsNullOrEmpty(SceneToActivate) && scene.name == SceneToActivate)
			{
				SceneManager.SetActiveScene(scene);
				SceneToActivate = null;
			}

			if (OnSceneLoaded != null)
				OnSceneLoaded(scene.name, scene, scene.name == MainChildSceneName);
		}

		private void SceneManager_sceneUnloaded(Scene scene)
		{
			if (OnSceneUnLoaded != null)
				OnSceneUnLoaded(scene.name, scene, scene.name == MainChildSceneName);
		}

		#region Public interface

		/// <summary>
		/// Starts the emergency procedure.
		/// </summary>
		public void DoEmergency()
		{
			if (!Emergency)
			{
				Emergency = true;
				StopAllCoroutines();
				StartCoroutine(StartEmergency());
			}
		}

		/// <summary>
		/// Loads the default scene. Currently the default scene is the main menu.
		/// </summary>
		/// <param name="forceLoad"></param>
		public void LoadDefaultScene(bool forceLoad = false)
		{
			if (Emergency)
				return;

			//Unload all scenes but the main scene
			UnloadAllScenes();

			//Load default scene
			LoadScene(DEFAULT_SCENE, Transition.FadeWhite, forceLoad);
		}


		/// <summary>
		/// Loads the given scene as main scene. Loading a new main scene will unload all previous experience child scenes.
		/// </summary>
		/// <param name="scene">Scene name to load</param>
		/// <param name="transition">Scene transition</param>
		/// <param name="forceLoad">Force loading even if the given scene is already loaded</param>
		public void LoadScene(EScene scene, Transition transition, bool forceLoad = false)
		{
			if (Emergency)
				return;

			var config = GetSceneConfig(scene);
			if (config != null)
			{
				if (config != CurrentScene)
				{
					StartSceneLoad(config.SceneName, transition, false, true, true, config);
				}
			}
			else
			{
				Debug.LogWarningFormat("<color=lime>Tried to load invalid scene: {0}. Ignoring scene loading</color>", scene.ToString());
			}
		}

		/// <summary>
		/// Loads a new child scene.
		/// </summary>
		/// <param name="sceneName">Child scene name to load</param>
		/// <param name="transition">Scene transition</param>
		/// <param name="unloadOtherChilds">Indicates if all other child scenes should be unloaded. Default is false.</param>
		/// <param name="forceReload">Indicated if the scene has to be loaded even if it's already loaded</param>
		public void LoadChildScene(string sceneName, Transition transition, bool unloadOtherChilds = false, bool forceReload = false, bool awaitSceneSync = false)
		{
			if (Emergency)
				return;

			if (!string.IsNullOrEmpty(sceneName) && (!IsSceneLoaded(sceneName) || forceReload))
			{
				StartSceneLoad(sceneName, transition, true, unloadOtherChilds, awaitSceneSync: awaitSceneSync);
			}
		}

		/// <summary>
		/// Returns a list of all currently loaded experience scenes.
		/// </summary>
		/// <returns></returns>
		public List<Scene> GetLoadedExperienceScenes()
		{
			return GetLoadedChildScenes(false);
		}

		#endregion

		#region Internals

		private bool Emergency;
		private IEnumerator StartEmergency()
		{
			//Make sure the camera is faded in
			var cameraFader = GetCurrentVRCamFader();
			if (cameraFader)
				cameraFader.SetFaded(Transition.None);

			SceneManager.LoadScene(EMERGENCY_SCENE_NAME, LoadSceneMode.Additive);
			yield return UnloadChildScenes(false);
			yield return null;
		}

		private void StartSceneLoad(string sceneName, Transition transition, bool isChild, bool unloadOtherChilds = false, bool setActiveScene = true, SceneConfig mainScene = null, bool awaitSceneSync = false)
		{
			if (!isChild && mainScene == null)
			{
				Debug.LogErrorFormat("<color=lime>Trying to load a main scene without a giving a SceneConfig. Ignoring load: sceneName={0}</color>", sceneName);
				return;
			}

			//We need to unload first. Stop already started scene transitions
			if (unloadOtherChilds || !isChild)
			{
				StopAllCoroutines();
				FadeInRoutine = null;
				FadeOutRoutine = null;
			}

			//Start async transition
			StartCoroutine(StartSceneTransitionAsync(sceneName, transition, isChild, unloadOtherChilds, setActiveScene, mainScene, awaitSceneSync: awaitSceneSync));
		}

		private void SetupScene()
		{
			//Search for SceneSetup object
			var sceneSetups = FindObjectsOfType<SceneSetup>();

			SceneSetup setupToLoad = null;

			//Validate
			if (sceneSetups != null)
			{
				if (sceneSetups.Length == 1)
					setupToLoad = sceneSetups[0];
				else if (sceneSetups.Length > 1)
					Debug.LogError("<color=lime>Found more than one SceneSetup! Only one can be present at a time</color>");
			}

			//Load setup
			if (setupToLoad)
			{
				//Setup camera
				if (MainCameraController.Instance)
					MainCameraController.Instance.ReplaceCamera(setupToLoad.CameraTemplate);
			}
			else
			{
				//Load defaults
				//Setup camera
				if (MainCameraController.Instance)
					MainCameraController.Instance.ReplaceCamera(null);
			}

		}

		private void UnloadAllScenes()
		{
			var scenes = new List<Scene>();
			for (var i = 0; i < SceneManager.sceneCount; ++i)
				scenes.Add(SceneManager.GetSceneAt(i));

			foreach (var scene in scenes)
			{
				if (scene.name != MAIN_SCENE_NAME && scene.name != EMERGENCY_SCENE_NAME)
					SceneManager.UnloadSceneAsync(scene.name);
			}

			CurrentScene = null;
			Resources.UnloadUnusedAssets();
		}

		private VRCameraFader GetCurrentVRCamFader()
		{
			if (MainCameraController.Instance)
				return MainCameraController.Instance.CameraFader;
			else
				return null;
		}

		private SceneConfig GetSceneConfig(EScene scene)
		{
			SceneConfig config = null;
			if (SceneConfigs.TryGetValue(scene, out config))
			{
				return config;
			}
			return null;
		}

		private bool AllUnloaded(bool includeComponentScene, string exclude = null)
		{
			for (var i = 0; i < SceneManager.sceneCount; ++i)
			{
				var scene = SceneManager.GetSceneAt(i);

				if (scene.name != exclude && !IsMainScene(scene.name, !includeComponentScene))
					return false;
			}
			return true;
		}

		private bool IsMainScene(string sceneName, bool includeComponentScene)
		{
			if (sceneName == MAIN_SCENE_NAME)
				return true;

			if (includeComponentScene && CurrentScene != null && CurrentScene.SceneName == sceneName)
				return true;

			return false;
		}

		private bool IsSceneLoaded(string sceneName)
		{
			for (var i = 0; i < SceneManager.sceneCount; ++i)
			{
				var scene = SceneManager.GetSceneAt(i);
				if (scene.name == sceneName)
					return true;
			}
			return false;
		}

		private List<Scene> GetLoadedChildScenes(bool includeComponentScene)
		{
			var childScenes = new List<Scene>();

			for (var i = 0; i < SceneManager.sceneCount; ++i)
			{
				var scene = SceneManager.GetSceneAt(i);

				//Exclude unloaded scenes
				if (!scene.isLoaded)
					continue;

				//Exclude main scene
				if (scene.name == MAIN_SCENE_NAME)
					continue;

				if (scene.name == EMERGENCY_SCENE_NAME)
					continue;

				//Exclude component scene
				if (!includeComponentScene && CurrentScene != null && CurrentScene.SceneName == scene.name)
					continue;

				childScenes.Add(scene);
			}
			return childScenes;
		}

		#endregion

		#region Async handling
		private bool SceneLoadEventSubscribed = false;
		private bool SceneLoadedInSession = false;
		private bool SceneLoadTimedOut = false;
		private string AwaitSceneName;
		private void OnSceneLoadSynced(SceneLoadedInSession args)
		{
			SceneLoadTimedOut = args.SceneLoadTimeout;
			SceneLoadedInSession = args.Scenes.Contains(AwaitSceneName);
		}

		private IEnumerator FadeOutRoutine;
		private IEnumerator FadeInRoutine;
		private IEnumerator StartSceneTransitionAsync(string sceneName, Transition transition, bool isChild, bool unloadOtherChilds = false, bool setActiveScene = true, SceneConfig mainScene = null, bool awaitSceneSync = false)
		{
			Debug.LogFormat("<color=lime>Loading scene: sceneName={0}, transition={1}, isChild={2}, unloadOtherChilds={3}, setActiveScene={4}</color>", sceneName, transition.ToString(), isChild, unloadOtherChilds, setActiveScene);

			//Start fading out
			yield return FadeOutAsync(transition);
			Debug.Log("<color=lime>Screen faded out</color>");

			//Attach to scene loading monitor
			if (awaitSceneSync)
			{
				//Setup wait state
				SceneLoadedInSession = false;
				SceneLoadTimedOut = false;
				AwaitSceneName = sceneName;

				if (!SceneLoadEventSubscribed)
				{
					//Attach event only once
					NetworkInterface.Instance.Subscribe<SceneLoadedInSession>(OnSceneLoadSynced);
					SceneLoadEventSubscribed = true;
				}
			}

			//Remember main child scene name
			if (unloadOtherChilds && isChild)
			{
				MainChildSceneName = sceneName;
			}

			//Check if scene is already loaded -> reload
			if(isChild && GetLoadedChildScenes(false).Select(s => s.name).Contains(sceneName))
			{
				//Unload this one
				yield return SceneManager.UnloadSceneAsync(sceneName);
			}
			
			//Set current main scene
			if (!isChild)
				CurrentScene = mainScene;

			//Scene activations
			if (setActiveScene)
				SceneToActivate = sceneName;

			//Load requested scene
			var async = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
			if (async != null)
			{
				async.allowSceneActivation = true;

				//Wait for load to be done
				while (!async.isDone)
					yield return null;

				Debug.LogFormat("<color=lime>Scene loaded {0}</color>", sceneName);

				//Unload unused scenes...
				if (unloadOtherChilds || (!isChild && CurrentScene != null))
				{
					Debug.Log("<color=lime>Unloading schild scenes...</color>");
					yield return UnloadChildScenes(!isChild, sceneName);
					Debug.Log("<color=lime>Scenes unloaded</color>");
				}

				//Disable all development cameras
				try
				{
					var devCams = GameObject.FindGameObjectsWithTag(TAG_DEVELOPMENT_CAMERA);
					if (devCams != null && devCams.Length > 0)
					{
						foreach (var devCam in devCams)
							devCam.SetActive(false);
					}
				}
				catch (UnityException ex)
				{
					//Not bad... usually because the tag development camera is not defined in the experience
					Debug.LogFormat("<color=lime>Failed searching for development cameras: {0}</color>", ex.Message);
				}


				//Setup scene
				Debug.Log("<color=lime>Setup scene</color>");
				SetupScene();

				//Wait for scene load sync
				if (awaitSceneSync)
				{
					Debug.Log("<color=lime>Waiting scene sync...</color>");
					var localTimeout = Time.realtimeSinceStartup + 30f;
					while (!SceneLoadTimedOut && !SceneLoadedInSession)
					{
						//Check for local timeout
						if (NetworkInterface.Instance.IsClient && Time.realtimeSinceStartup > localTimeout)
							SceneLoadTimedOut = true;

						yield return null;
					}
					Debug.LogFormat("<color=lime>Scene sync done: TimeOut={0}</color>", SceneLoadTimedOut);
				}

				//Fade in
				Debug.Log("<color=lime>Fading in</color>");
				if (transition != Transition.None)
					yield return FadeInAsync();
			}
			else
			{
				Debug.LogErrorFormat("<color=lime>Failed to load scene {0}. Async operation was null!</color>", sceneName);
				//Scene load failed
				if (OnSceneLoadFailed != null)
					OnSceneLoadFailed(sceneName);
			}

			yield return true;
		}


		private IEnumerator FadeOutAsync(Transition transition)
		{
			if (transition != Transition.None)
			{
				//Do we already have a transition going on?
				if (FadeOutRoutine == null)
				{
					var cameraFader = GetCurrentVRCamFader();
					if (cameraFader)
					{
						//No... start fading
						//Is there a fade in current running? if yes, stop it.
						if (FadeInRoutine != null)
						{
							StopCoroutine(FadeInRoutine);
							FadeInRoutine = null;
						}

						FadeOutRoutine = cameraFader.DoFadeAsync(transition);
						StartCoroutine(FadeOutRoutine);
					}
				}

				if (FadeOutRoutine != null)
				{
					yield return FadeOutRoutine;
					FadeOutRoutine = null;
				}
			}

			yield return true;
		}

		private IEnumerator FadeInAsync()
		{
			if (FadeOutRoutine == null) //Only fadein if no other fade out is currently running
			{
				if (FadeInRoutine == null)
				{
					var cameraFader = GetCurrentVRCamFader();
					if (cameraFader)
					{
						//No... start fading in
						FadeInRoutine = cameraFader.DoFadeInAsync();
						StartCoroutine(FadeInRoutine);
					}
				}

				if (FadeInRoutine != null)
				{
					yield return FadeInRoutine;
					FadeInRoutine = null;
				}
			}

			yield return true;
		}

		private IEnumerator UnloadChildScenes(bool includeComponentScene, string exclude = null)
		{
			var unloading = new List<Scene>();

			while (!AllUnloaded(includeComponentScene, exclude))
			{
				foreach (var scene in GetLoadedChildScenes(includeComponentScene).Where(s => !unloading.Contains(s) && s.name != exclude))
				{
					Debug.LogFormat("<color=lime>Unloading scene: {0}</color>", scene.name);
					SceneManager.UnloadSceneAsync(scene);
					unloading.Add(scene);
				}
				yield return null;
			}

			//Free memory
			if (unloading.Count > 0)
				Resources.UnloadUnusedAssets();

			yield return true;
		}

		#endregion

		#region Classes

		private class SceneConfig
		{
			public ELocationComponentType ComponentType;
			public string SceneName;
		}

		#endregion
	}

}