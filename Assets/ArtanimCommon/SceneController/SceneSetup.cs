﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Artanim
{

/// <summary>
/// This behaviour holds scene scoped setup for the framework. Only one single SceneSetup is allowed to be present at a time.
/// </summary>
[AddComponentMenu("Artanim/Scene Setup")]
public class SceneSetup : MonoBehaviour
{
	[Tooltip("Scene scope camera override. The given camera template (prefab) will be used for this scene.")]
	public GameObject CameraTemplate;
}

}