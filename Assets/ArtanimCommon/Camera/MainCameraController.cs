﻿using Artanim.Location.Data;
using Artanim.Location.Monitoring;
using Artanim.Location.Network;
using Artanim.Vicon;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if UNITY_2017_3_OR_NEWER
using UnityEngine.XR;
#else
using UnityEngine.VR;
using XRNode = UnityEngine.VR.VRNode;
using XRNodeState = UnityEngine.VR.VRNodeState;
using XRSettings = UnityEngine.VR.VRSettings;
using XRDevice = UnityEngine.VR.VRDevice;
#endif

using HmdTrackingLost = Artanim.Location.Monitoring.ErrorTypes.Hmd.TrackingLost;

namespace Artanim
{

	/// <summary>
	/// Main controller for all camera interactions.
	/// </summary>
	[RequireComponent(typeof(HeadsetCalibration))]
	[RequireComponent(typeof(Animator))]
	public class MainCameraController : SingletonBehaviour<MainCameraController>
	{
		[Tooltip("Tracked player head position")]
		public ViconRigidBody HeadRigidBody;

		[Tooltip("Root position where the cameras are created")]
		public Transform CameraRoot;
		
		[Tooltip("Prefab to the camera created at runtime")]
		public GameObject DefaultCameraTemplate;

		[Tooltip("Prefab used as observer camera")]
		public GameObject DefaultObserverCameraTemplate;

		[Tooltip("Default prefab used as user message displayer. Update the ExperienceSettings to change the displayer.")]
		public GameObject DefaultUserMessageDisplayer;

		[Tooltip("Default init position of the VR camera when the player is not specified yet")]
		public Vector3 DefaultCameraPosition;

		[Tooltip("Offset applied by the calibration.")]
		public Transform CalibrationOffset;

		[Tooltip("Global mocap offset")]
		public Transform GlobalOffset;

		[Tooltip("Avatar offset")]
		public Transform AvatarOffset;

		[Tooltip("Correction for HMD sensor data")]
		public Transform SensorCorrection;

		[Tooltip("List of game objects used for displaying debug calibration info")]
		public GameObject[] CalibrationDebugDisplay;

		[Tooltip("Network Activated component for toggling debug headset crosshair")]
		public NetworkActivated CrosshairNetworkActivated;


		public VRCameraFader CameraFader
		{
			get
			{
				if (PlayerCamera && PlayerCamera.activeInHierarchy)
					return PlayerCameraFader;
				else
					return ObserverCameraFader;
			}
		}

		public GameObject ActiveCamera
		{
			get
			{
				if (PlayerCamera && PlayerCamera.activeInHierarchy)
					return PlayerCamera;
				else
					return ObserverCamera;
			}
		}

		public bool CalibrationCross
		{
			get
			{
				return (CalibrationDebugDisplay != null) && (CalibrationDebugDisplay.Count(go => go != null && go.activeInHierarchy) > 0);
			}
			set
			{
				if (CalibrationDebugDisplay != null)
				{
					foreach (var go in CalibrationDebugDisplay)
					{
						go.SetActive(value);
					}
				}
			}
		}
		
		public GameObject PlayerCamera { get; private set; }
		public GameObject ObserverCamera { get; private set; }

		public IUserMessageDisplayer UserMessageDisplayer { get; private set; }

		private Player Player;
		private HeadsetCalibration HeadsetCalibration;
		private GameObject CurrentCameraTemplate;

		private VRCameraFader PlayerCameraFader;
		private VRCameraFader ObserverCameraFader;
		
		#region Public interface

		/// <summary>
		/// Init camera for a player. The camera rig will be offset to the given players head (rigidbody).
		/// </summary>
		/// <param name="player">Player configuration</param>
		public void InitPlayerCamera(Player player)
		{
			var skeleton = Location.SharedData.SharedDataUtils.FindChildSharedData<SkeletonConfig>(player.SkeletonId);
			if (player != null && skeleton != null)
			{
				Player = player;

				var headRigidBody = skeleton.SkeletonSubjectNames[(int)ESkeletonSubject.Head];
				
				if (!string.IsNullOrEmpty(headRigidBody))
				{
					//Init
					InitPlayer(headRigidBody);
					Debug.LogFormat("<color=yellow>Initialized camera to head rigid body: {0}</color>", headRigidBody);
				}
				else
				{
					Debug.LogError("<color=yellow>Failed to initialize player camera. The given skeleton does not have a valid head rigidbody</color>");
				}
			}
			else
			{
				if(DevelopmentMode.CurrentMode != EDevelopmentMode.Standalone) //Skeleton is not available in standalone mode
				{
					Debug.LogError("<color=yellow>Failed to initialize player camera. The given player is invalid</color>");
				}
			}
		}

		/// <summary>
		/// Removes the player initialization. The camera is positioned to the given DefaultCameraOffset.
		/// If no VR headset is available the player camera is switched off and the observer camera is
		/// enabled instead.
		/// </summary>
		public void ResetPlayerCamera()
		{
			ResetPlayer();
		}

		/// <summary>
		/// Calibrate / recalibrate player headset.
		/// The headset calibration only works if a player has already be initialized (InitPlayerCamera)
		/// and a VR headset is available.
		/// The normal calibration will delay until the headset is steady.
		/// </summary>
		public void Calibrate()
		{
			HeadsetCalibration.Calibrate();
		}

		/// <summary>
		/// Forces an instant recalibration.
		/// The headset calibration only works if a player has already be initialized (InitPlayerCamera)
		/// and a VR headset is available.
		/// </summary>
		public void CalibrateNow()
		{
			HeadsetCalibration.Calibrate(true);
		}

		/// <summary>
		/// Replace the current VR camera with the given one
		/// </summary>
		/// <param name="cameraTemplate">The new camera</param>
		public void ReplaceCamera(GameObject cameraTemplate)
		{
			InitCamera(cameraTemplate);
		}

		/// <summary>
		/// Show/hide the headset calibration crosshair
		/// </summary>
		public void DoToggleCalibrationCross()
		{
			if (CrosshairNetworkActivated)
			{
				CrosshairNetworkActivated.Toggle();
			}
		}

		public void ShowCalibrationCross()
		{
			if (NetworkInterface.Instance.ComponentType == ELocationComponentType.ExperienceClient)
				CalibrationCross = true;
		}

		public void HideCalibrationCross()
		{
			if (NetworkInterface.Instance.ComponentType == ELocationComponentType.ExperienceClient)
				CalibrationCross = false;
		}

		#endregion

		#region Unity events

		void Awake()
		{
			HeadsetCalibration = GetComponent<HeadsetCalibration>();

			//Hide calibration cross by default
			CalibrationCross = false;

			Init();
			ResetPlayer();
		}

		void LateUpdate()
		{
			//Apply global offset
			if (GlobalOffset && GlobalMocapOffset.Instance)
			{
				GlobalOffset.localPosition = GlobalMocapOffset.Instance.GlobalPositionOffset;
				GlobalOffset.localRotation = GlobalMocapOffset.Instance.GlobalRotationOffset;
			}

			//Apply avatar specific offset on client only
			if (NetworkInterface.Instance.IsClient && AvatarOffset && AvatarOffsetController.Instance)
			{
				var avatarOffset = AvatarOffsetController.Instance.MainPlayerOffset;
				AvatarOffset.localPosition = avatarOffset != null ? avatarOffset.localPosition : Vector3.zero;
				AvatarOffset.localRotation = avatarOffset != null ? avatarOffset.localRotation : Quaternion.identity;
			}
		}

		#endregion

		#region Events

		private void OnErrorReportOpened(ErrorReporting.IReport report)
		{
			if (report.Data.GetType() == typeof(HmdTrackingLost))
			{
				UserMessageDisplayer.DisplayUserMessage(
					EUserMessageType.Warning,
					SDKTexts.ID_TRACKING_LOST,
					isTextId: true,
					defaultText: SDKTexts.SDK_DEFAULT_TEXTS[SDKTexts.ID_TRACKING_LOST]);
			}
		}

		private void OnErrorReportClosing(ErrorReporting.IReport report)
		{
			if (report.Data.GetType() == typeof(HmdTrackingLost))
			{
				UserMessageDisplayer.HideUserMessage();
			}
		}

		#endregion

		#region Internals

		private void Init()
		{
#if UNITY_5_6
			if(XRDevice.isPresent)
			{
				InputTracking.disablePositionalTracking = true;
			}
#endif

			//Init observer camera fader
			ObserverCameraFader = CreateObserverCamera();
			ObserverCamera = ObserverCameraFader.gameObject;

			//Init user message displayer
			UserMessageDisplayer = CreateUserMessageDisplayer();

			//Create default camera
			InitCamera(GetDefaultCameraTemplate());

			//Init rigidbody rotation when no HMD is present and enabled
			if (HeadRigidBody)
			{
				HeadRigidBody.UpdateRotation = !XRSettings.enabled;
			}
		}

		private void InitCamera(GameObject cameraTemplate)
		{
			if (!cameraTemplate)
				cameraTemplate = GetDefaultCameraTemplate();

			if (cameraTemplate)
			{
				if (cameraTemplate == CurrentCameraTemplate)
					return;

				Debug.LogFormat("<color=yellow>Creating camera from template: {0}</color>", cameraTemplate.name);

				//Create camera
				var newCameraInstance = Instantiate(cameraTemplate);
				if(newCameraInstance)
				{
					//Add tracking controller to camera
					var camTrackingController = newCameraInstance.AddComponent<CameraTrackingController>();
					camTrackingController.Setup(SensorCorrection);

					var newCameraFader = newCameraInstance.GetComponent<VRCameraFader>();

					if (newCameraFader)
					{
						//Check for rotation on the template. If there is one it will fail since unity stores the initial rotation on
						//VR cameras.
						if (newCameraInstance.transform.rotation != Quaternion.identity)
							Debug.LogErrorFormat("<color=yellow>The given camera template has a rotation. Set it to 0 in Unity! template={0}, rotation: {1}</color>", newCameraInstance.name, newCameraInstance.transform.rotation.eulerAngles);

						//Set active according to prev camera if available
						newCameraInstance.SetActive(PlayerCamera && PlayerCamera.activeInHierarchy);

						//Replace current camera with new one
						UnityUtils.RemoveAllChildren(CameraRoot);

						//Parent new camera to camera root and disable it
						newCameraInstance.transform.SetParent(CameraRoot);
						newCameraInstance.transform.localPosition = Vector3.zero;
						newCameraInstance.transform.localRotation = Quaternion.identity;

						//Set previous faded value
						CopyFaderState(CameraFader, newCameraFader);

						//Assign new camera
						PlayerCamera = newCameraInstance;
						PlayerCameraFader = newCameraFader;
						CurrentCameraTemplate = cameraTemplate;
					}
					else
					{
						Debug.LogError("<color=yellow>The given camera template does not have a CameraFader script attached to it</color>");
						DestroyImmediate(newCameraInstance);
						return;
					}
				}
				else
				{
					Debug.LogError("<color=yellow>The given camera template could not be created</color>");
					DestroyImmediate(newCameraInstance);
					return;
				}
			}
			else
			{
				Debug.LogError("<color=yellow>No camera template set</color>");
			}
		}

		private IUserMessageDisplayer CreateUserMessageDisplayer()
		{
			var messageDisplayerTemplate = DefaultUserMessageDisplayer;
			if(ConfigService.Instance.ExperienceSettings.DefaultUserMessageDisplayer)
			{
				if(ConfigService.Instance.ExperienceSettings.DefaultUserMessageDisplayer.GetComponent<IUserMessageDisplayer>() != null)
				{
					messageDisplayerTemplate = ConfigService.Instance.ExperienceSettings.DefaultUserMessageDisplayer;
				}
				else
				{
					Debug.LogWarningFormat("The user message displayer configured in the ExperienceSettings ({0}) does not have a behaviour implementing the IUserMessageDisplayer interface attached. Using SDK default displayer", ConfigService.Instance.ExperienceSettings.DefaultUserMessageDisplayer.name);
				}
			}

			var messageDisplayer = UnityUtils.InstantiatePrefab<IUserMessageDisplayer>(messageDisplayerTemplate, transform);
			return messageDisplayer;
		}

		private VRCameraFader CreateObserverCamera()
		{
			GameObject cameraTemplate = null;

			//Override server camera. Only when used as server and no dev mode
			if(NetworkInterface.Instance.IsServer && DevelopmentMode.CurrentMode == EDevelopmentMode.None)
			{
				cameraTemplate = ConfigService.Instance.ExperienceSettings.DefaultServerCameraTemplate;
			}
			else
			{
				//Use observer camera in settings
				cameraTemplate = ConfigService.Instance.ExperienceSettings.DefaultObserverCameraTemplate;
			}

			//Verify template from settings
			if(cameraTemplate && !cameraTemplate.GetComponent<VRCameraFader>())
			{
				//Fall back to default
				Debug.LogWarningFormat("The observer camera configured in the ExperienceSettings ({0}) does not have a VRCameraFader attached. Using SDK default camera", cameraTemplate.name);
				cameraTemplate = null;
			}

			if (!cameraTemplate)
				cameraTemplate = DefaultObserverCameraTemplate;
			
			if (ConfigService.Instance.ExperienceSettings.DefaultObserverCameraTemplate)
			{
				if (ConfigService.Instance.ExperienceSettings.DefaultObserverCameraTemplate.GetComponent<VRCameraFader>() != null)
					cameraTemplate = ConfigService.Instance.ExperienceSettings.DefaultObserverCameraTemplate;
				else
					Debug.LogWarningFormat("The observer camera configured in the ExperienceSettings ({0}) does not have a VRCameraFader attached. Using SDK default camera", ConfigService.Instance.ExperienceSettings.DefaultObserverCameraTemplate.name);
			}


			var observerCamera = UnityUtils.InstantiatePrefab<VRCameraFader>(cameraTemplate, GlobalOffset);

			//Position: Apply template offset as local offset
			observerCamera.transform.localPosition = cameraTemplate.transform.position;
			observerCamera.transform.rotation = cameraTemplate.transform.rotation;

			return observerCamera;
		}

		private void InitPlayer(string headRigidBodyName)
		{
			//Setup rigidbody
			HeadRigidBody.ResetRigidbodyName(headRigidBodyName);

			//Only monitor rigidbody in clients
			if (NetworkInterface.Instance.ComponentType == ELocationComponentType.ExperienceClient)
			{
				float timeout = (float)ErrorReporting.Instance.GetParamValue<HmdTrackingLost>("RigidbodyTimeout", 3);
				var errorData = new HmdTrackingLost()
				{
					HmdName = headRigidBodyName,
					Timeout = timeout,
				};
				HeadRigidBody.StartMonitoringTimeout(errorData, timeout);
			}

			//This is to make sure we don't subscribe twice
			ErrorReporting.Instance.ReportOpened -= OnErrorReportOpened;
			ErrorReporting.Instance.ReportClosing -= OnErrorReportClosing;

			//Attach timeout
			ErrorReporting.Instance.ReportOpened += OnErrorReportOpened;
			ErrorReporting.Instance.ReportClosing += OnErrorReportClosing;

			//Enable camera
			EnablePlayerCamera();

			//Setup the calibration
			HeadsetCalibration.Setup(Player, HeadRigidBody, CalibrationOffset);

			//Calibrate
			if (XRDevice.isPresent)
			{
				//Do a first calibration to have an initial player position.
				HeadsetCalibration.Calibrate(true);

				//Start a normal calibration to have a more precise calibration of the players head.
				HeadsetCalibration.Calibrate(false);
			}
		}
		
		private void ResetPlayer()
		{
			//Release rigidbody
			HeadRigidBody.ResetRigidbodyName(null);
			HeadRigidBody.transform.localPosition = DefaultCameraPosition;
			HeadRigidBody.transform.localRotation = Quaternion.identity;
			HeadRigidBody.StopMonitoringTimeout();
			ErrorReporting.Instance.ReportOpened -= OnErrorReportOpened;
			ErrorReporting.Instance.ReportClosing -= OnErrorReportClosing;

			//Remove the user message if displayed
			if (UserMessageDisplayer != null)
				UserMessageDisplayer.HideUserMessage();

			//Reset player
			Player = null;

			//Enable observer camera if needed
			if(NetworkInterface.Instance.ComponentType != ELocationComponentType.ExperienceClient)
			{
				//Server, Observer....
				EnableObserverCamera();
			}
			else
			{
				//Client...
				if (XRDevice.isPresent)
					EnablePlayerCamera();
				else
					EnableObserverCamera();
			}
		}

		private void EnablePlayerCamera()
		{
			//Setup camera fader
			CopyFaderState(CameraFader, PlayerCameraFader);

			//Diable observer
			if (ObserverCamera) ObserverCamera.SetActive(false);

			//Enable player camera
			if (PlayerCamera) PlayerCamera.SetActive(true);
		}

		private void EnableObserverCamera()
		{
			//Setup camera fader
			CopyFaderState(CameraFader, ObserverCameraFader);

			//Enable observer
			if (ObserverCamera) ObserverCamera.SetActive(true);

			//Disable player camera
			if (PlayerCamera) PlayerCamera.SetActive(false);
		}


		private void CopyFaderState(VRCameraFader from, VRCameraFader to)
		{
			if(from && to && from != to)
			{
				Debug.LogFormat("<color=yellow>Copying fader state from {0} to {1}, state={2}</color>", from.gameObject.name, to.gameObject.name, from.TargetTransition);
				to.SetFaded(from.TargetTransition);
			}
		}

		private GameObject GetDefaultCameraTemplate()
		{
			if (ConfigService.Instance.ExperienceSettings != null && ConfigService.Instance.ExperienceSettings.DefaultCameraTemplate != null)
				return ConfigService.Instance.ExperienceSettings.DefaultCameraTemplate;
			else
				return DefaultCameraTemplate;
		}

		#endregion
	}

}