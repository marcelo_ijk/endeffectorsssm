﻿using Artanim.Location.SharedData;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Artanim
{
	public class AvatarViewCameraController : MonoBehaviour
	{
		public enum ELookDirection { Front, Back, Left, Right, }

		public GameObject CameraTemplate;

		public ELookDirection LookDirection;

		public Vector3 CameraPositionOffset;

		public GameObject PanelShowPlayers;
		public GameObject PanelViewControls;
		public Toggle ToggleShowPlayers;
		public Toggle ToggleOrthographic;

		public Transform[] Targets;
		public Transform[] Cameras;

		public float PlayerViewCullingRange = 0.5f;
		public LayerMask PlayerViewCullingMask;

		public float AvatarDistance = 2f;

		public void SetViewFront() { LookDirection = ELookDirection.Front; }
		public void SetViewBack() { LookDirection = ELookDirection.Back; }
		public void SetViewLeft() { LookDirection = ELookDirection.Left; }
		public void SetViewRight() { LookDirection = ELookDirection.Right; }

		public void SetOrthographic(bool orthographicView)
		{
			if(Cameras != null)
			{
				foreach (var cam in Cameras)
					cam.GetComponent<Camera>().orthographic = orthographicView;
			}
		}

		public void ShowPlayers(bool showPlayers)
		{
			CreateLayout();
		}

		private void OnEnable()
		{
			CreateLayout();

			if(GameController.Instance)
			{
				GameController.Instance.OnJoinedSession += Instance_OnJoinedSession;
				GameController.Instance.OnLeftSession += Instance_OnLeftSession;
				GameController.Instance.OnSessionPlayerJoined += Instance_OnSessionPlayerJoined;
				GameController.Instance.OnSessionPlayerLeft += Instance_OnSessionPlayerLeft;
			}
		}
		
		private void OnDisable()
		{
			if(GameController.Instance)
			{
				GameController.Instance.OnJoinedSession -= Instance_OnJoinedSession;
				GameController.Instance.OnSessionPlayerJoined -= Instance_OnSessionPlayerJoined;
				GameController.Instance.OnSessionPlayerLeft -= Instance_OnSessionPlayerLeft;
			}
		}

		private void Instance_OnJoinedSession(Location.Data.Session session, System.Guid playerId)
		{
			CreateLayout();
		}

		private void Instance_OnLeftSession()
		{
			CreateLayout();
		}

		private void Instance_OnSessionPlayerLeft(Location.Data.Session session, System.Guid playerId)
		{
			CreateLayout();
		}

		private void Instance_OnSessionPlayerJoined(Location.Data.Session session, System.Guid playerId)
		{
			CreateLayout();
		}

		private void LateUpdate()
		{
			if (Targets != null && Targets.Length > 0)
			{
				for (var i = 0; i < Targets.Length; ++i)
				{
					PositionCamera(Targets[i], Cameras[i]);
				}
			}
		}

		private void PositionCamera(Transform target, Transform camera)
		{
			var direction = GetLookDirection(target);
			direction.Normalize();
			camera.position = direction * AvatarDistance + target.position + CameraPositionOffset;

			camera.LookAt(target.position + CameraPositionOffset);
		}

		private Vector3 GetLookDirection(Transform target)
		{
			switch (LookDirection)
			{
				case ELookDirection.Front:
					return target.forward;
				case ELookDirection.Back:
					return target.forward * -1;
				case ELookDirection.Left:
					return target.right * -1;
				case ELookDirection.Right:
					return target.right;
				default:
					return target.forward;
			}
		}

		private void CreateLayout()
		{
			if(ToggleShowPlayers)
			{
				if(ToggleShowPlayers.isOn)
				{
					BuildPlayerViews();
				}
				else
				{
					ClearPlayerViews();

					//Show or hide view controls based on available players
					ShowHideViewControls();
				}
			}
		}

		private void ShowHideViewControls()
		{
			//Show hide view controls in function if there are players available
			if (PanelShowPlayers)
				PanelShowPlayers.SetActive(GameController.Instance.RuntimePlayers.Count > 0);

			if (PanelViewControls)
				PanelViewControls.SetActive(ToggleShowPlayers.isActiveAndEnabled && ToggleShowPlayers.isOn);
		}

		private void BuildPlayerViews()
		{
			if (CameraTemplate)
			{
				//Clear avatar states
				ResetAvatarStates();

				//Clear existing cameras
				ClearPlayerViews();

				//Find all targets and create cameras
				Targets = new Transform[GameController.Instance.RuntimePlayers.Count];
				Cameras = new Transform[Targets.Length];
				for (var p = 0; p < GameController.Instance.RuntimePlayers.Count; ++p)
				{
					var player = GameController.Instance.RuntimePlayers[p];

					//Keep target transform
					Targets[p] = player.AvatarController.GetAvatarRoot();

					//Create camera AvatarViewCamera
					var avatarViewCamera = UnityUtils.InstantiatePrefab<AvatarViewCamera>(CameraTemplate, transform);
					avatarViewCamera.name = string.Format("Camera for {0}", Targets[p].name);

					//Set player name visual
					avatarViewCamera.SetPlayer(player, this);

					//Setup camera
					var cam = avatarViewCamera.GetComponent<Camera>();
					cam.cullingMask = PlayerViewCullingMask.value;
					cam.clearFlags = CameraClearFlags.SolidColor;
					cam.backgroundColor = GameController.Instance.GetPlayerColor(player.Player.ComponentId, Color.black);
					cam.nearClipPlane = AvatarDistance - PlayerViewCullingRange;
					cam.farClipPlane = AvatarDistance + PlayerViewCullingRange;
					if (ToggleOrthographic)
						cam.orthographic = ToggleOrthographic.isOn;

					Cameras[p] = avatarViewCamera.transform;
				}

				//Show or hide view controls based on available players
				ShowHideViewControls();

				//Calc views grid dimension
				var gridDimension = CalcGridDimension(Targets.Length);

				//Layout cameras
				var camIndex = 0;
				var camW = 1f / gridDimension.x;
				var camH = 1f / gridDimension.y;
				for (var line = 0; line < gridDimension.y; ++line)
				{
					for (var col = 0; col < gridDimension.x; ++col)
					{
						if (camIndex < Cameras.Length)
						{
							var camera = Cameras[camIndex].GetComponent<Camera>();

							//Set viewport rect based on grid size
							var camRect = camera.rect;
							camRect.width = camW;
							camRect.height = camH;
							camRect.x = col * camW;
							camRect.y = (gridDimension.y - 1 - line) * camH;
							camera.rect = camRect;
						}

						camIndex++;
					}
				}
			}
			else
			{
				Debug.LogError("No camera template set");
			}
		}

		private void ClearPlayerViews()
		{
			if (Cameras != null)
			{
				foreach (var cam in Cameras)
					Destroy(cam.gameObject);
				Cameras = null;
			}

			if(Targets != null)
			{
				Targets = null;
			}
		}

		private Vector2 CalcGridDimension(int numElements)
		{
			var gridDimension = new Vector2();
			gridDimension.y = Mathf.Floor(Mathf.Sqrt(numElements));
			gridDimension.x = Mathf.Floor(Mathf.Ceil(numElements / gridDimension.y));

			Debug.LogFormat("Calculated grid for {0} elments: {0}", numElements, gridDimension);
			return gridDimension;
		}

		#region Avatar view control

		private int LastAvatarRenderersUpdate = -1;
		private Dictionary<Guid, AvatarRenderersState> AvatarRendererStates;

		public void PrepareAvatarVisibility(Guid playerId)
		{
			UpdateAvatarStates();
			
			for(var p = 0; p < GameController.Instance.RuntimePlayers.Count; ++p)
			{
				var player = GameController.Instance.RuntimePlayers[p];

				//Hide all but the requested player
				if(player.Player.ComponentId != playerId)
				{
					var avatarState = AvatarRendererStates[player.Player.ComponentId];
					for (var r = 0; r < avatarState.Renderers.Length; ++r)
					{
						avatarState.Renderers[r].enabled = false;
					}
				}
			}
		}

		public void ResetAvatarsVisibility()
		{
			for (var p = 0; p < GameController.Instance.RuntimePlayers.Count; ++p)
			{
				var avatarState = AvatarRendererStates[GameController.Instance.RuntimePlayers[p].Player.ComponentId];
				for (var r = 0; r < avatarState.Renderers.Length; ++r)
				{
					avatarState.Renderers[r].enabled = avatarState.RendererStates[r];
				}
			}

			//for (var p=0; p < AvatarRendererStates.Values.Count; ++p)
			//{

			//	var avatarState = AvatarRendererStates.Values.ElementAt(p);
			//	for (var r = 0; r < avatarState.Renderers.Length; ++r)
			//	{
			//		avatarState.Renderers[r].enabled = avatarState.RendererStates[r];
			//	}
			//}
		}

		private void ResetAvatarStates()
		{
			AvatarRendererStates = new Dictionary<Guid, AvatarRenderersState>();
			LastAvatarRenderersUpdate = -1;
		}

		private void UpdateAvatarStates()
		{
			//Only update once per frame
			if (LastAvatarRenderersUpdate != Time.frameCount)
			{
				//Debug.LogFormat("Update for frame: {0}", Time.frameCount);

				AvatarRenderersState avatarState;
				for (var p = 0; p < GameController.Instance.RuntimePlayers.Count; ++p)
				{
					var player = GameController.Instance.RuntimePlayers[p];

					if (!AvatarRendererStates.TryGetValue(player.Player.ComponentId, out avatarState))
					{
						avatarState = new AvatarRenderersState();
						avatarState.PlayerId = player.Player.ComponentId;
						AvatarRendererStates.Add(player.Player.ComponentId, avatarState);
					}

					//Update state
					if (avatarState.RendererStates == null)
					{
						//Set renderers
						var renderers = player.PlayerInstance.GetComponentsInChildren<Renderer>();
						avatarState.RendererStates = new bool[renderers.Length];
						avatarState.Renderers = new Renderer[renderers.Length];

						for(var r = 0; r < renderers.Length; ++r)
						{
							avatarState.Renderers[r] = renderers[r];
							avatarState.RendererStates[r] = renderers[r].enabled;
						}
					}
					else
					{
						//Just update the current state
						for (var r = 0; r < avatarState.Renderers.Length; ++r)
						{
							avatarState.RendererStates[r] = avatarState.Renderers[r].enabled;
						}
					}

					
				}

				LastAvatarRenderersUpdate = Time.frameCount;
			}
		}

		private class AvatarRenderersState
		{
			public Guid PlayerId { get; set; }
			public bool[] RendererStates { get; set; }
			public Renderer[] Renderers { get; set; }
		}
		
		#endregion

	}

}