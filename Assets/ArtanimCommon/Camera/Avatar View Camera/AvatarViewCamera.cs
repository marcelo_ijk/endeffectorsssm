﻿using Artanim.Location.Data;
using Artanim.Location.SharedData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Artanim
{
	public class AvatarViewCamera : MonoBehaviour
	{
		public Text TextPlayerName;

		private RuntimePlayer Player;
		private AvatarViewCameraController ViewController;
		private int DefaultPlayerLayer;

		public void SetPlayer(RuntimePlayer player, AvatarViewCameraController controller)
		{
			if(player != null)
			{
				Player = player;
				ViewController = controller;
				DefaultPlayerLayer = Player.PlayerInstance.layer;
				if (TextPlayerName)
				{
					var clientSharedId = SharedDataUtils.FindLocationComponent(Player.Player.ComponentId).SharedId;
					TextPlayerName.text = clientSharedId.ToString();
				}
			}
		}

		private void OnPreRender()
		{
			ViewController.PrepareAvatarVisibility(Player.Player.ComponentId);
		}

		private void OnPostRender()
		{
			ViewController.ResetAvatarsVisibility();
		}
	}
}