﻿using Artanim.Location.Data;
using Artanim.Monitoring;
using Artanim.Vicon;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using AFAU = Artanim.Utils.ArrayUtils;

#if UNITY_2017_3_OR_NEWER
using UnityEngine.XR;
#else
using UnityEngine.VR;
using XRNode = UnityEngine.VR.VRNode;
using XRNodeState = UnityEngine.VR.VRNodeState;
using XRSettings = UnityEngine.VR.VRSettings;
using XRDevice = UnityEngine.VR.VRDevice;
#endif

namespace Artanim
{

	public class HeadsetCalibration : MonoBehaviour
	{
		public bool IsCalibrated { get { return InternalIsCalibrated; } }

		public Quaternion InitialRotationOffset { get; private set; }

		[SerializeField]
		private bool NeedCalibration = true;

		[SerializeField]
		private bool InternalIsCalibrated = false;

		[SerializeField]
		private bool DebugCalibrateNow = false;

		Player Player;
		ViconRigidBody HeadRigidBody;
		Transform CalibrationOffset;

		MetricsChannel<HeadsetRotationMetricsData> _headsetRotationMetrics;
		MetricsChannel<UserLatencyMetricsData> _userLatencyMetrics;
		MetricsChannel<float> _oculusDriftMetrics;

		enum CalibrationMode { Manual, Continuous };
		CalibrationMode _calibrationMode;

		bool _calibrated;

		// The data structure used to send the rotation metrics
		[StructLayout(LayoutKind.Sequential)]
		struct HeadsetRotationMetricsData
		{
			public float RigidBody;
			public float HMD;
			public float RigidBody_x;
			public float HMD_x;
			public float RigidBody_z;
			public float HMD_z;
		}

		// The data structure used to send the user latency metrics
		[StructLayout(LayoutKind.Sequential)]
		struct UserLatencyMetricsData
		{
			public float Dt;
			public float TimeLag;
		}

		#region Public interface

		public void Setup(Player player, ViconRigidBody headRigidBody, Transform calibrationOffset)
		{
			Player = player;
			HeadRigidBody = headRigidBody;
			CalibrationOffset = calibrationOffset;
		}

		/// <summary>
		/// Start headset calibration sequence.
		/// In order to calibrate a VR headset has to be attached.
		/// </summary>
		/// <param name="calibrateNow">Override the delayed calibration and calibrate the headset immediately</param>
		public void Calibrate(bool calibrateNow = false)
		{
			if (Player != null && XRSettings.enabled)
			{
				_logCal("Calibrate received, calibrateNow = " + calibrateNow);

				if (calibrateNow)
					CalibrateHeadset();
				else
					_requireCalibration();
			}
		}

		#endregion

		#region Unity events

		void Awake()
		{
			//Load calibration settings
			_loadCalibrationConfig();
			_curCalibrationWait = 0.0f;
		}

#if HEADSET_METRICS
		IEnumerator Start()
		{
			int windowSize = 128;
			var latency = new LatencyEstimation(windowSize);

			var waitEndOfFrame = new WaitForEndOfFrame();
			while (true)
			{
				yield return waitEndOfFrame;
				if (enabled)
				{
					SendMetrics(latency);
				}
			}
		}

		void SendMetrics(LatencyEstimation latency)
		{
			var camCtrl = MainCameraController.Instance;
			if (HeadRigidBody && (camCtrl != null) && (camCtrl.PlayerCamera != null))
			{
				var rot = HeadRigidBody.RigidbodyRotation;
				var rotVR = camCtrl.PlayerCamera.transform.rotation;

				// Push new coords
				latency.Push(rot.y, rotVR.y, Time.realtimeSinceStartup);

				var rotationData = new HeadsetRotationMetricsData()
				{
					RigidBody = rot.y,
					HMD = rotVR.y,
					//TODO TEMP!
					RigidBody_x = rot.x,
					HMD_x = rotVR.x,
					RigidBody_z = rot.z,
					HMD_z = rotVR.z,
				};
				unsafe
				{
					_headsetRotationMetrics.SendRawData(new System.IntPtr(&rotationData));
				}

				float angle = Quaternion.Angle(rot, rotVR);
				unsafe
				{
					_oculusDriftMetrics.SendRawData(new System.IntPtr(&angle));
				}

				// Mean time interval between consecutive packages
				var latencyData = new UserLatencyMetricsData()
				{
					Dt = latency.LastGoodMuDt,
					TimeLag = latency.LastGoodTimeLag,
				};
				unsafe
				{
					_userLatencyMetrics.SendRawData(new System.IntPtr(&latencyData));
				}
			}
		}
#endif

		void OnEnable()
		{
#if HEADSET_METRICS
			// Create metrics channels
			_headsetRotationMetrics = MetricsManager.Instance.GetChannelInstance<HeadsetRotationMetricsData>(MetricsAction.Create, "Headset Rotation");
			_userLatencyMetrics = MetricsManager.Instance.GetChannelInstance<UserLatencyMetricsData>(MetricsAction.Create, "User Latency");
			_oculusDriftMetrics = MetricsManager.Instance.GetChannelInstance<float>(MetricsAction.Create, "Oculus Drift");
#endif
		}

		void OnDisable()
		{
			// Destroy metrics channels
			if (_headsetRotationMetrics != null)
			{
				_headsetRotationMetrics.Dispose();
				_headsetRotationMetrics = null;
			}
			if (_userLatencyMetrics != null)
			{
				_userLatencyMetrics.Dispose();
				_userLatencyMetrics = null;
			}
			if (_oculusDriftMetrics != null)
			{
				_oculusDriftMetrics.Dispose();
				_oculusDriftMetrics = null;
			}
		}

		void Update()
		{
			if (Player != null && XRSettings.enabled)
			{
				if (_calibrationMode == CalibrationMode.Continuous)
				{
					UpdateContinuousCalibration();
				}
				else
				{
					UpdateCalibration();
				}
			}
		}

#endregion

#region Continuous Calibration

		// smoothing alpha, the largest the error angle the quicker the drift error gets corrected, at a minimum rate of 0.005f
		float _smoothingAlpha = 0.001f;
		// Whether or not we only correct the camera rotation around the vertical axis
		bool _correctYOnly = false;
		// Threshold for skipping smooth correction and do an instant calibrations (snap)
		float _correctThreshold = 10f;
		// Number of continuous frames with enough error before snapping
		int _correctThresholdNumFrames = 45, _correctThresholdCounter;
		// Keep last value to detect a change
		uint _lastRigidbodyFrameNumber;

		void UpdateContinuousCalibration()
		{
			// Don't do anything until it's ready
			if (!_calibrated) return;

			// Also don't apply a correction if the rigidbody hasn't moved
			if (_lastRigidbodyFrameNumber != HeadRigidBody.LastUpdateFrameNumber)
			{
				_lastRigidbodyFrameNumber = HeadRigidBody.LastUpdateFrameNumber;

				//
				// Below is code given by Henrique :)
				//

				// current error (biased by the optical tracking latency)
				Quaternion R_h_v = HeadRigidBody.RigidbodyRotation * Quaternion.Inverse(InputTracking.GetLocalRotation(XRNode.CenterEye));
				Quaternion R_h_v_s = CalibrationOffset.localRotation;
				if (_correctYOnly)
				{
					// normally, we should normalize the quat after such operation, but Unity seems to take care of that
					R_h_v.x = R_h_v.z = 0;
					R_h_v_s.x = R_h_v_s.z = 0;
				}

				// check error
				if (Quaternion.Angle(R_h_v_s, R_h_v) > _correctThreshold)
				{
					++_correctThresholdCounter;
				}
				else
				{
					_correctThresholdCounter = 0;
				}

				// update rotation
				if (_correctThresholdCounter > _correctThresholdNumFrames)
				{
					// immediate re-orientation
					CalibrationOffset.localRotation = R_h_v;
					_correctThresholdCounter = 0;
				}
				else
				{
					// new offset
					CalibrationOffset.localRotation = Quaternion.Slerp(R_h_v_s, R_h_v, _smoothingAlpha);
				}
			}
			else if (_correctThresholdCounter <= _correctThresholdNumFrames)
			{
				// Count missed frames as errors, so we have an immediate re-orientation when getting tracking back
				++_correctThresholdCounter;
			}
		}

#endregion

#region Internals

		private void UpdateCalibration()
		{
			//Check for delayed calibration process
			if (NeedCalibration)
			{
				if (HeadRigidBody.IsTransformUpToDate)
					_checkForCalibration(Time.deltaTime); // We got vicon tracking -> do the calibration process.
				else
				{
					_logCal("Lost tracking, reseting calibration");
					_requireCalibration(); // We lost Vicon tracking -> reset the calibration process.
				}
			}

			//Check for immediate calibration: Used for debugging in Unity
			if (DebugCalibrateNow)
			{
				CalibrateHeadset();
				DebugCalibrateNow = false;
			}
		}

		private void CalibrateHeadset()
		{
			Debug.Log("Starting headset calibration");

			// Compute initial rotation offset between rigidbody and headset
			InitialRotationOffset = HeadRigidBody.RigidbodyRotation * Quaternion.Inverse(InputTracking.GetLocalRotation(XRNode.CenterEye));

			// And apply it
			CalibrationOffset.localRotation = InitialRotationOffset;

			// Tag as ready
			_calibrated = true;
		}

		private float _calibrateDelay, _stabilizationDuration, _angularVelocityThresholdDegree;

		private struct _HeadSample
		{
			//TODO: remove the unused methods...
			public Vector3 right, up;

			public _HeadSample(Vector3 right_, Vector3 up_)
			{
				right = right_;
				up = up_;
			}

			public _HeadSample(Transform t)
			{
				right = t.right;
				up = t.up;
			}

			public void set(Transform t)
			{
				right = t.right;
				up = t.up;
			}

			public static float minCos(_HeadSample a, _HeadSample b)
			{
				return Mathf.Min(Vector3.Dot(a.right, b.right), Vector3.Dot(a.up, b.up));
			}

			public static _HeadSample mean(_HeadSample[] smps, int nSamples)
			{
				_HeadSample result = new _HeadSample(Vector3.zero, Vector3.zero);
				return result;
			}
		}
		private struct _AngularSample
		{
			//TODO: remove the unused methods...
			public float av, dt; // Angular Velocity (degrees per secs) & Delta Time (secs).

			public _AngularSample(float av_, float dt_) { av = av_; dt = dt_; }
			public _AngularSample(_HeadSample t0, _HeadSample t1, float dt_)
			{
				float cos = _HeadSample.minCos(t0, t1);
				av = Mathf.Rad2Deg * Mathf.Acos(cos) / dt_;
				dt = dt_;
			}
			public void set(_HeadSample t0, _HeadSample t1, float dt_)
			{
				float cos = _HeadSample.minCos(t0, t1);
				av = Mathf.Rad2Deg * Mathf.Acos(cos) / dt_;
				dt = dt_;
			}
		}

		private bool _stabilizing = false;
		private float _stableDuration = 0.0f, _curCalibrationWait = 0.0f;
		private _HeadSample _lastHeadSample;
		private int _maxSamples = 8, _nSamples = 0, _iSample = 0;
		private _AngularSample[] _headRotSamples;

		private void _loadCalibrationConfig()
		{
			var conf = ConfigService.Instance.Config.Location.Client.HMD;
			_calibrateDelay = conf.CalibrationDelay;
			_stabilizationDuration = conf.CalibrationStabilizationDuration;
			_angularVelocityThresholdDegree = conf.CalibrationAngularVelocityThreshold;
			_calibrationMode = conf.ContinuousCalibration ? CalibrationMode.Continuous : CalibrationMode.Manual;
			_smoothingAlpha = Mathf.Abs(conf.ContinuousCorrectionFactor);
			_correctYOnly = conf.ContinuousCorrectionFactor >= 0;
			_correctThreshold = conf.ContinuousCorrectionThreshold;
			_correctThresholdNumFrames = conf.ContinuousCorrectionThresholdNumFrames;
		}

		private void _requireCalibration()
		{
			NeedCalibration = true;
			_stableDuration = 0.0f;
			_headRotSamples = AFAU.ArrayResize(_headRotSamples, _maxSamples);
			_resetSamples();
			_stabilizing = false;
		}

		private void _resetSamples()
		{
			_lastHeadSample = new _HeadSample(HeadRigidBody.transform);
			_nSamples = 0;
			_iSample = 0;
		}

		//TODO: We might want to log the states evolution for this.
		private void _checkForCalibration(float dt)
		{
			if (_curCalibrationWait < _calibrateDelay)
			{
				// We just wait until the delay is passed...
				_curCalibrationWait += dt;
			}
			else
			{
				// Initial wait delay passed -> normal calibration process.

				// Add sample to 'ring buffer':
				_HeadSample curHeadSample = new _HeadSample(HeadRigidBody.transform);
				if (_nSamples < _maxSamples)
				{
					// Not yet full, we increase both counter and pointer:
					_headRotSamples[_nSamples].set(_lastHeadSample, curHeadSample, dt);
					_iSample = ++_nSamples;
				}
				else
				{
					// Buffer is full, we just wrap around the pointer.
					_iSample %= _maxSamples;
					_headRotSamples[_iSample++].set(_lastHeadSample, curHeadSample, dt);
				}
				_lastHeadSample = curHeadSample;

				// Check for stability:
				if (_isStable())
				{
					if (_stabilizing)
					{
						_stableDuration += dt;
						if (_stabilizationDuration <= _stableDuration)
						{
							// Trigger VR helmet calibration:
							CalibrateHeadset();

							// We done with calibration here:
							NeedCalibration = false;

							//Send calibration message to location controller
							_logCal("Calibration DONE for " + Player.SkeletonId + " on " + Player.ComponentId);
						} // else -> we still have to wait more time...
					}
					else
					{
						// First stable frame -> reset stability phase.
						_stabilizing = true;
						_stableDuration = 0.0f;

						_logCal("Calibration stabilized for " + Player.SkeletonId + " on " + Player.ComponentId);
					}
				}
				else _stabilizing = false; // Obviously we're not done yet...
			}
		}

		private bool _isStable()
		{
			int nSamples = _nSamples;
			if (2 < nSamples)
			{
				// Just scan all recorded samples and check if none of the angular
				// velocities exceeds the threshold limit.
				for (int eSample = 0; eSample < nSamples; ++eSample)
					if (_angularVelocityThresholdDegree < _headRotSamples[eSample].av)
					{
						_logCal("! Angle Threshold failed !");
						return false;
					}
				//Note: for now we ignore the 'dt' for the samples, but we could
				// use it to limit the tested frames in time.
				//WARNING: to have a 'real' dt usage we should scan samples backward
				// wrapping from the current pointer...
				return true;
			}
			else _logCal("! Not enough samples !");
			return false;
		}

		private static bool _debugLogCalibration = false;
		private static LogSystem.Logger _logger;
		private static void _logCal(string msg)
		{
			if (_logger == null)
			{
				_logger = LogSystem.LogManager.Instance.GetLogger("HeadCalib");
			}
			if (_debugLogCalibration)
			{
				_logger.Warn(msg);
			}
			else
			{
				_logger.Debug(msg);
			}
		}

#endregion

	}

}