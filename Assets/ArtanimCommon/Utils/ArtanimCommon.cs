﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

namespace Artanim
{
	public static class ArtanimCommon
	{
#if UNITY_EDITOR
		public static string EditorCommonDir
		{
			get
			{
				string dir = Path.Combine(Application.dataPath, "ArtanimCommon");
				if (!Directory.Exists(dir))
				{
					dir = Path.Combine(Application.dataPath, "ArtanimCommon(Link)");
				}
				return dir;
			}
		}
#endif
	}
}