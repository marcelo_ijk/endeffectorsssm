﻿using Artanim.Location.Messages;
using Artanim.Location.Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

namespace Artanim
{

	[AddComponentMenu("Artanim/Experience Trigger Controller")]
	public class ExperienceTriggerController : MonoBehaviour
	{
		public enum ESyncMode { ClientAndServer, Client, Server }

		[Tooltip("Mode defining on which component type the actions are triggered.")]
		public ESyncMode SyncMode = ESyncMode.Server;

		public TriggerAction[] TriggerActions;

		void OnEnable()
		{
			if (NetworkInterface.Instance != null && NeedTrigger())
			{
				NetworkInterface.Instance.Subscribe<ExperienceTrigger>(NetworkMessage_ExperienceTrigger);
			}
		}

		void OnDisable()
		{
			if (NetworkInterface.Instance != null && NeedTrigger())
			{
				NetworkInterface.Instance.Unsubscribe<ExperienceTrigger>(NetworkMessage_ExperienceTrigger);
			}
		}

		private void NetworkMessage_ExperienceTrigger(ExperienceTrigger args)
		{
			//Debug.LogFormat("Experience trigger received: TriggerName={0}", args.TriggerName);
			if (NeedTrigger())
			{
				if (TriggerActions != null && TriggerActions.Length > 0 && args != null && !string.IsNullOrEmpty(args.TriggerName))
				{
					Debug.LogFormat("Experience trigger event received. Searching for corresponding action. TriggerName={0}", args.TriggerName);
					var action = TriggerActions.FirstOrDefault(t => t.TriggerName == args.TriggerName);
					if (action != null)
					{
						Debug.LogFormat("Found action for experience trigger={0}", args.TriggerName);
						action.Action.Invoke();
					}
				}
			}
		}

		protected bool NeedTrigger()
		{
			switch (SyncMode)
			{
				case ESyncMode.ClientAndServer:
					return true;
				case ESyncMode.Client:
					if (NetworkInterface.Instance.IsClient)
						return true;
					break;
				case ESyncMode.Server:
					if (NetworkInterface.Instance.IsServer)
						return true;
					break;
			}
			return false;
		}

	}

	[System.Serializable]
	public class TriggerAction
	{
		public string TriggerName;
		public UnityEvent Action;
	}

}