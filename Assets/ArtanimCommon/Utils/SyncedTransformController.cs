﻿using Artanim.Location.Data;
using Artanim.Location.Messages;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Artanim.Location.Network;

namespace Artanim
{
	public class SyncedTransformController : SingletonBehaviour<SyncedTransformController>
	{
		#region Events

		public delegate void OnTransformRegisteredHandler(NetworkSyncedTransform syncedTransform);
		public event OnTransformRegisteredHandler OnTransformRegistered;

		public delegate void OnTransformUnregisteredHandler(NetworkSyncedTransform syncedTransform);
		public event OnTransformUnregisteredHandler OnTransformUnregistered;

		#endregion

		private Dictionary<string, NetworkSyncedTransform> SyncedTransforms = new Dictionary<string, NetworkSyncedTransform>();

		public List<NetworkSyncedTransform> RegisteredTransforms
		{
			get
			{
				return SyncedTransforms.Values.ToList();
			}
		}


		#region Public Interface

		/// <summary>
		/// Register or update a synced transform
		/// </summary>
		/// <param name="objectId">ObjectId of the synced transform. This value cannot be null.</param>
		/// <param name="transform">Transform to register.</param>
		/// <returns>True if registration was successfull</returns>
		public bool RegisterTransform(NetworkSyncedTransform syncedTransform)
		{
			if (AddOrUpdateTransform(syncedTransform))
			{
				Debug.LogFormat("Registered synced transform. ObjectId={0}, Transform={1}", syncedTransform.ObjectId, syncedTransform.name);

				//Notify
				if (OnTransformRegistered != null)
					OnTransformRegistered(syncedTransform);

				return true;
			}
			return false;
		}

		/// <summary>
		/// Unregisters a synced transform
		/// </summary>
		/// <param name="objectId">ObjectId of the synced transform to be unregistered. This value cannot be null.</param>
		/// <returns>True if the unregistration was successfull</returns>
		public bool UnregisterTransform(string objectId)
		{
			var removedTransform = RemoveTransform(objectId);
			if (removedTransform)
			{
				Debug.LogFormat("Unregistered or updated synced transform. ObjectId={0}, Transform={1}", objectId, removedTransform ? removedTransform.name : "<null>");

				//Notify
				if (OnTransformUnregistered != null)
					OnTransformUnregistered(removedTransform);

				return true;
			}

			return false;
		}

		/// <summary>
		/// Unregisters all registered synced transforms 
		/// </summary>
		public void UnregisterAll()
		{
			foreach(var syncedTransform in SyncedTransforms.ToArray())
			{
				UnregisterTransform(syncedTransform.Key);
			}
		}

		/// <summary>
		/// Checks if the given objectId is already registered.
		/// </summary>
		/// <param name="objectId"></param>
		/// <returns>True if the objectId is registered as synced transform</returns>
		public bool IsRegistered(string objectId)
		{
			return SyncedTransforms.ContainsKey(objectId);
		}

		#endregion

		#region Unity Events

		private void OnEnable()
		{
			if(NetworkInterface.Instance.IsClient)
			{
				NetworkInterface.Instance.Subscribe<SyncedTransforms>(OnSyncedTransforms);
			}
		}

		private void OnDisable()
		{
			if (NetworkInterface.Instance.IsClient)
			{
				NetworkInterface.Instance.Unsubscribe<SyncedTransforms>(OnSyncedTransforms);
			}
		}


		//public int NumSent;
		//public int SizeSent;

		private Dictionary<string, SyncedTransform> TransformCache = new Dictionary<string, SyncedTransform>();
		private SyncedTransforms MessageCache = new SyncedTransforms();
		private void LateUpdate()
		{
			if(GameController.Instance.CurrentSession != null && NetworkInterface.Instance.IsServer && SyncedTransforms.Count > 0)
			{
				MessageCache.Transforms.Clear();

				//Collect synced transforms
				for(var i=0; i < SyncedTransforms.Count; ++i)
				//foreach(var syncedTransformInstance in SyncedTransforms.Values)
				{
					var syncedTransformInstance = SyncedTransforms.ElementAt(i).Value;

					if(syncedTransformInstance && (syncedTransformInstance.SyncPosition || syncedTransformInstance.SyncRotation || syncedTransformInstance.SyncScale) )
					{
						//Get cached value
						SyncedTransform cachedTransform;
						if (!TransformCache.TryGetValue(syncedTransformInstance.ObjectId, out cachedTransform))
						{
							//Create cached entry
							cachedTransform = new SyncedTransform { ObjectId = syncedTransformInstance.ObjectId, };
							TransformCache.Add(cachedTransform.ObjectId, cachedTransform);
						}

						var doSend = false;

						//Position?
						if (syncedTransformInstance.SyncPosition)
						{
							if (SetSyncedPosition(syncedTransformInstance.transform, syncedTransformInstance.SyncSpace, cachedTransform))
								doSend = true;
						}
						else
							cachedTransform.Position = null;

						//Rotation?
						if (syncedTransformInstance.SyncRotation)
						{
							if (SetSyncedRotation(syncedTransformInstance.transform, syncedTransformInstance.SyncSpace, cachedTransform))
								doSend = true;
						}
						else
							cachedTransform.Rotation = null;

						//Scale?
						if (syncedTransformInstance.SyncScale)
						{
							if (SetSyncedScale(syncedTransformInstance.transform, cachedTransform))
								doSend = true;
						}
						else
							cachedTransform.Scale = null;

						if(doSend)
							MessageCache.Transforms.Add(cachedTransform);
					}
				}

				//Send update
				if(MessageCache.Transforms.Count > 0)
				{
					NetworkInterface.Instance.SendMessage(MessageCache);

					//NumSent = MessageCache.Transforms.Count;
					//SizeSent = new MessagePackSerializer().Serialize(MessageCache).Length;
				}
				//else
				//{
				//	NumSent = 0;
				//	SizeSent = 0;
				//}
			}
			//else
			//{
			//	NumSent = 0;
			//	SizeSent = 0;
			//}
		}

		#endregion

		#region Location Events

		private void OnSyncedTransforms(SyncedTransforms syncedTransforms)
		{
			if(syncedTransforms.Transforms.Count > 0)
			{
				foreach(var syncedTransform in syncedTransforms.Transforms)
				{
					NetworkSyncedTransform localTransform;
					if(SyncedTransforms.TryGetValue(syncedTransform.ObjectId, out localTransform))
					{
						//Apply position
						if(syncedTransform.Position != null)
						{
							var position = new Vector3(syncedTransform.Position.X, syncedTransform.Position.Y, syncedTransform.Position.Z);
							if (localTransform.SyncSpace == NetworkSyncedTransform.ESyncSpace.Global)
								localTransform.transform.position = position;
							else
								localTransform.transform.localPosition = position;
						}

						//Apply rotation
						if(syncedTransform.Rotation != null)
						{
							var rotation = new Quaternion(syncedTransform.Rotation.X, syncedTransform.Rotation.Y, syncedTransform.Rotation.Z, syncedTransform.Rotation.W);
							if (localTransform.SyncSpace == NetworkSyncedTransform.ESyncSpace.Global)
								localTransform.transform.rotation = rotation;
							else
								localTransform.transform.localRotation = rotation;
						}

						//Apply scale
						if(syncedTransform.Scale != null)
						{
							var scale = new Vector3(syncedTransform.Scale.X, syncedTransform.Scale.Y, syncedTransform.Scale.Z);
							localTransform.transform.localScale = scale;
						}
					}
				}
			}
		}

		#endregion

		#region Internals

		private bool SetSyncedPosition(Transform transform, NetworkSyncedTransform.ESyncSpace space, SyncedTransform syncedTransform)
		{
			var currentPosition = space == NetworkSyncedTransform.ESyncSpace.Global ? transform.position : transform.localPosition;
			if(syncedTransform.Position == null || currentPosition.x != syncedTransform.Position.X || currentPosition.y != syncedTransform.Position.Y || currentPosition.z != syncedTransform.Position.Z)
			{
				if (syncedTransform.Position == null)
					syncedTransform.Position = new PositionClass();

				syncedTransform.Position.X = currentPosition.x;
				syncedTransform.Position.Y = currentPosition.y;
				syncedTransform.Position.Z = currentPosition.z;
				return true;
			}

			return false;
		}

		private bool SetSyncedRotation(Transform transform, NetworkSyncedTransform.ESyncSpace space, SyncedTransform syncedTransform)
		{
			var currentRotation = space == NetworkSyncedTransform.ESyncSpace.Global ? transform.rotation : transform.localRotation; 

			if(syncedTransform.Rotation == null || currentRotation.x != syncedTransform.Rotation.X || currentRotation.y != syncedTransform.Rotation.Y || currentRotation.z != syncedTransform.Rotation.Z || currentRotation.w != syncedTransform.Rotation.W)
			{
				if (syncedTransform.Rotation == null)
					syncedTransform.Rotation = new RotationClass();

				syncedTransform.Rotation.X = currentRotation.x;
				syncedTransform.Rotation.Y = currentRotation.y;
				syncedTransform.Rotation.Z = currentRotation.z;
				syncedTransform.Rotation.W = currentRotation.w;
				return true;
			}

			return false;
		}

		private bool SetSyncedScale(Transform transform, SyncedTransform syncedTransform)
		{
			var currentScale = transform.localScale;
			if (syncedTransform.Scale == null || currentScale.x != syncedTransform.Scale.X || currentScale.y != syncedTransform.Scale.Y || currentScale.z != syncedTransform.Scale.Z)
			{
				if (syncedTransform.Scale == null)
					syncedTransform.Scale = new PositionClass();

				syncedTransform.Scale.X = currentScale.x;
				syncedTransform.Scale.Y = currentScale.y;
				syncedTransform.Scale.Z = currentScale.z;
				return true;
			}

			return false;
		}

		
		private bool AddOrUpdateTransform(NetworkSyncedTransform syncedTransform)
		{
			if(syncedTransform && !string.IsNullOrEmpty(syncedTransform.ObjectId))
			{
				if(transform)
				{
					if(!SyncedTransforms.ContainsKey(syncedTransform.ObjectId))
					{
						SyncedTransforms.Add(syncedTransform.ObjectId, syncedTransform);
					}
					else
					{
						Debug.LogFormat("<color=red>Replaced registered synced transform for ObjectId={0}. Check the ObjectId's in the experience if you didn't wanted to replace it.</color>", syncedTransform.ObjectId);
						SyncedTransforms[syncedTransform.ObjectId] = syncedTransform;
					}
					return true;

				}
				else
				{
					Debug.LogWarning("Cannot register transform without a valid transform assigned.");
					return false;
				}
			}
			else
			{
				Debug.LogWarning("Cannot register transform without a valid ObjectId assigned.");
				return false;
			}
		}

		private NetworkSyncedTransform RemoveTransform(string objectId)
		{
			if(!string.IsNullOrEmpty(objectId))
			{
				if(SyncedTransforms.ContainsKey(objectId))
				{
					var syncedTransform = SyncedTransforms[objectId];
					SyncedTransforms.Remove(objectId);

					//Remove from cache
					if(TransformCache.ContainsKey(objectId))
						TransformCache.Remove(objectId);

					return syncedTransform;
				}
				else
				{
					Debug.LogWarningFormat("Cannot unregister transform. Transform with ObjectId={0} was not registered.", objectId);
					return null;
				}
			}
			else
			{
				Debug.LogWarning("Cannot unregister transform without a valid ObjectId assigned.");
				return null;
			}
		}

		#endregion

	}

}