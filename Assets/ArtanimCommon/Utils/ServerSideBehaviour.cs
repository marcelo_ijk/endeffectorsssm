﻿using UnityEngine;
using System.Collections;
using Artanim.Location.Network;

namespace Artanim
{

	/// <summary>
	/// Abstract MonoBehaviour using Awake to enable/disable itself based on the component type of the application.
	/// For testing reasons the behaviour will not disable when used in the editor.
	/// </summary>
	public abstract class ServerSideBehaviour : MonoBehaviour
	{
		void Awake()
		{
			enabled = NetworkInterface.Instance.IsServer;
		}
	}

}