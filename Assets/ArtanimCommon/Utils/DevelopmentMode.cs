﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Artanim
{
	public enum EDevelopmentMode { None, ClientServer, Standalone }

	public static class DevelopmentMode
	{
		public const string KEY_DEVELOPMENT_MODE = "ArtanimDevelopmentMode";

		public const string AXIS_STANDALONE_MOVE_FORWARD = "Standalone Move Forward";
		public const string AXIS_STANDALONE_MOVE_STRAFING = "Standalone Move Strafing";
		public const string AXIS_STANDALONE_ROTATION = "Standalone Rotate";
		public const string BUTTON_STANDALONE_RECALIBRATE = "Standalone Recalibrate";

		public const string AXIS_STANDALONE_MENU_OPEN = "Standalone Menu Open";
		public const string AXIS_STANDALONE_MENU_SCENE_NAVIGATE = "Standalone Scene Navigate";
		public const string BUTTON_STANDALONE_LOAD_PREVIEV_SCENE = "Standalone Scene Load";
		public const string AXIS_STANDALONE_PICKUP_LEFT = "Standalone Pickup Left";
		public const string AXIS_STANDALONE_PICKUP_RIGHT = "Standalone Pickup Right";

#if !UNITY_EDITOR
		private static bool DevelopmentModeInitialized;
		private static EDevelopmentMode RuntimeDevelopmentMode = EDevelopmentMode.None;
#endif

		/// <summary>
		/// Current development mode. If not running in Unity editor, EDevelopmentMode.None is returned.
		/// </summary>
		public static EDevelopmentMode CurrentMode
		{
			get
			{
#if UNITY_EDITOR
				return (EDevelopmentMode)UnityEditor.EditorPrefs.GetInt(KEY_DEVELOPMENT_MODE, 1);
#else
				if(!DevelopmentModeInitialized)
				{
					//Try read from command line. If not set, no dev mode active
					for (var i = 0; i < Environment.GetCommandLineArgs().Length; ++i)
					{
						var argument = Environment.GetCommandLineArgs()[i].Trim();
						if (argument.ToLower().StartsWith("DevelopmentMode=".ToLower()))
						{
							//Dev mode set in command line
							try
							{
								RuntimeDevelopmentMode = (EDevelopmentMode)Enum.Parse(typeof(EDevelopmentMode), argument.Trim().Substring("DevelopmentMode=".Length), true);
								break;
							}
							catch (Exception)
							{
								RuntimeDevelopmentMode = EDevelopmentMode.None;
								break;
							}
						}
					}
					DevelopmentModeInitialized = true;
				}

				return RuntimeDevelopmentMode;
#endif
			}
		}

		/// <summary>
		/// Reads input axis. If standalone inputs are not setup, 0f is returned.
		/// </summary>
		/// <param name="name">Input axis name</param>
		/// <returns></returns>
		public static float GetAxis(string name)
		{
			if(IsStandaloneInputsSetup)
			{
				return Input.GetAxis(name);
			}
			else
			{
				return 0f;
			}
		}

		/// <summary>
		/// Reads input button. If standalone inputs are not setup, false is retunred.
		/// </summary>
		/// <param name="name">Button name</param>
		/// <returns></returns>
		public static bool GetButtonUp(string name)
		{
			if(IsStandaloneInputsSetup)
			{
				return Input.GetButtonUp(name);
			}
			else
			{
				return false;
			}
		}

		private static bool? InputsValid = null;
		/// <summary>
		/// Checks if the standalone inputs are setup.
		/// </summary>
		public static bool IsStandaloneInputsSetup
		{
			get
			{
				if(!InputsValid.HasValue)
				{
					try
					{
						//Axis
						Input.GetAxis(AXIS_STANDALONE_MOVE_FORWARD);
						Input.GetAxis(AXIS_STANDALONE_MOVE_STRAFING);
						Input.GetAxis(AXIS_STANDALONE_ROTATION);
						Input.GetAxis(AXIS_STANDALONE_MENU_OPEN);
						Input.GetAxis(AXIS_STANDALONE_MENU_SCENE_NAVIGATE);

						//Buttons
						Input.GetButton(BUTTON_STANDALONE_RECALIBRATE);
						Input.GetButton(BUTTON_STANDALONE_LOAD_PREVIEV_SCENE);

						InputsValid = true;
					}
					catch
					{
						Debug.LogError("Standalone inputs are not setup in your project. Setup inputs using Artanim->Inputs menu in the editor.");
						InputsValid = false;
					}
				}
				return InputsValid.Value;
			}
		}
	}
}