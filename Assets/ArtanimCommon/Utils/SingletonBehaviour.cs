﻿using UnityEngine;
using System.Collections;

namespace Artanim
{

public abstract class SingletonBehaviour<T> : MonoBehaviour where T : SingletonBehaviour<T>
{
	private static T _instance;

	public static bool HasInstance
	{
		get
		{
			return _instance != null;
		}
	}

	public static T Instance
	{
		get
		{
			if(_instance == null)
			{
				_instance = FindObjectOfType<T>();
			}
			return _instance;
		}

		protected set
		{
			_instance = value;
		}
	}
	
}

}