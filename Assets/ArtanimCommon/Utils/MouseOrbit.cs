﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Artanim
{

	public class MouseOrbit : MonoBehaviour
	{
		public enum EMouseButton { Left, Right }

		public Vector3 LookAtOffset;

		public float SpeedHorizontal = 25.0f;
		public float SpeedVertical = 25.0f;

		public float VerticalMinLimit = -20f;
		public float VerticalMaxLimit = 90f;

		public float DistanceMin = 1f;
		public float DistanceMax = 7f;

		public float SlowdownTime = 5f;

		public EMouseButton MouseButton;
		public float Distance = 5.0f;

		private float RotationYAxis = 0.0f;
		private float RotationXAxis = 0.0f;

		private float VelocityX = 0.0f;
		private float VelocityY = 0.0f;

		private Vector2 LastMousePerc;
		private Vector3 LookAtTarget;


		// Use this for initialization
		void Start()
		{
			Vector3 angles = transform.eulerAngles;
			RotationYAxis = angles.y;
			RotationXAxis = angles.x;

			if(MainCameraController.Instance)
				LookAtTarget = MainCameraController.Instance.GlobalOffset.position;
		}


		void Update()
		{
			//Distance
			Distance += Input.mouseScrollDelta.y * -0.25f;
			Distance = Mathf.Clamp(Distance, DistanceMin, DistanceMax);

			if (Input.GetMouseButton((int)MouseButton))
			{
				//VelocityX += SpeedHorizontal * Input.GetAxis("Mouse X") * 0.02f;
				//VelocityY += SpeedVertical * Input.GetAxis("Mouse Y") * 0.02f;

				var mousePos = Input.mousePosition;
				var currentMousePerc = new Vector2(mousePos.x / Screen.width, mousePos.y / Screen.height);

				if (LastMousePerc == Vector2.zero)
					LastMousePerc = currentMousePerc;

				VelocityX += SpeedHorizontal * (currentMousePerc.x - LastMousePerc.x);
				VelocityY += SpeedVertical * (currentMousePerc.y - LastMousePerc.y);

				LastMousePerc = currentMousePerc;
			}
			else
			{
				LastMousePerc = Vector2.zero;
			}

			RotationYAxis += VelocityX;
			RotationXAxis -= VelocityY;

			RotationXAxis = ClampAngle(RotationXAxis, VerticalMinLimit, VerticalMaxLimit);

			Quaternion rotation = Quaternion.Euler(RotationXAxis, RotationYAxis, 0);

			Vector3 negDistance = new Vector3(0.0f, 0.0f, -Distance);
			Vector3 position = rotation * negDistance + LookAtTarget + LookAtOffset;
			transform.localRotation = rotation;
			transform.localPosition = position;

			VelocityX = Mathf.Lerp(VelocityX, 0, Time.smoothDeltaTime * SlowdownTime);
			VelocityY = Mathf.Lerp(VelocityY, 0, Time.smoothDeltaTime * SlowdownTime);
		}

		public static float ClampAngle(float angle, float min, float max)
		{
			if (angle < -360F)
				angle += 360F;
			if (angle > 360F)
				angle -= 360F;
			return Mathf.Clamp(angle, min, max);
		}
	}

}