﻿using Artanim.Location.Messages;
using Artanim.Location.Network;
using Artanim.Monitoring.Utils;
using Artanim.Vicon;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Artanim
{
	public class AvatarOffsetController : SingletonBehaviour<AvatarOffsetController>
	{
		public enum ESyncMode { Synced, Unsynced }

		#region Events

		public delegate void OnPlayerOffsetRegisteredHandler(Guid playerId, Transform registeredOffsetTransform, Transform avatarOffsetTransform, ESyncMode syncMode);
		public event OnPlayerOffsetRegisteredHandler OnPlayerOffsetRegistered;

		public delegate void OnPlayerOffsetUnregisteredHandler(Guid playerId, Transform registeredOffsetTransform, Transform avatarOffsetTransform, ESyncMode syncMode);
		public event OnPlayerOffsetUnregisteredHandler OnPlayerOffsetUnregistered;

		#endregion

		private Dictionary<Guid, OffsetRegistration> RegisteredTransforms = new Dictionary<Guid, OffsetRegistration>();

		/// <summary>
		/// Main players avatar offset or null of no offset is registered.
		/// </summary>
		public Transform MainPlayerOffset
		{
			get
			{
				Transform playerTransform = null;
				var currentPlayer = GameController.Instance.CurrentPlayer;
				if (currentPlayer != null)
				{
					playerTransform = currentPlayer.AvatarOffset;
				}
				return playerTransform;
			}
		}

		#region Unity events

		void Start()
		{
			if (NetworkInterface.Instance != null)
			{
				NetworkInterface.Instance.Subscribe<AvatarOffsetsStateUpdate>(NetworkMessage_AvatarOffsetsStateUpdate);

				if (!NetworkInterface.Instance.IsServer)
					NetworkInterface.Instance.Subscribe<AvatarOffsetsUpdate>(NetworkMessage_SyncedAvatarOffsetsUpdate);
			}
		}

		void OnDestroy()
		{
			if (NetworkInterface.Instance != null)
			{
				NetworkInterface.Instance.Unsubscribe<AvatarOffsetsStateUpdate>(NetworkMessage_AvatarOffsetsStateUpdate);

				if (!NetworkInterface.Instance.IsServer)
					NetworkInterface.Instance.Unsubscribe<AvatarOffsetsUpdate>(NetworkMessage_SyncedAvatarOffsetsUpdate);
			}
		}

		private AvatarOffsetsUpdate OffsetMessageCache = null;
		void LateUpdate()
		{
#if EXP_PROFILING
			ExpProfiling.MarkAvOffStart();
#endif

			//Server side synced offsets
			if (NetworkInterface.Instance.IsServer && RegisteredTransforms.Count > 0 && GameController.Instance.CurrentSession != null)
			{
				//Init cache if needed
				if (OffsetMessageCache == null)
				{
					OffsetMessageCache = new AvatarOffsetsUpdate();
					OffsetMessageCache.AvatarOffsets = new List<AvatarTransform>();
				}

				//Broadcast offset message
				var invalidOffsets = new List<Guid>();
				OffsetMessageCache.AvatarOffsets.Clear();
				foreach (var avatarOffset in RegisteredTransforms)
				{
					if (avatarOffset.Value.SyncMode == ESyncMode.Synced)
					{
						if(avatarOffset.Value.RegisteredOffsetTransform)
						{
							//Send the current transform of the registered offset. 
							OffsetMessageCache.AvatarOffsets.Add(new AvatarTransform
							{
								PlayerId = avatarOffset.Key,
								Position = NetworkTransformUtils.ToPosition(avatarOffset.Value.RegisteredOffsetTransform.localPosition),
								Orientation = NetworkTransformUtils.ToRotation(avatarOffset.Value.RegisteredOffsetTransform.localRotation),
							});
						}
						else
						{
							invalidOffsets.Add(avatarOffset.Key);
						}
					}
				}

				//Debug.Log("Sending avatar offsets");
				NetworkInterface.Instance.SendMessage(OffsetMessageCache);

				//Clean invalid offsets
				if (invalidOffsets.Count > 0)
				{
					foreach(var invalidOffset in invalidOffsets)
					{
						Debug.LogWarningFormat("Unregistering invalid avatar offset for player={0}", invalidOffset);
						UnregisterAvatarOffset(invalidOffset);
					}
				}
					
			}

			

			//All components unsynced offsets or development modes where client is also server (driver of the offset) or if we're server (source of offset)
			if (RegisteredTransforms.Count > 0 && GameController.Instance.CurrentSession != null)
			{
				foreach (var unsyncedAvatarOffset in RegisteredTransforms)
				{
					//Update unsynced offsets or local offset if we're the driver
					if (unsyncedAvatarOffset.Value.SyncMode == ESyncMode.Unsynced || NetworkInterface.Instance.IsServer)
					{
						//Update avatar offset locally
						UpdateAvatarOffsetTransform(unsyncedAvatarOffset.Value);
					}
				}
			}

#if EXP_PROFILING
			ExpProfiling.MarkAvOffEnd();
#endif
		}

		#endregion

		#region Public interface

		/// <summary>
		/// Registers an offset transform for an avatar.
		/// This method can only be called in the server context.
		/// </summary>
		/// <param name="playerId"></param>
		/// <param name="offsetTransform"></param>
		public void RegisterAvatarOffset(Guid playerId, AvatarOffset avatarOffset, bool moveAvatarToTransform = false, ESyncMode syncMode = ESyncMode.Synced)
		{
			if (NetworkInterface.Instance.IsServer)
			{
				if (playerId != Guid.Empty)
				{
					if (avatarOffset != null && !string.IsNullOrEmpty(avatarOffset.ObjectId))
					{
						//Start registration
						var player = GameController.Instance.GetPlayerByPlayerId(playerId);
						if (player != null)
						{
							Debug.LogFormat("Sending avatar offset registration for: PlayerId={0}, OffsetId={1}", player.Player.ComponentId, avatarOffset.ObjectId);
							//Send registration update
							NetworkInterface.Instance.SendMessage(new AvatarOffsetsStateUpdate
							{
								Action = AvatarOffsetsStateUpdate.EAction.Register,
								SyncMode = (int)syncMode,
								OffsetObjectId = avatarOffset.ObjectId,
								MoveToTransform = moveAvatarToTransform,
								InitialTransform = new AvatarTransform
								{
									PlayerId = playerId,
									Position = NetworkTransformUtils.ToPosition(avatarOffset.transform.localPosition),
									Orientation = NetworkTransformUtils.ToRotation(avatarOffset.transform.localRotation),
								},
							});
						}
						else
						{
							Debug.LogErrorFormat("Failed to register avatar offset for playerId={0}. Player was not found.", playerId.ToString());
						}
					}
					else
					{
						Debug.LogError("Cannot register avatar offset. AvatarOffset is invalid.");
					}
				}
				else
				{
					Debug.LogError("Cannot register avatar offset. PlayerId is empty.");
				}
			}
		}


		public void UnregisterAvatarOffset(AvatarOffset avatarOffset)
		{
			if(NetworkInterface.Instance.IsServer)
			{
				var registeredTransform = RegisteredTransforms.FirstOrDefault(r => r.Value.RegisteredOffsetTransform == avatarOffset.transform);
				if(registeredTransform.Key != Guid.Empty)
				{
					UnregisterAvatarOffset(registeredTransform.Key);
				}
			}
		}


		/// <summary>
		/// Unregisters an offset transform for a avatar.
		/// This method can only be called in the server context.
		/// </summary>
		/// <param name="playerId">Player id to remove the offset</param>
		/// <param name="resetToZero">Reset the player to zero offset position.</param>
		public void UnregisterAvatarOffset(Guid playerId, bool resetToZero = true)
		{
			if (NetworkInterface.Instance.IsServer && playerId != Guid.Empty && RegisteredTransforms.ContainsKey(playerId))
			{
				Debug.LogFormat("Unregistering avatar offset for player: {0}", playerId.ToString());
				
				var offsetTransform = RegisteredTransforms[playerId];
				if(offsetTransform != null && offsetTransform.AvatarOffsetTransform) //The transform can be invalid when if not properly unregistered by the experience
				{
					//Reset avatar to original?
					if(resetToZero && offsetTransform.AvatarOffsetTransform)
					{
						offsetTransform.AvatarOffsetTransform.localPosition = Vector3.zero;
						offsetTransform.AvatarOffsetTransform.localRotation = Quaternion.identity;
					}
				}

				//Notify clients
				if(NetworkInterface.Instance.SessionId != Guid.Empty)
				{
					NetworkInterface.Instance.SendMessage(new AvatarOffsetsStateUpdate
					{
						Action = AvatarOffsetsStateUpdate.EAction.Unregister,
						InitialTransform = new AvatarTransform
						{
							PlayerId = playerId,

							//The transform can be invalid when if not properly unregistered by the experience
							Position = NetworkTransformUtils.ToPosition(offsetTransform.AvatarOffsetTransform ? offsetTransform.AvatarOffsetTransform.localPosition : Vector3.zero),

							//The transform can be invalid when if not properly unregistered by the experience
							Orientation = NetworkTransformUtils.ToRotation(offsetTransform.AvatarOffsetTransform ? offsetTransform.AvatarOffsetTransform.localRotation : Quaternion.identity),
						},
					});
				}
			}
		}

		/// <summary>
		/// Removes all avatar offsets.
		/// This method can only be called in the server context.
		/// </summary>
		public void UnregisterAllAvatarOffsets(bool resetToZero = true)
		{
			if (NetworkInterface.Instance.IsServer)
			{
				foreach (var avatarOffsetKey in RegisteredTransforms.Keys.ToArray())
				{
					UnregisterAvatarOffset(avatarOffsetKey, resetToZero);
				}
			}
		}

		#endregion

		#region Network events

		private void NetworkMessage_AvatarOffsetsStateUpdate(AvatarOffsetsStateUpdate args)
		{
			var player = GameController.Instance.GetPlayerByPlayerId(args.InitialTransform.PlayerId);

			if (player != null)
			{
				//Register offset
				if (args.Action == AvatarOffsetsStateUpdate.EAction.Register)
				{
					//Remove existing offset
					if (RegisteredTransforms.ContainsKey(player.Player.ComponentId))
						RegisteredTransforms.Remove(player.Player.ComponentId);

					//Search local AvatarOffset with corresponding ID
					var registeredOffset = FindObjectsOfType<AvatarOffset>().FirstOrDefault(o => o.ObjectId == args.OffsetObjectId);
					if(registeredOffset)
					{
						Debug.LogFormat("Registering avatar offset: PlayerId={0}, OffsetId={1}", player.Player.ComponentId, registeredOffset.ObjectId);

						//Set the registered avatar offset to the initial position if its synced
						if ((ESyncMode)args.SyncMode == ESyncMode.Synced)
						{
							registeredOffset.transform.localPosition = NetworkTransformUtils.FromPosition(args.InitialTransform.Position);
							registeredOffset.transform.localRotation = NetworkTransformUtils.FromRotation(args.InitialTransform.Orientation);
						}

						//Do we need to move the avatar to the registered transform?
						if(args.MoveToTransform)
						{
							player.AvatarOffset.position = registeredOffset.transform.position;
							player.AvatarOffset.rotation = registeredOffset.transform.rotation;
						}

						//Create registration
						var offsetRegistration = new OffsetRegistration
						{
							SyncMode = (ESyncMode)args.SyncMode,

							RegisteredOffsetTransform = registeredOffset.transform,
							AvatarOffsetTransform = player.AvatarOffset,

							StartParentPosition = registeredOffset.transform.position,
							StartParentRotation = registeredOffset.transform.rotation,

							StartAvatarPosition = player.PlayerInstance.transform.position,
							StartAvatarRotation = player.PlayerInstance.transform.rotation,
						};

						//Register new offset.
						RegisteredTransforms.Add(player.Player.ComponentId, offsetRegistration);
					}
					else
					{
						Debug.LogErrorFormat("Failed to register AvatarOffset. AvatarOffset not found with ID: {0}", args.OffsetObjectId);
					}

					//Notify event
					if (OnPlayerOffsetRegistered != null)
						OnPlayerOffsetRegistered(player.Player.ComponentId, registeredOffset.transform, player.AvatarOffset, (ESyncMode)args.SyncMode);
				}

				//Unregister offset
				else if (args.Action == AvatarOffsetsStateUpdate.EAction.Unregister)
				{
					//This is the case when running in developer mode client/server
					//In this case we want the client to run as server and bypass the state updates
					//to avoid clearing the server registrations
					OffsetRegistration offsetRegistration = null;
					if(RegisteredTransforms.TryGetValue(player.Player.ComponentId, out offsetRegistration))
					{
						Debug.LogFormat("AvatarOffset status update: Unregister player {0}", args.InitialTransform.PlayerId);

						//Remove registration
						if (RegisteredTransforms.ContainsKey(player.Player.ComponentId))
						{
							RegisteredTransforms.Remove(player.Player.ComponentId);
						}

						//When synced, reset to given position and rotation. Depending on unregistration this will leave the avatar at the current position or reset to zero.
						//When position is zero reset to avatar offset locally even when unsynced.
						var resetPosition = NetworkTransformUtils.FromPosition(args.InitialTransform.Position);
						var resetRotation = NetworkTransformUtils.FromRotation(args.InitialTransform.Orientation);
						if (offsetRegistration.SyncMode == ESyncMode.Synced || resetPosition == Vector3.zero)
						{
							player.AvatarOffset.localPosition = resetPosition;
							player.AvatarOffset.localRotation = resetRotation;
						}

						//Notify event
						if (OnPlayerOffsetUnregistered != null)
							OnPlayerOffsetUnregistered(
								player.Player.ComponentId,
								offsetRegistration != null ? offsetRegistration.RegisteredOffsetTransform : null,
								offsetRegistration != null ? offsetRegistration.AvatarOffsetTransform : null,
								offsetRegistration != null ? offsetRegistration.SyncMode : ESyncMode.Synced);
					}
				}
			}
			else
			{
				Debug.LogErrorFormat("Failed to update avatar offset registration. Unable to find avatar with playerId={0}.", args.InitialTransform.PlayerId);
			}
		}

		private void NetworkMessage_SyncedAvatarOffsetsUpdate(AvatarOffsetsUpdate args)
		{
			//Update avatar offsets
			foreach (var avatarOffset in args.AvatarOffsets)
			{
				OffsetRegistration registeredOffset;
				if(RegisteredTransforms.TryGetValue(avatarOffset.PlayerId, out registeredOffset))
				{
					//Position the registered avatar offset. Don't do it on the server since it's the driver. Exclude client/server dev mode since it won't work in Unity otherwise.
					if(!NetworkInterface.Instance.IsServer && DevelopmentMode.CurrentMode != EDevelopmentMode.ClientServer)
					{
						registeredOffset.RegisteredOffsetTransform.localPosition = NetworkTransformUtils.FromPosition(avatarOffset.Position);
						registeredOffset.RegisteredOffsetTransform.localRotation = NetworkTransformUtils.FromRotation(avatarOffset.Orientation);
					}
					
					UpdateAvatarOffsetTransform(registeredOffset);
				}
			}
		}

		private void UpdateAvatarOffsetTransform(OffsetRegistration offsetRegistration)
		{
			//Calculate the position and rotation the avatar as if it was child of the registered offset
			var parentMatrix = Matrix4x4.TRS(
					offsetRegistration.RegisteredOffsetTransform.position,
					offsetRegistration.RegisteredOffsetTransform.rotation * Quaternion.Inverse(offsetRegistration.StartParentRotation),
					offsetRegistration.RegisteredOffsetTransform.lossyScale);

			offsetRegistration.AvatarOffsetTransform.position = parentMatrix.MultiplyPoint3x4(offsetRegistration.StartAvatarPosition - offsetRegistration.StartParentPosition);
			offsetRegistration.AvatarOffsetTransform.localRotation = (offsetRegistration.RegisteredOffsetTransform.rotation * Quaternion.Inverse(offsetRegistration.StartParentRotation)) * offsetRegistration.StartAvatarRotation;
		}

		#endregion

		#region Internal Classes

		private class OffsetRegistration
		{
			public ESyncMode SyncMode { get; set; }

			public Transform RegisteredOffsetTransform { get; set; }
			public Transform AvatarOffsetTransform { get; set; }

			//Position and orientation of the parent at registration time
			public Vector3 StartParentPosition { get; set; }
			public Quaternion StartParentRotation { get; set; }

			//Position and orientation of the avatar at registration time
			public Vector3 StartAvatarPosition { get; set; }
			public Quaternion StartAvatarRotation { get; set; }
		}

		#endregion 
	}

}