﻿using Artanim.Location.Network;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Artanim
{
	/// <summary>
	/// The GameObject assigned to this behaviour will be only enabled when running on the server. 
	/// </summary>
	[AddComponentMenu("Artanim/Server Side GameObject")]
	public class ServerSideGameObject : MonoBehaviour
	{
		void Awake()
		{
#if !UNITY_EDITOR
			gameObject.SetActive(NetworkInterface.Instance.IsServer);
#endif
		}
	}
}