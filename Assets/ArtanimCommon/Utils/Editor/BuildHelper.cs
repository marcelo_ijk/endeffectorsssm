﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

// Class is in global namespace so its methods may be called from the command line with the "-executeMethod" switch
public static class ArtanimBuildHelper
{
	public static void BuildScenes()
	{
		Artanim.BuildHelper.BuildScenes();
	}
}

namespace Artanim
{
	public static class BuildHelper
	{
#if LOAD_SINGLE_CONFIG
		[UnityEditor.MenuItem("Artanim/Build Package")]
		public static void BuildPackage()
		{
			Debug.Log("Building scenes");

			string dir = @"C:\Users\Olivier\Documents\dev\_TestTrack";
			string expName = "Experience.exe";
			string data = System.IO.Path.Combine(dir, expName + "_Data");

			Debug.Log(data);

			// Get scenes
			var scenes = EditorBuildSettings.scenes.Select(s => s.path).ToArray();

			// Build
			//string err = BuildPipeline.BuildPlayer(EditorBuildSettings.scenes, expName, BuildTarget.StandaloneWindows64, BuildOptions.StrictMode);
			string err = "";

			Debug.Log("Building scenes DONE -> " + err);

			var startInfo = new System.Diagnostics.ProcessStartInfo();
			//startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
			startInfo.FileName = "cmd.exe";
			startInfo.Arguments = "/c powershell Compress-Archive -Path \"" + data + "\" -DestinationPath D:\\archive.zip";
			//var process = 
			Debug.Log("zip -> " + startInfo.Arguments);
			System.Diagnostics.Process.Start(startInfo);
			//process.WaitForExit();
			//Debug.Log(process.ExitCode);
			//process.Close();
		}
#endif

		public static void BuildScenes()
		{
			// Get output dir
			string flag = "-buildFilename";
			string buildFilename = GetCmdLineArg(flag);
			if (string.IsNullOrEmpty(buildFilename))
			{
				throw new System.InvalidOperationException("Empty build filename, use flag " + flag);
			}
			buildFilename = buildFilename.Replace("\\", "/");

			// Get scenes
			var scenes = EditorBuildSettings.scenes.Select(s => s.path).ToArray();
			if ((EditorBuildSettings.scenes == null) || (EditorBuildSettings.scenes.Length == 0))
			{
				throw new System.InvalidOperationException("Empty build filename, use flag " + flag);
			}

			// Build
			EditorUserBuildSettings.SetPlatformSettings("Standalone", "CopyPDBFiles", "true");
			string err = BuildPipeline.BuildPlayer(scenes, buildFilename, BuildTarget.StandaloneWindows64, BuildOptions.StrictMode);

			// Check for error
			if (!string.IsNullOrEmpty(err))
			{
				Debug.LogError(err);
				throw new System.Exception(err);
			}
		}

		// Get a command line argument
		private static string GetCmdLineArg(string cmdName)
		{
			var args = System.Environment.GetCommandLineArgs();
			for (int i = 0; i < args.Length; i++)
			{
				if ((args[i] == cmdName) && ((i + 1) < args.Length))
				{
					return args[i + 1];
				}
			}
			return null;
		}
	}
}