﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Artanim.Monitoring;
using Artanim.Monitoring.Utils;

namespace Artanim
{
	public class TrackingQualityMetrics : IDisposable
	{
		class QualityMetrics
		{
			public MetricsChannel<float> MetricChannel;
			public ulong Ticketid;         // Ticket id when opened (meaning we've consistently having bad quality on the rigidbody for sometime)
			public uint FirstFrameNumber;  // First bad frame
			public uint LastFrameNumber;   // Last bad frame
		}

		Dictionary<string, QualityMetrics> _mapQualityMetrics = new Dictionary<string, QualityMetrics>();
		MetricsChannelTemplate<float> _metricChannelTemplate = MetricsManager.Instance.GetChannelTemplate<float>(MetricsAction.Create, "Tracking Quality");

		// Config values
		float _qualityThreshold = float.NaN;
		uint _numFramesSlidingWindow;
		bool _sendBadQuality;

		public TrackingQualityMetrics()
		{
		}

		// Check the tracking quality for the given rigidbody and trigger a quality metrics event when tracking isn't good enough
		public bool CheckQuality(string rigidbodyName, float quality, uint frameNumber)
		{
			// Get metrics for the rigidbody
			var qualityMetrics = GetQualityMetrics(rigidbodyName);

			// Check quality
			bool isBadQuality = ((_qualityThreshold > 0) && (quality > _qualityThreshold));
			if (isBadQuality)
			{
				// Store current bad frame number
				qualityMetrics.LastFrameNumber = frameNumber;

				// Check if we have a ticket going on
				if (qualityMetrics.Ticketid == 0)
				{
					// If not store first bad frame number
					if (qualityMetrics.FirstFrameNumber == 0)
					{
						qualityMetrics.FirstFrameNumber = frameNumber;
					}

					// Wait some delay before opening the ticket
					if ((frameNumber - qualityMetrics.FirstFrameNumber) >= _numFramesSlidingWindow)
					{
						qualityMetrics.Ticketid = qualityMetrics.MetricChannel.OpenQualityTicket("");
					}
				}

				// Send the current quality if a ticket is opened
				if (_sendBadQuality && qualityMetrics.Ticketid > 0)
				{
					qualityMetrics.MetricChannel.SendQualityTicketEventValue(qualityMetrics.Ticketid, 1, "", ref quality);
				}
			}
			else
			{
				// The quality is good enough, reset first bad frame number
				qualityMetrics.FirstFrameNumber = 0;

				// And close the ticket after some delay
				if ((qualityMetrics.Ticketid > 0)
					&& ((frameNumber - qualityMetrics.LastFrameNumber) >= _numFramesSlidingWindow))
				{
					qualityMetrics.MetricChannel.CloseQualityTicket(qualityMetrics.Ticketid, "");
					qualityMetrics.Ticketid = 0;
				}
			}

			return !isBadQuality;
		}

		QualityMetrics GetQualityMetrics(string rigidbodyName)
		{
			QualityMetrics qualityMetrics;
			_mapQualityMetrics.TryGetValue(rigidbodyName, out qualityMetrics);
			if (qualityMetrics == null)
			{
				qualityMetrics = new QualityMetrics
				{
					MetricChannel = _metricChannelTemplate.GetInstance(MetricsAction.Create, rigidbodyName),
					FirstFrameNumber = 0,
					Ticketid = 0
				};
				_mapQualityMetrics.Add(rigidbodyName, qualityMetrics);

				// Initialize config values
				if (float.IsNaN(_qualityThreshold))
				{
					_qualityThreshold = (float)qualityMetrics.MetricChannel.GetParamValue("threshold", 3);
					_numFramesSlidingWindow = (uint)qualityMetrics.MetricChannel.GetParamValue("num frames sliding window", 500);
					_sendBadQuality = qualityMetrics.MetricChannel.GetParamValue("send bad quality", 0) > 0;
				}
			}
			return qualityMetrics;
		}

		#region IDisposable Support

		private bool disposedValue = false; // To detect redundant calls

		protected virtual void Dispose(bool disposing)
		{
			if (!disposedValue)
			{
				if (disposing)
				{
					foreach (var qm in _mapQualityMetrics)
					{
						qm.Value.MetricChannel.Dispose();
					}

					_mapQualityMetrics.Clear();

					if (_metricChannelTemplate != null)
					{
						_metricChannelTemplate.Dispose();
						_metricChannelTemplate = null;
					}
				}

				disposedValue = true;
			}
		}

		// This code added to correctly implement the disposable pattern.
		public void Dispose()
		{
			// Do not change this code. Put cleanup code in Dispose(bool disposing) above.
			Dispose(true);
		}

		#endregion
	}
}