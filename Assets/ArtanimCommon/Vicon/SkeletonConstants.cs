﻿using Artanim.Location.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Artanim.Vicon
{
	public static class SkeletonConstants
	{
		public const string SkeletonPrefix = "Skeleton-";

		static string _pelvisPrefix;

		static string PelvisPrefix
		{
			get
			{
				if (_pelvisPrefix == null)
				{
					_pelvisPrefix = ConfigService.Instance.Config.Location.IKServer.Skeleton.PelvisPrefix;
				}
				return _pelvisPrefix;
			}
		}

		static string _headPrefix;

		static string HeadPrefix
		{
			get
			{
				if (_headPrefix == null)
				{
					_headPrefix = ConfigService.Instance.Config.Location.IKServer.Skeleton.HeadPrefix;
				}
				return _headPrefix;
			}
		}

		static string[] _rigidbodyNamePrefixes;

		static string[] RigidbodyNamePrefixes
		{
			get
			{
				if (_rigidbodyNamePrefixes == null)
				{
					var skelton = ConfigService.Instance.Config.Location.IKServer.Skeleton;
					_rigidbodyNamePrefixes = new[]
					{
						skelton.PelvisPrefix,
						skelton.HeadPrefix,
						skelton.RightHand,
						skelton.LeftHand,
						skelton.RightFoot,
						skelton.LeftFoot,
					};
				}
				return _rigidbodyNamePrefixes;
			}
		}

		public static bool IsSkeletonMainSubject(string rigidbodyName)
		{
			return rigidbodyName.StartsWith(PelvisPrefix);
		}

		public static bool IsSkeletonHeadSubject(string rigidbodyName)
		{
			return rigidbodyName.StartsWith(HeadPrefix);
		}

		public static bool IsSkeletonRigidbody(string rigidbodyName)
		{
			foreach(var prefix in RigidbodyNamePrefixes)
			{
				if (rigidbodyName.StartsWith(prefix))
					return true;
			}
			return false;
		}

		public static string GetSkeletonShortnameFromRigidbody(string rigidbodyName)
		{
			foreach (var prefix in RigidbodyNamePrefixes)
			{
				if (rigidbodyName.StartsWith(prefix))
					return rigidbodyName.Substring(prefix.Length);
			}
			return string.Empty;
		}

		public static string BuildRigidbodyName(ESkeletonSubject subject, string shortname)
		{
			return RigidbodyNamePrefixes[(int)subject] + shortname;
		}

		public static string BuildSkeletonName(string shortname)
		{
			return SkeletonPrefix + shortname;
		}

		public static string GetSkeletonShortname(string skeletonFullName)
		{
			if (skeletonFullName.StartsWith(SkeletonPrefix))
				return skeletonFullName.Substring(SkeletonPrefix.Length);
			return skeletonFullName;
		}
	}
}