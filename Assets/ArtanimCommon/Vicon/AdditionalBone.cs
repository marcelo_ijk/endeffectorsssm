﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Artanim.Vicon
{
	[Serializable]
	public class AdditionalBone
	{
		public ERigBones Bone;
		public Transform BoneTransform;
	}
}
