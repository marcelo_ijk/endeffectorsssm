﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Artanim.Location.Config;

namespace Artanim.Vicon
{
	public interface IViconConnector
	{
		IViconConnectorStats Stats { get; }

		bool IsConnected { get; }

		void Init(ViconConfig config);

		void Connect();

		void Disconnect();

		bool UpdateRigidBodies();
	}

	// Interface to the stats (actual implementation is done by class implementing IViconConnector)
	public interface IViconConnectorStats
	{
		string ConnectionInfo { get; }

		uint FrameNumber { get; }

		DateTime FrameCaptureTime { get; }

		float FrameCaptureLatency { get; }

		float FrameProcessLatency { get; }

		int ProcessedFrames { get; }

		int ErrorCounter { get; }

		int EmptyUpdates { get; }
	}
}
