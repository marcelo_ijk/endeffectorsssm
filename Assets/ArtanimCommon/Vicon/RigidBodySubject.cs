﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Artanim.Vicon
{
	/// <summary>
	/// holds a segment's data ready for use by unity
	/// values have been transposed from right handed coord to left handed
	/// </summary>
	public class ViconSegmentTransform
	{
		public Vector3 globalTranslation;
		public Quaternion globalRotation;
		public bool isUpToDate;
		public bool isCorrected;
	}

	public class RigidBodySubject
	{
		public string Name { get; private set; }
		public bool IsSkeletonMainSubject { get; private set; }
		public bool IsSkeletonHeadSubject { get; private set; }
		public bool IsSkeletonSubject { get; private set; }
		public bool SkeletonInitialized { get; set; }

		public bool CreateVisual { get; set; }

		public DateTime LastUpdate { get; private set; }
		public UInt32 LastUpdateFrameNumber { get; private set; }

		public ViconSegmentTransform Segment { get; private set; }

		const int NUM_FRAMES_THRESHOLD_FOR_RESTART = 150; // 1 second

		public RigidBodySubject(string name)
		{
			Name = name;
			IsSkeletonMainSubject = SkeletonConstants.IsSkeletonMainSubject(Name);
			IsSkeletonHeadSubject = SkeletonConstants.IsSkeletonHeadSubject(Name);
			IsSkeletonSubject = SkeletonConstants.IsSkeletonRigidbody(Name);
			Segment = new ViconSegmentTransform();

		}

		// Use this method to reset frame number, when Tracker's loop a trial for examples
		public void ResetFrameNumber()
		{
			LastUpdateFrameNumber = 0;
		}

		public void UpdateTransform(UInt32 frameNumber, Vector3 position, Quaternion rotation)
		{
			bool isUpToDate = false, isCorrected = false;

			// Check that we have a valid frame number
			// (the counter gets reset to 0 every time Tracker is looping a trial and that we are using the SDK connector
			// and we also have another check for looping in case we missed the frame 0, which can happen with the broadcast connector)
			if ((frameNumber == 0) || (frameNumber > LastUpdateFrameNumber)
				|| ((frameNumber + NUM_FRAMES_THRESHOLD_FOR_RESTART) < LastUpdateFrameNumber))
			{
				//if ((frameNumber - 1) != LastUpdateFrameNumber) Debug.LogError("Skipped a frame for " + Name + "  " + frameNumber + "  " + LastUpdateFrameNumber);

				bool isNullPosition = (position == Vector3.zero);
				bool skipRotation = false;

				// Position at origin as signals an invalid value
				if (isCorrected || (!isNullPosition))
				{
					Segment.globalTranslation = position;
					if (!skipRotation)
					{
						Segment.globalRotation = rotation;
					}
					LastUpdate = DateTime.Now;
					LastUpdateFrameNumber = frameNumber;
					isUpToDate = true;
				}

				//Debug.LogFormat("Updated rigidbody: Name={0}, Active={1}, Translation({2}, {3}, {4}), Rotation=({5}, {6}, {7})",
				//    Name, Active,
				//    Segment.globalTranslation.x, Segment.globalTranslation.y, Segment.globalTranslation.z,
				//    Segment.globalRotation.x, Segment.globalRotation.y, Segment.globalRotation.z);
			}
			else if (frameNumber != LastUpdateFrameNumber)
			{
				Debug.LogWarningFormat("Wrong update sequence. CurrentFrameNumber={0}, LastUpdateFrameNumber={1}", frameNumber, LastUpdateFrameNumber);
			}

			// Store tracking behavior
			Segment.isUpToDate = isUpToDate;
			Segment.isCorrected = isCorrected;
		}
	}
}
