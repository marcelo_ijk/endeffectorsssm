﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Net;
using System.Net.Sockets;
using Artanim.Location.Config;
using Artanim.Utils;
using Artanim.Location.Network;

using AAU = Artanim.Utils.ArrayUtils;
using Artanim.Monitoring;

namespace Artanim.Vicon
{
	public class ViconClientConnector
		: IViconConnector
	{
		// All times are in seconds
		struct SystemLatency
		{
			public byte FrameNumberDelta;
			public float CaptureDeltaTime;
			public float CaptureLatency;
			public float IkLatencyCumul;
			public float MessageLatencyCumul;
			public float TotalLatency;
		}

		private bool _isConnected = false;
		private ViconIKConnectorStats _stats = new ViconIKConnectorStats { ConnectionInfo = "IK connection" };
		private Location.Data.IKServer _ikServer;

		private MetricsChannel<SystemLatency> _systemLatencyMetrics;
		private DateTime _lastCaptureTime;

		public IViconConnectorStats Stats
		{
			get { return _stats; }
		}

		public bool IsConnected
		{
			get { return _isConnected; }
		}

		public void Init(ViconConfig config)
		{
		}

		public void Connect()
		{
			_isConnected = true;

			// Create metrics channels
			if (_systemLatencyMetrics == null)
			{
				_systemLatencyMetrics = MetricsManager.Instance.GetChannelInstance<SystemLatency>(MetricsAction.Create, "System Latency");
			}
			_lastCaptureTime = new DateTime(0, DateTimeKind.Utc);

			Location.SharedData.SharedDataUtils.ComponentRemoved += SharedDataUtils_ComponentRemoved;
		}

		public void Disconnect()
		{
			_isConnected = false;

			// Destroy metrics channels
			if (_systemLatencyMetrics != null)
			{
				_systemLatencyMetrics.Dispose();
				_systemLatencyMetrics = null;
			}

			Location.SharedData.SharedDataUtils.ComponentRemoved -= SharedDataUtils_ComponentRemoved;
		}

		public bool UpdateRigidBodies()
		{
			bool dataUpdated = false;

			//Reet rigidbodies update state
			for(var i=0; i < ViconController.Instance.RigidBodySubjects.Count; ++i)
			{
				var rb = ViconController.Instance.RigidBodySubjects[i];
				rb.Segment.isUpToDate = false;
			}

			//Get latest rigid bodies
			var ikUpdate = RuntimeLocationComponent.IkUpdate;
			if ((ikUpdate != null) && (ikUpdate.FrameNumber != Stats.FrameNumber) && (ikUpdate.Rigidbodies != null) && (ikUpdate.Rigidbodies.Length > 0))
			{
				_stats.FrameProcessTimestamp = Profiling.CPProfiler.Timestamp;

				var captureTime = ikUpdate.CaptureTime;
				if (_lastCaptureTime.Ticks > 0)
				{
					var data = new SystemLatency()
					{
						FrameNumberDelta = (byte)Mathf.Max(0, (int)ikUpdate.FrameNumber - (int)_stats.FrameNumber),
						CaptureDeltaTime = (float)(captureTime - _lastCaptureTime).TotalSeconds,
						CaptureLatency = ikUpdate.CaptureLatency,
						IkLatencyCumul = (float)(ikUpdate.SendTime - captureTime).TotalSeconds,
						MessageLatencyCumul = (float)(ikUpdate.ReceptionTime - captureTime).TotalSeconds,
						TotalLatency = (float)(DateTime.UtcNow - captureTime).TotalSeconds,
					};
					unsafe
					{
						_systemLatencyMetrics.SendRawData(new IntPtr(&data));
					}
				}
				_lastCaptureTime = captureTime;

				_stats.FrameCaptureTime = captureTime;
				_stats.FrameCaptureLatency = ikUpdate.CaptureLatency;
				_stats.FrameProcessLatency = (float)(ikUpdate.SendTime - captureTime).TotalSeconds - ikUpdate.CaptureLatency;

				//Get component data
				if (_ikServer == null)
				{
					_ikServer = Location.SharedData.SharedDataUtils.Components.OfType<Location.Data.IKServer>().FirstOrDefault();
				}
				if (_ikServer != null)
				{
					//Iterate through rigidbodies
					int nRigidbodyNames = _ikServer.RigidbodyNames != null ? _ikServer.RigidbodyNames.Count : 0;
					int nRigidBodies = AAU.ArrayLength(ikUpdate.Rigidbodies);

					for (int eRigidBody = 0; eRigidBody < nRigidBodies; ++eRigidBody)
					{
						var rigidBodyUpdate = ikUpdate.Rigidbodies[eRigidBody];
						// Check if we have a name for it and if the rotation is valid
						if ((!rigidBodyUpdate.Ignore) && (eRigidBody < nRigidbodyNames))
						{
							var position = NetworkTransformUtils.FromPosition(rigidBodyUpdate.Position);
							var rotation = NetworkTransformUtils.FromRotation(rigidBodyUpdate.Orientation);

							// Get rigidbody name
							string rigidbodyName = _ikServer.RigidbodyNames[eRigidBody];

							// Update rigidbody
							UpdateRigidbody(rigidbodyName, ikUpdate.FrameNumber, position, rotation);
						}
					}
					dataUpdated = true;

					_stats.FrameNumber = ikUpdate.FrameNumber;
					++_stats.ProcessedFrames;
				}
			}
			else
			{
				++_stats.EmptyUpdates;
			}
			
			return dataUpdated;
		}
		
		void UpdateRigidbody(string rigidbodyName, uint frameNumber, Vector3 position, Quaternion rotation)
		{
			var rigidBody = ViconController.Instance.GetOrCreateRigidBody(rigidbodyName);

			//Update subject
			rigidBody.UpdateTransform(frameNumber, position, rotation);
		}

		private void SharedDataUtils_ComponentRemoved(Location.Data.LocationComponent locationComponent)
		{
			if (locationComponent == _ikServer)
			{
				_ikServer = null;
			}
		}
	}

	public class ViconIKConnectorStats : IViconConnectorStats
	{
		public string ConnectionInfo { get; set; }

		public uint FrameNumber { get; set; }

		public DateTime FrameCaptureTime { get; set; }

		public long FrameProcessTimestamp { get; set; }

		public float FrameCaptureLatency { get; set; }

		public float FrameProcessLatency { get; set; }

		public int ProcessedFrames { get; set; }

		public int ErrorCounter { get; set; }

		public int EmptyUpdates { get; set; }
	}
}