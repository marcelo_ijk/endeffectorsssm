﻿//#define EULER_ROTATIONS
//#define ANGLEAXIS_ROTATIONS

using Artanim.Location.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Artanim.Vicon
{
	public static class NetworkTransformUtils
	{
		public static Position ToPosition(Vector3 position)
		{
			return new Position
			{
				X = position.x,
				Y = position.y,
				Z = position.z,
			};
		}

		public static Vector3 FromPosition(Position position)
		{
			return new Vector3(position.X, position.Y, position.Z);
		}

		public static Rotation ToRotation(Quaternion rotation)
		{
#if EULER_ROTATIONS
			var eulers = rotation.eulerAngles;
			return new RotationEuler
			{
				X = eulers.x,
				Y = eulers.y,
				Z = eulers.z,
			};
#elif ANGLEAXIS_ROTATIONS
			float angle;
			Vector3 axis;
			rotation.ToAngleAxis(out angle, out axis);
			return new RotationAngleAxis
			{
				X = axis.x,
				Y = axis.y,
				Z = axis.z,
				Angle = angle,
			};
#else
			return new Rotation
				{
					X = rotation.x,
					Y = rotation.y,
					Z = rotation.z,
					W = rotation.w,
				};
#endif
		}

		public static Quaternion FromRotation(Rotation rotation)
		{
#if EULER_ROTATIONS
			return Quaternion.Euler(rotation.X, rotation.Y, rotation.Z);
#elif ANGLEAXIS_ROTATIONS
			return Quaternion.AngleAxis(rotation.Angle, new Vector3(rotation.X, rotation.Y, rotation.Z));
#else
			return new Quaternion(rotation.X, rotation.Y, rotation.Z, rotation.W);
#endif
		}
	}
}
