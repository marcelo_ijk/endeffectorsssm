﻿using UnityEngine;
using System.Collections;

namespace Artanim.Vicon
{
	public static class Referential
	{
		public static readonly float ViconToUnityScale = 0.001f;

		// Map position from Tracker's space to Unity's space
		public static Vector3 ViconPositionToUnity(Vector3 v)
		{
			var unityVec = new Vector3(-v.y, v.z, v.x);
			return unityVec * ViconToUnityScale;
		}

		// Map rotation from Tracker's space to Unity's space
		public static Quaternion ViconRotationToUnity(float rxDeg, float ryDeg, float rzDeg)
		{
			var rX = Quaternion.AngleAxis(ryDeg, Vector3.right);
			var rY = Quaternion.AngleAxis(90 - rzDeg, Vector3.up);
			var rZ = Quaternion.AngleAxis(-rxDeg, Vector3.forward);
			return rZ * rX * rY;
		}
	}
}