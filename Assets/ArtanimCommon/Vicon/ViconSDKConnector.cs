﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Runtime.Remoting.Messaging;
using Artanim.Location.Config;
using Artanim.Location.Data;
using Artanim.Location.Network;
using Artanim.Location.Helpers;
using Artanim.Monitoring;
using Artanim.Monitoring.Utils;

using Result = Artanim.TrackerSdk.Result;
using StreamMode = Artanim.TrackerSdk.StreamMode;

namespace Artanim.Vicon
{
	public class ViconSDKConnector : IViconConnector
	{
		#region Stats class

		// Connection statistics
		class ViconSdkStats : IViconConnectorStats
		{
			public string ConnectionInfo { get; set; }

			public uint FrameNumber { get; set; }

			public DateTime FrameCaptureTime { get; set; }

			public long FrameProcessTimestamp { get; set; }

			public float FrameCaptureLatency { get; set; }

			public float FrameProcessLatency { get; set; }

			public int ProcessedFrames { get; set; }

			public int ErrorCounter { get { return 0; } }

			public int EmptyUpdates { get; set; }
		}

		#endregion

		private delegate Result ConnectWorkerDelegate(string hostAndPort);

		private IAsyncResult _connectAsync;
		private ViconSdkStats _stats = new ViconSdkStats(); // Need to be initialized since the beginning

		private ViconConfig Config;
		private string TrackerEndpoint;
		private List<RigidBodySubject> RigidbodiesListCache = new List<RigidBodySubject>();
		private List<RigidBodySubject> RigidbodiesListCache2 = new List<RigidBodySubject>();

		private struct TrackerRigidbodyData
		{
			public string Name;
			public double[] Position;
			public double[] Rotation;
			public double Quality;
		}
		private List<TrackerRigidbodyData> _rigidbodyData = new List<TrackerRigidbodyData>(100);

		private MetricsChannel<IKProfiling.Timings> _ikTimingsMetrics;
		private TrackingQualityMetrics _trackingQM;

		public IViconConnectorStats Stats
		{
			get { return _stats; }
		}

		public bool IsConnected
		{
			get; private set;
		}

		public bool AutoReconnect
		{
			get; private set;
		}

		public bool RunningForIkServer
		{
			get; private set;
		}

		public void Init(ViconConfig config)
		{
			Config = config;
			TrackerEndpoint = HelperMethods.GetTrackerIP();
		}

		public void Connect()
		{
			if((Config == null) && (Stats == null))
			{
				Debug.LogWarning("Vicon connector not initialized, use Init method first");
				return;
			}
			if(IsConnected)
			{
				Debug.LogWarning("Vicon connector is already connected, call disconnect first if you want to reconnect");
				return;
			}

			try
			{
				if (!string.IsNullOrEmpty(TrackerEndpoint))
				{
					lock(this)
					{
						if(_connectAsync != null)
						{
							Debug.LogFormat("Connection to Vicon host already running: {0}", TrackerEndpoint);
						}
						else
						{
							Debug.LogFormat("Starting connection to Vicon host: {0}", TrackerEndpoint);

							TrackerSdk.Instance.Init();
							_stats = new ViconSdkStats();
							_trackingQM = new TrackingQualityMetrics();

							RunningForIkServer = NetworkInterface.Instance.ComponentType == ELocationComponentType.IKServer;

							// Start connection
							var connectWorker = new ConnectWorkerDelegate(ConnectWorker);
							var async = AsyncOperationManager.CreateOperation(null);
							var completedCallback = new AsyncCallback(ConnectAsyncCompletedCallback);
							_connectAsync = connectWorker.BeginInvoke(TrackerEndpoint, completedCallback, async);
						}
					}
				}
				else
				{
					Debug.LogWarning("No Vicon host configured!");
				}
			}
			catch(Exception ex)
			{
				Debug.LogErrorFormat("Exception when trying to connect to Vicon Host: {0}\n{1}\n{2}", TrackerEndpoint, ex.Message, ex.StackTrace);
				Disconnect();
			}
		}

		public void Disconnect()
		{
			var async = _connectAsync;
			if(async != null)
			{
				Debug.LogWarning("Waiting on Vicon..."); // This message might show up too late in Unity console when stopping the game

				// Wait on the connection thread to complete to avoid problems like crashes on DLL unloading
				float progress = 0;
				float inc = 0.05f;
				while(!async.IsCompleted)
				{
					progress += inc;
#if UNITY_EDITOR
					UnityEditor.EditorUtility.DisplayProgressBar("Vicon Connect", "Waiting on Vicon connect to complete before quitting", progress);
#endif
					System.Threading.Thread.Sleep(1000);
				}
#if UNITY_EDITOR
				UnityEditor.EditorUtility.ClearProgressBar();
#endif
			}

			if (_ikTimingsMetrics != null)
			{
				_ikTimingsMetrics.Dispose();
				_ikTimingsMetrics = null;
			}

			if (_trackingQM != null)
			{
				_trackingQM.Dispose();
				_trackingQM = null;
			}

			if(IsConnected)
			{
				AutoReconnect = false;

				Debug.LogFormat("Disconnecting from Vicon host: {0}", TrackerEndpoint);

				TrackerSdk.Instance.Disconnect();

				IsConnected = false;
			}
		}

		public bool UpdateRigidBodies()
		{
			bool updated = false;

			if(IsConnected)
			{
#if IK_PROFILING
				IKProfiling.MarkWaitStart();
#endif
				//Retrieve Vicon frame
				var result = TrackerSdk.Instance.GetFrame();
				if(result == Result.Success)
				{
					_stats.FrameProcessTimestamp = Profiling.CPProfiler.Timestamp;
					++_stats.ProcessedFrames;

#if IK_PROFILING
					IKProfiling.MarkWaitEnd();
					if (_ikTimingsMetrics == null)
					{
						_ikTimingsMetrics = MetricsManager.Instance.GetChannelInstance<IKProfiling.Timings>(MetricsAction.Create, "IK Profiling");
					}
					var frame = IKProfiling.Frame;;
					unsafe
					{
						_ikTimingsMetrics.SendRawData(new IntPtr(&frame));
					}
#endif

					//Get latency total as early as possible so we can compute the time at which the frame was captured with as little error as possible
					var latencyResult = TrackerSdk.Instance.GetLatencyTotal();
					if(latencyResult.Result != Result.Success)
						Debug.LogWarningFormat("Failed to get Vicon's latency total: {0}", latencyResult.Result);

					// Compute frame capture time
					var captureTime = DateTime.UtcNow.AddSeconds(-latencyResult.Total);
					_stats.FrameCaptureTime = captureTime;
					_stats.FrameCaptureLatency = (float)latencyResult.Total;

#if IK_PROFILING
					IKProfiling.StartFrame((float)latencyResult.Total);
					IKProfiling.MarkRbStart();
#endif

					// Get current frame number
					uint frameNumber = 0;
					var frameNumberResult = TrackerSdk.Instance.GetFrameNumber();
					if(frameNumberResult.Result == Result.Success)
					{
						frameNumber = frameNumberResult.FrameNumber;

						// Reset rigid bodies frame number when looping
						if (frameNumber < _stats.FrameNumber)
						{
							var rbSubjects = ViconController.Instance.RigidBodySubjects;
							for (int i = 0, iMax = rbSubjects.Count; i < iMax; ++i)
							{
									rbSubjects[i].ResetFrameNumber();
							}
						}
					}

					if((_stats.FrameNumber + 1) < frameNumber)
					{
						//TODO
						//Debug.LogError("Skipped frames between " + _stats.LastFrameNumber + " and " + frameNumber);
					}
					_stats.FrameNumber = frameNumber;

					// Store rigidbodies to send them to other components (only for the IK server in DDS_IK mode)
					var rigidBodies = RigidbodiesListCache;
					if(rigidBodies != null)
					{
						rigidBodies.Clear();
					}

					// Keep track of all rigid bodies updated this frame
					var foundRigidBodies = RigidbodiesListCache2;
					foundRigidBodies.Clear();

					// Reset list that stores the data read from Tracker
					_rigidbodyData.Clear();

					// Get all rigidbodies from Tracker
					for (uint index = 0, iMax = TrackerSdk.Instance.GetSubjectCount().SubjectCount; index < iMax; index++)
					{
						var rigidbodyName = TrackerSdk.Instance.GetSubjectName(index).SubjectName;
						var posResponse = TrackerSdk.Instance.GetSegmentGlobalTranslation(rigidbodyName, rigidbodyName);
						var rotResponse = TrackerSdk.Instance.GetSegmentGlobalRotationEulerXYZ(rigidbodyName, rigidbodyName);
						var qualityResponse = TrackerSdk.Instance.GetObjectQuality(rigidbodyName);

						var position = posResponse.Translation;
						var rotation = rotResponse.Rotation;

						if ((posResponse.Result == Result.Success) && (position != null) && (position.Length >= 3)
							&& (rotResponse.Result == Result.Success) && (rotation != null) && (rotation.Length >= 3))
						{
							_rigidbodyData.Add(new TrackerRigidbodyData()
							{
								Name = rigidbodyName,
								Position = position,
								Rotation = rotation,
								Quality = qualityResponse.Quality,
							});
						}
					}
#if IK_PROFILING
					IKProfiling.MarkRbRead();
#endif
					// Process retrieved rigidbodies data
					for (int i = 0, iMax = _rigidbodyData.Count; i < iMax; ++i)
					{
						var data = _rigidbodyData[i];
						string rigidbodyName = data.Name;
						double[] position = data.Position, rotation = data.Rotation;
						Vector3 pos, rot;
						float quality = (float)data.Quality;

						// Check quality
						if (_trackingQM.CheckQuality(rigidbodyName, quality, frameNumber))
						{
							// Convert to Vector3
							pos = new Vector3((float)position[0], (float)position[1], (float)position[2]);
							rot = Mathf.Rad2Deg * new Vector3((float)rotation[0], (float)rotation[1], (float)rotation[2]);
						}
						else
						{
							//Debug.LogWarningFormat("Skipping object transform update {0} because of low quality: {1}", rigidBodyName, quality);
							pos = rot = Vector3.zero;
						}

						// Get our rigidbody
						var subject = ViconController.Instance.GetOrCreateRigidBody(rigidbodyName);

						//Update rigidbody with Unity specific conversion
						subject.UpdateTransform(
							frameNumber,
							Referential.ViconPositionToUnity(pos),
							Referential.ViconRotationToUnity(rot.x, rot.y, rot.z));
						foundRigidBodies.Add(subject);
						updated = true;

						//Store rigid body to send
						if ((rigidBodies != null) && (!subject.IsSkeletonSubject || subject.IsSkeletonHeadSubject))
						{
							rigidBodies.Add(subject);
						}
					}

					// Update objects that didn't received any position
					{
						var rbSubjects = ViconController.Instance.RigidBodySubjects; // We want this variable to stay in this scope
						if (foundRigidBodies.Count < rbSubjects.Count)
						{
							for (int i = 0, iMax = rbSubjects.Count; i < iMax; ++i)
							{
								var subject = rbSubjects[i];
								if (!foundRigidBodies.Contains(subject))
								{
									subject.UpdateTransform(frameNumber, Vector3.zero, Quaternion.identity);
									updated = true;
								}
							}
						}

					}

					_stats.FrameProcessLatency = Profiling.CPProfiler.DurationSince(_stats.FrameProcessTimestamp);

#if IK_PROFILING
					IKProfiling.MarkRbWrite();
#endif
				}
				else
				{
					++_stats.EmptyUpdates;

					if(result == Result.NotConnected)
					{
						// We are disconnect
						IsConnected = false;
						Debug.LogWarning("Got disconnected from Vicon");
						_stats.ConnectionInfo = _stats.ConnectionInfo.Replace("Connected", "Disconnected");
					}
					else if(result != Result.NoFrame)
					{
						Debug.LogWarningFormat("Failed to retrieve Vicon frame: Result={0}", result.ToString());
					}
				}
			}

			if((!IsConnected) && AutoReconnect)
			{
				Connect();
			}

			return updated;
		}

		#region Internals

		Result ConnectWorker(string hostAndPort)
		{
			Result ret = Result.NotImplemented;

			// Connect to Vicon server
			_stats.ConnectionInfo = string.Format("Connecting to {0}", hostAndPort);
			ret = TrackerSdk.Instance.Connect(hostAndPort);
			
			return ret;
		}

		private void ConnectAsyncCompletedCallback(IAsyncResult ar)
		{
			System.ComponentModel.AsyncOperation async;
			Result result;

			try
			{
				// get the original worker delegate and the AsyncOperation instance
				var worker = (ConnectWorkerDelegate)((AsyncResult)ar).AsyncDelegate;
				async = (System.ComponentModel.AsyncOperation)ar.AsyncState;

				// finish the asynchronous operation
				result = worker.EndInvoke(ar);
				if(result != Result.Success)
				{
					_stats.ConnectionInfo = _stats.ConnectionInfo.Replace("Connecting", "Failed to connect");
				}
				else
				{
					_stats.ConnectionInfo = _stats.ConnectionInfo.Replace("Connecting", "Connected");
				}
			}
			finally
			{
				// clear the running task flag
				lock(this)
				{
					_connectAsync = null;
				}
			}

			// raise the completed event
			var completedArgs = new ConnectionCompletedEventArgs(result, null, false, null);
			async.PostOperationCompleted(
				delegate (object e) { OnConnectionCompleted((AsyncCompletedEventArgs)e); },
				completedArgs);
		}

		/// <summary>
		/// connection completed event handler
		/// configures the connection if successful
		/// sets state
		/// </summary>
		/// <param name="e">async completed event (ConnectionCompletedEventArgs)</param>
		void OnConnectionCompleted(AsyncCompletedEventArgs e)
		{
			Result result = ((ConnectionCompletedEventArgs)e).Result;

			if(result == Result.Success)
			{
				// When running the IK module we want to wait for the next available frame
				// Any other component doesn't want to be blocked if there is no pending frame
				var mode = RunningForIkServer ? StreamMode.ServerPush : StreamMode.ClientPullPreFetch;
				TrackerSdk.Instance.SetStreamMode(mode);

				// We want the kinematic segment data
				TrackerSdk.Instance.EnableSegmentData();

				Debug.LogFormat("Successfully connected to Vicon host: Host={0}", TrackerEndpoint);

				IsConnected = AutoReconnect = true;
			}
			else
			{
				IsConnected = false;
			}
		}

		/// <summary>
		/// holds the result struct returned from TrackerSdk.Instance.Connect method
		/// </summary>
		private class ConnectionCompletedEventArgs : AsyncCompletedEventArgs
		{
			private Result _result;

			public ConnectionCompletedEventArgs(
				Result result,
				Exception error,
				bool cancelled,
				object userState)
				: base(error, cancelled, userState)
			{
				_result = result;
			}
			public Result Result
			{
				get
				{
					this.RaiseExceptionIfNecessary();
					return _result;
				}
			}
		}

#endregion

	}
}
