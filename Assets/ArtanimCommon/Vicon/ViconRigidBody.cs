﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;
using Artanim.Location.Monitoring;

namespace Artanim.Vicon
{

	public class ViconRigidBody : MonoBehaviour
	{
		[SerializeField][FormerlySerializedAs("rigidBodyName")]
		private string _RigidbodyName;

		public bool UpdatePosition = true;
		public bool UpdateRotation = true;

		[SerializeField]
		Transform OutOfDateMarker = null;

		private static ViconController ViconController;

		public string RigidbodyName { get { return _RigidbodyName; } }

		public bool IsTransformUpToDate { get; private set; }

		public Vector3 RigidbodyPosition { get; private set; }

		public Quaternion RigidbodyRotation { get; private set; }

		public uint LastUpdateFrameNumber { get; private set; }

		public void ResetRigidbodyName(string name)
		{
			_RigidbodyName = name;
			Update();
		}

		public void StartMonitoringTimeout(Location.Monitoring.ErrorTypes.IErrorData timeoutErrorData, float timeout = 0)
		{
			if (timeoutErrorData == null) throw new System.ArgumentNullException("timeoutErrorData");

			TimeoutErrorData = timeoutErrorData;

			if (timeout > 0)
			{
				TimeoutSeconds = timeout;
			}
		}

		public void StopMonitoringTimeout()
		{
			TimeoutErrorData = null;
		}

		private void Start()
		{
			if (ViconController == null)
			{
				ViconController = ViconController.Instance;
			}
		}

		private void OnEnable()
		{
			//Create standalone pickup if needed
			SetupStandalonePickupable();
		}

		private void OnDestroy()
		{
			//Dispose monitoring channel if available
			//if(MonitoringChannel != null)
			//{
			//	MonitoringChannel.Dispose();
			//	MonitoringChannel = null;
			//}
		}

		private void Update()
		{
			ProfilingMetrics.MarkRbUpdateStart();

			IsTransformUpToDate = false;

			if (ViconController && !string.IsNullOrEmpty(_RigidbodyName))
			{
				// Get the rigid body
				var rigidBody = ViconController.TryGetRigidBody(_RigidbodyName); //TODO we should do this only if null

				// And update the transform is there is a change
				if (rigidBody != null)
				{
					if (OutOfDateMarker)
						OutOfDateMarker.gameObject.SetActive(!rigidBody.Segment.isUpToDate);

					if (LastUpdateFrameNumber != rigidBody.LastUpdateFrameNumber)
					{
						RigidbodyPosition = rigidBody.Segment.globalTranslation;
						RigidbodyRotation = rigidBody.Segment.globalRotation;

						// Update transform
						if (UpdatePosition)
							transform.localPosition = RigidbodyPosition;

						if (UpdateRotation)
							transform.localRotation = RigidbodyRotation;

						// Keep track of last frame number
						LastUpdateFrameNumber = rigidBody.LastUpdateFrameNumber;

						// And track the update status
						IsTransformUpToDate = true;
					}

					if (TimeoutErrorData != null)
					{
						MonitorTracking(rigidBody);
					}
				}
			}

			ProfilingMetrics.MarkRbUpdateEnd();
		}


		#region Rigidbody update monitoring

		private float TimeoutSeconds = 3f;
		private bool LastIsUpToDate = true;
		private bool HasNotified;
		private float TimeoutTime;
		private Location.Monitoring.ErrorTypes.IErrorData TimeoutErrorData;
		private ErrorReporting.IReport Report;

		private void MonitorTracking(RigidBodySubject rigidBody)
		{
			if(LastIsUpToDate != rigidBody.Segment.isUpToDate)
			{
				//State change
				if(rigidBody.Segment.isUpToDate)
				{
					//RB is updated again
					if(HasNotified)
					{
						var message = string.Format("Rigidbody is now back to normal: {0}", _RigidbodyName);
						Debug.Log(message);

						//Monitoring
						if (Report != null)
						{
							Report.Close(message);
							Report = null;
						}
					}

					TimeoutTime = 0f;
					HasNotified = false;
				}
				else
				{
					//RB not updated anymore
					TimeoutTime = Time.unscaledTime + TimeoutSeconds;
				}
				LastIsUpToDate = rigidBody.Segment.isUpToDate;
			}
			else
			{
				if(!LastIsUpToDate && Time.unscaledTime > TimeoutTime && !HasNotified)
				{
					//RB is timed out
					var message = string.Format("Rigidbody timed out after {0} seconds. (Rigidbody={1})", TimeoutSeconds, _RigidbodyName);
					Debug.LogWarning(message);

					//Monitoring
					if (Report != null) Debug.LogError("An error report is already open for this rigidbody");
					Report = ErrorReporting.Instance.CreateReport(TimeoutErrorData);
					Report.SendMessage(message);

					HasNotified = true;
				}
			}
		}
		#endregion

		#region Standalone pickup

		private void SetupStandalonePickupable()
		{
			if(DevelopmentMode.CurrentMode == EDevelopmentMode.Standalone)
			{
				if (!GetComponent<StandalonePickupable>() && !GetComponentInParent<MainCameraController>()) //Exclude camera controller rigidbody
				{
					gameObject.AddComponent<StandalonePickupable>();
				}
			}
		}

		#endregion
	}
}