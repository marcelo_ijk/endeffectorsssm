﻿//#define DISABLE_SKELETON_SEND
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Artanim.Location.Config;
using Artanim.Location.Network;
using Artanim.Location.Data;

namespace Artanim
{
	namespace Vicon
	{
		public class ViconController : SingletonBehaviour<ViconController>
		{
			private const string SKELETON_ID_FORMAT = "Skeleton-{0}";

			[SerializeField]
			Text TextConnectionInfo = null;

			[SerializeField]
			GameObject DefaultRigidBody = null;

			[SerializeField]
			Transform RigidBodyRoot = null;

			[SerializeField]
			Toggle ToggleRigidbodiesList = null;

			[SerializeField]
			GameObject PanelRigidBodiesList = null;

			[SerializeField]
			Text TextRigidbodiesList = null;

			[SerializeField]
			bool CreateRigidBodyVisuals = true;

			bool _autoConnectOnce = true; // It will automatically connect once to the Vicon controller

			Dictionary<string, RigidBodySubject> _rigidbodies = new Dictionary<string, RigidBodySubject>();
			RigidBodySubject[] _cache_rigidbodies = new RigidBodySubject[0]; // Be sure to update this array every time a RigidBodySubject is added or removed from the above field

			ViconConfig ViconConfig;
			IViconConnector ViconConnector;
			int UpdateCounter; // Count updates for display purposes

			public event System.Action<RigidBodySubject> OnRigidbodySubjectAdded;
			//public event System.Action<RigidBodySubject> OnRigidbodySubjectRemoved; //TODO implement this event, and uncomment usage in SkeletonRecognizer

			public bool DataUpdated; //TODO awful hack!

			public long UpdateTimestamp
			{
				get; private set;
			}

			public IViconConnectorStats Stats
			{
				get { return ViconConnector.Stats; }
			}

			public IList<RigidBodySubject> RigidBodySubjects
			{
				get { return _cache_rigidbodies; }
			}

			void Start()
			{
				Instance = this;

				ViconConfig = ConfigService.Instance.Config.Infrastructure.Vicon;

				//Create vicon connector
				if (NetworkInterface.Instance.ComponentType == ELocationComponentType.IKServer)
				{
					//IK server uses SDK P2P connection
					ViconConnector = new ViconSDKConnector();
				}
				else
				{
					//Everyone else listen to IK server DDS messages
					ViconConnector = new ViconClientConnector();
					_autoConnectOnce = true;
				}

				Debug.LogFormat("Initializing Vicon connector of type: {0}", ViconConnector.GetType().Name);
				ViconConnector.Init(ViconConfig);
			}

			void OnDisable()
			{
				if (ViconConnector != null)
					ViconConnector.Disconnect();
			}

			void Update()
			{
#if EXP_PROFILING
				ExpProfiling.MarkVcStart();
#endif
				UpdateTimestamp = Profiling.CPProfiler.Timestamp;

				// Create rigid body visual if needed
				if (CreateRigidBodyVisuals)
				{
					DoCreateRigidBodyVisuals();
				}

				if (_autoConnectOnce)
				{
					StartViconConnection();
				}

				DataUpdated = ViconConnector.UpdateRigidBodies();

#if EXP_PROFILING
				float updateRbDuration = Profiling.CPProfiler.DurationSince(UpdateTimestamp);
				float messageLatency = (float)(System.DateTime.UtcNow - ViconConnector.Stats.FrameCaptureTime).TotalSeconds - updateRbDuration;
				ExpProfiling.SetIkUpdateLatency(ViconConnector.Stats.FrameCaptureLatency, ViconConnector.Stats.FrameProcessLatency, messageLatency);
#endif

				if (TextConnectionInfo)
					TextConnectionInfo.text = ViconConnector.Stats.ConnectionInfo;

				//Update rigidbody list
				if (UpdateCounter % 100 == 0)
				{
					UpdateRigidbodiesList();
				}

				UpdateCounter++;

				ProfilingMetrics.MarkVcEnd();
			}
			
			void DoCreateRigidBodyVisuals()
			{
				// Create the visual for all rigidbodies that don't have one yet
				for (int i = 0, iMax = _cache_rigidbodies.Length; i < iMax; ++i)
				{
					var subject = _cache_rigidbodies[i];
					if (subject.CreateVisual)
					{
						CreateRigidBodyVisual(subject);
						subject.CreateVisual = false;
					}
				}
			}

			#region Public interface

			public bool IsConnected
			{
				get { return ViconConnector.IsConnected; }
			}

			public void StartViconConnection()
			{
				if (ViconConnector != null && !ViconConnector.IsConnected)
				{
					if (ViconConnector.IsConnected)
					{
						ViconConnector.Disconnect();
					}
					ViconConnector.Connect();
					_autoConnectOnce = false;
				}
			}

			public RigidBodySubject GetOrCreateRigidBody(string rigidBodyName)
			{
				var subject = TryGetRigidBody(rigidBodyName);
				if(subject == null)
				{
					// Create rigidbody subject
					subject = new RigidBodySubject(rigidBodyName);
					subject.CreateVisual = CreateRigidBodyVisuals && !subject.IsSkeletonSubject;

					_rigidbodies.Add(rigidBodyName, subject);
					_cache_rigidbodies = _rigidbodies.Values.ToArray();

					// Trigger event
					if (OnRigidbodySubjectAdded != null) OnRigidbodySubjectAdded(subject);
				}
				return subject;
			}

			public RigidBodySubject TryGetRigidBody(string rigidBodyName)
			{
				if (!string.IsNullOrEmpty(rigidBodyName))
				{
					RigidBodySubject subject;
					_rigidbodies.TryGetValue(rigidBodyName, out subject);
					return subject;
				}
				else
				{
					Debug.LogWarning("Trying to get a rigid body with empty name!");
				}
				return null;
			}
			
#endregion

#region Location events

#endregion

#region Internals

			private void CreateRigidBodyVisual(RigidBodySubject subject)
			{
				//Create default visual
				if (RigidBodyRoot && DefaultRigidBody)
				{
					var instance = Instantiate(DefaultRigidBody, RigidBodyRoot) as GameObject;
					instance.transform.position = Vector3.zero;
					instance.transform.rotation = Quaternion.identity;
					instance.transform.localScale = Vector3.one;
					instance.name = subject.Name;

					var rigidBodyScript = instance.GetComponent<ViconRigidBody>();
					if (rigidBodyScript)
					{
						rigidBodyScript.ResetRigidbodyName(subject.Name);
					}
				}
			}

			private void UpdateRigidbodiesList()
			{
				if(PanelRigidBodiesList && TextRigidbodiesList && ToggleRigidbodiesList && ToggleRigidbodiesList.isOn)
				{
					PanelRigidBodiesList.SetActive(true);

					var rigidBodiesList = new StringBuilder();
					rigidBodiesList.AppendLine("Rigidbodies:");

					foreach(var rigidbodyName in _rigidbodies.Keys)
					{
						rigidBodiesList.AppendLine(rigidbodyName);
					}

					TextRigidbodiesList.text = rigidBodiesList.ToString();
				}
				else
				{
					if (PanelRigidBodiesList)
						PanelRigidBodiesList.SetActive(false);
				}
			}

#endregion
		}
	}
}
