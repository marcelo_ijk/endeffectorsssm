﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using Artanim.Location.Data;
using Artanim.Location.Messages;
using Artanim.Location.Monitoring;
using Artanim.Location.Network;
using Artanim.Location.SharedData;

#if UNITY_2017_3_OR_NEWER
using UnityEngine.XR;
#else
using UnityEngine.VR;
using XRNode = UnityEngine.VR.VRNode;
using XRNodeState = UnityEngine.VR.VRNodeState;
using XRSettings = UnityEngine.VR.VRSettings;
using XRDevice = UnityEngine.VR.VRDevice;
#endif

namespace Artanim
{

	public class RuntimeLocationComponent : MonoBehaviour
	{
		private const string ENV_VAR_HOSTNAME = "COMPUTERNAME";

		public Text TextExperienceName;
		public Text TextComponentType;
		public Text TextNetworkInfo;
		public Text TextSharedDataName;
		public Text TextNetworkGUID;

		Utils.RingBuffer<Action> _msgQueue = new Utils.RingBuffer<Action>(14); // Can queue up to 16384 messages

		//TODO Debug stuff to help tracking message lost issue
		long _dbg_inMsgCounter;
		long _dbg_outMsgCounter;
		LogSystem.Logger _dbg_msgLogger;

		void Awake()
		{
			bool isMono = (Type.GetType("Mono.Runtime") != null);
			Debug.LogFormat("Running with {0} runtime version {1}", isMono ? "Mono" : ".NET", Environment.Version);

			SharedDataUtils.Initialize();
		}

		void OnEnable()
		{
			//Update texts
			if (TextExperienceName) TextExperienceName.text = "Initializing...";
			if (TextComponentType) TextComponentType.text = "Initializing...";
			if (TextNetworkInfo) TextNetworkInfo.text = "Initializing...";
			if (TextNetworkGUID) TextNetworkGUID.text = "Initializing...";

			var componentType = SceneController.Instance.SceneComponentType;
			if (componentType == ELocationComponentType.Undefined)
			{
				Debug.LogError("Undefined component type");
			}
			else
			{
#if IK_PROFILING
				//Enable metrics for GC
				Monitoring.Utils.GCMonitor.StartMonitoringGC();
#endif
				_dbg_msgLogger = LogSystem.LogManager.Instance.GetLogger("DbgMsg");

				//Setup network connection
				NetworkInterface.Instance.InitializeFromConfig(componentType, action =>
				{
					_dbg_msgLogger.Info("Queuing message #" + ++_dbg_inMsgCounter);
					_msgQueue.TryPush(action);
				});

				NetworkInterface.Instance.Connect();
				MonitoringConnector.Initialize();

				Initialize();
			}
		}

		void OnApplicationQuit()
		{
			Debug.Log("OnApplicationQuit called");
		}

		void Initialize()
		{
			//Update texts
			if (TextExperienceName) TextExperienceName.text = ConfigService.Instance.ExperienceConfig.ExperienceName;
			if (TextComponentType) TextComponentType.text = NetworkInterface.Instance.ComponentType.ToString();
			if (TextSharedDataName) TextSharedDataName.text = SharedDataUtils.GetMyComponent<LocationComponent>().SharedId.ToString();
			if (TextNetworkGUID) TextNetworkGUID.text = NetworkInterface.Instance.NetworkGuid.ToString();
			UpdateNetworkTextInfo();

			//Attach events
			NetworkInterface.Instance.Subscribe<GoEmergencyState>(GoEmergencyState);
			NetworkInterface.Instance.Subscribe<ShutdownComponent>(NetworkMessage_ShutdownComponent);
			if (NetworkInterface.Instance.IsServer)
				NetworkInterface.Instance.Subscribe<RequestExperienceData>(NetworkMessage_RequestExperienceData);
			SharedDataUtils.ComponentAdded += SharedDataUtils_ComponentAddedRemoved;
			SharedDataUtils.ComponentRemoved += SharedDataUtils_ComponentAddedRemoved;

			//Set the component experience name to shared data
			SetComponentExperience();

			//Component type specific setup
			switch (NetworkInterface.Instance.ComponentType)
			{
				case ELocationComponentType.IKServer:
					break;

				case ELocationComponentType.ExperienceServer:

					//Disable VR
					DisableVR();

					//Setup quality settings
					SetupQualitySettings(NetworkInterface.Instance.ComponentType);

					break;

				case ELocationComponentType.ExperienceClient:

					//Setup quality settings
					SetupQualitySettings(NetworkInterface.Instance.ComponentType);

					//Render scale
#if UNITY_2017_3_OR_NEWER
					XRSettings.eyeTextureResolutionScale = ConfigService.Instance.Config.Location.Client.RenderScale;
					Debug.LogFormat("Set VR render scale to: {0}", XRSettings.eyeTextureResolutionScale);
#else
					XRSettings.renderScale = ConfigService.Instance.Config.Location.Client.RenderScale;
					Debug.LogFormat("Set VR render scale to: {0}", XRSettings.renderScale);
#endif

					//Handle development modes
					if (DevelopmentMode.CurrentMode != EDevelopmentMode.None)
					{
						NetworkInterface.Instance.DevelopmentMode = true;

						//Set mode in shared data
						var expClient = SharedDataUtils.GetMyComponent<ExperienceClient>();
						if (expClient != null)
						{
							expClient.ClientMode = ExperienceClient.EClientMode.ClientAndServer;
						}

						Debug.LogWarning("Network development model activated. The component will act as client and server.");
					}

					break;

				case ELocationComponentType.ExperienceObserver:

					//Disable VR
					DisableVR();

					//Setup quality settings
					SetupQualitySettings(NetworkInterface.Instance.ComponentType);

					break;

				case ELocationComponentType.HostessApp:
					break;
				
				case ELocationComponentType.SharedDataViewer:
					break;

				case ELocationComponentType.Undefined:
					break;

				default:
					break;
			}
		}

		void OnDisable()
		{
			//Detach events. (GoEmergencyState is not detached on purpose)
			NetworkInterface.Instance.Unsubscribe<ShutdownComponent>(NetworkMessage_ShutdownComponent);
			NetworkInterface.Instance.Unsubscribe<RequestExperienceData>(NetworkMessage_RequestExperienceData);
			SharedDataUtils.ComponentAdded -= SharedDataUtils_ComponentAddedRemoved;
			SharedDataUtils.ComponentRemoved -= SharedDataUtils_ComponentAddedRemoved;

			//Shutdown
			MonitoringConnector.Shutdown();
			NetworkInterface.Instance.Disconnect();
		}

		void OnDestroy()
		{
			NetworkInterface.DisposeSingleton();
			SharedDataUtils.Shutdown();
		}

		public static IKUpdate IkUpdate { get; private set; }

		void Update()
		{
#if EXP_PROFILING
			Monitoring.Utils.ExpProfiling.StartFrame();
#endif

			// Forward all events
			Action ev = null;
			while (_msgQueue.TryPull(ref ev))
			{
				if (ev != null)
				{
					try
					{
						_dbg_msgLogger.Info("Invoking message #" + ++_dbg_outMsgCounter);
						ev.Invoke();
					}
					catch (Exception e)
					{
						Debug.LogException(e);
					}
				}
				else
				{
					Debug.LogError("Null element in message queue");
				}
			}

			IkUpdate = NetworkInterface.Instance.GetLastIkUpdate();

			CheckIKUpdateTime();
		}

		private void CheckIKUpdateTime()
		{
			if(IkUpdate != null)
			{
				var millisSinceLastUpdate = (DateTime.Now.ToUniversalTime() - IkUpdate.ReceptionTime).TotalSeconds;
				if (millisSinceLastUpdate > 2)
				{
					Debug.LogErrorFormat("IK update timeout! {0}, {1:dd.MM.yyyy hh:mm:ss.ffff}, {2:dd.MM.yyyy hh:mm:ss.ffff}", millisSinceLastUpdate, IkUpdate.ReceptionTime, DateTime.Now.ToUniversalTime());
				}
			}
		}

		private void GoEmergencyState(GoEmergencyState args)
		{
			SceneController.Instance.DoEmergency();
		}

		private void NetworkMessage_ShutdownComponent(ShutdownComponent args)
		{
			Application.Quit();
		}

		private void NetworkMessage_RequestExperienceData(RequestExperienceData args)
		{
			string expName = ConfigService.Instance.ExperienceConfig.ExperienceName;
			string pathname = System.IO.Path.Combine(Utils.Paths.ConfigDir, expName + "_config.xml");
			Debug.Log("Sending experience config XML at path: " + pathname);
			var msg = new ExperienceData()
			{
				RecipientId = args.SenderId,
				ExperienceName = expName,
				ConfigXml = System.IO.File.ReadAllText(pathname),
			};
			NetworkInterface.Instance.SendMessage(msg);
		}

		private void SharedDataUtils_ComponentAddedRemoved(LocationComponent locationComponent)
		{
			UpdateNetworkTextInfo();
		}

		private void UpdateNetworkTextInfo()
		{
			if (TextNetworkInfo) TextNetworkInfo.text = string.Format("Domain={0}, Components={1}", NetworkInterface.Instance.NetBus.DomainID, SharedDataUtils.Components.Length);
		}

		private void DisableVR()
		{
			if (XRDevice.isPresent)
			{
				Debug.Log("Disable VR headset on observer.");
				XRSettings.enabled = false;
			}
		}

		private void SetComponentExperience()
		{
			var thisComponent = SharedDataUtils.GetMyComponent<LocationComponentWithExperience>();
			if(thisComponent != null && ConfigService.Instance.ExperienceConfig != null)
			{
				//Set the experience name
				thisComponent.ExperienceName = ConfigService.Instance.ExperienceConfig.ExperienceName;
			}
		}

		private void SetupQualitySettings(ELocationComponentType componentType)
		{
			var settingIndex = FindQualitySettingIndex(componentType);

			if (settingIndex == -1 && componentType == ELocationComponentType.ExperienceObserver)
			{
				//Try settings for client
				settingIndex = FindQualitySettingIndex(ELocationComponentType.ExperienceClient);
			}

			//Apply quality settings
			if(settingIndex > -1)
			{
				Debug.LogFormat("<color=white>Setting quality level set for {0} to {1}</color>", componentType.ToString(), QualitySettings.names[settingIndex]);
				QualitySettings.SetQualityLevel(settingIndex, true);
			}
			else
			{
				Debug.LogFormat("<color=white>No quality level found for {0}, leaving setup default level.</color>", componentType.ToString());
			}

			//Override VSync and target FPS if needed
			switch (componentType)
			{
				case ELocationComponentType.ExperienceServer:
					//VSync
					if (QualitySettings.vSyncCount != 0)
					{
						Debug.Log("<color=white>Overriding V-Sync for ExperienceServer. Setting it to 'Don't Sync'</color>");
						QualitySettings.vSyncCount = 0;
					}

					//Target FPS from config
					Application.targetFrameRate = ConfigService.Instance.ExperienceConfig.ServerFPS;
					Debug.LogFormat("<color=white>Set configured ExperienceServer target FPS to {0}</color>", Application.targetFrameRate);
					
					break;

				case ELocationComponentType.ExperienceClient:
					//VSync
					if (QualitySettings.vSyncCount != 1)
					{
						Debug.Log("<color=white>Overriding V-Sync for ExperienceClient. Setting it to 'Every V Blank'</color>");
						QualitySettings.vSyncCount = 1;
					}
					break;

				case ELocationComponentType.ExperienceObserver:
					//Leave as is
					break;
				default:
					break;
			}
		}

		private int FindQualitySettingIndex(ELocationComponentType componentType)
		{
			return Array.IndexOf(QualitySettings.names, componentType.ToString());
		}
	}

}