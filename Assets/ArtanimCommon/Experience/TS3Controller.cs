﻿using UnityEngine;
using UnityEngine.UI;
using Artanim.Location.Network;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using uint64 = System.UInt64;
using anyID = System.UInt16;
using Artanim.Location.SharedData;
using Artanim.Location.Data;

namespace Artanim
{
	/// <summary>
	/// 
	/// </summary>
	[RequireComponent(typeof(SilenceTsMix))]
	public class TS3Controller : MainThreadInvoker
	{
		#region Events

		public Action OnConnected;
		public Action OnDisconnected;
		public Action OnChannelsUpdated;

		#endregion

		[Tooltip("Current TS connection status")]
		public ConnectStatus ConnectStatus;

		[Tooltip("Current TS channel. (Editor debug, don't change)")]
		public string ChannelName;

		[Tooltip("Current TS nickname. (Editor debug, don't change)")]
		public string NickName;

		public GameObject DefaultTSAudioSourceTemplate;

		public bool IsConnected
		{
			get
			{
				//Debug.LogFormat("client={0}, status={1}", TeamSpeakClient != null ? "not null" : "null", TeamSpeakClient != null ? TeamSpeakClient.GetConnectionStatus().ToString() : "N/A");
				return TeamSpeakClient != null && (ConnectStatus)TeamSpeakClient.GetConnectionStatus() != ConnectStatus.STATUS_DISCONNECTED;
			}
		}

		private const string DEFAULT_CHANNEL_PASSWORD = "";

		private TeamSpeakClient TeamSpeakClient;

		#region Unity signals

		void OnEnable()
		{
			SharedDataUtils.ComponentAdded += SharedDataUtils_ComponentAdded;
			SharedDataUtils.ComponentRemoved += SharedDataUtils_ComponentRemoved;

			var silenceTsMix = GetComponent<SilenceTsMix>();
			if(silenceTsMix)
				silenceTsMix.enabled = true;
		}
		
		void OnDisable()
		{
			SharedDataUtils.ComponentAdded -= SharedDataUtils_ComponentAdded;
			SharedDataUtils.ComponentRemoved -= SharedDataUtils_ComponentRemoved;

			var silenceTsMix = GetComponent<SilenceTsMix>();
			if (silenceTsMix)
				silenceTsMix.enabled = false;
		}
		
		void OnApplicationQuit()
		{
			DisconnectTeamspeak(true);
		}

		#endregion

		#region Public interface

		/// <summary>
		/// Connect to location teamspeak server to the given channel and with the given nickname.
		/// </summary>
		/// <param name="channelName">Name of the channel to join</param>
		/// <param name="nickName">Nickname to use</param>
		public void ConnectTeamspeak(string channelName, string nickName)
		{
			ChannelName = channelName;
			NickName = nickName;

			if (!IsConnected)
			{
				TeamSpeakClient = TeamSpeakClient.GetInstance();

				//Attach TS events
				TeamSpeakCallbacks.onTalkStatusChangeEvent += OnTalkStatusChangeEvent;
				TeamSpeakCallbacks.onServerErrorEvent += TeamSpeakCallbacks_onServerErrorEvent;
				TeamSpeakCallbacks.onConnectStatusChangeEvent += TeamSpeakCallbacks_onConnectStatusChangeEvent;
				TeamSpeakCallbacks.onNewChannelCreatedEvent += TeamSpeakCallbacks_onNewChannelCreatedEvent;
				TeamSpeakCallbacks.onDelChannelEvent += TeamSpeakCallbacks_onDelChannelEvent;
				TeamSpeakCallbacks.onIgnoredWhisperEvent += TeamSpeakCallbacks_onIgnoredWhisperEvent;

				//enabling logging of some pre defined errors.
				TeamSpeakClient.logErrors = false;

				//Connect client
				string host = ConfigService.Instance.Config.Infrastructure.TeamSpeak.HostOverride;
				if (!string.IsNullOrEmpty(host))
				{
					TSLog("Using TS server host override");
				}
				else
				{
					// Get TS server shared data and extract IP address
					var tsInfo = SharedDataUtils.FindSharedData<Location.Data.TeamspeakServer>();
					if (tsInfo == null)
					{
						TSLogWarning("TS server shared data not found");
					}
					else if (string.IsNullOrEmpty(tsInfo.IPAddress))
					{
						TSLogError("TS server shared has an empty IP address");
					}
					else
					{
						host = tsInfo.IPAddress;
					}
				}
				if (!string.IsNullOrEmpty(host))
				{
					var tsConfig = ConfigService.Instance.Config.Infrastructure.TeamSpeak;
					var tsChannel = new string[] { ChannelName, "" }; // :-( This doesn't work anymore... TS will not create the channel
					var port = Location.Data.TeamspeakServer.Port;

					TSLogFormat("Connection to TS server: Address={0}, Port={1}, Channel={2}, Nickname={3}", host, port, channelName, NetworkInterface.Instance.NetworkGuid);
					TeamSpeakClient.StartClient(host, port, tsConfig.ServerPassword, NickName, ref tsChannel, DEFAULT_CHANNEL_PASSWORD);

					if (!TeamSpeakClient.started)
					{
						TSLogWarningFormat("Not connected to TS server: {0}:{1}", host, port);
					}
				}
			}
			else
			{
				TSLog("Teamspeak already connected");
			}
		}

		/// <summary>
		/// Disconnect from Teamspeak server if connected.
		/// </summary>
		/// <param name="forgetClientInfos">Indicated if the given nickname and channel have to be cleared. Only clear this info when we leave the session and not when we just get disconnected.</param>
		public void DisconnectTeamspeak(bool forgetClientInfos)
		{
			//Reset client id to shared data
			var thisClient = SharedDataUtils.GetMyComponent<ExperienceClient>();
			if (thisClient != null)
			{
				thisClient.TSClientId = 0;
			}

			//Remove all audio sources
			RemoveAllAudioSources();

			if (IsConnected)
			{
				TSLog("Disconnecting from TS server");
				TeamSpeakClient.StopClient();
				TeamSpeakClient = null;

				//Detach TS events
				TeamSpeakCallbacks.onTalkStatusChangeEvent -= OnTalkStatusChangeEvent;
				TeamSpeakCallbacks.onServerErrorEvent -= TeamSpeakCallbacks_onServerErrorEvent;
				TeamSpeakCallbacks.onNewChannelCreatedEvent -= TeamSpeakCallbacks_onNewChannelCreatedEvent;
				TeamSpeakCallbacks.onDelChannelEvent -= TeamSpeakCallbacks_onDelChannelEvent;
				TeamSpeakCallbacks.onIgnoredWhisperEvent -= TeamSpeakCallbacks_onIgnoredWhisperEvent;
				TeamSpeakCallbacks.onConnectStatusChangeEvent -= TeamSpeakCallbacks_onConnectStatusChangeEvent;

				ConnectStatus = ConnectStatus.STATUS_DISCONNECTED;

				if (forgetClientInfos)
				{
					ChannelName = null;
					NickName = null;
				}

				if (OnDisconnected != null)
					OnDisconnected();
			}
			else
			{
				TSLog("No disconnect... not connected to  TS server");
			}
		}

		/// <summary>
		/// Creates a new channel with the given name and joins it.
		/// Just join the channel if it channel already exists
		/// </summary>
		/// <param name="channelName">Channel name</param>
		public void CreateAndJoinChannel(string channelName)
		{
			var channelId = TeamSpeakClient.GetChannelIDFromChannelNames(new string[] { channelName, "" });
			if (channelId > 0)
			{
				JoinChannel(channelId);
			}
			else
			{
				//Create channel
				ChannelName = channelName;

				uint64 _channelID = 0;
				/* Set data of new channel. Use channelID of 0 for creating channels. */

				if (channelName != "")
					TeamSpeakClient.SetChannelVariableAsString(TeamSpeakClient.GetServerConnectionHandlerID(), _channelID, ChannelProperties.CHANNEL_NAME, channelName);

				/* Flush changes to server */
				TeamSpeakClient.FlushChannelCreation(TeamSpeakClient.GetServerConnectionHandlerID(), 0);

				Debug.LogFormat("Joining TS channel: {0}", channelName);
			}
		}

		/// <summary>
		/// Joins the channel with the given channel ID.
		/// Do nothing if the channel does not exist.
		/// </summary>
		/// <param name="channelId">TS channel id</param>
		public void JoinChannel(uint64 channelId)
		{
			if (channelId > 0)
			{
				Debug.LogFormat("Trying to join to channel: channelId={0}, we're in={1}", channelId, TeamSpeakClient.GetChannelOfClient(TeamSpeakClient.GetClientID()));
				if (channelId != TeamSpeakClient.GetChannelOfClient(TeamSpeakClient.GetClientID()))
				{
					Debug.LogFormat("Connecting to channelId: {0}", channelId);
					//Join channel
					TeamSpeakClient.RequestClientMove(TeamSpeakClient.GetClientID(), channelId, "");
				}
				else
				{
					Debug.LogFormat("Already in channel {0}, doing nothing.", channelId);
				}
			}
		}

		/// <summary>
		/// Initializes the audio source for the given player. If the given player is not connected to TS (anymore), the audio source will just be destroyed.
		/// If the player already has an audio source assigned, it will be destroyed first.
		/// The create audio source is configured in the experience settings or the SDK default is used.
		/// </summary>
		/// <param name="player">Player to be initialized</param>
		public void InitPlayer(RuntimePlayer player)
		{
			if (player == null)
				return;

			//Clear existing player audio source
			RemovePlayerAudioSource(player);

			//Create TS audio source
			if (player.Player != null)
			{
				var client = SharedDataUtils.FindLocationComponent<ExperienceClient>(player.Player.ComponentId);
				if (client != null)
				{
					//Check client TS id
					if(client.TSClientId > 0)
					{
						var isHostess = player.IsMainPlayer;

						//Get the audio source root. Other players: avatar head, self: camera rigidbody
						var audioRoot = !isHostess ?
								player.AvatarController.GetAvatarBodyPart(Location.Messages.EAvatarBodyPart.Head).transform :
								transform; // MainCameraController.Instance.PlayerCamera.transform;


						//Initialize player with configured audio prefab
						TSLogFormat("Initializing audio source for player: player={0}, tsClientId={1}", player.Player.ComponentId, client.TSClientId);
						player.TSAudioSource =
							UnityUtils.InstantiatePrefab<TeamSpeakAudioSource>(
								GetAudioSourceTemplate(isHostess),
								audioRoot);

						var localPosition = !isHostess ? Vector3.zero : new Vector3(0f, 0f, 1f); //Hostess in front of player
						var localRotation = !isHostess ? Quaternion.identity : Quaternion.Euler(0f, 180f, 0f); //Hostess looking at player

						player.TSAudioSource.transform.localPosition = localPosition;
						player.TSAudioSource.transform.localRotation = localRotation;

						if (isHostess)
						{
							//Attach camera follower
							if (!player.TSAudioSource.GetComponent<CameraAttachedGameObject>())
							{
								var camAttached = player.TSAudioSource.gameObject.AddComponent<CameraAttachedGameObject>();
								camAttached.Distance = 1f;
								camAttached.LookAt = true;
							}
						}

						player.TSAudioSource.Initialize(client.TSClientId, isHostess); //Main player -> hostess source
					}
					else
					{
						//Don't need to do anything. ClientId 0 means disconnected from TS and we already cleaned up the audio source for it earlier.
					}
				}
				else
				{
					Debug.LogError("Failed to initialize teamspeak for player. Client component not found or TS client id is invalid.");
				}
			}
			else
			{
				Debug.LogError("Failed to initialize teamspeak for player. Player was null.");
			}
		}

		#endregion

		#region Teamspeak events

		private void TeamSpeakCallbacks_onIgnoredWhisperEvent(ulong serverConnectionHandlerID, ushort clientID)
		{
			TeamSpeakClient.AllowWhispersFrom(clientID);
		}

		private void OnTalkStatusChangeEvent(uint64 serverConnectionHandlerID, int status, int isReceivedWhisper, anyID clientID)
		{
			TSLogFormat("TS status change event: {0} {1}", serverConnectionHandlerID, status);
		}

		private void TeamSpeakCallbacks_onServerErrorEvent(ulong serverConnectionHandlerID, string errorMessage, uint error, string returnCode, string extraMessage)
		{
			TSLogErrorFormat("TS server error: {0}", errorMessage);
		}

		private void TeamSpeakCallbacks_onConnectStatusChangeEvent(ulong serverConnectionHandlerID, int newStatus, uint errorNumber)
		{
			ConnectStatus = (ConnectStatus)newStatus;
			TSLogFormat("Connect status changed to: {0}", ConnectStatus);

			if (errorNumber > 0)
			{

				if(public_errors.ERROR_connection_lost == errorNumber)
				{
					//Connection lost... disconnect to clean up
					TSLogError("TS status change event: connection lost to TS server, disconnecting");
					DisconnectTeamspeak(false);
				}
				else
				{
					TSLogErrorFormat("TS status change event: Error={0}, Message={1}", errorNumber, TeamSpeakClient.GetErrorMessage(errorNumber));
				}
			}
			else
			{
				if (ConnectStatus == ConnectStatus.STATUS_CONNECTION_ESTABLISHED)
				{
					TSLogFormat("TS connected");

					//Set client id to shared data
					var thisClient = SharedDataUtils.GetMyComponent<ExperienceClient>();
					if(thisClient != null)
					{
						Invoke(() =>
						{
							thisClient.TSClientId = TeamSpeakClient.GetClientID();
						});
					}

					//Set configures void activate level
					var voiceActivationLevle = ConfigService.Instance.Config.Infrastructure.TeamSpeak.VoiceActivationLevel;
					TSLogFormat("Setting voice activation level to: {0}", voiceActivationLevle);
					TeamSpeakClient.SetPreProcessorConfigValue(TeamSpeakClient.PreProcessorConfig.voiceactivation_level, voiceActivationLevle);

					//Switch channel
					CreateAndJoinChannel(ChannelName);

					//Mute microphone if needed
					if(ConfigService.Instance.ExperienceConfig != null && ConfigService.Instance.ExperienceConfig.TSMuteMic)
					{
						TSLog("Muting TS client input device.");
						TeamSpeakClient.SetClientSelfVariableAsInt(ClientProperties.CLIENT_INPUT_DEACTIVATED, (int)InputDeactivationStatus.INPUT_DEACTIVATED);
					}

					if (OnConnected != null)
						OnConnected();
				}
				else if (ConnectStatus == ConnectStatus.STATUS_DISCONNECTED)
				{
					TSLogFormat("TS disconnected");
					DisconnectTeamspeak(false);
				}
			}
		}

		private void TeamSpeakCallbacks_onDelChannelEvent(ulong serverConnectionHandlerID, ulong channelID, ushort invokerID, string invokerName, string invokerUniqueIdentifier)
		{
			if (OnChannelsUpdated != null)
				OnChannelsUpdated();
		}

		private void TeamSpeakCallbacks_onNewChannelCreatedEvent(ulong serverConnectionHandlerID, ulong channelID, ulong channelParentID, ushort invokerID, string invokerName, string invokerUniqueIdentifier)
		{
			//Is it our channel and do we have to switch?
			if (TeamSpeakClient.GetChannelVariableAsString(channelID, ChannelProperties.CHANNEL_NAME) == ChannelName && TeamSpeakClient.GetChannelOfClient(TeamSpeakClient.GetClientID()) != channelID)
			{
				JoinChannel(channelID);
			}

			if (OnChannelsUpdated != null)
				OnChannelsUpdated();
		}

		#endregion

		#region Location events

		private void SharedDataUtils_ComponentAdded(Location.Data.LocationComponent locationComponent)
		{
			if(!IsConnected && !string.IsNullOrEmpty(ChannelName) && !string.IsNullOrEmpty(NickName) && (locationComponent is TeamspeakServer))
			{
				TSLogFormat("TS server was started now, trying to connect to channel={0}, nickname={1}", ChannelName, NickName);
				ConnectTeamspeak(ChannelName, NickName);
			}
		}

		private void SharedDataUtils_ComponentRemoved(LocationComponent locationComponent)
		{
			if(locationComponent is TeamspeakServer)
			{
				TSLog("TS server was stopped, disconnecting.");
				DisconnectTeamspeak(false);
			}
		}

		#endregion

		#region Internals

		private GameObject GetAudioSourceTemplate(bool hostess)
		{
			GameObject audioSourceTemplate = null;

			if(ConfigService.Instance.ExperienceSettings != null)
			{
				//Default experience source
				audioSourceTemplate = ConfigService.Instance.ExperienceSettings.TeamspeakAudioSource;

				//Hostess?
				if(hostess && ConfigService.Instance.ExperienceSettings.TeamspeakHostessAudioSource != null)
					audioSourceTemplate = ConfigService.Instance.ExperienceSettings.TeamspeakHostessAudioSource;
			}

			//Validate
			if(audioSourceTemplate != null)
			{
				if(!audioSourceTemplate.GetComponent<TeamSpeakAudioSource>())
				{
					Debug.LogWarningFormat("The setup teamspeak audiosource does not have a TeamSpeakAudioSource behaviour attached: {0}. Using SDK default template.", audioSourceTemplate.name);
					audioSourceTemplate = DefaultTSAudioSourceTemplate;
				}
			}

			if (audioSourceTemplate == null)
				audioSourceTemplate = DefaultTSAudioSourceTemplate;
			
			return audioSourceTemplate;
		}

		private void RemovePlayerAudioSource(RuntimePlayer player)
		{
			if(player.TSAudioSource)
			{
				TSLogFormat("Destroying TS audiosource for player: {0}", player.Player.ComponentId);
				Destroy(player.TSAudioSource.gameObject);
				player.TSAudioSource = null;
			}
		}

		private void RemoveAllAudioSources()
		{
			Invoke(() =>
			{
				foreach (var source in FindObjectsOfType<TeamSpeakAudioSource>())
				{
					Destroy(source.gameObject);
				}
			});
		}

		#endregion

		#region Logging

		private string GetErrorMsessage(uint tsError)
		{
			string msg = null;
			IntPtr strPtr = IntPtr.Zero;
			if (TeamSpeakInterface.ts3client_getErrorMessage(tsError, out strPtr) == public_errors.ERROR_ok)
			{
				string tmp_str = System.Runtime.InteropServices.Marshal.PtrToStringAnsi(strPtr);
				msg = string.Copy(tmp_str);
				TeamSpeakInterface.ts3client_freeMemory(strPtr);
			}
			return msg ?? tsError.ToString();
		}

		private void TSLog(string message)
		{
			Debug.Log("<color=orange>" + message + "</color>");
		}

		private void TSLogFormat(string format, params object[] args)
		{
			TSLog(string.Format(format, args));
		}

		private void TSLogWarning(string message)
		{
			Debug.LogWarning("<color=orange>" + message + "</color>");
		}

		private void TSLogWarningFormat(string format, params object[] args)
		{
			TSLogWarning(string.Format(format, args));
		}

		private void TSLogError(string message)
		{
			Debug.LogError("<color=orange>" + message + "</color>");
		}

		private void TSLogErrorFormat(string format, params object[] args)
		{
			TSLogError(string.Format(format, args));
		}

		#endregion
	}

}