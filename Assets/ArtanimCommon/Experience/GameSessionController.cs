﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Artanim.Experience;
using Artanim.Location.Network;

namespace Artanim
{
	[RequireComponent(typeof(GameController))]
	public class GameSessionController : SingletonBehaviour<GameSessionController>
	{
		public RectTransform GameSessionDebugPanel;

		public delegate void OnValueUpdatedHandler(string key, object value, bool playerValue = false, bool isInitializing = false);
		public event OnValueUpdatedHandler OnValueUpdated;

		GameSessionValues _gameSession;

		void OnEnable()
		{
			GameController.Instance.OnJoinedSession += GameController_OnJoinedSession;
			GameController.Instance.OnLeftSession += GameController_OnLeftSession;
		}

		void OnDisable()
		{
			GameController.Instance.OnJoinedSession -= GameController_OnJoinedSession;
			GameController.Instance.OnLeftSession -= GameController_OnLeftSession;
		}

		void OnDestroy()
		{
			// Clear
			GameController_OnLeftSession();
		}

		public T GetValue<T>(string key, T defaultValue, Guid playerId = default(Guid))
		{
			if (_gameSession != null)
			{
				return _gameSession.GetValue(key, defaultValue, playerId);
			}
			return defaultValue;
		}

		public void SetValue<T>(string key, T value, Guid playerId = default(Guid))
		{
			if (_gameSession == null)
			{
				// This is to show when SetValue is called before the game session is ready
				// but it will also happen when the component has left the session (and in this case we don't really care)
				Debug.LogWarning("<color=teal>Cannot set a Game Session Value because controller is not yet ready</color>");
			}
			else
			{
				if(NetworkInterface.Instance.IsServer)
				{
					if(!string.IsNullOrEmpty(key))
					{
						_gameSession.SetValue(key, value, playerId);
					}
					else
					{
						Debug.LogWarning("<color=teal>Cannot set game session value. The given key was empty or null.</color>");
					}
				}
				else
				{
					Debug.LogWarningFormat("<color=teal>Cannot change game session value {0}. Only the server is allowed to change values</color>", key);
				}
			}
		}

		public void DumpGameSession()
		{
			if (GameSessionDebugPanel)
			{
				GameSessionDebugPanel.gameObject.SetActive(true);
				var text = GameSessionDebugPanel.GetComponentInChildren<Text>();
				if (text != null)
				{
					var sessionDump = "N/A";
					if (_gameSession != null)
					{
						sessionDump = _gameSession.DebugSessionState;
						if (string.IsNullOrEmpty(sessionDump))
						{
							sessionDump = "<empty session>";
						}
					}
						
					text.text = sessionDump;
				}
			}
		}

		void GameController_OnJoinedSession(Location.Data.Session session, Guid playerId)
		{
			//Create / init game session
			if (_gameSession != null)
			{
                //Leave session first
				_gameSession.OnValueUpdated -= GameSession_OnValueUpdated;
				_gameSession.Dispose();
				_gameSession = null;
			}
			_gameSession = new GameSessionValues(session.SharedId, playerId);
			_gameSession.OnValueUpdated += GameSession_OnValueUpdated;
		}

		void GameController_OnLeftSession()
		{
			//Clear Game Session
			if (_gameSession != null)
			{
				_gameSession.OnValueUpdated -= GameSession_OnValueUpdated;
				_gameSession.Dispose();
				_gameSession = null;
			}
		}

		void GameSession_OnValueUpdated(string key, object value, bool playerValue = false, bool isInitializing = false)
		{
			if (OnValueUpdated  != null)
			{
				OnValueUpdated(key, value, playerValue, isInitializing);
			}
		}
	}
}