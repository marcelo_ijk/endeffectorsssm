﻿using Artanim.Utils;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Artanim
{
	public class ScreenshotController : SingletonBehaviour<ScreenshotController>
	{
		private const int SCREENSHOT_DEPTH = 24;
		private const string SCREENSHOT_NAME_FORMAT = "{0}_{1:0000}_{2}.{3}";

		private RenderTexture _screenshotRenderTexture;
		private RenderTexture ScreenshotRenderTexture
		{
			get
			{
				if (!_screenshotRenderTexture)
				{
					_screenshotRenderTexture = new RenderTexture(ConfigService.Instance.Config.Location.Client.ScreenshotWidth, ConfigService.Instance.Config.Location.Client.ScreenshotHeight, SCREENSHOT_DEPTH);
				}
				return _screenshotRenderTexture;
			}
		}

		private int ScreenshotNum;

		private void OnEnable()
		{
			if (GameController.Instance)
				GameController.Instance.OnJoinedSession += Instance_OnJoinedSession;
		}

		private void OnDisable()
		{
			if (GameController.Instance)
				GameController.Instance.OnJoinedSession -= Instance_OnJoinedSession;
		}

		private void Instance_OnJoinedSession(Location.Data.Session session, System.Guid playerId)
		{
			ScreenshotNum = 0;
		}


		/// <summary>
		/// Takes a screenshot of the given camera and stores it in the configured format and resolution.
		/// </summary>
		/// <param name="screenshotLabel">Custom label used in the image file name</param>
		/// <param name="camera">Camera to take the screenshot from. If no camera is given, the player main camera (HMD view) is used.</param>
		public void TakeScreenshot(string screenshotLabel, Camera camera = null)
		{
			if (!camera)
			{
				//Player view screenshot
				camera = MainCameraController.Instance.PlayerCamera.GetComponent<Camera>();
			}

			if(!camera)
			{
				Debug.LogErrorFormat("Failed to create screenshot. No camera set and player camera could not be found.");
				return;
			}

			if (string.IsNullOrEmpty(screenshotLabel))
				screenshotLabel = "Unknown";

			//Take hires screenshot
			var prevRT = camera.targetTexture;

			camera.targetTexture = ScreenshotRenderTexture;
			camera.Render();
			RenderTexture.active = ScreenshotRenderTexture;
			var screenshot = new Texture2D(ScreenshotRenderTexture.width, ScreenshotRenderTexture.height, TextureFormat.RGB24, false);
			screenshot.ReadPixels(new Rect(0, 0, Instance.ScreenshotRenderTexture.width, Instance.ScreenshotRenderTexture.height), 0, 0);
			RenderTexture.active = null;

			byte[] bytes;
			if (ConfigService.Instance.Config.Location.Client.ScreenshotFileType == Location.Config.EScreenshotFileType.JPG)
				bytes = screenshot.EncodeToJPG();
			else
				bytes = screenshot.EncodeToPNG();

			var screenshotFile = GetScreenshotFilePath(screenshotLabel, ++ScreenshotNum);

			//Ensure directory is present
			Directory.CreateDirectory(Path.GetDirectoryName(screenshotFile));

			File.WriteAllBytes(screenshotFile, bytes);
			Debug.LogFormat("Stored new screenshot to: {0}", screenshotFile);

			camera.targetTexture = prevRT;
		}

		private string GetScreenshotFilePath(string cameraTag, int screenshotNum)
		{
			//PlayerId
			var playerId = GameController.Instance.CurrentPlayer.Player.TicketId;
			if (string.IsNullOrEmpty(playerId))
				playerId = GameController.Instance.CurrentPlayer.Player.ComponentId.ToString();

			//SessionId
			var sessionId = GameController.Instance.CurrentSession.SharedId.Guid.ToString();

			//Root
			var screenshotsRoot = Paths.ScreenshotDir;

			var fileName = string.Format(SCREENSHOT_NAME_FORMAT, playerId, screenshotNum, cameraTag, ConfigService.Instance.Config.Location.Client.ScreenshotFileType.ToString().ToLower());

			var filePath = Path.Combine(screenshotsRoot, sessionId.ToString());
			filePath = Path.Combine(filePath, fileName);

			return filePath;
		}

	}
}