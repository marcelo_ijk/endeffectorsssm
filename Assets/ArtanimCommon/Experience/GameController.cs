﻿using Artanim.Location.Data;
using Artanim.Location.Messages;
using Artanim.Location.Network;
using Artanim.Location.SharedData;
using Artanim.Monitoring.Utils;
using Artanim.Vicon;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Artanim
{

	/// <summary>
	/// Holder structure for runtime player information
	/// </summary>
	public class RuntimePlayer
	{
		public bool IsMainPlayer { get; set; }
		public Player Player { get; set; }
		public Transform AvatarOffset { get; set; }
		public AvatarController AvatarController { get; set; }
		public GameObject PlayerInstance { get; set; }
		public Transform TeamspeakListenerRoot { get; set; }

		public TeamSpeakAudioSource TSAudioSource;
	}


	/// <summary>
	/// Main controller for game client and server.
	/// Responsible to start a session, create avatars.
	/// It will first load a construct scene which is supposed to be the visual content of the experience before the player's skeleton is calibrated and the session is created.
	/// Once the session is created the construct scene is unloaded.
	/// This class also controls the initialization of the Vicon and TS3 connection. Additionally, it's transform is the root of the GlobalMocapOffset if used. This allows to offset
	/// all players depending on the experiences need.
	/// </summary>
	[RequireComponent(typeof(GlobalMocapOffset))]
	[RequireComponent(typeof(AvatarOffsetController))]
	[RequireComponent(typeof(ViconController))]
	[RequireComponent(typeof(TS3Controller))]
	public class GameController : SingletonBehaviour<GameController>
	{
		public enum ESceneTransitionSync { ExperienceDefault, DontWaitForSync, WaitForSync }

		public string ConstructScene = "Construct Scene";

		private float TIMEOUT_SCENE_LOAD_MONITOR = 30f;

		#region Events and actions

		public delegate void OnJoinedSessionHandler(Session session, Guid playerId);
		public event OnJoinedSessionHandler OnJoinedSession;

		public delegate void OnLeftSessionHandler();
		public event OnLeftSessionHandler OnLeftSession;

		public delegate void OnPlayerCalibratedHandler(RuntimePlayer player);
		public event OnPlayerCalibratedHandler OnPlayerCalibrated;

		public delegate void OnSceneLoadedInSessionHandler(string[] sceneNames, bool sceneLoadTimedOut);
		public event OnSceneLoadedInSessionHandler OnSceneLoadedInSession;

		public delegate void OnMainPlayerPropertyChangedHandler(Player player, string propertyName);
		public event OnMainPlayerPropertyChangedHandler OnMainPlayerPropertyChanged;

		public delegate void OnSessionPlayerJoinedHandler(Session session, Guid playerId);
		public event OnSessionPlayerJoinedHandler OnSessionPlayerJoined;

		public delegate void OnSessionPlayerLeftHandler(Session session, Guid playerId);
		public event OnSessionPlayerLeftHandler OnSessionPlayerLeft;

		public delegate void OnSessionStartedHandler();
		public event OnSessionStartedHandler OnSessionStarted;

		#endregion


		public Transform AvatarsRoot;

		public Text TextSessionId;
		public Text TextStatus;

		public Toggle ToggleAudio;

		public GameObject StandaloneControllerTemplate;

		private readonly List<RuntimePlayer> _RuntimePlayers = new List<RuntimePlayer>();
		public List<RuntimePlayer> RuntimePlayers
		{
			get { return _RuntimePlayers; }
		}

		/// <summary>
		/// The current player id in the location. This value is null if running in server.
		/// </summary>
		public Guid CurrentPlayerId
		{
			get
			{
				var mainPlayer = CurrentPlayer;
				return mainPlayer != null ? mainPlayer.Player.ComponentId : Guid.Empty;
			}
		}

		public RuntimePlayer CurrentPlayer
		{
			get
			{
				for (int i = 0, iMax = RuntimePlayers.Count; i < iMax; ++i)
				{
					var player = RuntimePlayers[i];
					if (player.IsMainPlayer)
					{
						return player;
					}
				}
				return null;
			}
		}

		public Session CurrentSession { get; private set; }

		private ViconController ViconController;
		private TS3Controller TS3Controller;

		private Coroutine SceneLoadingMonitor;

		#region Unity events

		void Start()
		{
			ViconController = GetComponent<ViconController>();
			TS3Controller = GetComponent<TS3Controller>();

			if (TextSessionId)
				TextSessionId.text = "Not in session";

			//Disable TeamSpeak until needed
			TS3Controller.enabled = false;

			//Handle standalone mode
			if (DevelopmentMode.CurrentMode == EDevelopmentMode.Standalone)
			{
				//Create standalone controller
				UnityUtils.InstantiatePrefab<StandaloneController>(StandaloneControllerTemplate, transform);
			}
		}

		void OnEnable()
		{
			SceneController.Instance.OnSceneLoadFailed += OnSceneLoadFailed;
			SceneController.Instance.OnSceneLoaded += OnSceneLoaded;
			SceneController.Instance.OnSceneUnLoaded += OnSceneUnLoaded;

			if (SharedDataUtils.IsInitialized)
				Initialize();
			else
				SharedDataUtils.Initialized += Initialize;
		}

		void Initialize()
		{
			SharedDataUtils.Initialized -= Initialize;
			SharedDataUtils.SessionRemoved += SharedDataUtils_SessionRemoved;

			//Attach events
			NetworkInterface.Instance.Subscribe<RecalibratePlayer>(NetworkMessage_RecalibratePlayer);
			NetworkInterface.Instance.Subscribe<LoadGameScene>(NetworkMessage_LoadGameScene);
			NetworkInterface.Instance.Subscribe<TerminateSession>(NetworkMessage_TerminateSession);
			NetworkInterface.Instance.Subscribe<ComponentJoinSession>(NetworkMessage_ComponentJoinSession);
			NetworkInterface.Instance.Subscribe<ComponentJoinedSession>(NetworkMessage_ComponentJoinedSession);
			NetworkInterface.Instance.Subscribe<ComponentLeaveSession>(NetworkMessage_ComponentLeaveSession);
			NetworkInterface.Instance.Subscribe<ComponentLeftSession>(NetworkMessage_ComponentLeftSession);

			switch (NetworkInterface.Instance.ComponentType)
			{
				case ELocationComponentType.ExperienceServer:
					NetworkInterface.Instance.Subscribe<StartSession>(NetworkMessage_StartSession);
					OnSceneLoadedInSession += GameController_OnSceneLoadedInSession;
					break;

				case ELocationComponentType.ExperienceClient:
					NetworkInterface.Instance.Subscribe<StartSession>(NetworkMessage_StartSession);
					break;

				case ELocationComponentType.ExperienceObserver:
					break;

				default:
					break;
			}

			//Enable / disable audio
			InitAudio();

			PrepareForNewSession();
		}
		
		void OnDisable()
		{
			SharedDataUtils.Initialized -= Initialize;
			SharedDataUtils.SessionRemoved += SharedDataUtils_SessionRemoved;


			//Detach events
			NetworkInterface.Instance.Unsubscribe<RecalibratePlayer>(NetworkMessage_RecalibratePlayer);
			NetworkInterface.Instance.Unsubscribe<LoadGameScene>(NetworkMessage_LoadGameScene);
			NetworkInterface.Instance.Unsubscribe<ComponentJoinSession>(NetworkMessage_ComponentJoinSession);
			NetworkInterface.Instance.Unsubscribe<ComponentJoinedSession>(NetworkMessage_ComponentJoinedSession);
			NetworkInterface.Instance.Unsubscribe<ComponentLeaveSession>(NetworkMessage_ComponentLeaveSession);
			NetworkInterface.Instance.Unsubscribe<ComponentLeftSession>(NetworkMessage_ComponentLeftSession);
			NetworkInterface.Instance.Unsubscribe<TerminateSession>(NetworkMessage_TerminateSession);

			if (SceneController.Instance != null)
			{
				SceneController.Instance.OnSceneLoadFailed -= OnSceneLoadFailed;
				SceneController.Instance.OnSceneLoaded -= OnSceneLoaded;
				SceneController.Instance.OnSceneUnLoaded -= OnSceneUnLoaded;
			}

			//Disconnect TeamSpeak
			if (NetworkInterface.Instance.IsClient && TS3Controller.isActiveAndEnabled)
			{
				TS3Controller.DisconnectTeamspeak(true);
			}
		}

		void Update()
		{
#if EXP_PROFILING
			ExpProfiling.MarkGmCtrlStart();
#endif

			var ikUpdate = RuntimeLocationComponent.IkUpdate;
			if ((ikUpdate != null) && (ikUpdate.Skeletons != null))
			{
				for (int i = 0, iMax = ikUpdate.Skeletons.Length; i < iMax; ++i)
				{
					var skeletonUpdate = ikUpdate.Skeletons[i];
					for (int j = 0, jMax = RuntimePlayers.Count; j < jMax; ++j)
					{
						if (RuntimePlayers[j].AvatarController.SkeletonId == skeletonUpdate.SkeletonId)
						{
							RuntimePlayers[j].AvatarController.UpdateSkeleton(skeletonUpdate, ikUpdate.FrameNumber);
							break;
						}
					}
				}
			}

#if EXP_PROFILING
			ExpProfiling.MarkGmCtrlEnd();
#endif
		}

		void OnApplicationQuit()
		{
			if(CurrentSession != null)
			{
				if (NetworkInterface.Instance.IsServer)
				{
					//End session
					EndSession();
				}
				else if(NetworkInterface.Instance.IsClient)
				{
					//Leave session
					NetworkInterface.Instance.SendMessage(new ComponentLeftSession
					{
						SessionId = CurrentSession.SharedId,
					});
				}
			}
			
		}

		private void OnSceneLoaded(string sceneName, Scene scene, bool isMainChildScene)
		{
			UpdateComponentExperienceScenes();
		}

		private void OnSceneUnLoaded(string sceneName, Scene scene, bool isMainChildScene)
		{
			UpdateComponentExperienceScenes();
		}

		private void GameController_OnSceneLoadedInSession(string[] sceneNames, bool sceneLoadTimedOut)
		{
			Debug.LogFormat("<color=lightblue>Scenes loaded in session: {0}, timed out: {1}</color>", string.Join(", ", sceneNames), sceneLoadTimedOut);
		}

		#endregion

		#region Public interface

		/// <summary>
		/// Let the client "join" a session to observe it. This method only applies as Observer component.
		/// </summary>
		/// <param name="session">Session to observe</param>
		public void ObserveSession(Session session, ExperienceClient client = null)
		{
			if(NetworkInterface.Instance.ComponentType == ELocationComponentType.ExperienceObserver && session != null)
			{
				//Session switch?
				if (CurrentSession != session)
				{
					Debug.LogFormat("<color=lightblue>Observing session: session={0}, client={1}</color>", session.SharedId.Description, client != null ? client.SharedId.Description : "Observer");

					//Join the session
					JoinSession(session.SharedId, client != null ? client.SharedId : Guid.Empty);
				}
				else
				{
					Debug.LogFormat("<color=lightblue>Switching observed player: session={0}, client={1}</color>", session.SharedId.Description, client != null ? client.SharedId.Description : "Observer");
					JoinSession(session.SharedId, client != null ? client.SharedId : Guid.Empty);
				}
			}
		}


		/// <summary>
		/// Terminates the current session.
		///	All assigned components will leave the session and be available for new sessions.
		/// This method only has an effect when running as experience server.
		/// </summary>
		public void EndSession()
		{
			if (NetworkInterface.Instance.IsServer && CurrentSession != null)
			{
				Debug.LogFormat("<color=lightblue>Terminating session...</color>");

				//End session
				NetworkInterface.Instance.SendMessage(new TerminateSession());
			}
		}

		/// <summary>
		/// Request to switch to the next scene in the experience config.
		/// If the current scene is the last on the list, the first scene is loaded.
		/// This method only has an effect when running as experience server and when a ExperienceSettings is available.
		/// </summary>
		public void LoadNextScene()
		{
			if(NetworkInterface.Instance.IsServer && CurrentSession != null)
			{
				var config = ConfigService.Instance.ExperienceConfig;
				if (config != null)
				{
					var currentScene = config.StartScenes.FirstOrDefault(s => s.SceneName == CurrentSession.CurrentScene);
					if(currentScene != null)
					{
						var newSceneIndex = config.StartScenes.IndexOf(currentScene) + 1;
						if (newSceneIndex >= config.StartScenes.Count)
							newSceneIndex = 0;

						currentScene = config.StartScenes[newSceneIndex];
						LoadGameScene(currentScene.SceneName, Transition.None, true);
					}
					else
					{
						Debug.LogWarning("<color=lightblue>Failed to load next scene. Current scene not found in experience config</color>");
					}
				}
				else
				{
					Debug.LogWarning("<color=lightblue>Failed to load next scene. No experience config found</color>");
				}
			}
			else
			{
				Debug.LogWarningFormat("<color=lightblue>Fail: {0}, {1}</color>", NetworkInterface.Instance.IsServer, CurrentSession);
			}
		}

		/// <summary>
		/// Request to switch to the previous scene in the experience config.
		/// If the current scene is the first on the list, the last scene is loaded.
		/// This method only has an effect when running as experience server and when a ExperienceSettings is available.
		/// </summary>
		public void LoadPrevScene()
		{
			if (NetworkInterface.Instance.IsServer && CurrentSession != null)
			{
				var config = ConfigService.Instance.ExperienceConfig;
				if (config != null)
				{
					var currentScene = config.StartScenes.FirstOrDefault(s => s.SceneName == CurrentSession.CurrentScene);
					if (currentScene != null)
					{
						var newSceneIndex = config.StartScenes.IndexOf(currentScene) - 1;
						if (newSceneIndex < 0)
							newSceneIndex = config.StartScenes.Count - 1;

						currentScene = config.StartScenes[newSceneIndex];
						LoadGameScene(currentScene.SceneName, Transition.None, true);
					}
					else
					{
						Debug.LogWarning("<color=lightblue>Failed to load previous scene. Current scene not found in experience config</color>");
					}
				}
				else
				{
					Debug.LogWarning("<color=lightblue>Failed to load previous scene. No experience config found</color>");
				}
			}
		}

		/// <summary>
		/// Loads an experience scene with a specified transition on all session components.
		/// The parameter unloadOthers specifies if all other child scenes should be unloaded or if they should stay loaded.
		/// The optional parameter forceReload specifies if the scene must be reloaded even if it’s already loaded.
		/// This method only has an effect when running as experience server.
		/// </summary>
		/// <param name="sceneName">Name of the scene to load</param>
		/// <param name="transition">Transition used to load the scene</param>
		/// <param name="unloadOthers">Indicates if other scenes should be unloaded or not</param>
		/// <param name="forceReload">Forces the load of the scene even if it is already loaded</param>
		/// <param name="awaitSceneSync">Indicates if components have to wait faded out until all have loaded the scene</param>
		public void LoadGameScene(string sceneName, Transition transition, bool unloadOthers, bool forceReload = false, ESceneTransitionSync sceneTransitionSync = ESceneTransitionSync.ExperienceDefault)
		{
			if (NetworkInterface.Instance.IsServer)
			{
				Debug.LogFormat("<color=lightblue>Loading game scene: {0}</color>", sceneName);

				//Scene sync type
				var awaitSceneSync = false;
				if(sceneTransitionSync != ESceneTransitionSync.ExperienceDefault)
					awaitSceneSync = sceneTransitionSync == ESceneTransitionSync.WaitForSync;
				else
					awaitSceneSync = ConfigService.Instance.ExperienceSettings.DefaultSceneLoadingType == ExperienceSettings.ESceneLoadingType.WaitForSync;


				NetworkInterface.Instance.SendMessage(new LoadGameScene
				{
					SceneName = sceneName,
					Transition = transition,
					UnloadOthers = unloadOthers,
					ForceReload = forceReload,
					AwaitSceneSync = awaitSceneSync,
				});

				//Update session current scene info
				CurrentSession.CurrentScene = sceneName;
			}
		}

		/// <summary>
		///
		/// </summary>
		public void DoUpdateAudio()
		{
			if (ToggleAudio)
				AudioListener.volume = ToggleAudio.isOn ? 1f : 0f;
		}

		/// <summary>
		///
		/// </summary>
		public void LeaveSession()
		{
			if (NetworkInterface.Instance.IsServer)
			{
				//Terminate session
				EndSession();
			}
			else
			{
				//Just leave session
				NetworkMessage_TerminateSession(null);
			}
		}

		/// <summary>
		///
		/// </summary>
		/// <param name="playerId"></param>
		/// <returns></returns>
		public RuntimePlayer GetPlayerByPlayerId(Guid playerId)
		{
			if (playerId != Guid.Empty)
			{
				return RuntimePlayers.FirstOrDefault(p => p.Player.ComponentId == playerId);
			}
			return null;
		}

		/// <summary>
		/// Requests the IK server to calibrate the current player
		/// </summary>
		public void StartAvatarCalibration()
		{
			if (NetworkInterface.Instance.ComponentType == ELocationComponentType.ExperienceClient)
			{
				//Send calibration message
				NetworkInterface.Instance.SendMessage(new RecalibratePlayer
				{
					ExperienceClientId = SharedDataController.Instance.ComponentSharedData.SharedId,
				});
			}
		}

		/// <summary>
		/// Retrieves the short name of the skeleton as listed in the config file
		/// </summary>
		/// <param name="playerId">The player id for which to retrieve the skeleton short name. By default the current player is used</param>
		/// <returns>Skeleton short name</returns>
		public string GetPlayerSkeletonShortName(Guid playerId = default(Guid))
		{
			if (playerId == Guid.Empty)
				playerId = CurrentPlayer.Player.ComponentId;

			var player = GetPlayerByPlayerId(playerId);
			if ((player != null) && (player.Player != null))
			{
				var skeleton = SharedDataUtils.FindChildSharedData<SkeletonConfig>(player.Player.SkeletonId);
				if (skeleton != null)
					return SkeletonConstants.GetSkeletonShortname(skeleton.Name);
			}
			return null;
		}

		/// <summary>
		/// Retrieves the mapped backback color as Unity color.
		/// </summary>
		/// <param name="playerId">If Guid.Empty is passed, the current player is used</param>
		/// <param name="defaultColor">Color returned if no mapping was found.</param>
		/// <returns>Unity color</returns>
		public Color GetPlayerColor(Guid playerId, Color defaultColor)
		{
			var skeletonName = GetPlayerSkeletonShortName(playerId);
			if (!string.IsNullOrEmpty(skeletonName))
			{
				var mapping = ConfigService.Instance.Config.Location.Hostess.HostBackpackMappings.FirstOrDefault(m => m.SkeletonName == skeletonName);
				if ((mapping != null) && (!string.IsNullOrEmpty(mapping.Color)))
				{
					return UnityUtils.ARGBStringToUnityColor(mapping.Color);
				}
			}
			return defaultColor;
		}

		#endregion

		#region Network events

		private void NetworkMessage_LoadGameScene(LoadGameScene args)
		{
			InternalLoadGameScene(args.SceneName, args.Transition, unloadOtherChilds: args.UnloadOthers, forceReload: args.ForceReload, awaitSceneSync: args.AwaitSceneSync);
		}

		private void NetworkMessage_StartSession(StartSession args)
		{
			//Validate to be sure...
			if (CurrentSession == null)
			{
				Debug.LogErrorFormat("<color=lightblue>Trying to start session {0}. But current session was null</color>", args.SessionId);
				return;
			}

			if (CurrentSession.SharedId != args.SessionId)
			{
				Debug.LogErrorFormat("<color=lightblue>Trying to start session {0}. But current session is not the same</color>", args.SessionId);
				return;
			}

			//In case of restart we're getting the start scene override in the request. Override the startscene in the current session
			if (!string.IsNullOrEmpty(args.StartSceneOverride))
			{
				CurrentSession.StartScene = args.StartSceneOverride;
			}

			StartCurrentSession();
		}

		private void NetworkMessage_ComponentJoinSession(ComponentJoinSession args)
		{
			Guid componentId = NetworkInterface.Instance.NetworkGuid;
			if (args.ComponentId == componentId)
			{
				//Check our status first
				var status = SharedDataUtils.GetMyComponent<LocationComponentWithSession>().Status;
				if (status == ELocationComponentStatus.ReadyForSession)
				{
					//We need to join...
					JoinSession(args.SessionId, componentId);
				}
				else
				{
					Debug.LogErrorFormat("Component {0} unable to join session={1}, because it's status is {2} and it's assigned to session={3}", componentId, args.SessionId, status, SharedDataUtils.GetMyComponent<LocationComponentWithSession>().SessionId);
				}
			}
		}

		private void NetworkMessage_ComponentJoinedSession(ComponentJoinedSession args)
		{
			//Someone else of our session joined?
			if ((CurrentSession != null) && (args.SessionId == CurrentSession.SharedId) && args.SenderId != NetworkInterface.Instance.NetworkGuid)
			{
				//Yes... init him to our session
				var expClient = SharedDataUtils.FindLocationComponent<ExperienceClient>(args.SenderId);
				if (expClient != null)
				{
					var player = CurrentSession.Players.FirstOrDefault(p => p.ComponentId == args.SenderId);
					if (player != null)
					{
						//Add player
						CreatePlayer(player, false);

						//Notify joined
						if (OnSessionPlayerJoined != null)
							OnSessionPlayerJoined(CurrentSession, player.ComponentId);
					}
					else
					{
						Debug.LogErrorFormat("<color=lightblue>Failed to add component to session. Unable to find component in location status. ComponentId={0}</color>", args.SenderId);
					}
				}
			}
		}

		private void NetworkMessage_ComponentLeaveSession(ComponentLeaveSession args)
		{
			//Is it me?
			if (args.ComponentId == NetworkInterface.Instance.NetworkGuid)
			{
				//Yes
				var session = CurrentSession;
				
				PrepareForNewSession();

				//Update status if server
				if (session != null && SharedDataController.Instance.OwnsSharedData(session))
				{
					//Update session status
					session.Status = ESessionStatus.Ended;

					// And destroy it
					SharedDataController.Instance.RemoveSharedData(session);
				}
			}
		}

		private void NetworkMessage_ComponentLeftSession(ComponentLeftSession args)
		{
			//Remove component if he left our session and its not me
			if(args.SenderId != NetworkInterface.Instance.NetworkGuid && CurrentSession != null && args.SessionId == CurrentSession.SharedId)
			{
				var clientToLeave = SharedDataUtils.FindSharedData<ExperienceClient>(args.SenderId);
				if (clientToLeave != null)
				{
					var playerToLeave = RuntimePlayers.FirstOrDefault(p => p.Player.ComponentId == args.SenderId);

					RemovePlayer(playerToLeave);

					//Notify leave
					if (OnSessionPlayerLeft != null)
						OnSessionPlayerLeft(CurrentSession, playerToLeave.Player.ComponentId);
				}
			}
		}

		private void NetworkMessage_RecalibratePlayer(RecalibratePlayer args)
		{
			//Only update player if it's part of the session
			if (IsClientFromMySession(args.ExperienceClientId))
			{
				var runtimePlayer = RuntimePlayers.FirstOrDefault(p => p.Player.ComponentId == args.ExperienceClientId);
				if (runtimePlayer != null)
				{
					if (runtimePlayer.Player.SkeletonId != Guid.Empty)
					{
						//Calibrate camera
						if (runtimePlayer.IsMainPlayer)
						{
							// Instant recalibration should work ok as player is supposed to not move during skeleton calibration
							MainCameraController.Instance.CalibrateNow();
							// Also run a "stable" calibration just to be sure
							MainCameraController.Instance.Calibrate();
						}

						//Start avatar calibration if needed (Only client, exclude observer): Clients send RigSetup to IK
						if (NetworkInterface.Instance.ComponentType == ELocationComponentType.ExperienceClient)
							runtimePlayer.AvatarController.StartCalibration(NetworkInterface.Instance.IsClient && runtimePlayer.IsMainPlayer, runtimePlayer.Player);

						//Show player avatar on next avatar update
						runtimePlayer.AvatarController.ShowAvatar(true);

						//Notify
						if (OnPlayerCalibrated != null)
							OnPlayerCalibrated(runtimePlayer);
					}
					else
					{
						Debug.LogErrorFormat("<color=lightblue>Failed to calibrate player. The given player does not have a skeleton assigned. ComponentId={0}</color>", args.ExperienceClientId);
					}
				}
				else
				{
					Debug.LogErrorFormat("<color=lightblue>Failed to calibrate player. Could not find a player with the given componentId={0}</color>", args.ExperienceClientId);
				}
			}
		}

		private bool IsClientFromMySession(Guid clientId)
		{
			if (CurrentSession != null && clientId != Guid.Empty)
			{
				return CurrentSession.Players.Any(p => p.ComponentId == clientId);
			}

			return false;
		}

		private void NetworkMessage_TerminateSession(TerminateSession args)
		{
			Debug.Log("<color=lightblue>Leaving session and going back to construct scene</color>");

			var session = CurrentSession;

			//Back to construct
			PrepareForNewSession();

			//Update status if server
			if (session != null && SharedDataController.Instance.OwnsSharedData(session))
			{
				//Update session status
				session.Status = ESessionStatus.Ended;

				// And destroy it
				SharedDataController.Instance.RemoveSharedData(session);
			}
		}

		private void SharedDataUtils_SessionRemoved(Session session)
		{
			if(CurrentSession != null && CurrentSession.SharedId == session.SharedId)
			{
				//Our session just got removed :-( Prepare for new session
				Debug.LogWarning("My session got removed. Preparing for new session.");
				PrepareForNewSession();
			}
		}

		#endregion

		#region Internals

		private void UpdateComponentExperienceScenes()
		{
			//Get all currently loaded experience scenes
			var currentExperienceScenes = string.Join(";", SceneController.Instance.GetLoadedExperienceScenes().OrderBy(s => s.name).Select(s => s.name).ToArray());
			Debug.LogFormat("Currently loaded experience scenes: {0}", currentExperienceScenes);

			//Store loaded experience scenes in shared data for the loading monitor
			var thisComponent = SharedDataUtils.GetMyComponent<LocationComponentWithSession>();
			if (thisComponent != null)
			{
				thisComponent.LoadedExperienceSceneNames = currentExperienceScenes;
			}

			//Start monitoring?
			if (NetworkInterface.Instance.IsServer)
			{
				//When parallel loads happen, stop the current monitor
				if (SceneLoadingMonitor != null)
					StopCoroutine(SceneLoadingMonitor);

				//Start the scene monitor
				SceneLoadingMonitor = StartCoroutine(MonitorSceneLoading(currentExperienceScenes));
			}

		}

		private IEnumerator MonitorSceneLoading(string scenesToMonitor)
		{

			if (!string.IsNullOrEmpty(scenesToMonitor) && CurrentSession != null)
			{
				Debug.LogFormat("Started scene monitoring for scenes: {0}", scenesToMonitor);

				bool allLoaded = false;
				bool isTimeout = false;

				float timeOutTime = 0f;

				IEnumerable<LocationComponentWithSession> stillLoading = null;
				while (!allLoaded && !isTimeout)
				{
					yield return new WaitForSecondsRealtime(0.2f);

					var allSessionComponents = SharedDataUtils.Components.OfType<LocationComponentWithSession>().Where(c => c.SessionId == CurrentSession.SharedId);
					stillLoading = allSessionComponents.Where(c => c.LoadedExperienceSceneNames != scenesToMonitor);

					//Debug.LogErrorFormat("Loading monitor: #all={0}, #loading={1}, all={2}, stillLoading={3}",
					//	allSessionComponents.Count(),
					//	stillLoading.Count(),
					//	string.Join(", ", allSessionComponents.Select(c => c.SharedId.ToString()).ToArray()),
					//	string.Join(", ", stillLoading.Select(c => c.SharedId.ToString()).ToArray()));

					if(stillLoading.Count() != 0)
					{
						//Not all loaded... wait

						//Handle timeout
						if(timeOutTime == 0f && stillLoading.Count() < allSessionComponents.Count())
						{
							//At least one loaded... start timeout
							timeOutTime = Time.realtimeSinceStartup + TIMEOUT_SCENE_LOAD_MONITOR;
						}
						else if(timeOutTime > 0f)
						{
							//Check timeout
							isTimeout = Time.realtimeSinceStartup > timeOutTime;
						}
					}
					else
					{
						//All done
						allLoaded = true;
					}
				}

				if(isTimeout)
				{
					Debug.LogErrorFormat("Scene loading terminated with timeout: timeout={0}sec, scenes={1}, still loading={2}",
						TIMEOUT_SCENE_LOAD_MONITOR,
						scenesToMonitor,
						stillLoading != null ? string.Join(", ", stillLoading.Select(c => c.SharedId.ToString()).ToArray()) : "null");
				}
				else
				{
					Debug.LogFormat("Scene loading done. All components loaded in time. scenes={0}", scenesToMonitor);
				}


				var loadedScenes = scenesToMonitor.Split(';');
				//Send sync event if needed
				NetworkInterface.Instance.SendMessage(new SceneLoadedInSession()
				{
					Scenes = loadedScenes,
					SceneLoadTimeout = isTimeout,
				});

				//Notify local listeners
				if (OnSceneLoadedInSession != null)
				{
					//Notify scene load
					OnSceneLoadedInSession(loadedScenes, isTimeout);
				}
			}
		}


		private void StartCurrentSession()
		{
			//Update status
			SetComponentStatus(ELocationComponentStatus.InSession);

			//Server side requests to load game scene
			if (NetworkInterface.Instance.IsServer)
			{
				//Don't reload start scene if it's the same as the construct scene (scene we're currently on)
				LoadGameScene(CurrentSession.StartScene, Transition.FadeBlack, unloadOthers: true, forceReload: CurrentSession.StartScene != SceneController.Instance.MainChildSceneName);

				//Update session status
				CurrentSession.Status = ESessionStatus.Started;
				CurrentSession.StartedTime = DateTime.UtcNow;
			}

			//Notify
			if (OnSessionStarted != null)
				OnSessionStarted();
		}

		private void JoinSession(Guid sessionId, Guid componentId)
		{
			var session = SharedDataUtils.FindChildSharedData<Session>(sessionId);
			if (session == null)
			{
				Debug.LogErrorFormat("Component {0} could not join session={1} because it didn't find it", componentId, session.SharedId);
			}
			//Are we part of that session?
			else if (((session.ExperienceServerId == NetworkInterface.Instance.NetworkGuid) ||
				 session.Players.Any(p => p.ComponentId == NetworkInterface.Instance.NetworkGuid) ||
				 NetworkInterface.Instance.ComponentType == ELocationComponentType.ExperienceObserver))
			{
				//Yep... let's go!
				Debug.LogFormat("<color=lightblue>Joining session={0}</color>", session.SharedId);

				//Init component session
				SetComponentSession(session);

				//Display sessionId
				if (TextSessionId)
					TextSessionId.text = string.Format("Session ID: {0}", session.SharedId);

				//Create session players / avatars
				//First remove all leftovers
				RemoveAllPlayers();

				//Create player and avatars
				foreach (var player in CurrentSession.Players)
				{
					var isMainPlayer = player.ComponentId == NetworkInterface.Instance.NetworkGuid || player.ComponentId == componentId;
					CreatePlayer(player, isMainPlayer);
				}

				//Connect streaming topics
				NetworkInterface.Instance.ConnectStreamingTopics();
				Debug.Log("<color=lightblue>Connecting to streaming topics</color>");

				//Game client specifics
				var currentPlayer = CurrentPlayer;
				if (
					currentPlayer != null &&
					(NetworkInterface.Instance.ComponentType == ELocationComponentType.ExperienceClient || NetworkInterface.Instance.ComponentType == ELocationComponentType.ExperienceObserver)
					) //Here we need to check for real component type... in editor we're client and server
				{
					var thisPlayer = currentPlayer.Player;

					//Init camera
					MainCameraController.Instance.InitPlayerCamera(thisPlayer);

					if (thisPlayer != null)
					{
						//TeamSpeak?
						if (thisPlayer.TSEnabled && NetworkInterface.Instance.ComponentType == ELocationComponentType.ExperienceClient)
						{
							TS3Controller.enabled = true;
							TS3Controller.ConnectTeamspeak(CurrentSession.TSChannel, thisPlayer.ComponentId.ToString());
						}
					}
					else
					{
						//Very very bad one!
						Debug.LogErrorFormat("<color=lightblue>Failed to find the playerId={0} in session={1} but the client component was in the session components list!!!</color>", currentPlayer.Player.ComponentId, session.SharedId);
					}
				}
				else
				{
					//Set observer camera
					MainCameraController.Instance.ResetPlayerCamera();
				}

				//Load game start scene. If we're joining a started session, load the current scene from the game session values otherwise load the construct scene.
				string sceneName = (CurrentSession.Status != ESessionStatus.Started ? GetConstructSceneName() : session.CurrentScene);
				InternalLoadGameScene(sceneName, Transition.FadeBlack, unloadOtherChilds: true, forceReload: false, awaitSceneSync: false);

				//Connect to Vicon
				ViconController.StartViconConnection();

				//Update component status
				SetComponentStatus(ELocationComponentStatus.PreparingSession);

				//Signal other components
				if(NetworkInterface.Instance.ComponentType != ELocationComponentType.ExperienceObserver)
				{
					NetworkInterface.Instance.SendMessage(new ComponentJoinedSession
					{
						SessionId = session.SharedId,
					});
				}

				try
				{
					//Signal session joined
					if (OnJoinedSession != null)
					{
						OnJoinedSession(session, componentId);
					}
				}
				catch (Exception e)
				{
					Debug.LogException(e);
				}
			}
			else
			{
				Debug.LogErrorFormat("Component {0} could not join session={1} because it's not part of it", componentId, session.SharedId);
			}
		}

		private void InitAudio()
		{
			if (NetworkInterface.Instance.ComponentType == ELocationComponentType.ExperienceServer)
			{
				AudioListener.volume = ConfigService.Instance.Config.Location.Server.AudioEnabled ? 1f : 0f;

				//Update toggle
				if (ToggleAudio)
					ToggleAudio.isOn = ConfigService.Instance.Config.Location.Server.AudioEnabled;
			}
		}

		private void OnSceneLoadFailed(string sceneName)
		{
			if (CurrentSession != null && sceneName == CurrentSession.StartScene)
			{
				Debug.LogErrorFormat("<color=lightblue>Failed to load game start scene {0}... Going back to construct...</color>", sceneName);

				//Check if we need to kill the session
				if(NetworkInterface.Instance.IsServer && CurrentSession != null)
				{
					Debug.LogErrorFormat("<color=lightblue>Server terminating session because the requested scene could not loaded: {0}</color>", sceneName);
					EndSession();
				}
				else
				{
					//Clients and observer
					PrepareForNewSession();
				}
			}
		}

		private void CreatePlayer(Player player, bool mainPlayer)
		{
			if (player == null)
			{
				Debug.LogError("<color=lightblue>Failed to create player. The given player is null</color>");
				return;
			}

			//Try to create avatar
			GameObject avatarInstance = null;
			if (AvatarsRoot)
			{
				if (player.Avatar != null)
				{
					//Remove player first if he's already around
					RemovePlayer(player);

					//Attach player and client state updates
					player.PropertyChanged += Client_or_Player_PropertyChanged;
					var client = SharedDataUtils.FindLocationComponent<ExperienceClient>(player.ComponentId);
					if (client != null)
						client.PropertyChanged += Client_or_Player_PropertyChanged;
					else
						Debug.LogWarningFormat("Failed to find experience client component with id: {0}", player.ComponentId);
					
					//Create
					var avatarTemplate = Resources.Load<GameObject>(player.Avatar.AvatarResource);
					if (avatarTemplate)
					{
						avatarInstance = Instantiate(avatarTemplate);
						if (avatarInstance)
						{
							//Setup actor
							var avatarController = avatarInstance.GetComponent<AvatarController>();
							if (avatarController)
							{
								//Create avatar offset root
								var avatarOffset = new GameObject(string.Format("AvatarOffset-{0}", player.ComponentId.ToString()));
								avatarOffset.transform.SetParent(AvatarsRoot);
								avatarOffset.transform.localPosition = Vector3.zero;
								avatarOffset.transform.localRotation = Quaternion.identity;


								//Init actor
								avatarController.InitAvatar(
									player,
									mainPlayer,
									player.Status == EPlayerStatus.Calibrated); //Show avatar if player is already calibrated. Can happen for e.g. when the hostess switches the avatar after calibration

								//Parent avatar visuals
								//avatarInstance.transform.SetParent(AvatarsRoot);
								avatarInstance.transform.SetParent(avatarOffset.transform);
								avatarInstance.transform.localPosition = Vector3.zero;
								avatarInstance.transform.localRotation = Quaternion.identity;

								//Find head for main player: if VR is enabled use VR headset transform, otherwise use skeleton head
								Transform teamspeakListenerRoot = null;

								//Main player specific
								if (mainPlayer)
								{
									//Camera
									MainCameraController.Instance.InitPlayerCamera(player);

									//Teamspeak
									teamspeakListenerRoot = avatarController.HeadBone;

									//Keep track of rigidbodies for metrics
									var skeleton = SharedDataUtils.FindChildSharedData<SkeletonConfig>(player.SkeletonId);
									if (skeleton != null)
									{
										SetupRigidbodyMonitoring(skeleton);
									}
									else
									{
										Debug.LogWarning("Skeleton not found for player");
									}

									//Language
									TextService.Instance.SetLanguage(player.Language);

									//Subtitle
									SubtitleController.Instance.ShowSubtitles = player.ShowSubtitles;
								}

								//Make sure it's active
								avatarInstance.gameObject.SetActive(true);

								//Register player
								var runtimePlayer = new RuntimePlayer()
								{
									IsMainPlayer = mainPlayer,
									Player = player,
									AvatarOffset = avatarOffset.transform,
									AvatarController = avatarController,
									PlayerInstance = avatarInstance,
									TeamspeakListenerRoot = teamspeakListenerRoot,
								};
								RuntimePlayers.Add(runtimePlayer);

								//Init TS player if he's already connected to TS
								if(NetworkInterface.Instance.ComponentType == ELocationComponentType.ExperienceClient && client != null && client.TSClientId > 0)
								{
									TS3Controller.InitPlayer(runtimePlayer);
								}

								Debug.LogFormat("<color=lightblue>Created player: avatarResource={0}, IsMainPlayer={1}, PlayerId={2}, SkeletonId={3}</color>", player.Avatar.AvatarResource, mainPlayer, player.ComponentId, player.SkeletonId);
								return;
							}
							else
							{
								Debug.LogErrorFormat("<color=lightblue>Failed to create player. Could not find avatar controller={0}</color>", player.Avatar.AvatarResource);
							}
						}
						else
						{
							Debug.LogErrorFormat("<color=lightblue>Failed to create player. Could not instantiate avatar resource={0}</color>", player.Avatar.AvatarResource);
						}
					}
					else
					{
						Debug.LogErrorFormat("<color=lightblue>Failed to create player. Could not find avatar resource={0}</color>", player.Avatar.AvatarResource);
					}
				}
				else
				{
					Debug.LogErrorFormat("<color=lightblue>Failed to create player. Player avatar is null. Player={0}</color>", player.ComponentId);
				}
			}
			else
			{
				Debug.LogErrorFormat("<color=lightblue>Failed to create player. No Avatar root defined</color>");
			}

			//Something went wrong... remove avatar instance if it was already created
			if(avatarInstance)
			{
				Destroy(avatarInstance);
			}

		}

		private void RemoveAllPlayers()
		{
			//Cleanly remove all known registered players
			foreach(var player in RuntimePlayers.ToArray())
			{
				RemovePlayer(player);
			}

			//Make sure all avatars are really cleared for the next session, remove all avatars in case they haven't been removed earlier.
			if (AvatarsRoot)
			{
				for (var c = 0; c < AvatarsRoot.childCount; c++)
					Destroy(AvatarsRoot.GetChild(c).gameObject);

				RuntimePlayers.Clear();
			}
		}

		private void RemovePlayer(Player player)
		{
			var removePlayer = RuntimePlayers.FirstOrDefault(p => p.Player.ComponentId == player.ComponentId);
			if (removePlayer != null)
			{
				RemovePlayer(removePlayer);
			}
		}

		private void RemovePlayer(RuntimePlayer player)
		{
			if (player != null && player.PlayerInstance != null)
			{
				//Destroy avatar
				Destroy(player.AvatarOffset.gameObject);

				//Remove from list
				RuntimePlayers.Remove(player);

				//Detach property change events
				player.Player.PropertyChanged -= Client_or_Player_PropertyChanged;
				var client = SharedDataUtils.FindLocationComponent<ExperienceClient>(player.Player.ComponentId);
				if (client != null)
					client.PropertyChanged -= Client_or_Player_PropertyChanged;

				if (player.IsMainPlayer)
				{
					ClearRigidbodyMonitoring();
				}

				//Cleanup shared data if we're owner
				if (CurrentSession != null && SharedDataController.Instance.OwnsSharedData(CurrentSession))
				{
					CurrentSession.Players.Remove(player.Player);

					//We're owner of the session... check if there's still a player around...
					if(CurrentSession.Players.Count ==  0)
					{
						//Ok, we're done... terminate the session
						EndSession();
					}
				}
			}
		}

		private void Client_or_Player_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			//Player properties
			var player = sender as Player;
			if (player != null)
			{
				if (e.PropertyName == "Avatar")
				{
					Debug.LogFormat("<color=lightblue>Avatar changed on player = {0} to {1}</color>", player.ComponentId, player.Avatar.Name);
					CreatePlayer(player, CurrentPlayerId == player.ComponentId);
				}

				if(e.PropertyName == "SkeletonId")
				{
					Debug.LogFormat("<color=lightblue>Skeleton changed on player = {0} to {1}</color>", player.ComponentId, player.Avatar.Name);
					CreatePlayer(player, CurrentPlayerId == player.ComponentId);
				}

				//Notify experience listeners
				if (CurrentPlayerId == player.ComponentId && OnMainPlayerPropertyChanged != null)
				{
					OnMainPlayerPropertyChanged(player, e.PropertyName);
				}
			}

			//Client properties
			var client = sender as ExperienceClient;
			if(client != null)
			{
				if (NetworkInterface.Instance.ComponentType == ELocationComponentType.ExperienceClient && e.PropertyName == "TSClientId")
				{
					Debug.LogFormat("<color=lightblue>Teamspeak clientId changed on player = {0} to {1}</color>", client.SharedId, client.TSClientId);
					TS3Controller.InitPlayer(GetPlayerByPlayerId(client.SharedId));
				}
			}
		}

		private void PrepareForNewSession()
		{
			Debug.Log("<color=lightblue>Preparing for new session</color>");

			var oldSessionId = CurrentSession != null ? CurrentSession.SharedId : Guid.Empty;

			//Leave session
			SetComponentSession(null);

			//Remove avatars
			RemoveAllPlayers();

			//Setup camera
			MainCameraController.Instance.ResetPlayerCamera();

			//Load construct scene
			InternalLoadGameScene(GetConstructSceneName(), Transition.FadeBlack, unloadOtherChilds: true, forceReload: true, awaitSceneSync: false);

			//Disconnect streaming topics
			NetworkInterface.Instance.DisconnectStreamingTopics();
			Debug.Log("<color=lightblue>Streaming topics disconnected</color>");

			//Client specific
			if (NetworkInterface.Instance.ComponentType == ELocationComponentType.ExperienceClient) //Here we need to check for real component type... in editor we're client and server
			{
				//Disconnect from Teamspeak
				if (TS3Controller && TS3Controller.isActiveAndEnabled)
				{
					TS3Controller.DisconnectTeamspeak(true);
				}
			}

			if (TextSessionId)
				TextSessionId.text = "Not in session";

			//Update component status
			SetComponentStatus(ELocationComponentStatus.ReadyForSession);

			//Signal other components if we really left a session
			if(oldSessionId != Guid.Empty)
			{
				NetworkInterface.Instance.SendMessage(new ComponentLeftSession
				{
					SessionId = oldSessionId,
				});
			}

			//Notify experience
			if (OnLeftSession != null)
			{
				try
				{
					OnLeftSession();
				}
				catch(Exception ex)
				{
					Debug.LogErrorFormat("Failed to notify experience OnLeftSession. Error={0}\n{1}", ex.Message, ex.StackTrace);
				}
			}
		}

		private void InternalLoadGameScene(string sceneName, Transition transition, bool unloadOtherChilds = false, bool forceReload = false, bool awaitSceneSync = false)
		{
			Debug.LogFormat("<color=lightblue>Loading new game scene: sceneName={0}, transition={1}, unloadOtherChilds={2}, forceReload={3}, awaitSceneSync={4}</color>",
				sceneName, transition, unloadOtherChilds, forceReload, awaitSceneSync);

			SceneController.Instance.LoadChildScene(sceneName, transition, unloadOtherChilds, forceReload, awaitSceneSync);
		}

		private void SetComponentStatus(ELocationComponentStatus status)
		{
			//Update component status
			SharedDataUtils.GetMyComponent<LocationComponentWithSession>().Status = status;

			if (TextStatus)
				TextStatus.text = string.Format("{0} {1}",
					DevelopmentMode.CurrentMode != EDevelopmentMode.None ? string.Format("<color=#F62121FF>Dev. mode: {0}</color>  ", DevelopmentMode.CurrentMode) : "",
					status.ToString());
		}

		private void SetComponentSession(Session session)
		{
			var sessionId = session != null ? session.SharedId.Guid : Guid.Empty;

			NetworkInterface.Instance.SessionId = sessionId;
			CurrentSession = session != null ? session : null;

			var thisComponent = SharedDataUtils.GetMyComponent<LocationComponentWithSession>();
			if (thisComponent != null)
			{
				thisComponent.SessionId = sessionId;
			}
		}

		private string GetConstructSceneName()
		{
			var constructScene = ConstructScene;

			if (ConfigService.Instance.ExperienceSettings && !string.IsNullOrEmpty(ConfigService.Instance.ExperienceSettings.ConstructSceneName))
				constructScene = ConfigService.Instance.ExperienceSettings.ConstructSceneName;

			return constructScene;
		}

		#endregion

		#region Monitoring

		private void SetupRigidbodyMonitoring(SkeletonConfig skeleton)
		{
			foreach (ESkeletonSubject e in Enum.GetValues(typeof(ESkeletonSubject)))
			{
				Monitoring.Monitoring.Instance.ProcessContext.AddKeyValue("RB_" + e, skeleton.SkeletonSubjectNames[(int)e]);
			}
		}

		private void ClearRigidbodyMonitoring()
		{
			//Keep track of rigidbodies for metrics
			foreach (ESkeletonSubject e in Enum.GetValues(typeof(ESkeletonSubject)))
			{
				Monitoring.Monitoring.Instance.ProcessContext.AddKeyValue("RB_" + e, "");
			}
		}

		#endregion
	}

}
