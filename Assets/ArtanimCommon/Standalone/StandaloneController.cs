﻿using Artanim.Location.Config;
using Artanim.Location.Data;
using Artanim.Location.Hostess;
using Artanim.Location.Network;
using Artanim.Location.SharedData;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Artanim.Vicon;

namespace Artanim
{
	/// <summary>
	/// Controller instantiated by the GameController when running in standalone dev mode.
	/// It creates a new session (using the HostessController), adds the current player to it and starts the session with the
	/// selected scene from the main menu.
	/// </summary>
	public class StandaloneController : MonoBehaviour
	{
		private const string RESOURCE_STANDALONE_AVATAR = "Avatars/PFB_Standalone";

		public Dropdown DropdownStanaloneScenes;

		//3D UI
		public GameObject Canvas3D;
		public Text TextFPS;
		public Text TextSceneName;

		private HostessController HostessController;
		private ExperienceClient ThisClient;

		void Start()
		{
			if (SharedDataUtils.IsInitialized)
				Initialize();
			else
				SharedDataUtils.Initialized += Initialize;
		}

		void Initialize()
		{
			HostessController = new HostessController();
			ThisClient = SharedDataUtils.GetMyComponent<ExperienceClient>();

			if(ThisClient != null)
			{
				StartCoroutine(InitializeStandaloneSession());
			}
			else
			{
				Debug.LogError("Unable to initialize standalone development mode. Component must be of type client");
			}


			//Init scenes dropdown
			if (DropdownStanaloneScenes)
			{
				DropdownStanaloneScenes.gameObject.SetActive(true);
				DropdownStanaloneScenes.ClearOptions();

				//Load scenes
				DropdownStanaloneScenes.AddOptions(ConfigService.Instance.ExperienceConfig.StartScenes.Select(s => s.SceneName).ToList());
			}

			if(GameController.Instance)
				GameController.Instance.OnSceneLoadedInSession += Instance_OnSceneLoadedInSession;
		}

		private void LateUpdate()
		{
			UpdateRightHandHUD();

		}

		public void DoSelectScene()
		{
			if(DropdownStanaloneScenes && GameController.Instance)
			{
				GameController.Instance.LoadGameScene(DropdownStanaloneScenes.options[DropdownStanaloneScenes.value].text, Location.Messages.Transition.FadeBlack, true);
			}
		}

		/// <summary>
		/// Scene changed... update the scene dropdown
		/// </summary>
		/// <param name="sceneNames"></param>
		/// <param name="sceneLoadTimedOut"></param>
		private void Instance_OnSceneLoadedInSession(string[] sceneNames, bool sceneLoadTimedOut)
		{
			if(DropdownStanaloneScenes)
			{
				//Update dropdown selection
				var sceneOption = DropdownStanaloneScenes.options.FirstOrDefault(o => o.text == GameController.Instance.CurrentSession.CurrentScene);
				if (sceneOption != null)
					DropdownStanaloneScenes.value = DropdownStanaloneScenes.options.IndexOf(sceneOption);
			}
		}

		/// <summary>
		/// Create a "fake" session, add the current player to it and start the session with the selected scene from the PlayerPrefs (set by the main menu)
		/// </summary>
		/// <returns></returns>
		private IEnumerator InitializeStandaloneSession()
		{
			//Create a new session
			var session = HostessController.PrepareNewSession();
			session.Experience = ConfigService.Instance.ExperienceConfig.ExperienceName;
			
			//Create fake player
			var player = HostessController.PrepareSessionPlayer(ThisClient, false);

			//Fake initialize player
			player.Status = EPlayerStatus.Calibrated;
			player.Avatar = new Location.Config.Avatar
			{
				Name = "Standalone",
				AvatarResource = RESOURCE_STANDALONE_AVATAR,
			};

			//Join the session
			HostessController.RequestPlayerJoinSession(session, player);

			//Wait for session join
			while (NetworkInterface.Instance.SessionId == Guid.Empty)
				yield return null;
			
			yield return new WaitForSeconds(2f);

			//Find the scne to start
			if(ConfigService.Instance.ExperienceConfig != null && ConfigService.Instance.ExperienceConfig.StartScenes.Count > 0)
			{
				Scene startScene = null;
				var startSceneName = PlayerPrefs.GetString(MainMenu.KEY_LAST_SELECTED_SCENE);
				if(!string.IsNullOrEmpty(startSceneName))
				{
					//Search for the start scene
					startScene = ConfigService.Instance.ExperienceConfig.StartScenes.FirstOrDefault(s => s.SceneName == startSceneName);
				}

				if(startScene == null)
				{
					Debug.LogWarningFormat("Failed to find startscene with name {0} (set by the menu) in experience config. Loading first scene.", startSceneName);
					startScene = ConfigService.Instance.ExperienceConfig.StartScenes[0];
				}

				//Start the session
				session.StartScene = startScene.SceneName;
				var result = HostessController.StartSession(session, ConfigService.Instance.ExperienceConfig);

				if (result != StartSessionStatus.SessionStarted)
				{
					Debug.LogErrorFormat("Failed to start in standalone mode. Error: {0}", result);
				}
			}
			else
			{
				Debug.LogError("Failed to start in standalone mode. No experience config found used for the start scene. Please add an experience settings asset to your project and create an experience config for this project.");
			}

			yield return null;
		}


		private int PreviewSceneIndex;
		private bool SceneNavigationClear;
		private void UpdateRightHandHUD()
		{
			if(Canvas3D && DropdownStanaloneScenes && DevelopmentMode.GetAxis(DevelopmentMode.AXIS_STANDALONE_MENU_OPEN) == 1f)
			{
				if(!Canvas3D.activeInHierarchy)
				{
					//Init
					Canvas3D.SetActive(true);

					//Scene name
					if (TextSceneName)
					{
						PreviewSceneIndex = DropdownStanaloneScenes.value;
						TextSceneName.color = Color.yellow;
						TextSceneName.text = DropdownStanaloneScenes.options[PreviewSceneIndex].text;
					}
				}

				//Position UI
				var rightHand = GameController.Instance.CurrentPlayer.AvatarController.GetAvatarBodyPart(Location.Messages.EAvatarBodyPart.RightHand);
				Canvas3D.transform.position = rightHand.transform.position;
				Canvas3D.transform.rotation = rightHand.transform.rotation;

				//Update UI
				if (TextFPS)
					TextFPS.text = string.Format("{0:0} FPS", FPSMetrics.FpsAvg);
				
				//Scene navigation
				var sceneNavAxis = DevelopmentMode.GetAxis(DevelopmentMode.AXIS_STANDALONE_MENU_SCENE_NAVIGATE);
				if (SceneNavigationClear && sceneNavAxis != 0f)
				{
					//Preselect scene
					PreviewSceneIndex += sceneNavAxis > 0 ? 1 : -1;

					//Loop
					if (PreviewSceneIndex > DropdownStanaloneScenes.options.Count - 1)
						PreviewSceneIndex = 0;
					else if (PreviewSceneIndex < 0)
						PreviewSceneIndex = DropdownStanaloneScenes.options.Count - 1;

					//Display preselected scene name
					TextSceneName.text = DropdownStanaloneScenes.options[PreviewSceneIndex].text;

					//Set color for current scene
					TextSceneName.color = PreviewSceneIndex == DropdownStanaloneScenes.value ? Color.yellow : Color.white;
					
					SceneNavigationClear = false;
				}
				else if(sceneNavAxis == 0f)
				{
					SceneNavigationClear = true;
				}

				//Scene selection
				if(Input.GetButtonUp(DevelopmentMode.BUTTON_STANDALONE_LOAD_PREVIEV_SCENE))
				{
					GameController.Instance.LoadGameScene(DropdownStanaloneScenes.options[PreviewSceneIndex].text, Location.Messages.Transition.FadeBlack, true);
				}

			}
			else
			{
				Canvas3D.SetActive(false);
			}

		}
	}
}