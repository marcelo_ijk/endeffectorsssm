﻿using Artanim;
using Artanim.Location.Messages;
using Artanim.Vicon;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

#if UNITY_2017_3_OR_NEWER
using UnityEngine.XR;
#else
using UnityEngine.VR;
using XRNode = UnityEngine.VR.VRNode;
using XRNodeState = UnityEngine.VR.VRNodeState;
using XRSettings = UnityEngine.VR.VRSettings;
using XRDevice = UnityEngine.VR.VRDevice;
#endif

namespace Artanim
{
	[RequireComponent(typeof(IKListener))]
	[RequireComponent(typeof(AvatarController))]
	public class StandaloneAvatarController : MonoBehaviour
	{
		private const float DEFAULT_PLAYER_HEIGHT = 1.4f;

		public float MaxMovementSpeed = 2f;
		public float MaxRotationSpeed = 80f;

		public Transform HandRoot;
		public Transform FeetRoot;

		public Transform AvatarRoot;

		private AvatarController AvatarController;
		
		private void Start()
		{
			AvatarController = GetComponent<AvatarController>();


			//Init VR
			if(XRSettings.enabled)
			{
				//Disable IK listener
				GetComponent<IKListener>().enabled = false;

				//Set player height (adjusted for seated experience)
				if (MainCameraController.Instance)
				{
					var camPos = MainCameraController.Instance.HeadRigidBody.transform.position;
					camPos.y = DEFAULT_PLAYER_HEIGHT;
					MainCameraController.Instance.HeadRigidBody.transform.position = camPos;
				}

				//Set hands to default height
				if (HandRoot)
					HandRoot.localPosition = new Vector3(0f, DEFAULT_PLAYER_HEIGHT, 0f);

				//Center tracking position
				InputTracking.trackingAcquired += InputTracking_trackingAcquired;
				InputTracking.Recenter();
			}
			else
			{
				Debug.LogError("No headset found. Use Oculus headset with Oculus Touch to control the avatar in standalong mode.");
			}

			//Check inputs
		}

		private void InputTracking_trackingAcquired(XRNodeState node)
		{
			if(node.nodeType == XRNode.Head && node.tracked)
			{
				InputTracking.Recenter();
			}
		}

		private bool IsCalibrated;
		private void LateUpdate()
		{
			if (XRSettings.enabled)
			{
				if (MainCameraController.Instance && MainCameraController.Instance.HeadRigidBody && AvatarRoot)
				{
					//Update head
					UpdateHead(AvatarController.GetAvatarBodyPart(EAvatarBodyPart.Head).transform);

					//Update movement
					UpdateMovement();

					//Update hands
					UpdateHand(AvatarController.GetAvatarBodyPart(EAvatarBodyPart.LeftHand).transform, XRNode.LeftHand);
					UpdateHand(AvatarController.GetAvatarBodyPart(EAvatarBodyPart.RightHand).transform, XRNode.RightHand);

					//Update feet position
					UpdateFeet();

					//Update calibration
					if (DevelopmentMode.GetButtonUp(DevelopmentMode.BUTTON_STANDALONE_RECALIBRATE) || !IsCalibrated)
					{
						InputTracking.Recenter();
						IsCalibrated = true;
					}
				}
			}
		}

		/// <summary>
		/// Adjust head to the VR camera transform
		/// </summary>
		/// <param name="headTransform"></param>
		private void UpdateHead(Transform headTransform)
		{
			if(headTransform)
			{
				headTransform.position = MainCameraController.Instance.PlayerCamera.transform.position;
				headTransform.rotation = MainCameraController.Instance.PlayerCamera.transform.rotation;
			}
		}

		/// <summary>
		/// Applies tracked hands to the avatar body parts
		/// </summary>
		/// <param name="handTransform"></param>
		/// <param name="node"></param>
		private void UpdateHand(Transform handTransform, XRNode node)
		{
			if(handTransform)
			{
				handTransform.localPosition = InputTracking.GetLocalPosition(node);
				handTransform.localRotation = InputTracking.GetLocalRotation(node);
			}
		}

		/// <summary>
		/// Updates the translation and the rotation using the left / right analog sticks
		/// </summary>
		private void UpdateMovement()
		{
			var movement = new Vector2(
				DevelopmentMode.GetAxis(DevelopmentMode.AXIS_STANDALONE_MOVE_STRAFING), 
				DevelopmentMode.GetAxis(DevelopmentMode.AXIS_STANDALONE_MOVE_FORWARD)) * MaxMovementSpeed * Time.unscaledDeltaTime;

			var rotation = DevelopmentMode.GetAxis(DevelopmentMode.AXIS_STANDALONE_ROTATION) * MaxRotationSpeed * Time.unscaledDeltaTime;


			//TODO: code to translate the avatar/camera towards the look direction of the hmd
			//var camFwd = 0.8f < Mathf.Abs(Vector3.Dot(MainCameraController.Instance.PlayerCamera.transform.forward, Vector3.up)) ?
			//                MainCameraController.Instance.PlayerCamera.transform.up :
			//                MainCameraController.Instance.PlayerCamera.transform.forward;
			////var velocity = new Vector3(camFwd.x * movement.x, 0f, camFwd.z * movement.y);
			//var velocity = (camFwd * movement.y) + MainCameraController.Instance.PlayerCamera.transform.right * movement.x;
			//velocity.y = 0f;
			//transform.position += velocity;

			//Update controlled avatar position
			AvatarRoot.Translate(movement.x, 0f, movement.y, Space.Self);
			AvatarRoot.Rotate(Vector3.up, rotation);

			//Apply this position to the main cam controller (simulate rigid body movement). Just offset X and Z since we don't navigate in vertical.
			MainCameraController.Instance.HeadRigidBody.transform.localPosition = new Vector3(
				AvatarRoot.localPosition.x, MainCameraController.Instance.HeadRigidBody.transform.localPosition.y, AvatarRoot.localPosition.z);

			MainCameraController.Instance.HeadRigidBody.transform.localRotation = AvatarRoot.localRotation;
		}

		/// <summary>
		/// 
		/// </summary>
		private void UpdateFeet()
		{
			if(FeetRoot)
			{
				//Set feet root to floor of head
				var newFeetPosition = MainCameraController.Instance.PlayerCamera.transform.localPosition;
				newFeetPosition.y = transform.localPosition.y;
				FeetRoot.transform.localPosition = newFeetPosition;

				//Set feet rotation to head
				var newFeetRotation = FeetRoot.transform.localRotation.eulerAngles;
				newFeetRotation.y = MainCameraController.Instance.PlayerCamera.transform.localRotation.eulerAngles.y;
				FeetRoot.transform.localRotation = Quaternion.Euler(newFeetRotation);
			}
		}
	}
}