﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Artanim.Location.Network;

namespace Artanim
{
	// This behaviour exists only to shutdown destroy the managed singletons that keep the artanim-native DLL loaded.
	// By destroying those singletons, we ensure that the artanim-native DLL is unloaded when exiting play mode.
	// We do this for 2 reasons:
	// - be able to update artanim-native when not in play mode (otherwise the file is locked)
	// - properly shutdown the native libraries so they can finish their job
	//   (and as a consequence they are not to be used by some editor code)
	public class NativeController
		: MonoBehaviour
	{
		public void Awake()
		{
			// Show the console:
			//Utils.WinKernel.ShowConsole();

			// Add the libs directory into the current path so native DLLs are found
#if UNITY_EDITOR
			string dllPath = Path.Combine(ArtanimCommon.EditorCommonDir, "Libs");
#else
			// In a build, all our native DLLs are copied into the Plugins folder
			string dllPath = Path.Combine(Application.dataPath, "Plugins");
#endif

			string currentPath = Environment.GetEnvironmentVariable("PATH");
			if (!currentPath.Contains(dllPath))
			{
				Environment.SetEnvironmentVariable("PATH", dllPath + Path.PathSeparator + currentPath);
			}

#if UNITY_EDITOR
			// We want to get notified when we exit play mode
			SubscribePlayModeStateChanged();
#endif
		}

#if UNITY_EDITOR

		[UnityEditor.MenuItem("Artanim/Unload Native DLL")]
		static void UnloadNativeDLL()
		{
			string nativeDllName = Utils.NativeModulesList.ArtanimNativeDllName;

			// Dispose the managed singletons that use the artanim-native DLL
			Debug.Log("Shutting down " + nativeDllName + " DLL");

			// Manually dispose some of our managed singletons
			NetworkInterface.DisposeSingleton();
			if (Location.SharedData.SharedDataController.HasInstance)
				Location.SharedData.SharedDataUtils.Shutdown();
			Location.Monitoring.ErrorReporting.DisposeSingleton();
			Monitoring.MetricsManager.DisposeSingleton();
			Monitoring.Monitoring.DisposeSingleton();
			TrackerSdk.DisposeSingleton();
			LogSystem.LogManager.DisposeSingleton();

			// Dispose all reminding instances using the native DLL
			string fullPathname = Utils.NativeModulesList.ForceUnloadModule(nativeDllName);

			// Check if the DLL was unloaded from memory
			if (!string.IsNullOrEmpty(fullPathname))
			{
				if (IsNativeDllLoaded(fullPathname))
				{
					Debug.LogError(fullPathname + " not unloaded!");
				}
				else
				{
					// Hide console window created by DDS
					IntPtr hConsole = Utils.WinKernel.GetConsoleWindow();
					if (hConsole != IntPtr.Zero)
					{
						Utils.WinKernel.ShowWindow(hConsole, Utils.WinKernel.SW_HIDE);
					}
				}
			}
		}

		static bool IsNativeDllLoaded(string dllPathname)
		{
			return Utils.WinKernel.GetModuleHandle(dllPathname) != IntPtr.Zero;
		}

#if UNITY_5 || UNITY_2017_1

		static void SubscribePlayModeStateChanged()
		{
			UnityEditor.EditorApplication.playmodeStateChanged += PlaymodeStateChanged;
		}

		static void PlaymodeStateChanged()
		{
			// Check that we are exiting play mode
			if (!UnityEditor.EditorApplication.isPlaying)
			{
				try
				{
					UnloadNativeDLL();
				}
				finally
				{
					// Unsubscribe from callback
					UnityEditor.EditorApplication.playmodeStateChanged -= PlaymodeStateChanged;
				}
			}
		}

#else

		static void SubscribePlayModeStateChanged(bool unsubscribe = false)
		{
			UnityEditor.EditorApplication.playModeStateChanged += PlaymodeStateChanged;
		}

		static void PlaymodeStateChanged(UnityEditor.PlayModeStateChange state)
		{
			// Check that we are exiting play mode
			if (state == UnityEditor.PlayModeStateChange.EnteredEditMode)
			{
				try
				{
					UnloadNativeDLL();
				}
				finally
				{
					// Unsubscribe from callback
					UnityEditor.EditorApplication.playModeStateChanged -= PlaymodeStateChanged;
				}
			}
		}
#endif

#endif
			}
		}