﻿using UnityEngine;
using System.Collections;
using System;
using System.Linq;
using Artanim.Location.Messages;
using UnityEngine.UI;

namespace Artanim
{

	public class MainMenu : MonoBehaviour
	{
		public const string KEY_LAST_SELECTED_SCENE = "ArtanimStandaloneScene";

		public Button ButtonServer;
		public Button ButtonClient;
		public Button ButtonObserver;
		public Dropdown DropdownStanaloneScenes;

		public Text TextDevelopmentMode;

		void Start()
		{
			//Set dev mode text
			if(TextDevelopmentMode)
			{
				TextDevelopmentMode.text = DevelopmentMode.CurrentMode != EDevelopmentMode.None ? string.Format("Development Mode: {0}", DevelopmentMode.CurrentMode.ToString()) : "";
			}

			//Disable buttons when running in standalone mode
			if(DevelopmentMode.CurrentMode == EDevelopmentMode.Standalone)
			{
				if (ButtonServer)
					ButtonServer.interactable = false;

				if (ButtonObserver)
					ButtonObserver.interactable = false;

				//Init scenes dropdown
				if(DropdownStanaloneScenes)
				{
					DropdownStanaloneScenes.gameObject.SetActive(true);

					DropdownStanaloneScenes.ClearOptions();
					
					//Load scenes
					DropdownStanaloneScenes.AddOptions(ConfigService.Instance.ExperienceConfig.StartScenes.Select(s => s.SceneName).ToList());

					//Preselect, from player prefs
					var lastSelectedScene = PlayerPrefs.GetString(KEY_LAST_SELECTED_SCENE);
					if(!string.IsNullOrEmpty(lastSelectedScene))
					{
						//Try to preselect last scene
						var lastOption = DropdownStanaloneScenes.options.FirstOrDefault(o => o.text == lastSelectedScene);
						if (lastOption != null)
							DropdownStanaloneScenes.value = DropdownStanaloneScenes.options.IndexOf(lastOption);
					}
				}
			}
			else
			{
				if (DropdownStanaloneScenes)
					DropdownStanaloneScenes.gameObject.SetActive(false);
			}
		}

		public void DoQuit()
		{
			Application.Quit();
		}


		public void DoLoadIKTest()
		{
			if (SceneController.Instance)
				SceneController.Instance.LoadScene(SceneController.EScene.IK, Transition.FadeBlack);
		}

		public void DoLoadExperienceObserver()
		{
			if (SceneController.Instance)
				SceneController.Instance.LoadScene(SceneController.EScene.ExperienceObserver, Transition.FadeWhite);
		}

		public void DoLoadExperienceClient()
		{
			if (SceneController.Instance)
				SceneController.Instance.LoadScene(SceneController.EScene.ExperienceClient, Transition.FadeWhite);
		}

		public void DoLoadExperienceServer()
		{
			if (SceneController.Instance)
				SceneController.Instance.LoadScene(SceneController.EScene.ExperienceServer, Transition.FadeWhite);
		}

		public void DoSelectScene()
		{
			//Save selected scene to editor prefs
			if(DropdownStanaloneScenes)
				PlayerPrefs.SetString(KEY_LAST_SELECTED_SCENE, DropdownStanaloneScenes.options[DropdownStanaloneScenes.value].text);
		}
	}
}