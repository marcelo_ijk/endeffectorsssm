﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using ViconDataStreamSDK.CSharp;


//using CSML;
using LightweightMatrixCSharp;
using Artanim;

using RootMotion.FinalIK;
using RootMotion.Demos;
using System.Linq;
using System.Text;

public class StreamingViconData : MonoBehaviour
{



    public String HostName = "localhost";
    public String Port = "801";

    public double Offset = 0.0;
    public float Height = 0;
    float LastKeyPressedTime = 0f;


    private Dictionary<string, Transform> rbMapping = new Dictionary<string, Transform>();
    static float ViconToUnityScale = 1e-3f;


    VRIKCalibrationController CalibCtrl;
    VRIKCalibrator.Settings settings;

    private ViconDataStreamSDK.CSharp.Client m_Client;
    //private GameObject m_CameraObject;
    public enum Bool
    {
        False = 0,
        True
    }

    private static class OVRPluginServices
    {
        public const string OvrPluginName = "OVRPlugin";
        [DllImport(OvrPluginName, CallingConvention = CallingConvention.Cdecl)]
        public static extern Bool ovrp_SetTrackingPositionEnabled(Bool value);
    }



    bool SetupViconClient()
    {
        //m_Client = new ViconDataStreamSDK.CSharp.RetimingClient();
        //m_Client.SetAxisMapping(Direction.Forward, Direction.Up, Direction.Right);

        m_Client = new ViconDataStreamSDK.CSharp.Client();
        //m_Client.SetAxisMapping(Direction.Left, Direction.Up, Direction.Forward);
        m_Client.SetAxisMapping(Direction.Forward, Direction.Up, Direction.Right);

        String Host = String.Concat(HostName, ":", Port);
        print("Connecting to " + Host + "...");
        Int32 Attempt = 0;
        while (!m_Client.IsConnected().Connected)
        {
            // Direct connection
            Output_Connect ConnectOutput = m_Client.Connect(Host);
            print("Connect result: " + ConnectOutput.Result);

            Attempt += 1;
            if (Attempt == 3)
                break;
            System.Threading.Thread.Sleep(200);
        }
        m_Client.EnableSegmentData();

        return true;
    }





    // Use this for initialization
    void Start()
    {

        SetupViconClient();


    }




    void LateUpdate()
    {



        if (Input.GetKey(KeyCode.UpArrow))
        {
            if (Time.realtimeSinceStartup > LastKeyPressedTime + 2f)
            {

                LastKeyPressedTime = Time.realtimeSinceStartup;
            }
        }


        if (m_Client.IsConnected().Connected)
        {
            //m_Client.UpdateFrame(Offset);

            //Retrieve Vicon frame
            Output_GetFrame Frame = m_Client.GetFrame();

            if (Frame.Result == Result.Success)
            {
                float CurrentTime = Time.realtimeSinceStartup;

                for (uint id = 0; id < m_Client.GetSubjectCount().SubjectCount; id++)
                {
                    var rbname = m_Client.GetSubjectName(id).SubjectName;
                    Output_GetSubjectRootSegmentName RootName = m_Client.GetSubjectRootSegmentName(rbname);


                    //Output_GetSegmentGlobalTranslation Pos = m_Client.GetSegmentGlobalTranslation(RootName.SegmentName, rbname);
                    //Output_GetSegmentGlobalRotationEulerXYZ Euler = m_Client.GetSegmentGlobalRotationEulerXYZ(RootName.SegmentName, rbname);
                    Output_GetSegmentGlobalTranslation Pos = m_Client.GetSegmentGlobalTranslation(rbname, RootName.SegmentName);
                    Output_GetSegmentGlobalRotationEulerXYZ Euler = m_Client.GetSegmentGlobalRotationEulerXYZ(rbname, RootName.SegmentName);
                    Output_GetSegmentGlobalRotationQuaternion Rot = m_Client.GetSegmentGlobalRotationQuaternion(rbname, RootName.SegmentName);


                    Vector3 pos = TransformPositionViconToUnity(Pos.Translation);
                    //Quaternion rot = TransformRotationViconToUnity(Euler.Rotation);
                    Quaternion rot = TransformRotationViconToUnity2(Rot.Rotation);
                    //Vector3 pos = new Vector3((float)Pos.Translation[0], (float)Pos.Translation[1], (float)Pos.Translation[2]) * ViconToUnityScale;
                    //Quaternion rot = Quaternion.Euler(new Vector3((float)Euler.Rotation[0], (float)Euler.Rotation[1], (float)Euler.Rotation[2]) * Mathf.Rad2Deg);
                    //Quaternion rot = new Quaternion((float)Rot.Rotation[0], (float)Rot.Rotation[1], (float)Rot.Rotation[2], (float)Rot.Rotation[3]);




                    if (!rbMapping.ContainsKey(rbname))
                    {
                        createRigidBody(rbname, pos, rot);
                    }

                    rbMapping[rbname].position = pos;
                    rbMapping[rbname].rotation = rot;

                }
            }
        }

    }




    void createRigidBody(string rbName, Vector3 pos, Quaternion rot)
    {

        //GameObject rb = GameObject.CreatePrimitive(PrimitiveType.Cube) as GameObject;
        GameObject rb = new GameObject() as GameObject;
        rb.transform.parent = this.transform;
        rb.name = rbName;
        rb.transform.position = pos;
        rb.transform.rotation = rot;
        //rb.transform.localScale = new Vector3(0.1f, 0.05f, 0.025f);
        //rb.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);

        rbMapping.Add(rbName, rb.transform);
    }



    Vector3 TransformPositionViconToUnity(double[] pos)
    {
        //return (new Vector3(-(float)pos[1], (float)pos[2], (float)pos[0]) * ViconToUnityScale);
        return (new Vector3(-(float)pos[0], (float)pos[1], (float)pos[2]) * ViconToUnityScale);
    }


    Quaternion TransformRotationViconToUnity(double[] rot)
    {
        Quaternion rZ = Quaternion.AngleAxis(-(float)rot[0] * Mathf.Rad2Deg, Vector3.forward);
        Quaternion rX = Quaternion.AngleAxis((float)rot[1] * Mathf.Rad2Deg, Vector3.left);
        Quaternion rY = Quaternion.AngleAxis(-(float)rot[2] * Mathf.Rad2Deg, Vector3.up);
        return rZ * rX * rY;
    }

    Quaternion TransformRotationViconToUnity2(double[] rot)
    {
        return new Quaternion((float)-rot[0], (float)rot[1], (float)rot[2], (float)-rot[3]);
    }
}
